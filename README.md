# CSL Manager

CSL Manager is a web-application developed jointly by the teams of the national veterinary school of Alfort (EnvA, Maisons-Alfort) and the national school of veterinary medicine, food sciences and engineering Oniris, Nantes).

This application is intended for veterinary clinical skills labs, made up of a large number of workshops on which students in initial training are practicing the acquisition of technical gestures and procedures in open-access mode.

It allows students to self-evaluate and generate exportable statistics on the exercises completed or still to be done.

It also allows the manager of the clinical skills lab, through an administration interface, to manage the list of workshops (creation / modification / deletion), to monitor the progress of students and to produce a set of statistics on student activity .

It also interfaces with Moodle instances using WebServices, which allows to create a link to a course where additional documents (videos, records, etc.) are stored and where the student's learning can feed his Training courses. 


#### Features
* Workshop management
* User management (create, import from LDAP or CSV)
* Room management
* Profile management
* Assessment scales management
* Global and user stats (+ CSV export)
* Student's self assessment
* Communication with Moodle (via webservices)

## Installation

### Prerequisites

This web-application requiress :
* a web server
* php ≥ 5.5
* php5-ldap
* php5-mysql
* php5-pdo
* php5-gd
* [composer](https://getcomposer.org) (coupling management)

### Installation

Cloner ou décompresser l'archive  
`git clone https://git.vet-alfort.fr/gabriel.poma/my.vetsims`  
Entrer dans le répertoire  
Copier le fichier `config-dist.php` en `config.php` et remplir selon les besoins  
Exécuter `composer install && composer dump-autoload --optimize` pour installer les dépendances

## Versioning

CSL Manager is using Semantic versioning (http://semver.org)

## Developpers
Gabriel Poma (Alfort),
 Arthur Valvert, Rémi André (Oniris)

## License
This project is licensed under the terms of the GNU General Public License as published by the Free Software Foundation version 3
