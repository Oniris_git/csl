<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//namespace VetSims\Hook;
use CSLManager\Administration\Helper\Config;
use CSLManager\Administration\Mapper\ConfigMapper;
use CSLManager\Administration\Mapper\UserMapper;
use League\Container\Container;

/**
 * Execute after validation
 * @param $data mixed
 * @return mixed
 * @throws \Exception
 */
function after_validation($data = [])
{

    if (!extension_loaded('curl')) {
        throw new \Exception('Extension curl non chargée');
    }

    // Récupérer les informations de EVE (id utilisateur)
    // Appeler la librairie externe moodle pour enregistrer la notation dans
    $config = require __DIR__ . '/../../config.php';
    $config = new Config($config);

    // Met à disposition le container
    /** @var $container Container */
    /** @var $configDB */
    require __DIR__.'/../dependencies.php';

    //header('Content-Type: text/plain');

    $token = $configDB['tokenMoodle'];
    $urlexterne = $configDB['domainMoodle'];

    $useremail = $data['email'];
    $devoirid = $data['devoir'];
    $grade = $data['grade'];

    $post_data = [
        'field' => 'email',
        'values' => [$useremail]
    ];

    // On retrouve les informations de l'utilisateur -> $post
    $serverUrl = $urlexterne . 'webservice/rest/server.php?wstoken=' . $token
        . '&wsfunction=core_user_get_users_by_field'
        . '&moodlewsrestformat=json';

    $curl = curl_init($serverUrl);

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post_data));

    $post = json_decode(curl_exec($curl));

    curl_close($curl);

    if (isset($post->exception)) {
        $message = $post->message.PHP_EOL;
        if ($config->get('debug') && isset($post->debuginfo)) {
            $message .= 'DEBUG : ' . $post->debuginfo;
        }
        throw new \Exception('[Moodle] ' . $message);
    }

    if (count($post) === 0) {
        throw new \Exception('EXCEPTION_MOODLE_NO_USER_FOUND');
    }

    $userid = $post[0]->id;
    unset($post);

    // On ajoute une note dans la base
    $serverurl = $urlexterne . 'webservice/rest/server.php?wstoken=' . $token
        . '&wsfunction=mod_assign_save_grade'
        . '&moodlewsrestformat=json';

    $post_data = [
        'assignmentid' => (int)get_assign_id($container->get('db'), $devoirid),
        'userid' => $userid,
        'grade' => $grade,
        'attemptnumber' => -1,
        'addattempt' => true,
        'workflowstate' => '',
        'applytoall' => false,
        'plugindata' => [],
        'advancedgradingdata' => []
    ];

    $curl = curl_init($serverurl);

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post_data));

    $post = json_decode(curl_exec($curl));
    curl_close($curl);

    if (isset($post->exception)) {
        $message = $post->message.PHP_EOL;
        if ($config->get('debug') && isset($post->debuginfo)) {
            $message .= 'DEBUG : ' . $post->debuginfo;
        }
        throw new \Exception('[Moodle] ' . $message);
    }

    return TXT_VALIDATION_SUCCESS;
}

function sync_departments($args = []) {
    if (!extension_loaded('curl')) {
        throw new \Exception('Extension curl non chargée');
    }

    // Récupérer les informations de EVE (id utilisateur)
    // Appeler la librairie externe moodle pour enregistrer la notation dans
    $config = require __DIR__ . '/../../config.php';
    $config = new Config($config);

    // Met à disposition le container
    /** @var $container Container */
    /** @var $configDB */
    require __DIR__.'/../dependencies.php';

    //header('Content-Type: text/plain');

    $token = $configDB['tokenMoodle'];
    $urlexterne = $configDB['domainMoodle'];

    $userMapper = new UserMapper($connector);
    $configMapper = new ConfigMapper($connector);
    $all = $userMapper->getUsers();
    $users = [];

    foreach ($all as $user) {
        $users[trim($user->getEmail())] = $user;
    }

    $courseid = $configMapper->selectConfigByName('moodle', 'course')['value'];

    // On retrouve les informations de l'utilisateur -> $post
    $serverUrl = $urlexterne . 'webservice/rest/server.php?wstoken=' . $token
        . '&wsfunction=core_enrol_get_enrolled_users'
        . '&moodlewsrestformat=json&courseid='.$courseid;

    $curl = curl_init($serverUrl);

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

    $post = json_decode(curl_exec($curl));

    curl_close($curl);

    $i = 0;
    foreach ($post as $mdl_user) {
        if (isset($users[trim($mdl_user->email)])) {
            $csl_user = $users[trim($mdl_user->email)];
            if (isset($mdl_user->department)) {
                $csl_user->setDescription($mdl_user->department);
            }
            $userMapper->alter($csl_user);
            $i++;
        }
    }
    if (isset($post->exception)) {
        $message = $post->message.PHP_EOL;
        if ($config->get('debug') && isset($post->debuginfo)) {
            $message .= 'DEBUG : ' . $post->debuginfo;
        }
        throw new \Exception('[Moodle] ' . $message);
    }

    return $i;
}

/**
 * Retourne le lien interne du devoir moodle
 * mis à jour via le cron
 * @param \PDO $pdo Connection à la base de données
 * @param int $devoir_id Id du devoir dans moodle
 * @return mixed
 */
function get_assign_id(\PDO $pdo, $devoir_id)
{
    $sql = "SELECT module_id
		FROM moodle_mod
		WHERE devoir_id = " . $pdo->quote($devoir_id, PDO::PARAM_INT);

    $stmt = $pdo->query($sql, \PDO::FETCH_ASSOC);

    return $stmt->fetchColumn();
}
