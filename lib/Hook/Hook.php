<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Appels de fonctions personnalisées
 * après certains évènements
 *
 * Les fonctions appelées sont dans le
 * fichier HookFunctions.php
 */

namespace CSLManager\Hook;

class Hook
{
    protected $hook_available = false;

    /**
     * Signals must equals functions in HookFunctions.php
     * @var array
     */
    protected $signals = [];

    public function __construct(array $signals)
    {
        $this->signals = $signals;
    }

    /**
     * @return boolean
     */
    public function isHookAvailable()
    {
        return $this->hook_available;
    }

    /**
     * @param boolean $hook_available
     */
    public function setHookAvailable($hook_available)
    {
        $this->hook_available = $hook_available;
    }

    /**
     * @param $signal string
     * @param $args array
     * @return mixed
     * @throws \Exception
     */
    public function emit_signal($signal, $args)
    {
        include __DIR__ . '/HookFunctions.php';
        if (!in_array($signal, $this->signals) || !$this->signals[$signal]) {
            throw new \Exception('Impossible d\'exécuter le crochet. Crochet inconnu ou désactivé.');
        } else {
            return $signal($args);
        }
    }
}
