<?php

namespace CSLManager\Administration\Entity;

class WorkshopAttempt
{
    protected $id;
    protected $upn;
    protected $id_workshop;
    protected $label;
    protected $debut;
    protected $fin;
    protected $scale;
    protected $comment;
    protected $id_scale_type;
    //protected $scales;
    protected $duration;
    protected $full_label;
    protected $order_scale;
    protected $scale_label;

    /**
     * WorkshopAttempt constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        if (isset($data['id'])) {
            $this->id = $data['id'];
        }

        if (isset($data['order_scale'])){
            $this->order_scale=$data['order_scale'];
        }

        if (isset($data['label'])){
            $this->label=$data['label'];
        }

        if (isset($data['scale_label'])){
            $this->scale_label=$data['scale_label'];
        }

        $this->upn = $data['upn'];
        $this->id_workshop = $data['id_workshop'];
        $this->full_label = $data['full_label'];

        if (isset($data['debut'])) {
            $this->debut = $data['debut'];
        }
        if (isset($data['fin'])) {
            $this->fin = $data['fin'];
        }
        if (isset($data['scale'])) {
            $this->scale = $data['scale'];
        }
        if (isset($data['comment'])) {
            $this->comment = $data['comment'];
        }
        if (isset($data['id_scale_type'])) {
            $this->id_scale_type = $data['id_scale_type'];
        }
        if (isset($data['duration'])) {
            $this->duration = $data['duration'];
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUpn()
    {
        return $this->upn;
    }


    /**
     * @return string
     */
    public function getDebut()
    {
        return $this->debut;
    }

    /**
     * @return string
     */
    public function getFin()
    {
        return $this->fin;
    }


    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    
    function getDuration() {
        return $this->duration;
    }

    /**
     * @return mixed
     */
    public function getIdWorkshop()
    {
        return $this->id_workshop;
    }

    /**
     * @param mixed $id_workshop
     */
    public function setIdWorkshop($id_workshop)
    {
        $this->id_workshop = $id_workshop;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getScale()
    {
        return $this->scale;
    }

    /**
     * @param mixed $scale
     */
    public function setScale($scale)
    {
        $this->scale = $scale;
    }

    /**
     * @return mixed
     */
    public function getIdScaleType()
    {
        return $this->id_scale_type;
    }

    /**
     * @param mixed $id_scale_type
     */
    public function setIdScaleType($id_scale_type)
    {
        $this->id_scale_type = $id_scale_type;
    }

    /**
     * @return mixed
     */
    public function getScales()
    {
        return $this->scales;
    }

    /**
     * @param mixed $scales
     */
    public function setScales($scales)
    {
        $this->scales = $scales;
    }

    /**
     * @return mixed
     */
    public function getFullLabel()
    {
        return $this->full_label;
    }

    /**
     * @param mixed $full_label
     */
    public function setFullLabel($full_label)
    {
        $this->full_label = $full_label;
    }

    /**
     * @return mixed
     */
    public function getOrderScale()
    {
        return $this->order_scale;
    }

    /**
     * @param mixed $order_scale
     */
    public function setOrderScale($order_scale)
    {
        $this->order_scale = $order_scale;
    }

    /**
     * @return mixed
     */
    public function getScaleLabel()
    {
        return $this->scale_label;
    }

    /**
     * @param mixed $scale_label
     */
    public function setScaleLabel($scale_label)
    {
        $this->scale_label = $scale_label;
    }


}
