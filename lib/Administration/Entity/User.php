<?php

/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace CSLManager\Administration\Entity;

class User {

    protected $id;
    protected $upn;
    protected $firstName;
    protected $name;
    protected $email;
    protected $id_profile;
    protected $profile_name;
    protected $deleted;
    protected $promo;
    protected $nom_promo;
    protected $request;
    protected $open;
    protected $token;
    protected $auth;
    protected $description;



    public function __construct(array $data) {
        if (!array_key_exists('upn', $data) || !array_key_exists('id_profile', $data) || !array_key_exists('max_difficulty', $data)
        ) {
            throw new \Exception('EXCEPTION_USER_ENTITY_MALFORMED');
        }

        if (array_key_exists('id', $data)) {
            $this->id = (int) $data['id'];
        }

        $UpnName = '';
        if (isset(explode('.', strstr($data['upn'], '@', 1))[1])) {
            $UpnName = explode('.', strstr($data['upn'], '@', 1))[1];
        }

        $this->upn = $data['upn'];
        $this->firstName = (isset($data['first_name'])) ? $data['first_name'] : ucfirst(explode('.', strstr($data['upn'], '@', 1))[0]);

        $this->name = (isset($data['name'])) ? $data['name'] : ucfirst($UpnName);

        $this->id_profile = $data['id_profile'];
        if (array_key_exists('profile_name', $data)) {
            $this->profile_name = $data['profile_name'];
        }
        if (array_key_exists('deleted', $data)) {
            $this->deleted = $data['deleted'];
        }
        $this->promo = $data['max_difficulty'];
        if (array_key_exists('nom_promo', $data)) {
            $this->nom_promo = $data['nom_promo'];
        }
        if(isset($data['email'])){
            $this->email = $data['email'];
        }
        if(isset($data['request'])){
            $this->request = $data['request'];
        }
        if(isset($data['open'])){
            $this->open = $data['open'];
        }
        if(isset($data['token'])){
            $this->token = $data['token'];
        }
        if(isset($data['auth'])){
            $this->auth = $data['auth'];
        }
        if(isset($data['description'])){
            $this->description = $data['description'];
        }

    }

    function getEmail() {
        return $this->email;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    public function getId() {
        return $this->id;
    }

    public function getUpn() {
        return $this->upn;
    }

    public function getIdProfile() {
        return $this->id_profile;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function getName() {
        return $this->name;
    }

    public function getProfileName() {
        return $this->profile_name;
    }

    public function getNomPromo() {
        return $this->nom_promo;
    }

    public function getPromo() {
        return $this->promo;
    }

    public function setIdProfile($id_profile) {
        $this->id_profile = $id_profile;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    public function setUpn($upn) {
        $this->upn = $upn;
    }

    public function setPromo($promo) {
        $this->promo = $promo;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param mixed $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @return mixed
     */
    public function getOpen()
    {
        return $this->open;
    }

    /**
     * @param mixed $open
     */
    public function setOpen($open)
    {
        $this->open = $open;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getAuth()
    {
        return $this->auth;
    }

    /**
     * @param mixed $auth
     */
    public function setAuth($auth)
    {
        $this->auth = $auth;
    }


}
