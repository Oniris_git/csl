<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 19/12/2018
 * Time: 14:36
 */

namespace CSLManager\Administration\Entity;


class Language
{

    protected $title;
    protected $path_img;
    protected $path_file;

    public function __construct($data){

        $this->title = $data['title'];
        $this->path_file = $data['path_file'];
        $this->path_img = $data['path_img'];
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getPathImg()
    {
        return $this->path_img;
    }

    /**
     * @param mixed $path_img
     */
    public function setPathImg($path_img)
    {
        $this->path_img = $path_img;
    }

    /**
     * @return mixed
     */
    public function getPathFile()
    {
        return $this->path_file;
    }

    /**
     * @param mixed $path_file
     */
    public function setPathFile($path_file)
    {
        $this->path_file = $path_file;
    }




}