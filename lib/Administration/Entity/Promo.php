<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace CSLManager\Administration\Entity;

class Promo
{
    protected $id;
    protected $difficulty;
    protected $title;
    protected $description;

    public function __construct(array $data)
    {
        $this->id = $data['id'];
        $this->difficulty = $data['difficulty_level'];
        $this->title = $data['title'];
        $this->description = $data['description'];
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDifficulty()
    {
        return $this->difficulty;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getShortDescription()
    {
        return substr($this->description, 0, 20);
    }
}
