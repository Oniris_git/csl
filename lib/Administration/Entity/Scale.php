<?php

namespace CSLManager\Administration\Entity;

class Scale
{
    protected $id;
    protected $scale_item_code;
    protected $label;
    protected $deleted;
    protected $idScaleType;
    protected $order_scale;

    public function __construct(array $data)
    {
        if (isset($data['id'])) {
            $this->id = $data['id'];
        }

        $this->scale_item_code = $data['scale_item_code'];
        $this->label = $data['label'];
        $this->deleted = $data['deleted'];
        $this->idScaleType = $data['id_scale_type'];
        $this->order_scale = $data['order_scale'];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @return mixed
     */
    public function getScaleItemCode()
    {
        return $this->scale_item_code;
    }

    /**
     * @param mixed $scale_item_code
     */
    public function setScaleItemCode($scale_item_code)
    {
        $this->scale_item_code = $scale_item_code;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getIdScaleType()
    {
        return $this->idScaleType;
    }

    /**
     * @param mixed $idScaleType
     */
    public function setIdScaleType($idScaleType)
    {
        $this->idScaleType = $idScaleType;
    }

    /**
     * @return mixed
     */
    public function getOrderScale()
    {
        return $this->order_scale;
    }

    /**
     * @param $order_scale
     */
    public function setOrderScale($order_scale)
    {
        $this->order_scale = $order_scale;
    }


}
