<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace CSLManager\Administration\Entity;

class Workshop
{
    protected $id;
    protected $label;
    protected $full_label;
    protected $description;
    protected $deleted;
    protected $available;
    protected $id_moodle;
    protected $id_room;
    protected $id_difficulty;
    protected $count_to_validate;
    protected $duration;
    protected $ceiling_duration;
    protected $id_scale_type;
    protected $qrcode;
    protected $id_category;
    protected $id_sub_category;
    protected $id_discipline;
    protected $id_priority_level;
    protected $id_species;

    /**
     * Workshop constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        if (isset($data['id'])) {
            $this->id = $data['id'];
        }
        $this->label = $data['label'];
        $this->full_label = $data['full_label'];
        $this->description = $data['description'];
        $this->deleted = $data['deleted'];
        $this->available = $data['available'];
        $this->id_moodle = $data['id_moodle'];
        $this->id_room = $data['id_room'];
        $this->id_difficulty = $data['id_difficulty'];
        $this->count_to_validate = $data['count_to_validate'];
        $this->duration = $data['duration'];
        $this->ceiling_duration = $data['ceiling_duration'];
        $this->id_scale_type = $data['id_scale_type'];
        $this->id_category = $data['id_category'];
        $this->id_sub_category = $data['id_sub_category'];
        $this->id_discipline = $data['id_discipline'];
        $this->id_priority_level = $data['id_priority_level'];
        $this->id_species = $data['id_species'];
        
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return mixed
     */
    public function getFullLabel()
    {
        return $this->full_label;
    }

    /**
     * @return mixed
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getShortDescription()
    {
        return substr($this->description, 0, 30) . '…';
    }

    /**
     * @return mixed
     */
    public function getAvailable()
    {
        return $this->available;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @return mixed
     */
    public function getIdDifficulty()
    {
        return $this->id_difficulty;
    }

    /**
     * @return mixed
     */
    public function getIdMoodle()
    {
        return $this->id_moodle;
    }

    /**
     * @return mixed
     */
    public function getCountToValidate()
    {
        return $this->count_to_validate;
    }

    /**
     * @return mixed
     */
    public function getIdRoom()
    {
        return $this->id_room;
    }

    /**
     * @return mixed
     */
    public function getIdScaleType()
    {
        return $this->id_scale_type;
    }


    /**
     * @return mixed
     */
    public function getQrcode()
    {
        return $this->qrcode;
    }

    /**
     * @param $qrcode
     */
    public function setQrcode($qrcode)
    {
        $this->qrcode = $qrcode;
    }
    
    /**
     * @return mixed
     */
    public function getIdCategory()
    {
        return $this->id_category;
    }

    /**
     * @return mixed
     */
    public function getIdSubCategory()
    {
        return $this->id_sub_category;
    }

    /**
     * @return mixed
     */
    public function getIdDiscipline()
    {
        return $this->id_discipline;
    }

    /**
     * @return mixed
     */
    public function getIdSpecies()
    {
        return $this->id_species;
    }
    
    function getCeilingDuration() {
        return $this->ceiling_duration;
    }

    /**
     * @return mixed
     */
    public function getIdPriorityLevel()
    {
        return $this->id_priority_level;
    }

    /**
     * @param mixed $id_priority_level
     */
    public function setIdPriorityLevel($id_priority_level)
    {
        $this->id_priority_level = $id_priority_level;
    }

    /**
     * @return mixed
     */
    public function getPriorityLevel()
    {
        return $this->priority_level;
    }

    /**
     * @param mixed $priority_level
     */
    public function setPriorityLevel($priority_level)
    {
        $this->priority_level = $priority_level;
    }

    /**
     * @param mixed $id_scale_type
     */
    public function setIdScaleType($id_scale_type)
    {
        $this->id_scale_type = $id_scale_type;
    }
}
