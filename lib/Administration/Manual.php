<?php

namespace CSLManager\Administration;

use Vespula\Auth\Adapter\AdapterInterface;
use CSLManager\Administration\Entity\User;
use CSLManager\Administration\Mapper\Mapper;

/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 07/02/2019
 * Time: 15:33
 */

class Manual extends Mapper implements AdapterInterface
{

    /**
     * validate the username and password.
     *
     * @param array $credentials Array with keys 'username' and 'password'
     * @return boolean
     */
    public function authenticate(array $credentials)
    {
        $query = "SELECT password FROM user WHERE upn = '".$credentials['username']."'";
        $result = $this->db->query($query);

        foreach ($result as $row):
            if(password_verify($credentials['password'], $row['password']) == true):
                return true;
            else:
                return false;
            endif;
        endforeach;
    }

    /**
     * Find extra userdata. This will be stored in the session
     *
     * @param $username
     * @return User Userdata specific to the adapter
     * @throws \Exception
     */
    public function lookupUserData($username)
    {
        $sql = "SELECT u.id, u.upn, u.name, u.first_name, u.email, u.description, u.id_profile, u.deleted,
 				u.max_difficulty, p.label AS profile_name, d.title AS nom_promo, u.request, u.open, u.token, u.auth
			FROM user u
			LEFT JOIN difficulty d ON d.id = u.max_difficulty
			LEFT JOIN profils p ON p.id = u.id_profile
			WHERE u.upn = :upn"
            . $this->limit
            . $this->offset;

        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([':upn' => $username]);

        if ($result && ($r = $stmt->fetch(\PDO::FETCH_ASSOC))) {
            return new User($r);
        } else {
            throw new \Exception('User not found');
        }
    }

    /**
     * Get the most recent error for debugging purposes
     *
     * @return string Error (should be a constant)
     */
    public function getError()
    {
        // TODO: Implement getError() method.
    }
}