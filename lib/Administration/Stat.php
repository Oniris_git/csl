<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Gère les stats
 */

namespace CSLManager\Administration;

use CSLManager\Administration\Mapper\Mapper;
use CSLManager\Administration\Entity\Workshop;
use CSLManager\Administration\Entity\WorkshopAttempt;

class Stat extends Mapper
{

    /**
     * @param bool|false|string $u Global or user
     * @return array
     * @throws \Exception
     */
    public function getPopularWorkshopAttempt($u = false)
    {
        $att = [];
        $query = "SELECT wa.full_label, count(wa.id) AS times
				FROM workshop_attempt wa, workshop ws, user u
				WHERE ws.id = wa.id_workshop AND u.id = wa.id_user";

        if ($u) {
            $query .= 'AND upn = :upn ';
            $att = [':upn' => $u];
        }

        $query .= 'GROUP BY wa.id ORDER BY times DESC '
            . $this->limit;

        $stmt = $this->db->prepare($query);
        $stmt->execute($att);

        $results = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $results[] = $row;
        }
        return $results;
    }

    /**
     * Return total time on workshop_attempt
     * @param string $type
     * @param null   $champs
     * @param null   $value
     * @return bool
     * @throws \Exception
     */
    public function getTotalTime($type = 'global', $champs = null, $value = null)
    {
        $att = [':value' => $value];

        switch ($type) {
            case 'user':
                $query = "SELECT sum(TIMESTAMPDIFF(SECOND, debut, fin)) AS time
					FROM workshop_attempt, user WHERE fin IS NOT NULL 
					AND user.$champs = :value
					AND user.id = workshop_attempt.id_user";

                break;
            case 'workshop':
                $query = "SELECT sum(TIMESTAMPDIFF(SECOND, debut, fin)) AS time
					FROM workshop_attempt, user, workshop WHERE fin IS NOT NULL 
					AND workshop.$champs = :value
					AND workshop.id = workshop_attempt.id_workshop";
                break;
            case 'global':
            default:
                $att = [];
                $query = "SELECT sum(TIMESTAMPDIFF(SECOND, debut, fin)) AS time
					FROM workshop_attempt WHERE fin IS NOT NULL ";
                break;
        }

        $stmt = $this->db->prepare($query);
        $result = $stmt->execute($att);

        if ($result) {
            return $stmt->fetchColumn();
        } else {
            throw new \Exception('User not found');
        }
    }

    /**
     * @param $workshopId
     * @return mixed
     * @throws \Exception
     */
    public function getTimeByWorkshop($workshopId){

        if($workshopId != null){
            $query = "SELECT sum(TIMESTAMPDIFF(SECOND, debut, fin)) AS time
					FROM workshop_attempt WHERE id_workshop = $workshopId";

            try{
                $stmt = $this->db->query($query);

                return $stmt->fetchColumn();
            } catch (Exception $ex) {
                throw new \Exception("Workshop don't exist");
            }

        }
    }

    /**
     * Return array workshop per month
     * @param null $uid Upn de l'utilisateur. Si null, retourne tout
     * @return array
     */
    public function getWorkshopAttemptPerMonth($uid = null)
    {
        $data = [];
        $datasets = [];
        $WHERE = "";

        if ($uid !== null) {
            $WHERE = " WHERE upn = " . $this->db->quote($uid, \PDO::PARAM_INT) . " ";
        }

        // Récupération des labels :
        $query = "SELECT month, year FROM v_workshop_attempt_per_month"
            . $WHERE
            . " GROUP BY month, year
			ORDER BY year, nmonth";

        $result = $this->db->query($query, \PDO::FETCH_ASSOC);

        if ($result) {
            foreach ($result as $row) {
                $data['labels'][] = $row['month'] . " " . $row['year'];
            }
        } else {
            $data['labels'] = [];
        }

        // Récupération des promos :
        $query = "SELECT DISTINCT promo  FROM v_workshop_attempt_per_month "
            . $WHERE;

        foreach ($this->db->query($query, \PDO::FETCH_ASSOC) as $row) {
            $datasets[]['label'] = $row['promo'] ;
        }

        //reset $WHERE
        $WHERE = " WHERE month = :month and year = :year and promo <=> :promo ";

        if ($uid !== null) {
            $WHERE .= " AND upn = " . $this->db->quote($uid, \PDO::PARAM_INT) . " ";
        }

        $query = "SELECT coalesce(sum(count),0) as Total, month, year FROM v_workshop_attempt_per_month"
            . $WHERE;

        $statement = $this->db->prepare($query);

        foreach ($datasets as $dataset) {
            $promo = $dataset['label'];
            $key = array_search($promo, array_column($datasets, "label"));

            foreach ($data['labels'] as $label) {
                $m = explode(" ", $label);
                $statement->bindParam(':month', $m[0]);
                $statement->bindParam(':year', $m[1]);
                $statement->bindParam(':promo', $promo);
                $statement->execute();

                $row = $statement->fetch(\PDO::FETCH_ASSOC);

                $datasets[$key]['data'][] = $row['Total'];
            }
        }

        $data['datasets'] = $datasets;

        return $data;
    }

    /**
     * @param null $upn Upn user / if null return all
     * @param null $order
     * @param null $filter
     * @return array
     */
    public function getScales($upn = null, $order= null, $filter= null)
    {
        $data = [];

        $WHERE = '';

        if ($upn !== null) {
            $WHERE = ' WHERE upn = ' . $this->db->quote($upn);
            if($filter !== null){
                 $WHERE = ' WHERE upn = ' . $this->db->quote($upn).'AND scale_type='.$filter;
            }
        }

        if($filter !== null && $upn== null){
            $WHERE = ' WHERE scale_type = '.$filter;
        }

        if ($order === null){
            $ORDER = ' ORDER BY start DESC';
        }else{
            $ORDER =  ' ORDER BY start ASC';
        }

        $query = "SELECT idUser, upn, scale, label, start FROM v_workshop_attempt_done ".$WHERE.$ORDER;

        foreach ($this->db->query($query, \PDO::FETCH_ASSOC) as $row) {
            $key =    strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($row['scale'], ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));

            $data[$key][] = [
                'idUser' => $row['idUser'],
                'scale' => $row['scale'],
                'label' => $row['label'],
                'start'=>$row['start'],
                'upn' => $row['upn']
            ];
        }
        return $data;
    }

    /**
     * Return workshop attempts distinct
     * @param null $upn Upn user / if null return all
     * @param null $filter
     * @return array
     */
    public function getScalesDistinct($upn = null,$filter= null)
    {
        $data = [];
        $WHERE = '';

        if($upn !== null){
            $WHERE= " WHERE upn = ". $this->db->quote($upn);
        }

        if($filter !== null){
            $WHERE = ' WHERE scale_type = '.$filter;
                if($upn !== null){
                    $WHERE= " WHERE scale_type = $filter AND upn=  ". $this->db->quote($upn);
                }
        }

        $query = "SELECT idUser, upn, scale, label, start from v_workshop_attempt_done".$WHERE." GROUP BY idUser,idWorkshop ORDER BY start DESC";

        foreach ($this->db->query($query, \PDO::FETCH_ASSOC) as $row) {
            $key =    strtolower(trim(preg_replace('~[^0-9a-z]+~i',
                '-',
                html_entity_decode(preg_replace(
                    '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i',
                    '$1',
                    htmlentities($row['scale'],
                        ENT_QUOTES,
                        'UTF-8')),
                    ENT_QUOTES,
                    'UTF-8')),
                '-'));

            $data[$key][] = [
                'idUser' => $row['idUser'],
                'scale' => $row['scale'],
                'label' => $row['label'],
                'start' => $row['start'],
                'upn' => $row['upn']
            ];
        }
        return $data;
    }



    /**
     * @param string $upn Upn user
     * @return array
     */
    public function getWorkshopAttemptDone($upn = null)
    {
        $data = [];

        $query = "SELECT count(*) from workshop WHERE deleted = 0";

        $data ['total'] = $this->db->query($query)->fetchColumn();

        $query = "SELECT count(distinct id_workshop) from workshop_attempt where fin is not null";

        if ($upn !== null) {
            $queryUser = "SELECT id from user where upn = " . $this->db->quote($upn);
            $user_id = $this->db->query($queryUser)->fetchColumn();

            $query .= " and id_user = $user_id";
        }

        $data['done'] = $this->db->query($query)->fetchColumn();
        $data['todo'] = $data['total'] - $data['done'];

        return $data;
    }

    /**
     * @return array v_workshop_attempt
     */
    public function getWorkshopAttemptTotal()
    {
        $data = [];

        $query = "SELECT * FROM v_workshop_attempt ";

        foreach ($this->db->query($query, \PDO::FETCH_ASSOC) as $row) {
            $data[] = $row;
        }
        return $data;
    }

    /**
     * @param $upn
     * @return array v_workshop_attempt_done by user
     */
    public function getWorkshopAttemptTotalByUser($upn)
    {
        $data = [];

        $query = "SELECT * FROM v_workshop_attempt WHERE upn = '$upn' ";

        foreach ($this->db->query($query, \PDO::FETCH_ASSOC) as $row) {
            $data[] = $row;
        }
        return $data;
    }

    /**
     * Count workshop attempts
     *
     * @param null $upn user / if null return all
     * @return int
     */
    public function getCountWorkshopAttempt($upn = null)
    {
        if ($upn !== null) {
            $upn = $this->db->quote($upn, \PDO::PARAM_STR);

            $query = "SELECT id FROM user WHERE upn = $upn";

            $stmt = $this->db->query($query);
            $id = $stmt->fetchColumn();
        }


        $query = "SELECT count(*) FROM workshop_attempt WHERE workshop_attempt.fin is not null ";

        if ($upn !== null) {
            $query .= "and id_user = $id";
        }

        $stmt = $this->db->query($query);
        return $stmt->fetchColumn();
    }

    /**
     * Count the number of workshop attempts per workshop
     * @param $idUser
     * @param $idWorkshop
     * @return mixed
     */
    public function getCountWorkshopAttemptByUserForWorkshop($idUser, $idWorkshop){

        if($idWorkshop !== null){

            $query = "SELECT count(id) FROM workshop_attempt 
                        WHERE id_user=$idUser 
                          AND id_workshop = $idWorkshop  
                          AND fin IS NOT NULL ";

            $result = $this->db->query($query);

            return $result->fetch();
        }
    }

    /**
     * Count the distinct workshop attempts by user
     * @param $idUser
     * @return array
     * @throws \Exception
     */
    public function getDistinctWorkshopAttemptByUser($idUser, $filter){

        try{
            if($idUser !== null){
                if($filter == 'global' || $filter == null){
                    $WHERE = ' ';
                }else{
                    $WHERE = ' AND ws.id_priority_level='.$filter;
                }
            $query = "SELECT DISTINCT id_workshop FROM workshop_attempt wa
                      INNER JOIN workshop ws 
                      ON ws.id=wa.id_workshop WHERE fin IS NOT NULL AND id_user = $idUser".$WHERE;
            $db= $this->db->query($query);
            $data=[];
            $i=0;
            while ($row = $db->fetch()){
                $data[$i]= $row;
                $i++;
            }
            return $data;
        }
        } catch (Exception $ex) {
            throw new \Exception("User don't exist");
        }
    }

    /**
     * The total count of granted priority level per workspace
     * @param Workshop $workshop
     * @return mixed
     * @throws \Exception
     */
    public function getCountGrantedByWorkshop(Workshop $workshop){

        if($workshop !== null){
            $workstationId=$workshop->getId();
            try{
               if($workshop->getIdScaleType() == 1){
                    $query = "SELECT COUNT(id) FROM workshop_attempt WHERE id_workshop = $workstationId "
                            . "AND ( scale = 'Acquis' OR scale = 'Ma�tris�' OR scale = 'AQ' OR scale = 'MQ') AND fin is not null";
                    $stmt = $this->db->query($query);

                    return $stmt->fetchColumn();
                }else if($workshop->getIdScaleType() == 2){
                   $query = "SELECT COUNT(id) FROM workshop_attempt WHERE id_workshop = $workstationId "
                           . "AND ( scale = 'AQ' OR scale = 'MQ' OR scale = 'Acquis' OR scale = 'Ma�tris�') AND fin is not null";
                   $stmt = $this->db->query($query);

                   return $stmt->fetchColumn();
                }
            } catch (Exception $ex) {
                throw new \Exception("Workshop don't exist");
            }
        }
    }

    /**
     * The distinct count of granted scales per workspace
     * @param Workshop $workshop
     * @return int
     * @throws \Exception
     */
    public function getDistinctGrantedByWorkshop(Workshop $workshop){

        if($workshop !== null){
           $workstationId=$workshop->getId();
           try{
                if($workshop->getIdScaleType() == 1){
                    $query = "SELECT DISTINCT id_user FROM workshop_attempt 
                                WHERE id_workshop = $workstationId 
                                AND ( scale = 'Acquis' OR scale = 'Ma�tris�' OR scale = 'AQ' OR scale = 'MQ') 
                                GROUP BY id_user";
                    $i=0;
                    foreach ($this->db->query($query) as $row){
                        $i++;
                    }
                    return $i;
                }else if($workshop->getIdScaleType() == 2){
                    $query = "SELECT DISTINCT id_user FROM workshop_attempt 
                                WHERE id_workshop = $workstationId 
                                AND ( scale = 'AQ' OR scale = 'MQ' OR scale = 'Acquis' OR scale = 'Ma�tris�') 
                                GROUP BY id_user";
                    $i=0;
                    foreach ($this->db->query($query) as $row){
                       $i++;
                    }
                    return $i;
                }
            } catch (Exception $ex) {
                throw new \Exception("Workshop don't exist");
            }
        }
    }

    /**
     * Select all comments from a workshop
     * @param $workshop
     * @return array
     * @throws \Exception
     */
    public function selectCommentsByWorkShop($workshop){

       if($workshop !== null){
           try{
                 $query = "SELECT content FROM workshop_comment WHERE id_workshop = $workshop";
                $stmt= $this->db->query($query);
                $data=[];
                $i=0;
                foreach ($this->db->query($query) as $row){
                    $data[$i]= $row;
                    $i++;
                }
                return $data;
            } catch (Exception $ex) {
                throw new \Exception("workshop don't exist");
            }
       }
    }

    /**
     * Select all users who have granted status for an workshop
     * @param $workshop
     * @return array
     * @throws \Exception
     */
    public function selectIdGrantedUsers(Workshop $workshop){
        if ($workshop !== null){
            try{
                $workstationId=$workshop->getId();
                $data=[];
                $i=0;
                if($workshop->getIdScaleType() == 1){
                    $query = "SELECT DISTINCT id_user FROM workshop_attempt 
                                WHERE id_workshop = $workstationId 
                                AND ( scale = 'Acquis' OR scale = 'Ma�tris�' ) 
                                GROUP BY id_user";
                    foreach ($this->db->query($query) as $row){
                        $data[$i]= $row;
                        $i++;
                    }
                    return $data;
                }else if($workshop->getIdScaleType() == 2){
                    $query = "SELECT DISTINCT id_user 
                                FROM workshop_attempt 
                                WHERE id_workshop = $workstationId 
                                AND ( scale = 'AQ' OR scale = 'MQ' ) 
                                GROUP BY id_user";
                    foreach ($this->db->query($query) as $row){
                        $data[$i]= $row;
                        $i++;
                    }
                    return $data;
                }
            } catch (Exception $ex) {
                throw new \Exception("Workshop don't exist");
            }

        }
    }


    /**
     * Get the total of workshop attempts for students who have granted status for the workshop
     * @param $userId
     * @param Workshop $workshop
     * @return array
     * @throws \Exception
     */
    public function getCountWorkshopAttemptForGrantedStudents($userId,Workshop $workshop){
        if($userId !== null){
            try{
               $workstationId= $workshop->getId();
                $data=[];
                $i=0;
                if($workshop->getIdScaleType() == 1){
                    $query = "SELECT count(id) FROM workshop_attempt 
                                WHERE id_user = $userId 
                                AND ( scale = 'Non')
                                AND id_workshop = $workstationId";

                    foreach ($this->db->query($query) as $row){
                        $data[$i]= $row;
                        $i++;
                    }
                    return $data;
                }else if($workshop->getIdScaleType() == 2){
                     $query = "SELECT count(id) FROM workshop_attempt 
                                  WHERE id_user = $userId 
                                  AND ( scale = 'NAQ' OR scale = 'ECQ') 
                                  AND id_workshop = $workstationId";

                    foreach ($this->db->query($query) as $row){
                        $data[$i]= $row;
                        $i++;
                    }
                    return $data;
                }
            } catch (Exception $ex) {
                throw new \Exception("User id don't exist");
            }

        }
    }

    /**
     * Select all workshop attempts which are not done by user
     * @param $user
     * @return array
     */
    public function selectWorkshopNotDoneByUser($user, $filter){
        if($user !== null){
            if($filter == 'global' || $filter == null){
                $WHERE = ' ';
            }else{
                $WHERE = ' AND ws.id_priority_level='.$filter;
            }
            $i=0;
            $data=[];
            try{
                $query = "SELECT ws.id, ws.label, ws.full_label, ws.deleted,
		  ws.description, ws.available, ws.id_moodle, ws.id_room,
		  ws.id_difficulty, ws.count_to_validate, ws.duration,ws.ceiling_duration,
		  ws.id_scale_type,ws.id_category,ws.id_sub_category,ws.id_discipline,ws.id_priority_level,ws.id_species
		FROM workshop ws
		WHERE deleted = 0 AND id NOT IN (SELECT id_workshop FROM workshop_attempt WHERE id_user = $user)$WHERE";

                $db=$this->db->query($query);
                foreach ($db as $row){
                    $data[$i]=new Workshop($row);
                    $i++;
                }
                return $data;
            } catch (Exception $ex) {
                throw $ex;
            }
        }
    }

    /**
     * Select all workshop attempts which are not done
     */
    public function selectWorkshopNotDone(){
                $query = "SELECT ws.id, ws.label, ws.full_label, ws.deleted,
                          ws.description, ws.available, ws.id_moodle, ws.id_room,
                          ws.id_difficulty, ws.count_to_validate, ws.duration,ws.ceiling_duration,
                          ws.id_scale_type,ws.id_category,ws.id_sub_category,ws.id_discipline,ws.id_priority_level,ws.id_species 
                        FROM workshop ws
                        WHERE deleted = 0 AND ws.full_label NOT IN (SELECT WorkshopFull FROM v_workshop_attempt)";
                $i=0;
                $data = [];
                $db=$this->db->query($query);
                foreach ($db as $row){
                    $data[$i]=new Workshop($row);
                    $i++;
                }

            if ($data == false){
                return $data = [];
            }else{
                return $data;
            }

    }

    /**
     * Get the workshop attempts 's time for a user
     * @param $idUser
     * @param $idWorkshopAttempt
     * @return mixed
     */
    public function getTotalTimeByUserByWorkshopAttempt($idUser , $idWorkshopAttempt)
    {
        try{
            $query = "SELECT sum(TIMESTAMPDIFF(SECOND, debut, fin)) AS time
			FROM workshop_attempt WHERE fin IS NOT NULL 
			AND id = $idWorkshopAttempt
                        AND id_user = $idUser";

            $stmt = $this->db->query($query);

            return $stmt->fetchColumn();
        } catch (Exception $ex) {
            throw new Exception("Workshop attempt don't exist");
        }
    }
}




