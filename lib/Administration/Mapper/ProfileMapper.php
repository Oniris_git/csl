<?php

namespace CSLManager\Administration\Mapper;

class ProfileMapper extends Mapper
{
    /**
     * @return array profiles
     */
    public function getProfiles()
    {
        $query = "SELECT p.id, p.label, p.description, p.deleted
			FROM profils p"
            . $this->limit
            . $this->offset;
        $stmt = $this->db->query($query);

        $results = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $results[] = $row;
        }
        return $results;
    }

    /**
     *
     * @param int $id
     * @return array profile
     * @throws \Exception
     */
    public function getProfileById($id)
    {
        $query = "SELECT p.id, p.label, p.description, p.deleted
			FROM profils p
			WHERE p.id = " . $this->db->quote($id, \PDO::PARAM_INT);

        $stmt = $this->db->query($query);

        if (($r = $stmt->fetch(\PDO::FETCH_ASSOC)) !== false) {
            return $r;
        } else {
            throw new \Exception('Profil introuvable');
        }
    }

    /**
     * @param array $profile
     * @throws \Exception
     */
    public function create(array $profile)
    {
        $query = "INSERT INTO profils
			(label, description, deleted)
			VALUES (:label, :description, 0)";

        $stmt = $this->db->prepare($query);
        $result = $stmt->execute([
            ':label' => $profile['label'],
            ':description' => $profile['description']
        ]);

        if (!$result) {
            throw new \Exception('Impossible de créer le profil : ' . $stmt->errorInfo()[2]);
        }

        // On lui rajoute le droit print:ownstats
        $id = $this->db->lastInsertId();
        $query = "INSERT INTO permissions (id_profile, id_right)
                VALUES (:id_profile,
                        (SELECT id FROM rights WHERE label = 'print:ownstats')
                       )";

        $stmt = $this->db->prepare($query);
        $result = $stmt->execute([
            ':id_profile' => $id
        ]);

        if (!$result) {
            throw new \Exception('Échec d\'ajout des droits : ' . $stmt->errorInfo()[2]);
        }
    }

    /**
     *
     * @param int $id
     * @param bool $status profile state (deleted = 1 / active = 0)
     * @throws \Exception
     */
    public function activate($id, $status)
    {
        $query = "UPDATE profils
			SET deleted = :state
			WHERE id = :id";

        $stmt = $this->db->prepare($query);
        $result = $stmt->execute([
            ':state' => $status,
            ':id' => $id,
        ]);

        if (!$result) {
            throw new \Exception('Impossible de modifier le profil : ' . $stmt->errorInfo()[2]);
        }
    }

    /**
     * @param array $profile
     * @throws \Exception
     */
    public function alter(array $profile)
    {
        $query = "UPDATE profils
			SET label = :label,
				description = :desc
			WHERE id = :id";

        $stmt = $this->db->prepare($query);
        $result = $stmt->execute([
            ':id' => $profile['id'],
            ':label' => $profile['label'],
            ':desc' => $profile['description'],
        ]);

        if (!$result) {
            throw new \Exception('Impossible de modifier le profil : ' . $stmt->errorInfo()[2]);
        }
    }

    /**
     * @param int $id
     * @return int|bool
     */
    public function delete($id)
    {
        $query = "DELETE FROM profils
            WHERE id =" . $this->db->quote($id, \PDO::PARAM_INT);

        return $this->db->exec($query);
    }
}
