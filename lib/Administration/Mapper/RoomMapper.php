<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 13/09/2018
 * Time: 11:31
 */

namespace CSLManager\Administration\Mapper;

/**
 * Class RoomMapper
 * @package VetSims\Administration\Mapper
 */
class RoomMapper extends Mapper
{
    /**
     * @return array rooms
     */
    public function getRooms()
    {
        $sql = "SELECT id, description FROM room";
        $results = [];

        foreach ($this->db->query($sql, \PDO::FETCH_ASSOC) as $item) {
            $results[] = $item;
        }
        return $results;
    }

    /**
     * @param int    $id
     * @param string $name room name
     * @throws \Exception
     */
    public function editRoom($id, $name)
    {
        $sql = "UPDATE room SET description = :description WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':description', $name, \PDO::PARAM_STR);
        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
        $result = $stmt->execute();

        if (!$result || $stmt->rowCount() === 0) {
            throw new \Exception('Impossible de modifier la salle : ' . $stmt->errorInfo()[2]);
        }
    }

    /**
     *
     * @param string $name new name
     * @throws \Exception
     */
    public function createRoom($name)
    {
        try{
            $sql = "INSERT INTO room (description) VALUES (?)";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(1, $name, \PDO::PARAM_STR);
            $result = $stmt->execute();
        } catch (Exception $ex) {
            throw new \Exception('Impossible de créer la salle : ' . $stmt->errorInfo()[2]);
        }

    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function deleteRoom($id)
    {
        $sql = "DELETE FROM room
			WHERE id = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(1, $id, \PDO::PARAM_INT);
        $result = $stmt->execute();

        if (!$result || $stmt->rowCount() === 0) {
            throw new \Exception('Impossible de supprimer la salle : ' . $stmt->errorInfo()[2]);
        }
    }
}