<?php

namespace CSLManager\Administration\Mapper;

class PermissionMapper extends Mapper
{
    /**
     * @var $rights array
     */
    protected $rights = [];

    /**
     *
     * @param array $rights
     */
    public function set(array $rights)
    {
        if (!$rights) {
            $rights = [];
        }
        $this->rights = $rights;
    }

    /**
     * Return true if valid
     *
     * @param string $rights
     * @return bool
     */
    public function check($rights)
    {
        return in_array($rights, $this->rights);
    }

    /**
     * Return rights from BDD
     *
     * @param string $id id user
     * @return array
     */
    public function load($id)
    {
        $sql = "SELECT r.label FROM rights r, permissions p,
                    user u
			WHERE r.id = p.id_right
                AND u.id_profile = p.id_profile
                AND u.upn = " . $this->db->quote($id);

        $stmt = $this->db->query($sql);
        return $stmt->fetchAll(\PDO::FETCH_COLUMN, 0);
    }

    /**
     * @param string $upn
     * @throws \Exception
     */
    public function loadProfile($upn)
    {
        $sql = "SELECT id FROM user WHERE upn = " . $this->db->quote($upn);
        $stmt = $this->db->query($sql);

        $this->profile = $stmt->fetch(\PDO::FETCH_ASSOC)['id'];
        // if (empty($this->profil)) throw new \Exception('NO_PROFILE');
    }

    /**
     * Return rights by profile
     *
     * @param int $id_profile
     * @return array rights
     */
    public function getByProfile($id_profile)
    {
        $sql = "SELECT r.id, r.label, r.description 
            FROM rights r, permissions p
			WHERE p.id_profile = " . $this->db->quote($id_profile, \PDO::PARAM_INT)
            . " AND p.id_right = r.id
            ORDER BY r.label";

        $stmt = $this->db->query($sql);
        $results = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $results[] = $row;
        }
        return $results;
    }
    
    function getRights() {
        return $this->rights;
    }

    /**
     * @param array $right
     * @return int
     * @throws \Exception
     */
    public function create(array $right)
    {
        try{
            $query = "INSERT INTO rights (label, description)
            VALUES (" .
                $this->db->quote($right['label'])
                . ", " .
                $this->db->quote($right['description'])
                . ")";
            return $this->db->exec($query);
        }catch (\Exception $e){
            throw new Exception("Requête non conforme");
        }


    }

    /**
     * Update right
     *
     * @param $id
     * @param $label
     * @param $description
     * @throws \Exception
     */
    public function alter($id, $label, $description)
    {
        $query = "UPDATE rights
			SET label = :label,
				description = :desc
			WHERE id = :id";

        $stmt = $this->db->prepare($query);
        $result = $stmt->execute([
            ':id' => $id,
            ':label' => $label,
            ':desc' => $description,
        ]);

        if (!$result) {
            throw new \Exception('Impossible de modifier le droit : ' . $stmt->errorInfo()[2]);
        }
    }

    /**
     * @param $id
     * @param $idProfile
     * @return int
     */
    public function delete($id, $idProfile)
    {
        $exec = "DELETE FROM permissions WHERE id_profile =$idProfile AND id_right=$id" ;

        return $this->db->exec($exec);
    }

    /**
     * return right not available for a profile
     *
     * @param int $id_profile
     * @return array rights
     */
    public function getRightsNotIn($id_profile)
    {
        $sql = "SELECT r.id, r.label, r.description 
            FROM rights r WHERE r.id
            NOT IN (SELECT r.id
            FROM rights r, permissions p
			WHERE p.id_profile = $id_profile
            AND p.id_right = r.id)";

        $stmt = $this->db->query($sql);
        $results = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $results[] = $row;
        }
        return $results;
    }

    /**
     * @param $idProfile
     * @param $idRight
     * @return int
     */
    public function insertPermission($idProfile, $idRight){
        try{
            $query = "INSERT INTO permissions (id_profile, id_right)
            VALUES (" .
                $this->db->quote($idProfile)
                . ", " .
                $this->db->quote($idRight)
                . ")";

            return $this->db->exec($query);
        }catch (\Exception $e){
            throw new Exception("Requête non conforme");
        }
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function getRightById($id){
        $query="SELECT r.id, r.label, r.description FROM rights r WHERE r.id = $id";

        $stmt = $this->db->query($query);

        if (($r = $stmt->fetch(\PDO::FETCH_ASSOC)) !== false) {
            return $r;
        } else {
            throw new \Exception('Droit introuvable');
        }
    }

}
