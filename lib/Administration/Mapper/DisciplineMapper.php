<?php


/**
 * Description of CategoryMapper
 *
 */

namespace CSLManager\Administration\Mapper;

class DisciplineMapper extends Mapper {

    /**
    * @return array
    */
    public function getDisciplines()
    {
        $sql = "SELECT id,  title, description
				FROM discipline";
        $stmt = $this->db->query($sql);

        $results = [];
        while ($row = $stmt->fetch()) {
            $results[] = $row;
        }
        return $results;
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function getDisciplineById($id)
    {
        $query = "SELECT d.id, d.title, d.description
			FROM discipline d
			WHERE d.id = " . $this->db->quote($id, \PDO::PARAM_INT);

        $stmt = $this->db->query($query);

        if (($r = $stmt->fetch(\PDO::FETCH_ASSOC)) !== false) {
            return $r;
        } else {
            throw new \Exception('Discipline introuvable');
        }
    }

    /**
     *
     * @param array $discipline
     * @return int
     * @throws \Exception
     */
    public function create(array $discipline)
    {

        $query = "INSERT INTO discipline (title, description)
            VALUES (" .
            $this->db->quote($discipline['title'])
            . ", " .
            $this->db->quote($discipline['description'])
            . ")";

        return $this->db->exec($query);
        
    }

    /**
     * @param array $discipline
     * @throws \Exception
     */
    public function alter(array $discipline)
    {
        $sql = "UPDATE discipline
			SET title = :title, description = :description
			WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            'id' => $discipline['id'],
            'title' => $discipline['title'],
            'description' => $discipline['description']
        ]);

        if (!$result || $stmt->rowCount() === 0) {
            throw new \Exception('Impossible de modifier le poste : ' . $stmt->errorInfo()[2]);
        }
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id) {
        $exec = "DELETE FROM discipline WHERE id =" . $this->db->quote($id, \PDO::PARAM_INT);

        return $this->db->exec($exec);
    }

}
