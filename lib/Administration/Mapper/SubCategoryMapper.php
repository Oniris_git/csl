<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoryMapper
 *
 * @author arthur.valvert
 */

namespace CSLManager\Administration\Mapper;

class SubCategoryMapper extends Mapper {

    /**
    * @return array
    */
    public function getSubCategories()
    {
        $sql = "SELECT id,  title, description
				FROM sub_category";
        $stmt = $this->db->query($sql);

        $results = [];
        while ($row = $stmt->fetch()) {
            $results[] = $row;
        }
        return $results;
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function getSubCategory($id)
    {
        $query = "SELECT s.id, s.title, s.description
			FROM sub_category s
			WHERE s.id = " . $this->db->quote($id, \PDO::PARAM_INT);

        $stmt = $this->db->query($query);

        if (($r = $stmt->fetch(\PDO::FETCH_ASSOC)) !== false) {
            return $r;
        } else {
            throw new \Exception('Category not found');
        }
    }

    /**
     *
     * @param array $subCategory
     * @return int
     * @throws \Exception
     */
    public function create(array $subCategory)
    {

        $query = "INSERT INTO sub_category (title, description)
            VALUES (" .
            $this->db->quote($subCategory['title'])
            . ", " .
            $this->db->quote($subCategory['description'])
            . ")";

        return $this->db->exec($query);
    }

    /**
     * @param array $subCategory
     * @throws \Exception
     */
    public function alter(array $subCategory)
    {
        $sql = "UPDATE sub_category
			SET title = :title, description = :description
			WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            'id' => $subCategory['id'],
            'title' => $subCategory['title'],
            'description' => $subCategory['description']
        ]);

        if (!$result || $stmt->rowCount() === 0) {
            throw new \Exception('Impossible to update : ' . $stmt->errorInfo()[2]);
        }
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        $exec = "DELETE FROM sub_category WHERE id =" . $this->db->quote($id, \PDO::PARAM_INT);

        return $this->db->exec($exec);
    }

}
