<?php


namespace CSLManager\Administration\Mapper;

use CSLManager\Administration\Entity\User;
use CSLManager\Administration\Entity\Workshop;
use CSLManager\Administration\Entity\WorkshopAttempt;

class WorkshopAttemptMapper extends Mapper
{

    /**
     * Select all workshops by $idWorkshop
     * @param $idWorkshop
     * @return array
     * @throws \Exception
     */
    public function selectUserByWorkshop($idWorkshop){
        if($idWorkshop !== null){
            try{
                try {
                    $query = "SELECT u.upn, wa.id, wa.id_user, wa.id_workshop as id_workshop, "
                            . "wa.debut, wa.fin, wa.scale, ws.full_label, ws.id_scale_type, ws.duration "
                            . "FROM user AS u "
                            . "LEFT JOIN workshop_attempt as wa ON wa.id_user=u.id "
                            . "LEFT JOIN workshop as ws ON ws.id=wa.id_workshop "
                            . "LEFT JOIN workshop_comment AS com ON ws.id=com.id_workshop  "
                            . "WHERE wa.id_workshop = $idWorkshop AND wa.fin IS NOT NULL";
                    $data=[];
                    $i=0;
                    foreach ($this->db->query($query) as $row){
                        $data[$i]=new WorkshopAttempt($row);
                        $i++;
                    }
                    return $data;
                } catch (Exception $ex) {
                    throw $ex;
                }
            } catch (Exception $ex) {
                throw new \Exception('User id does not exist');
            }
            
        }
    }

    /**
     * @return array
     */
    public function active()
    {
        $sql = "SELECT wa.id, wa.id_workshop, u.upn, ws.label,
				ws.full_label, wa.debut, ws.id_scale_type
			FROM workshop_attempt wa,
				workshop ws,
				user u
			WHERE wa.debut < NOW()
				AND wa.fin IS NULL
				AND wa.id_workshop = ws.id
				AND u.id = wa.id_user
			ORDER BY debut, ws.label";

        $stmt = $this->db->query($sql);
        $results = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $results[] = new WorkshopAttempt($row);
        }
        return $results;
    }

    /**
     * Select list of $idWorkshop by idUser and idWorkshop
     * @param $idUser
     * @param $idWorkshop
     * @return array
     */
     /*public function selectWorkshopAttemptByUserByWorkshop($idUser, $idWorkshop){
        if($idWorkshop !== null){
             $i=0;
             $data=[];
             try {
                 $query = "SELECT u.upn, wa.id, wa.id_user, wa.id_workshop as id_workshop, "
                     . "wa.debut, wa.fin, wa.scale, ws.full_label, ws.id_scale_type, ws.duration "
                     . "FROM user AS u "
                     . "LEFT JOIN workshop_attempt as wa ON wa.id_user=u.id "
                     . "LEFT JOIN workshop as ws ON ws.id=wa.id_workshop "
                     . "LEFT JOIN workshop_comment AS com ON ws.id=com.id_workshop  "
                     . "WHERE wa.id_user = $idUser AND wa.id_workshop = $idWorkshop AND wa.fin IS NOT NULL GROUP BY wa.id,ws.deleted = 0";

                 foreach ($this->db->query($query) as $row){
                     $data[$i]=new WorkshopAttempt($row);
                     $i++;
                 }
                 return $data;
             } catch (Exception $ex) {
                 throw $ex;
             }
         }

    }*/

    /**
     * Select list of $idWorkshop by idUser and idWorkshop
     * @param $idUser
     * @param $idWorkshop
     * @return array
     */
    public function selectWorkshopAttemptByUserByWorkshop($idUser, $idWorkshop){
        if($idWorkshop !== null){
            $i=0;
            $data=[];
            try {
                $query = "SELECT u.upn, wa.id, wa.id_user, wa.id_workshop 
                              AS id_workshop, wa.debut, wa.fin, wa.scale, ws.full_label, ws.id_scale_type, ws.duration, s.order_scale, s.label AS scale_label 
                              FROM user AS u 
                              RIGHT JOIN workshop_attempt AS wa  ON wa.id_user=u.id 
                              RIGHT JOIN workshop AS ws ON ws.id=wa.id_workshop 
                              RIGHT JOIN scale_type AS st ON st.id = ws.id_scale_type 
                              RIGHT JOIN scale AS s ON s.id_scale_type=st.id AND wa.scale = s.scale_item_code
                              WHERE wa.id_user = $idUser AND wa.id_workshop = $idWorkshop AND wa.fin IS NOT NULL GROUP BY wa.id,ws.deleted = 0 ";

                foreach ($this->db->query($query) as $row){
                    $data[$i]=new WorkshopAttempt($row);
                    $i++;
                }
                return $data;
            } catch (Exception $ex) {
                throw $ex;
            }
        }
    }


    /**
     * get list of distinct users per workshop
     * @param $idWorkshop
     * @return array
     */
    public function getDistinctIdUserByWorkshop($idWorkshop){
                
        if($idWorkshop !== null){
            
            $query = "SELECT DISTINCT id_user FROM workshop_attempt WHERE id_workshop = $idWorkshop AND fin IS NOT NULL GROUP BY id_user";
            
            $data = [];
             $i=0;
            foreach ($this->db->query($query) as $row){
                $data[$i] = $row;
                $i++;
            }
            return $data; 
        }
    }   

    
    /**
     * Delete all $idWorkshops which are out of time but finished
     * 
     */
    public function deleteTimeOutWorkshopAttempt(){
        
        $query= "DELETE from workshop_attempt WHERE TIMESTAMPDIFF(SECOND, debut, fin) > 7200";
        
        try{
           $this->db->query($query);
        } catch (Exception $ex) {
            throw new \Exception('Impossible to suppress');
        }
        
    }
    
    /**
     * Delete all $idWorkshops which are not finished in time
     * 
     */
    public function deleteTimeOutWorkshopAttemptNotFinish(){
        
        $query= "DELETE wa FROM `workshop_attempt` wa INNER JOIN workshop ws ON ws.id=wa.id_workshop 
                  WHERE TIMESTAMPDIFF(SECOND,wa.debut, NOW()) > 
                  (ws.ceiling_duration* 60 ) AND wa.fin is null";
        
        try{
           $stmt=$this->db->exec($query);
        } catch (Exception $ex) {
            throw new \Exception('Impossible to suppress');
        }
    }

    /**
     * Select all the tp for a user
     * @param $idUser
     * @return array
     * @throws \Exception
     */
    public function selectWorkshopAttemptByUser($idUser, $filter){
        if($idUser !== null){
            if($filter == 'global' || $filter == null){
                $WHERE = ' ';
            }else{
                $WHERE = ' AND ws.id_priority_level='.$filter;
            }
            try{
                $i=0;
                $data=[];
                try {
                    $query = 'SELECT u.upn, wa.id, wa.id_user, wa.id_workshop as id_workshop, 
                            wa.debut, wa.fin, wa.scale, ws.full_label, ws.id_scale_type, ws.duration FROM user AS u 
                            LEFT JOIN workshop_attempt as wa ON wa.id_user=u.id LEFT JOIN workshop as ws ON ws.id=wa.id_workshop 
                            LEFT JOIN workshop_comment AS com ON ws.id=com.id_workshop WHERE wa.id_user = '.$idUser.$WHERE.' AND wa.fin IS NOT NULL GROUP BY wa.id';
                    foreach ($this->db->query($query) as $row){
                        $data[$i]=new WorkshopAttempt($row);
                        $i++;
                    }
                    return $data;
                } catch (Exception $ex) {
                    throw $ex;
                }
            } catch (Exception $ex) {
                throw new \Exception('user id does not exist');
            }
            
        }
    }

    /**
     * @param User $user
     * @param Workshop $workshop
     * @return WorkshopAttempt
     * @throws \Exception
     */
    public function getWorkshopAttemptByIdUser(User $user, Workshop $workshop)
    {
        $result = [];
        $sql = "SELECT u.upn, wa.id, wa.id_user, wa.id_workshop,
			  ws.full_label,
			  wa.debut, wa.fin, wa.scale,
			  s.label as evaluation, s.order_scale
			FROM user u
			LEFT JOIN workshop_attempt wa ON u.id = wa.id_user
			LEFT JOIN workshop ws ON ws.id = wa.id_workshop
			LEFT JOIN scale s ON s.scale_item_code = wa.scale
			WHERE wa.id_user = ? AND wa.id_workshop = ?
			      AND wa.fin IS NULL";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(1, $user->getId(), \PDO::PARAM_INT);
        $stmt->bindValue(2, $workshop->getId(), \PDO::PARAM_INT);
        $result = $stmt->execute();

        if ($result && $this->lastRowCount() > 0) {
            return new WorkshopAttempt($stmt->fetch(\PDO::FETCH_ASSOC));
        }else if($result && $this->lastRowCount() == 0){
            return $result;
        } else {
            return false;
        }
    }

    /**
     * @param Workshop $workshop
     * @param WorkshopAttempt $workshopA
     * @throws \Exception
     */
    public function checkTime(Workshop $workshop, WorkshopAttempt $workshopA)
    {
        if (\DateTime::createFromFormat('Y-m-d H:i:s', $workshopA->getDebut())->add(new \DateInterval('PT' . $workshop->getDuration() . 'M'))
            >= new \DateTime()
        ) {
            throw new \Exception("Temps limite non atteint");
        }
    }

    /**
     * @param User $user
     * @return bool|mixed
     */
    public function getActive(User $user)
    {
        $user_id = $this->db->quote($user->getId(), \PDO::PARAM_INT);

        $sql = "SELECT id FROM workshop_attempt "
            . "WHERE id_user = $user_id "
            . "AND fin IS NULL AND debut IS NOT NULL";

        $result = $this->db->query($sql, \PDO::FETCH_ASSOC);

        return $this->lastRowCount() > 0 ? $result->fetchColumn() : false;
    }

    /**
     * @param User $user
     * @param Workshop $workshop
     * @return int
     * @throws \Exception
     */
    public function start(User $user, Workshop $workshop)
    {
        $workshop = $this->db->quote($workshop->getId(), \PDO::PARAM_INT);
        $user = $this->db->quote($user->getId(), \PDO::PARAM_INT);

        $time = new \DateTime('now');

        $sql = "INSERT INTO workshop_attempt
		SET debut = '" . $time->format('Y-m-d H:i:s') . "'
		, id_user = $user
		, id_workshop = $workshop";

        if ($this->db->exec($sql) !== 0) {
            return $this->db->lastInsertId();
        } else {
            throw new \Exception('Impossible to start workshop attempt');
        }
    }

    /**
     * @param $waId
     * @param $scale
     * @return bool
     */
    public function finish($waId, $scale)
    {
        $waId = $this->db->quote($waId, \PDO::PARAM_INT);
        $scale = $this->db->quote($scale);

        $time = new \DateTime('now');

        $sql = "UPDATE workshop_attempt SET fin = '" . $time->format('Y-m-d H:i:s') . "',
				scale = $scale
			WHERE id = $waId AND fin IS NULL";
        return $this->db->exec($sql);
    }


    /**
     * @param User $user
     * @param Workshop $workshop
     * @return int
     */
    public function checkCount(User $user, Workshop $workshop)
    {
        $user = $this->db->quote($user->getId(), \PDO::PARAM_INT);
        $workshop = $this->db->quote($workshop->getId(), \PDO::PARAM_INT);

        $sql = "SELECT count(id) as count FROM workshop_attempt
			WHERE id_user = $user 
			AND id_workshop = $workshop";
        $result = $this->db->query($sql, \PDO::FETCH_ASSOC);
        return $result->fetchColumn();
    }

    /**
     * @param WorkshopAttempt $wa
     * @param User $user
     * @return string Comment
     */
    public function getCommentsByWorkShopAttempt(WorkshopAttempt $wa, User $user)
    {
        $id_user = $this->db->quote($user->getId(), \PDO::PARAM_INT);
        $id_workshop = $this->db->quote($wa->getIdWorkshop(), \PDO::PARAM_INT);

        $sql = "SELECT content FROM workshop_comment"
            . " WHERE id_user = $id_user"
            . " AND id_workshop = $id_workshop";

        $result = $this->db->query($sql);

        return $this->lastRowCount() > 0 ? $result->fetchColumn() : '';
    }

    /**
     * @param $id_user
     * @param $id_workshop
     * @param $comment
     * @throws \Exception
     */
    public function updateComment($id_user, $id_workshop, $comment)
    {
        $id_user = $this->db->quote($id_user, \PDO::PARAM_INT);
        $id_workshop = $this->db->quote($id_workshop, \PDO::PARAM_INT);
        $comment = $this->db->quote($comment, \PDO::PARAM_INT);

        if (mb_strlen($comment) > 200) {
            throw new \Exception('Comment too long');
        }

        $sql = "INSERT INTO workshop_comment (id_user, id_workshop, content)"
            . " VALUES ($id_user, $id_workshop, $comment)"
            . " ON DUPLICATE KEY UPDATE content = $comment";

        $result = $this->db->exec($sql);

        if ($result === false) {
            throw new \Exception($this->db->errorInfo()[2]);
        }
    }

    /**
     * @param $id
     * @return WorkshopAttempt
     * @throws \Exception
     */
    public function getWorkshopAttemptById($id)
    {
        $id = $this->db->quote($id, \PDO::PARAM_INT);

        $sql = "SELECT wa.id, u.upn, wa.id_user, wa.id_workshop,
			  ws.full_label,
			  wa.debut, wa.fin, wa.scale, 
			  s.label as scale_label, s.order_scale 
			FROM user u 
			LEFT JOIN workshop_attempt wa ON u.id = wa.id_user
			LEFT JOIN workshop ws ON ws.id = wa.id_workshop
			LEFT JOIN scale s ON s.scale_item_code = wa.scale
			WHERE wa.id = $id";
        $result = $this->db->query($sql, \PDO::FETCH_ASSOC);
        if ($result !== false) {
            return new WorkshopAttempt($result->fetch());
        } else {
            throw new \Exception("Impossible to find workshop attempt");
        }
    }

    /**
     * @param $id
     * @throws \Exception
     */
    public function cancelWorkshopAttemptById($id){
        $sql = "DELETE wa FROM workshop_attempt wa WHERE wa.id=$id";

        try{
            $this->db->query($sql);
        }catch (\Exception $e){
            throw new \Exception($e);
        }
    }
}
