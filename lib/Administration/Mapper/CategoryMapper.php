<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoryMapper
 *
 * @author arthur.valvert
 */

namespace CSLManager\Administration\Mapper;

use CSLManager\Administration\Entity\Category;

class CategoryMapper extends Mapper
{

    /**
     * Return Categories
     *
     * @return array
     */
    public function getCategories()
    {
        $query = "SELECT id,  title, description
				FROM category";
        $stmt = $this->db->query($query);

        $results = [];
        while ($row = $stmt->fetch()) {
            $results[] = new Category($row);
        }
        return $results;
    }

    /**
     * Return Categories
     *
     * @return array
     */
    public function getCategorieByName($name)
    {
        try{
           $query = "SELECT id,  title, description
                FROM category WHERE title = '$name'";
            $stmt = $this->db->query($query);

            $results = [];
            while ($row = $stmt->fetch()) {
                return new Category($row);
            } 
        }catch(PDOException $e){
            throw new PDOException ($e->getMessage());
        }
        
        
    }

    /**
     * Return categories's information
     *
     * @param int id
     * @return array|bool
     */
    public function getCategoryById($id)
    {
        $query = "SELECT c.id, c.title, c.description
            FROM category c
			WHERE c.id = " . $this->db->quote($id, \PDO::PARAM_INT);

        $this->stmt = $this->db->query($query);

        if ($this->stmt === false) {
            return false;
        } else {
            return new Category($this->stmt->fetch(\PDO::FETCH_ASSOC));
        }
    }

    /**
     * Create new category
     *
     * @param array $category
     * @return int|bool
     */
    public function create(array $category)
    {
     $query = "INSERT INTO category (title, description)
            VALUES (" .
                $this->db->quote($category['title'])
            . ", " .
                $this->db->quote($category['description'])
            . ")";

        return $this->db->exec($query);
        
    }

    /**
     * Update category
     *
     * @param array $category
     * @return int|bool
     */
    public function alter(array $category)
    {
        $query = "UPDATE category
            SET title = " . $this->db->quote($category['title']) . ",
              description = " . $this->db->quote($category['description']) . "
			WHERE id = " . $this->db->quote($category['id']);

        return $this->db->exec($query);
    }

    /**
     * Delete category
     * @param int $id
     * @return int|bool
     */
    public function delete($id)
    {
        $query = "DELETE FROM category
            WHERE id =" . $this->db->quote($id, \PDO::PARAM_INT);

        return $this->db->exec($query);
    }
}
