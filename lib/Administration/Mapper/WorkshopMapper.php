<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


namespace CSLManager\Administration\Mapper;

use PDO;
use CSLManager\Administration\Entity\Workshop;
use CSLManager\Administration\Entity\WorkshoptAttempt;

class WorkshopMapper extends Mapper
{
    /**
     * @return array $results array workshops
     */
    public function getWorkshops()
    {
        $sql = "SELECT *
		FROM workshop ws
		WHERE deleted = 0
		ORDER BY ws.full_label ASC"
                . $this->limit
                . $this->offset;
        $stmt = $this->db->query($sql);

        $results = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $results[] = new Workshop($row);
        }
        return $results;
    }

    /**
     * @return array $results array workshops
     */
    public function getWorkshopsByCategory($id)
    {
        $sql = "SELECT ws.id, ws.label, ws.full_label, ws.deleted,
          ws.description, ws.available, ws.id_moodle, ws.id_room,
          ws.id_difficulty, ws.count_to_validate, ws.duration, ws.ceiling_duration,
          ws.id_scale_type,ws.id_category,ws.id_sub_category,ws.id_discipline,ws.id_priority_level,ws.id_species
        FROM workshop ws
        WHERE deleted = 0 AND ws.id_category = $id
        ORDER BY ws.full_label ASC"
                . $this->limit
                . $this->offset;

        $stmt = $this->db->query($sql);
        $results = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $results[] = new Workshop($row);
        }
        return $results;
    }

    /**
     * @param $filter
     * @return array
     */
    public function getWorkshopsByFilter($filter)
    {
        $sql = "SELECT ws.id, ws.label, ws.full_label, ws.deleted,
		        ws.description, ws.available, ws.id_moodle, ws.id_room, r.description,
		        ws.id_difficulty, dif.title as difficulty_title, ws.count_to_validate, 
		        ws.duration, ws.ceiling_duration,ws.id_scale_type ,ws.id_category,
		        c.title as category_title ,ws.id_sub_category,sc.title as sub_category_title,
		        ws.id_discipline ,d.title as discipline_title,ws.id_priority_level ,
		        pl.title as pl_title,ws.id_species , sp.title as species_title
		        FROM workshop ws 
                LEFT JOIN room as r ON r.id=ws.id_room
                LEFT JOIN category as c ON c.id=ws.id_category
                LEFT JOIN sub_category as sc ON sc.id=ws.id_sub_category
                LEFT JOIN discipline as d ON d.id=ws.id_discipline
                LEFT JOIN species as sp on sp.id=ws.id_species
                LEFT JOIN priority_level as pl on pl.id=ws.id_priority_level
                LEFT JOIN difficulty as dif on ws.id_difficulty= dif.id
		        WHERE deleted = 0
		        ORDER BY ws.$filter, ws.full_label"
                . $this->limit
                . $this->offset;

        $stmt = $this->db->query($sql);
        $i=0;
        $results = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $results[$i] =$row;
            $i++;
        }
        return $results;
    }

    /**
     * @param int $workshop_id
     * @return Workshop
     * @throws \Exception
     */
    public function getWorkshopById($workshop_id)
    {
        $sql = 'SELECT ws.id, ws.label, ws.full_label, ws.deleted,
		  ws.description, ws.available, ws.id_moodle, ws.id_room,
		  ws.id_difficulty, ws.count_to_validate, ws.duration, ws.ceiling_duration, ws.id_scale_type,ws.id_category,ws.id_sub_category,ws.id_discipline,ws.id_priority_level,ws.id_species 
		FROM workshop ws
		WHERE ws.id = ? AND deleted = 0';
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([$workshop_id]);

        if ($result && ($r = $stmt->fetch(\PDO::FETCH_ASSOC))) {
            return new Workshop($r);
        } else {
            throw new \Exception('Workshop unknown');
        }
    }

    /**
     * @param $workshopLabel
     * @return Workshop
     * @throws \Exception
     */
    public function getWorkshopByLabel($workshopLabel)
    {
        $sql = 'SELECT ws.id, ws.label, ws.full_label, ws.deleted,
		  ws.description, ws.available, ws.id_moodle, ws.id_room,
		  ws.id_difficulty, ws.count_to_validate, ws.duration, ws.ceiling_duration, ws.id_scale_type,ws.id_category,ws.id_sub_category,ws.id_discipline,ws.id_priority_level,ws.id_species 
		FROM workshop ws
		WHERE ws.label = ? AND deleted = 0';
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([$workshopLabel]);

        if ($result && ($r = $stmt->fetch(\PDO::FETCH_ASSOC))) {
            return new Workshop($r);
        } else {
            throw new \Exception('Workshop unknown');
        }
    }

    /**
     * @param Workshop $workshop
     * @throws \Exception
     */
    public function store(Workshop $workshop)
    {
        $sql = "INSERT INTO workshop
			(label, full_label, description, id_moodle, id_room, id_difficulty, duration,
				 count_to_validate, id_scale_type, id_category, id_sub_category, id_discipline, id_priority_level, id_species,
				  ceiling_duration) VALUES
			(:label, :full_label, :description, :id_moodle, :id_room, :id_difficulty, :duration,
				:count_to_validate, :id_scale_type, :id_category ,:id_sub_category,:id_discipline,:id_priority_level,:id_species
				, :ceiling_duration)";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            'label' => $workshop->getLabel(),
            'full_label' => $workshop->getFullLabel(),
            'description' => $workshop->getDescription(),
            //'deleted' => $workshop->getDeleted(),
            //'available'=> $workshop->getAvailable(),
            'id_moodle' => intval($workshop->getIdMoodle()),
            'id_room' => intval($workshop->getIdRoom()),
            'id_difficulty' => intval($workshop->getIdDifficulty()),
            'duration' => intval($workshop->getDuration()),
            'count_to_validate' => intval($workshop->getCountToValidate()),
            'id_scale_type' => intval($workshop->getIdScaleType()),
            'id_category' => intval($workshop->getIdCategory()),
            'id_sub_category' => intval($workshop->getIdSubCategory()),
            'id_discipline' => intval($workshop->getIdDiscipline()),
            
            'id_priority_level' => intval($workshop->getIdPriorityLevel()),
            'id_species' => intval($workshop->getIdSpecies()),
            'ceiling_duration' =>intval($workshop->getCeilingDuration()),
        ]);

        if (!$result || $stmt->rowCount() === 0) {
            throw new \Exception('Impossible to create workshop : ' . $stmt->errorInfo()[2]);
        }
    }

    /**
     * @param Workshop $workshop
     * @throws \Exception
     */
    public function alter(Workshop $workshop)
    {
        $sql = "UPDATE workshop
			SET label = :label, full_label = :full_label, description = :description, id_moodle = :id_moodle, id_room = :id_room, id_difficulty = :id_difficulty,
				 duration = :duration, count_to_validate = :count_to_validate, id_scale_type = :id_scale_type,
                 id_category = :id_category , id_sub_category = :id_sub_category, id_discipline = :id_discipline, 
                 id_priority_level = :id_priority_level, id_species = :id_species , ceiling_duration = :ceiling_duration 
			WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            'id' => $workshop->getId(),
            'label' => $workshop->getLabel(),
            'full_label' => $workshop->getFullLabel(),
            'description' => $workshop->getDescription(),
            'id_moodle' => $workshop->getIdMoodle(),
            'id_room' => intval($workshop->getIdRoom()),
            'id_difficulty' => intval($workshop->getIdDifficulty()),
            'duration' => intval( $workshop->getDuration()),
            'count_to_validate' => intval($workshop->getCountToValidate()),
            'id_scale_type' => intval($workshop->getIdScaleType()),
            'id_category' => intval($workshop->getIdCategory()),
            'id_sub_category' => intval($workshop->getIdSubCategory()),
            'id_discipline' => intval($workshop->getIdDiscipline()),
            'id_priority_level' => intval($workshop->getIdPriorityLevel()),
            'id_species' => intval($workshop->getIdSpecies()),
            'ceiling_duration' =>intval($workshop->getCeilingDuration())
        ]);

        if (!$result || $stmt->rowCount() === 0) {
            throw new \Exception('Impossible to update workshop : ' . $stmt->errorInfo()[2]);
        }
    }

    /**
     * @param int $workshopId
     * @throws \Exception
     */
    public function delete($workshopId)
    {
        $sql = "UPDATE workshop SET deleted = 1 WHERE id = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(1, $workshopId, \PDO::PARAM_INT);
        $result = $stmt->execute();

        if (!$result || $stmt->rowCount() === 0) {
            throw new \Exception('Impossible to delete workshop :' . $stmt->errorInfo()[2]);
        }
    }

    /**
     * @param Workshop $workshop
     */
    public function setScaleType(Workshop $workshop)
    {
        $scale_type = $this->db->quote($workshop->getIdScaleType(), \PDO::PARAM_INT);

        $sql = "SELECT scale_item_code AS code, label FROM scale
			WHERE deleted = 0 AND id_scale_type = $scale_type
			ORDER BY order_scale";

        $e = [];
        foreach ($this->db->query($sql, \PDO::FETCH_ASSOC) as $row) {
            $e[] = $row;
        }
        $workshop->setIdScaleType($e);
    }

    /**
     * Check if scale exist for workshop
     * @param Workshop $workshop
     * @param string
     * @throws \Exception
     */
    public function checkScale(Workshop $workshop, $scale)
    {
        $l = array_column($workshop->getIdScaleType(), 'code');
        if (!in_array($scale, $l)) {
            throw new \Exception('Scale does not exist');
        }
    }

}
