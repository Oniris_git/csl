<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


namespace CSLManager\Administration\Mapper;

abstract class Mapper
{
    /** @var \PDO $db Object PDO */
    protected $db;

    /** @var \PDOStatement $db Object PDOStatement */
    protected $stmt;

    /** @var string $limit SQL LIMIT option */
    protected $limit = '';

    /** @var string $offset SQL OFFSET option */
    protected $offset = '';

    /**
     * Mapper constructor.
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param int $number
     * @return $this
     */
    public function _paginate($number = 25)
    {
        $this->limit = ' LIMIT ' . $number;
        return $this;
    }

    /**
     * @param $number
     * @return $this
     */
    public function _offset($number)
    {
        $this->offset = ' OFFSET ' . $number;
        return $this;
    }

    /**
     * Last errors
     *
     * @return array
     */
    public function getPDOError()
    {
        return $this->db->errorInfo();
    }

    /**
     * Return last errors
     *
     * @return array
     */
    public function getPDOStatementError()
    {
        return $this->stmt->errorInfo();
    }

    /**
     *
     * @doc https://php.net/manual/fr/pdostatement.rowcount.php#113133
     * @return string
     */
    public function lastRowCount()
    {
        return $this->db->query("SELECT FOUND_ROWS()")->fetchColumn();
    }
}
