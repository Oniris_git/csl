<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace CSLManager\Administration\Mapper;

use CSLManager\Administration\Entity\Promo;

/**
 * Class PromoMapper
 * @package VetSims\Administration\Mapper
 */
class PromoMapper extends Mapper
{

    /**
     * @return array
     */
    public function getPromos()
    {
        $sql = "SELECT id, difficulty_level, title, description
				FROM difficulty";
        $stmt = $this->db->query($sql);

        $results = [];
        while ($row = $stmt->fetch()) {
            $results[] = new Promo($row);
        }
        return $results;
    }

    /**
     * @param $id
     * @return Promo
     * @throws \Exception
     */
    public function getPromoById($id)
    {
        try{
            $sql = "SELECT id, difficulty_level, title, description
				FROM difficulty WHERE id=$id";
            $stmt = $this->db->query($sql);

            $results = new Promo($stmt->fetch());
            return $results;
        } catch (Exception $ex) {
            throw new \Exception('L\'id n\'existe pas');
        }
    }
}
