<?php

/**
 * Classe SQL pour la table Specie
 *
 * @author arthur.valvert
 */

namespace CSLManager\Administration\Mapper;

use CSLManager\Administration\Entity\Species;


class SpecieMapper extends Mapper
{
    /**
     * @return array species
     */
    public function getSpecies()
    {
        $sql = "SELECT id,  title, description
				FROM species";
        $stmt = $this->db->query($sql);

        $results = [];
        while ($row = $stmt->fetch()) {
            $results[] = new Species($row);
        }
        return $results;
    }

    /**
     * @param int $id
     * @return array|bool specie
     */
    public function getSpecie($id)
    {
        $query = "SELECT s.id, s.title, s.description
			FROM species s
			WHERE s.id = " . $this->db->quote($id, \PDO::PARAM_INT);

        $this->stmt = $this->db->query($query);

        if ($this->stmt === false) {
            return false;
        } else {
            return new Species($this->stmt->fetch(\PDO::FETCH_ASSOC)) ;
        }
    }

    /**
     * @param array $specie
     * @return int|bool
     */
    public function create(array $specie)
    {

        $query = "INSERT INTO species (title, description)
            VALUES (" .
            $this->db->quote($specie['title'])
            . ", " .
            $this->db->quote($specie['description'])
            . ")";

        return $this->db->exec($query);
        
    }

    /**
     * @param array $specie
     * @return int|bool
     */
    public function alter(array $specie)
    {
        $query = "UPDATE species
            SET title = " . $this->db->quote($specie['title']) . ",
                description = " . $this->db->quote($specie['description']) . "
			WHERE id =  " . $this->db->quote($specie['id']);

        return $this->db->exec($query);
    }

    /**
     * @param int $id
     * @return int|bool
     */
    public function delete($id)
    {
        $exec = "DELETE FROM species
            WHERE id =" . $this->db->quote($id, \PDO::PARAM_INT);

        return $this->db->exec($exec);
    }
}
