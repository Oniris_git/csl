<?php

/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace CSLManager\Administration\Mapper;

use mysql_xdevapi\Exception;
use CSLManager\Administration\Entity\User;

class UserMapper extends Mapper {

    /**
     * @param $upn string user email
     * @return bool
     */
    public function exist($upn)
    {
        $sql = "SELECT count(*) FROM user u
                WHERE u.upn = " . $this->db->quote($upn, \PDO::PARAM_STR);
        $stmt = $this->db->query($sql);
        return $stmt->fetchColumn();
    }

    /**
     * @param User $user
     * @throws \Exception
     */
    public function create($user)
    {
        $sql = "INSERT INTO user
			(upn, name, first_name, email, description, deleted, max_difficulty, id_profile, auth, password, request, open, token) VALUES
			(:upn, :name, :first_name, :email, null, 0, 1, :id_profile,:auth, :password, :request, :open, :token )";

        if(!isset($user['email'])):
            $user['email'] = $user['mail'];
        endif;

        if($user['auth'] != "manual"):
            $user['password'] = null;
        endif;

        if(!isset($user['open'])):
            $user['open'] = 0;
        endif;

        if(!isset($user['token'])):
            $user['token'] = null;
        endif;

        if(!isset($user['password'])):
            $user['password'] = null;
        endif;

        if(!isset($user['request'])):
            $user['request'] = null;
        endif;

        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            ':upn' => $user['upn'],
            ':name' => $user['name'],
            ':first_name' => $user['firstName'],
            ':id_profile' => $user['id_profile'],
            ':email' => $user['email'],
            ':auth' => $user['auth'],
            ':password' => $user['password'],
            ':token' => $user['token'],
            ':request' => $user['request'],
            ':open' => $user['open']
        ]);
        if($result == false){
            throw new \Exception("Error Insert User check values : ".$result);
        }
    }

    /**
     * @param User $user
     * @throws \Exception
     */
    public function alter(User $user)
    {
        $sql = "UPDATE user SET
				upn = :upn,
				id_profile = :id_profile,
				deleted = :deleted,
				max_difficulty = :promo,
                description = :description
			WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            ':upn' => $user->getUpn(),
            ':id_profile' => $user->getIdProfile(),
            ':deleted' => $user->getDeleted(),
            ':promo' => $user->getPromo(),
            ':description' => $user->getDescription(),
            ':id' => $user->getId(),
        ]);
        if (!$result) {
            throw new \Exception('Could not update user');
        }
    }

    /**
     * @param $upn user email
     * @return int Id de l'utilisateur
     */
    public function userExist($upn) {
        $sql = "SELECT id FROM user WHERE upn = " . $this->db->quote($upn);
        $stmt = $this->db->query($sql);
        return $stmt->fetch(\PDO::FETCH_ASSOC)['id'];
    }

    /**
     * @param int $user_id
     * @return User
     * @throws \Exception
     */
    public function getUserById($user_id)
    {
        $sql = 'SELECT u.id, u.upn, u.name, u.first_name, u.description, u.id_profile, u.deleted,
 				u.max_difficulty, p.label AS profile_name, d.title AS nom_promo, u.request, u.open, u.token, u.auth, u.email
			FROM user u
			LEFT JOIN difficulty d ON d.id = u.max_difficulty
			LEFT JOIN profils p ON p.id = u.id_profile
			WHERE u.id = :id';
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([':id' => $user_id]);

        if ($result && ($r = $stmt->fetch(\PDO::FETCH_ASSOC))) {
            return new User($r);
        } else {
            throw new \Exception('User not found');
        }
    }

    /**
     * Return array user
     * @param null $order
     * @return array
     * @throws \Exception
     */
    public function getUsers($order= null)
    {
        $ORDER= ' ';
        if($order !== null){
            $ORDER = $order;
        }
        $sql = "SELECT u.id, u.upn, u.name, u.first_name, u.email, u.description, u.id_profile, u.deleted,
 				u.max_difficulty, p.label AS profile_name, d.title AS nom_promo
			FROM user u
			LEFT JOIN difficulty d ON d.id = u.max_difficulty
			LEFT JOIN profils p ON p.id = u.id_profile".$ORDER
                . $this->limit
                . $this->offset;
        $stmt = $this->db->query($sql);

        $results = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $results[] = new User($row);
        }
        return $results;
    }

    /**
     *
     * @return array user object
     * @throws \Exception
     */
    public function getUsersDown()
    {
        $sql = "SELECT u.id, u.upn, u.name, u.first_name, u.description, u.id_profile, u.deleted,
 				u.max_difficulty, p.label AS profile_name, d.title AS nom_promo
			FROM user u
			LEFT JOIN difficulty d ON d.id = u.max_difficulty
			LEFT JOIN profils p ON p.id = u.id_profil ORDER BY p.libelle DESC, u.nom DESC "
                . $this->limit
                . $this->offset;
        $stmt = $this->db->query($sql);

        $results = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $results[] = new User($row);
        }
        return $results;
    }

    /**
     * @return int total user
     */
    public function getTotalUsers() {
        $sql = "SELECT count(id) FROM user";
        $stmt = $this->db->query($sql);
        return $stmt->fetchColumn();
    }

    /**
     * Import users
     *
     * @param User[] $users array
     * @return int return number of users imported
     * @throws \Exception if user exist
     */
    public function import(array $users) {
        $sql = "INSERT INTO user
			(upn, id_profile, deleted, max_difficulty,email) VALUES
			(:upn, :id_profile, 0, :promo,:email)";
        $stmt = $this->db->prepare($sql);

        $count = 0;
        foreach ($users as $user) {
            $result = $stmt->execute([
                ':upn' => $user->getUpn(),
                ':id_profile' => $user->getIdProfile(),
                ':promo' => $user->getPromo(),
                ':email' => $user->getEmail()
            ]);
            $count++;
            if (!$result) {
                throw new \Exception('Could not import user. Probably already in database');
            }
        }
        return $count;
    }

    /**
     * search by name
     * @param string $q input
     * @return User[] array user objects
     * @throws \Exception
     */
    public function search($q)
    {
        $sql = "SELECT u.id, u.upn, u.name, u.first_name, u.description, u.id_profile, u.deleted,
 				u.max_difficulty, p.label AS profile_name, d.title AS nom_promo
			FROM user u
			LEFT JOIN difficulty d ON d.id = u.max_difficulty
			LEFT JOIN profils p ON p.id = u.id_profile
			WHERE u.upn LIKE :q "
                . $this->limit
                . $this->offset;
        $q=strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($q, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
        $stmt = $this->db->prepare($sql);
        $stmt->execute(['q' => '%' . $q . '%']);

        $results = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $results[] = new User($row);
        }
        return $results;
    }

    /**
     * @param $upn
     * @return User user object
     * @throws \Exception
     */
    public function getUserByUpn($upn)
    {
        $sql = "SELECT u.id, u.upn, u.name, u.first_name, u.email, u.description, u.id_profile, u.deleted,
 				u.max_difficulty, p.label AS profile_name, d.title AS nom_promo, u.request, u.open, u.token, u.auth
			FROM user u
			LEFT JOIN difficulty d ON d.id = u.max_difficulty
			LEFT JOIN profils p ON p.id = u.id_profile
			WHERE u.upn = :upn"
                . $this->limit
                . $this->offset;

        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([':upn' => $upn]);

        if ($result && ($r = $stmt->fetch(\PDO::FETCH_ASSOC))) {
            return new User($r);
        } else {
            throw new \Exception('User not found');
        }
    }

    /**
     * Change chart's legend
     * @param null $legend
     */
    public function alterLegend($legend = null){
            try{
                $query= "UPDATE user SET description = :legend";
                $stmt=$this->db->prepare($query);
                $stmt->execute(['legend' => $legend]);
            }catch (Exception $ex){
                throw new Exception("Error legend");
            }
    }

    /**
     * @param $upn
     * @return mixed
     */
    public function getRegisterType($upn){

        $query = "SELECT auth FROM user WHERE upn = '$upn'";
        $result = $this->db->query($query);

        foreach ($result as $row):
            return $row;
        endforeach;
    }

    /**
     * @param $password
     * @param $upn
     * @return bool
     */
    public function verifyPassword($password, $upn){

        $query = "SELECT password FROM user WHERE upn = '$upn'";
        $result = $this->db->query($query);

        foreach ($result as $row):
            return password_verify($password, $row['password']);
        endforeach;
    }

    /**
     * @param $token
     * @return User
     * @throws \Exception
     */
    public function selectUserByToken($token){

        $query = "SELECT * FROM user WHERE token = '$token'";
        $result = $this->db->query($query);

        foreach ($result as $row):
            return new User($row);
        endforeach;
    }

    /**
     * @param $userId
     * @param $password
     */
    public function updatePassword($userId, $password){

        $query = "UPDATE user SET
				request = null,
				password = '$password',
				open = 0,
				request = null,
				token = null
			WHERE id ='$userId'";

        $result = $this->db->exec($query);

        if($result != true):
            throw new Exception("Impossible to update password");
        endif;
    }

    public function requestPassword($newToken, $userId){
        $timestamp = date("Y-m-d H:i:s");
        $query = "UPDATE user SET
				request = '$timestamp',
				open = 1,
				token = '$newToken'
			WHERE id ='$userId'";

        $result = $this->db->exec($query);

        if($result != true):
            echo"plouf";
        endif;
    }

}
