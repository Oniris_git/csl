<?php

namespace CSLManager\Administration\Mapper;

use CSLManager\Administration\Entity\Scale;

class ScaleMapper extends Mapper
{
    /**
     * Get all evaluations
     * @return array
     */
    public function getScales()
    {
        $sql = "SELECT s.id, s.scale_item_code, s.label, s.deleted, s.id_scale_type, s.order_scale
			FROM scale s"
            . $this->limit
            . $this->offset;
        $stmt = $this->db->query($sql);

        $results = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $results[] = new Scale($row);
        }
        return $results;
    }

    /**
     * @param $id int
     * @return array
     */
    public function getScalesByTypeId($id)
    {
        $sql = "SELECT s.id, s.scale_item_code, s.label, s.deleted, s.id_scale_type, s.order_scale
			FROM scale s
			WHERE s.id_scale_type = :id
			AND s.deleted = 0
			ORDER BY order_scale";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([':id' => $id]);

        $results = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $results[] = new Scale($row);
        }
        return $results;
    }

    /**
     * @param $sid
     * @param array $order
     * @throws \Exception
     */
    public function setOrderByGroup($sid, array $order)
    {
        $sql = "UPDATE scale
			SET order_scale = :order_scale, id_scale_type = :id_scale_type
			WHERE id = :id AND (id_scale_type = :scale_type OR id_scale_type IS NULL)";
        $stmt = $this->db->prepare($sql);

        $result = [];
        foreach ($order as $key => $item) {
            $result[] = $stmt->execute(['id' => $item, 'order_scale' => $key + 1, 'id_scale_type' => $sid, 'scale_type' => $sid]);
        }
        if (in_array(false, $result)) {
            throw new \Exception('Update Error');
        }

    }

    /**
     * @return array scales which don't have scale type
     */
    public function getScalesNotInType()
    {
        $sql = "SELECT s.id, s.scale_item_code, s.label, s.deleted, s.id_scale_type, s.order_scale
			FROM scale s
			WHERE s.id_scale_type IS NULL
			
			ORDER BY order_scale";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();

        $results = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $results[] = new Scale($row);
        }
        return $results;
    }

    /**
     * @param array $array
     * @throws \Exception
     * @internal param Scale
     */
    public function create(array $array)
    {
        $sql = "INSERT INTO scale
          (scale_item_code, label, deleted, id_scale_type, order_scale)
		  VALUES (:scale_code, :label, 0, null, 0)";

        $stmt = $this->db->prepare($sql);

        $result = $stmt->execute([
                ':scale_code' => $array['scale-code'],
                ':label' => $array['scale-label'],
        ]);

        if($result != "1"){
            throw new \Exception("Error Insert check values");
        }
        
    }

    /**
     * @param $id
     * @return Scale
     * @throws \Exception
     */
    public function getScaleById($id)
    {
        $sql = "SELECT s.id, s.scale_item_code, s.label, s.deleted, s.id_scale_type, s.order_scale
			FROM scale s
			WHERE s.id = :id";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([':id' => $id]);

        if ($result && ($r = $stmt->fetch(\PDO::FETCH_ASSOC))) {
            return new Scale($r);
        } else {
            throw new \Exception('Scale not found');
        }
    }

    /**
     * @param $scale
     * @param bool
     * @throws \Exception
     */
    public function setStatus(Scale $scale, $status)
    {
        $sql = "UPDATE scale
			SET deleted = :state
			WHERE id = :id";

        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            ':state' => $status,
            ':id' => $scale->getId()
        ]);

        if (!$result) {
            throw new \Exception('Impossible to update' . $stmt->errorInfo()[2]);
        }
    }

    /**
     * @param $scale
     * @throws \Exception
     */
    public function unGroup(Scale $scale)
    {
        $sql = "UPDATE scale
			SET id_scale_type = NULL
			WHERE id = :id";

        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            ':id' => $scale->getId()
        ]);

        if (!$result) {
            throw new \Exception('Impossible to update ' . $stmt->errorInfo()[2]);
        }
    }

    /**
     * @param $scale
     * @param string
     * @throws \Exception
     */
    public function alter(Scale $scale, $label)
    {
        $sql = "UPDATE scale
			SET label = :label
			WHERE id = :id";

        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            ':label' => $label,
            ':id' => $scale->getId()
        ]);

        if (!$result) {
            throw new \Exception('Impossible to update' . $stmt->errorInfo()[2]);
        }
    }

    /**
     * @return array
     */
    public function getScaleTypes()
    {
        $sql = "SELECT st.id AS id, st.label AS label
		FROM scale_type st
		WHERE st.active = 1";

        $stmt = $this->db->query($sql);

        $results = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $results[] = $row;
        }
        return $results;
    }

    /**
     * @param array $array
     * @return mixed
     * @throws \Exception
     */
    public function createScaleType(array $array)
    {
        $sql = "INSERT INTO scale_type
			(label, active) VALUES (:label, 1)";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            ':label' => $array['label'],
        ]);
        if (!$result) {
            throw new \Exception('Impossible to create scale type');
        } else {
            return $this->getScaleTypes($this->db->lastInsertId());
        }
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function getScaleTypeById($id)
    {
        $sql = "SELECT st.id AS id, st.label AS label
		FROM scale_type st
		WHERE st.active = 1 AND st.id = :id";

        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([':id' => $id]);

        if ($result && ($r = $stmt->fetch(\PDO::FETCH_ASSOC))) {
            return $r;
        } else {
            throw new \Exception('Scale type not found');
        }
    }

    /**
     * @param $id
     * @return int
     * @throws \Exception
     */
    public function delete($id)
    {
        $sql = "DELETE FROM scale_type WHERE id = "
            . $this->db->quote($id);

        $affected_row = $this->db->exec($sql);
        if ($affected_row === false) {
            $errormsg = $this->db->errorInfo()[2];
            throw new \Exception("Error : ".$errormsg, 1);
        } else {
            return $affected_row;
        }
    }
}
