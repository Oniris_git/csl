<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoryMapper
 *
 * @author arthur.valvert
 */

namespace CSLManager\Administration\Mapper;

use CSLManager\Administration\Entity\PriorityLevel;

class PriorityLevelMapper extends Mapper {

    /**
    * @return array
    */
    public function getPriorityLevels()
    {
        $sql = "SELECT id,  title, description
				FROM priority_level";
        $stmt = $this->db->query($sql);

        $results = [];
        while ($row = $stmt->fetch()) {
            $results[] = new PriorityLevel($row);
        }
        return $results;
    }

    /**
     * @param $id
     * @return priority level
     * @throws \Exception
     */
    public function getPriorityLevelById($id)
    {
        $query = "SELECT pl.id, pl.title, pl.description
			FROM priority_level pl
			WHERE pl.id = " . $this->db->quote($id, \PDO::PARAM_INT);

        $stmt = $this->db->query($query);

        if (($r = $stmt->fetch(\PDO::FETCH_ASSOC)) !== false) {
            return $r;
        } else {
            throw new \Exception('Category introuvable');
        }
    }

    /**
     *
     * @param array $priorityLevel
     * @return int
     * @throws \Exception
     */
    public function create(array $priorityLevel)
    {

        $query = "INSERT INTO priority_level (title, description)
            VALUES (" .
            $this->db->quote($priorityLevel['title'])
            . ", " .
            $this->db->quote($priorityLevel['description'])
            . ")";

        return $this->db->exec($query);
        
    }

    /**
     * @param array $priorityLevel
     * @throws \Exception
     */
    public function alter(array $priorityLevel)
    {
        $sql = "UPDATE priority_level
			SET title = :title, description = :description
			WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            'id' => $priorityLevel['id'],
            'title' => $priorityLevel['title'],
            'description' => $priorityLevel['description']
        ]);

        if (!$result || $stmt->rowCount() === 0) {
            throw new \Exception('Impossible de modifier le poste : ' . $stmt->errorInfo()[2]);
        }
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        $exec = "DELETE FROM priority_level WHERE id =" . $this->db->quote($id, \PDO::PARAM_INT);

        return $this->db->exec($exec);
    }

}
