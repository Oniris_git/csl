<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 19/12/2018
 * Time: 14:35
 */

namespace CSLManager\Administration\Mapper;

use Exception;
use CSLManager\Administration\Mapper\Mapper;
use CSLManager\Administration\Entity\Language;

class LanguageMapper extends Mapper
{

    /**
     * @return array
     * @throws Exception
     */
    public function selectLanguages(){

        $query = "SELECT * FROM lang";

        $result=[];
        $i= 0;

        foreach ($this->db->query($query) as $row){
            $result[$i] =  new Language($row);
            $i++;
        }

        if($result != true){
            throw new Exception("Error : Something wrong");
        }

        return $result;

    }

    /**
     * @param Language $language
     * @throws Exception
     */
    public function insertLanguage(Language $language){

        $query = 'INSERT INTO lang (title, path_img, path_file) VALUES (:title , :path_img , :path_file)';

        $stmt = $this->db->prepare($query);

        $result = $stmt->execute([
            ':title' => $language->getTitle(),
            ':path_img' => $language->getPathImg(),
            ':path_file' => $language->getPathFile()
        ]);

        if($result != true){
            throw new Exception("Check your values insert cancelled");
        }
    }

    /**
     * @param $title
     * @throws Exception
     */
    public function deleteLanguageByTitle($title){

        $query = "DELETE FROM lang WHERE title = '$title'";

        $result = $this->db->exec($query);

        if($result != true){
            throw new Exception("Impossible to delete language");
        }
    }

    /**
     * @param $title
     * @return string|Language
     * @throws Exception
     */
    public function selectLanguageByTitle($title){
        $result = "";
        $query = "SELECT title, path_img, path_file FROM lang WHERE title = '$title'";

        foreach ($this->db->query($query) as $row):
            $result = new Language($row);
        endforeach;

        if($result != true){
            throw new Exception("Error : Tittle does not exist");
        }

        return $result;

    }

}