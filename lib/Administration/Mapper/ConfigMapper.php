<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 03/12/2018
 * Time: 16:03
 */

namespace CSLManager\Administration\Mapper;
use Exception;
use CSLManager\Administration\Mapper\Mapper;

class ConfigMapper extends Mapper
{

    /**
     * @param $type
     * @param $name
     * @param $value
     * @throws \Exception
     */
    public function insert($type, $name, $value){

        $query = "INSERT INTO config (type, name, value) VALUES ( :type, :name, :value) ";

            $stmt = $this->db->prepare($query);

            $result = $stmt->execute([
                ':type' => $type,
                ':value' => $value,
                ':name' => $name
            ]);
            if($result === false){
                throw  new Exception(TXT_NOTIFICATION_CHECK_VALUES);
            }

    }

    /**
     * @return array|\Exception|mixed
     */
    public function selectViews(){

        $query = "SELECT name, value FROM config WHERE type = 'view' ORDER BY name";

        $results = [];

        foreach ($this->db->query($query, \PDO::FETCH_ASSOC) as $item) {
            $results[] = $item;
        }
        return $results;
    }

    /**
     * @param $type
     * @param $name
     * @return array|\Exception|mixed
     */
    public function selectConfigByName($type, $name){

        $query = "SELECT value FROM config WHERE type = '$type' AND name = '$name' ";


        foreach ($this->db->query($query, \PDO::FETCH_ASSOC) as $item) {
            $result = $item;
        }
        return $result;
    }

    /**
     * @param $name
     * @param $value
     * @throws \Exception
     */
    public function alterView($name, $value){

        $query = "UPDATE config SET value = :value WHERE type = 'view' AND name = :name";

        $stmt = $this->db->prepare($query);
        $result = $stmt->execute([
            ':value' => $value,
            ':name' => $name
        ]);

        if ($result === false) {
            throw new \Exception('Impossible to alter view : ' . $stmt->errorInfo()[2]);
        }

    }

    /**
     * @param $name
     * @param $value
     * @throws \Exception
     */
    public function alterMoodle($name, $value){

        $query = "UPDATE config SET value = :value WHERE type = 'moodle' AND name = :name";

        $stmt = $this->db->prepare($query);
        $result = $stmt->execute([
            ':value' => $value,
            ':name' => $name
        ]);

        if ($result === false) {
            throw new \Exception('Impossible to alter moodle : ' . $stmt->errorInfo()[2]);
        }

    }

    /**
     * @param $type
     * @param $name
     * @param $value
     * @throws Exception
     */
    public function alterConfig($type, $name, $value){

        $query = "UPDATE config SET value = :value WHERE type = :type AND name = :name";

        $stmt = $this->db->prepare($query);
        $result = $stmt->execute([
            ':type' => $type,
            ':value' => $value,
            ':name' => $name
        ]);

        if ($result === false) {
            throw new \Exception('Impossible to alter moodle : ' . $stmt->errorInfo()[2]);
        }

    }

    /**
     * @param $name
     * @param $value
     * @throws \Exception
     */
    public function alterSMTP($name, $value){

        $query = "UPDATE config SET value = :value WHERE type = 'smtp' AND name = :name";

        $stmt = $this->db->prepare($query);
        $result = $stmt->execute([
            ':value' => $value,
            ':name' => $name
        ]);

        if ($result === false) {
            throw new \Exception('Impossible to alter smtp : ' . $stmt->errorInfo()[2]);
        }

    }

    /**
     * @param $type
     * @return mixed
     */
    public function selectType($type){

        $query = "SELECT * FROM config WHERE type = '$type'";

        $i= 0;
        foreach ($this->db->query($query, \PDO::FETCH_ASSOC) as $item) {
            $result[$i] = $item;
            $i++;
        }

        return $result;
    }

    public function deleteType($type){

        $query = "DELETE * FROM config WHERE type = '$type'";

        $result = $this->db->query($query);

        if($result != true){
            throw new Exception("Impossible to delete type");
        }
    }


}