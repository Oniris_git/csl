<?php


namespace CSLManager\Administration\Helper;

class Config
{
    protected static $config = [];

    /**
     * Config constructor.
     *
     * @param array $config Tableau de config
     */
    public function __construct(array $config = [])
    {
        self::$config = $config;
    }

    /**
     * Ajoute un tableau de config à la configuration
     *
     * @param array $config Tableau de config
     */
    public static function load(array $config = [])
    {
        self::$config = array_merge(self::$config, $config);
    }

    /**
     * Retourne la valeur associée à la clé
     *
     * @param $key string Clé de configuration
     * @return mixed|null Valeur
     */
    public function get($key, $fallback = null)
    {
        if ($this->has($key)) {
            return self::$config[$key];
        }

        return $fallback;
    }

    /**
     * Vérifie que la clé existe
     *
     * @param $key string Clé à chercher
     * @return bool
     */
    private function has($key)
    {
        return isset(self::$config[$key]);
    }

    /**
     * Dump le tableau de config
     *
     * @return array
     */
    public static function dump()
    {
        return self::$config;
    }
}
