<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Classe de gestion de session
 */

namespace CSLManager\Administration\Session;

use Vespula\Auth\Exception;

class Session extends \Vespula\Auth\Session\Session
{
    /**
     * @param string $form
     * @return string
     */
    public function generateToken($form)
    {
        $token = sha1(uniqid(microtime(), true));
        $this->setValue($form . "_token", $token);
        return $token;
    }

    /**
     * Ajoute une valeur à la session
     *
     * @param string $key
     * @param mixed  $value
     *
     */
    public function setValue($key, $value)
    {
        $this->store['userdata'][$key] = $value;
    }

    /**
     * @param string $form
     * @param string $token
     * @return bool
     * @throws \Exception
     */
    public function verifyToken($form, $token)
    {
        return $this->getValue($form . "_token") === $token;
    }

    /**
     * Retourne une valeur de session
     *
     * @param $key
     * @return mixed
     */
    public function getValue($key)
    {
        return (!empty($this->store['userdata'][$key])) ? $this->store['userdata'][$key] : null;
    }

    /**
     * @param string $key
     */
    public function unsetValue($key)
    {
        unset($this->store['userdata'][$key]);
    }
}
