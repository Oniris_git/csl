<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace CSLManager\Administration;

class CSV
{
    protected $header;
    protected $data;
    protected $delimiter = ';';
    protected $filename;

    public function __construct($filename = 'filename')
    {
        $this->filename = $filename;
    }

    public function setHead($array)
    {
        $this->header = $array;
    }

    public function setData($array)
    {
        $this->data = $array;
    }

    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
    }

    public function output()
    {
        header('Content-Encoding: UTF-8');
        header('Content-Type: application/csv;charset=UTF-8');
        header('Content-Disposition: attachment; filename="' . $this->filename . '.csv";');
        $f = fopen('php://output', 'w');
        fputs($f, "\xEF\xBB\xBF");

        fputcsv($f, $this->header, $this->delimiter);

        foreach ($this->data as $line) {
            fputcsv($f, $line, ';');
        }
        fclose($f);
    }
}
