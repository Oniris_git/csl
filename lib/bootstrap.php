<?php

use Vespula\Auth\Auth;
use Vespula\Auth\Exception;
use CSLManager\Administration\Mapper\PermissionMapper;
use CSLManager\Administration\Mapper\WorkshopAttemptMapper;
use CSLManager\Administration\Helper\Config;
use CSLManager\Administration\Mapper\UserMapper;
use CSLManager\Administration\Mapper\ConfigMapper;

// Autoloader
require __DIR__ . '/../vendor/autoload.php';

// Enregistrement du tableau de configuration
$config = require __DIR__ . '/../config.php';
$config = new Config($config);

// Chargement des dépendences dans le container
require __DIR__.'/dependencies.php';

require __DIR__ . '/../public/back-office/globalFunctions.php';

$logger = $container->get('logger');

// Debug mode
if ($config->get('debug')) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    $runner = new League\BooBoo\BooBoo([]);
    if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])
        && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest'
    ) {
        $runner->pushFormatter(new League\BooBoo\Formatter\JsonFormatter());
    } else {
        $runner->pushFormatter(new League\BooBoo\Formatter\HtmlTableFormatter());
    }
    $runner->pushHandler(new League\BooBoo\Handler\LogHandler($container->get('logger')));
    $runner->register(); // Registers the handlers
}

try {
    $session = $container->get('session');
    $adapter = $container->get('adapter');

    $auth = new Auth($adapter, $session);
} catch (Exception $e) {
    $logger->critical(
        TXT_LOG_ERROR,
        ['line' => __LINE__, 'message' => $e->getMessage()]
    );
    exit($e->getMessage());
}

if (!in_array($_SERVER['PHP_SELF'], $config->get('view')['anonymousAccess']) && !$auth->isValid()) {
    // Requete AJAX
    if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])
        && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest'
    ) {
        echo json_encode(['success' => false, 'message' => 'Vous avez été déconnecté.']);
        exit();
    }

    // Chargement page classique
    $url = $_SERVER['REQUEST_URI'];
    header("Location: /login.php?url=" . urlencode($url));
    exit();
}

$permission = new PermissionMapper($connector);
$permission->set((array)$session->getValue('rights'));

// Chargement des fichiers de langue
$lang = filter_input(INPUT_GET, 'lang', FILTER_SANITIZE_STRING);
if (isset($lang) && is_file(__DIR__.'/../lang/lang-'.$lang.'.inc.php')) {
    $session->setValue('lang', $lang);
}

$lang = $session->getValue('lang');

if (isset($lang)) {
    include(__DIR__.'/../lang/lang-'.$lang.'.inc.php');
} else if($configDB['defaultLang']) {
    if (is_file(__DIR__.'/../lang/lang-'.$configDB['defaultLang'].'.inc.php')) {
        include(__DIR__.'/../lang/lang-'.$configDB['defaultLang'].'.inc.php');
        $lang = $configDB['defaultLang'];
    }
}else{
    $browserLang = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
    $browserLang = strtolower(substr(chop($browserLang[0]), 0, 2));
    if (is_file(__DIR__.'/../lang/lang-'.$browserLang.'.inc.php')) {
        include(__DIR__.'/../lang/lang-'.$browserLang.'.inc.php');
        $lang = $browserLang;
    } else {
        //include(__DIR__.'/../lang/lang-'.$config->get('view')['lang'].'.inc.php');
        include(__DIR__.'/../lang/lang-'.$lang['value'].'.inc.php');
        $lang = $lang['value'];
    }
}


//delete all workshop attempts which are out of time
$wsMapper= new WorkshopAttemptMapper($connector);
$wsMapper->deleteTimeOutWorkshopAttempt();

//delete all workshop attempts which are not finished in time
$wsMapper->deleteTimeOutWorkshopAttemptNotFinish();

