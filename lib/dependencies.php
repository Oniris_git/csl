<?php

use League\Container\Container;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Vespula\Auth\Adapter\Ldap;
use CSLManager\Administration\Mapper\ConfigMapper;
use CSLManager\Administration\Session\Session;
use CSLManager\Administration\Manual;

$container = new Container();

// Enregistrement log
$log = new Logger('app');
$log->pushHandler(new StreamHandler($config->get('log')['logdir'].'/app.log'));
$container->add('logger', $log);

// Enregistrement de la connexion à la base de données
$db = $config->get('db');
$dsn = $db['type']
    . ':dbname=' . $db['base']
    . ';host=' . $db['server']
    . ';charset=utf8';

try {
    $container->add('db', new \PDO(
        $dsn,
        $db['user'],
        $db['password']
    ));

    $container->get('db')->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
} catch (PDOException $e) {
    /*$container->get('logger')
        ->critical(
            'Erreur de connexion à la base de données avec le message {message}',
            ['message' => $e->getMessage()]
        );

    // On affiche un message à l'utilisateur
    // TODO: Fonction dans Helpers
    echo "<html><meta charset=\"utf-8\"><body>";
    echo "<h1>Une erreur est survenue !</h1>";

    if ($config->get('debug')) {
        echo "<pre>" . $e->getMessage() . "</pre>";
    }

    echo "</body></html>";
    exit();*/
}


unset($dsn, $db);

/** @var \PDO $connector */
$connector = $container->get('db');

//load config information from db
$configMapper = new ConfigMapper($connector);
$configDB = [
    "mainTitle" => $configMapper->selectConfigByName('view','title')['value'],
    "logo" => $configMapper->selectConfigByName('view', 'logo')['value'],
    "support" => $configMapper->selectConfigByName('view','technique')['value'],
    "footer" => $configMapper->selectConfigByName('view','footer')['value'],
    "defaultLang" => $configMapper->selectConfigByName('view','lang')['value'],
    "authType" => $configMapper->selectConfigByName('auth','type')['value'],
    "legal" => $configMapper->selectConfigByName('view','legal')['value'],
    "domain" => $configMapper->selectConfigByName('view','domain')['value'],
    "domainMoodle" => $configMapper->selectConfigByName('moodle','domain')['value'],
    "courseMoodle" => $configMapper->selectConfigByName('moodle','course')['value'],
    "tokenMoodle" => $configMapper->selectConfigByName('moodle','token')['value'],
];

//if auth is like LDAP push instance into adapter
$authType = $configMapper->selectConfigByName("auth", "type")['value'];
if ($authType == "ldap"):
    $ldapBindOptions = [
        'basedn' => $configMapper->selectConfigByName("auth_ldap", 'basedn')['value'],
        'binddn' => $configMapper->selectConfigByName("auth_ldap", 'binddn')['value'],
        'bindpw' => $configMapper->selectConfigByName("auth_ldap", 'bindpw')['value'],
        'filter' => $configMapper->selectConfigByName("auth_ldap", 'filter')['value'],
    ];
    $ldapAttributes = [
        $configMapper->selectConfigByName("auth_ldap", 'attribute1')['value'],
        $configMapper->selectConfigByName("auth_ldap", 'attribute2')['value'],
        $configMapper->selectConfigByName("auth_ldap", 'attribute3')['value'],
        $configMapper->selectConfigByName("auth_ldap", 'attribute4')['value'],
        $configMapper->selectConfigByName("auth_ldap", 'attribute5')['value'],
        $configMapper->selectConfigByName("auth_ldap", 'attribute6')['value']
    ];
    $ldapOptions = [
        LDAP_OPT_PROTOCOL_VERSION => 3,
        LDAP_OPT_REFERRALS => true
    ];

    $configLdap = [
        'ldapUri' => $configMapper->selectConfigByName("auth_ldap", 'uri')['value'],
        'ldapPort' => $configMapper->selectConfigByName("auth_ldap", 'port')['value'],
        'ldapEsc' => $configMapper->selectConfigByName("auth_ldap", 'escapeChars')['value'],
        'filterImport' => $configMapper->selectConfigByName("auth_ldap", 'filterImport')['value'],
        'ldapBindOptions' => $ldapBindOptions,
        'ldapAttributes' => $ldapAttributes,
        'ldapOptions' => $ldapOptions
    ];

    unset($dsn, $db);

    // Pour l'authentification
    $container->add('session', new Session());
    $container->add('adapter', function () use ($configLdap, $config) {
        $ldap = new Ldap(
            $configLdap['ldapUri'],
            null,
            $configLdap['ldapBindOptions'],
            $configLdap['ldapOptions'],
            $configLdap['ldapAttributes'],
            $configLdap['ldapPort']
        );

        $ldap->setEscapeChars($configLdap['ldapEsc']);

        return $ldap;
    });
    $container->add('manual', function () use ($connector) {
        $manual = new Manual($connector);
        return $manual;
    });
else:
    $container->add('session', new Session());
    $container->add('adapter', function () use ($connector) {
    $manual = new Manual($connector);
        return $manual;
    });
    $container->add('manual', function () use ($connector) {
        $manual = new Manual($connector);
        return $manual;
    });
endif;
