<?php

/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
return [
    // Connexion à la base de données
    'db' => [
        'type' => 'mysql',
        'base' => 'cslmanager',
        'server' => 'localhost',
        'user' => 'root',
        'password' => '********'
    ],
    // Mail
    // Utilisé par feedback.php
    'mail' => [
        'host' => 'smtp.exemple.com',
        'secure' => 'tls', // ssl possible
        'port' => 587,
        'username' => 'webmailer@exemple.com',
        'password' => '********'
    ],
    // Authentification
    // ldap
    /** @doc https://bitbucket.org/jelofson/vespula.auth */
    'ldap' => [
        'uri' => 'ldap.exemple.com',
        'bindOptions' => [
            // Dossier de départ de la recherche utilisateur
            'basedn' => 'OU=users,DC=exemple,DC=com',
            // Utilisateur spécial qui ira chercher la personne
            'binddn' => 'utilisateur',
            'bindpw' => '*******',
            // Filtre de recherche. Par exemple : uid, mail, cn, ...
            // Le %s est remplacé par le nom d'utilisateur
            // **IMPORTANT** : Si le champ n'est pas une adresse mail,
            //     Il faudra probablement changer le filtre dans login.php #L66
            //     par FILTER_SANITIZE_STRING
            'filter' => "supannaliaslogin=%s"
        ],
        'ldapOptions' => [
            LDAP_OPT_PROTOCOL_VERSION => 3,
            LDAP_OPT_REFERRALS => true
        ],
        'port' => 389,
        // mail est requis
        // Le champs de l'username sera retourné quoi qu'il arrive
        // Par exemple : description, PostOfficeBox, ...
        // Utilisé par l'authentification et par le script d'import utilisateur
        'attributes' => [
            'username' => 'mail', // mail, samaccountname, supannaliaslogin, uuid...
            'firstname' => 'foo', // champs du prénom
            'lastname' => 'bar', // champs du nom
            'fullname' => 'cn', // champs du nom complet (prénom + nom)
            'email' => 'mail', // champs email
            'description' => 'postofficebox', // Status de l'utilisateur (enseignant / etudiant)
            'promo' => 'description' // Description supplémentaire
        ],
        // Filtre d'import d'utilisateur
        'filterImport' => '(mail=*@exemple.com)',
        'escapeChars' => '\\&!|=<>,-+"\';()'
    ],
    // Répertoire du fichier courant
    'base' => __DIR__ . '/',
    // Relatif au front-end
    'view' => [
        'title' => 'CslManager',
        'footer' => '&copy; Mon école',
        'logo' => '/path/to/logo',
        'technique' => 'admin@exemple.com', // Mail du support technique
        'anonymousAccess' => [ // Fichiers n'ayant pas besoin de connexion
            '/index.php', '/about.php', '/login.php'
        ],
        'lang' => 'fr',
    ],
    'hook' => true,
    'moodle' => [
        'token' => '0123456789abcdef0123456789abcdef', // À générer dans moodle
        'domain' => 'http://my.moodle.fr', // URL de moodle
        'course' => 123 // Numéro du cours Vetsims
    ],
    'log' => [
        'logdir' => __DIR__ . '/logs'
    ],
    // Pour afficher les informations de débuggage des WebServices Moodle,
    // Il faut activer le débuggage à  l'adresse Moodle
    // `/admin/settings.php?section=debugging`
    'debug' => true,
];
