function getXMLHttpRequest() {
    var xhr = null;
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
        } else {
            xhr = new XMLHttpRequest();
        }
    } else {
        alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
        return null;
    }
    return xhr;
}

function requestAdminProfile(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }else if (xhr.status === 500) {
            notification.notify("ERROR 500", 'danger');
        } else if (xhr.status === 404) {
            notification.notify("ERROR 404", 'danger');
        }
    };

    var valueProfile = document.getElementById("adminLstProfile").value;
    xhr.open("POST", "/../../back-office/ajax/admin/right.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("valueProfile=" + valueProfile);
}

function requestAddRight(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        } else if (xhr.status === 500) {
            notification.notify("ERROR 500", 'danger');
        } else if (xhr.status === 404) {
            notification.notify("ERROR 404", 'danger');
        }
    };

    var select = document.getElementById("lstRights");
    var i=0;
    var valueAdd=[];
    $("input[type='checkbox']:checked").each(
        function() {
            valueAdd[i]=$(this).attr('id');
            i++;
    });

    var valueProfile=document.getElementById("idProfile").value;
    xhr.open("POST", "/../../back-office/ajax/admin/right.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("valueProfile=" + valueProfile +"&valueAdd=" + valueAdd  );

}

function requestDeleteRight(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }else if (xhr.status === 500) {
            notification.notify("ERROR 500", 'danger');
        } else if (xhr.status === 404) {
            notification.notify("ERROR 404", 'danger');
        }
    };

    var i=0;
    var valueDelete=[];
    $("input[type='checkbox']:checked").each(
        function() {
            valueDelete[i]=$(this).attr('id');
            i++;
        });

    var valueProfile = document.getElementById("adminLstProfile").value;
    xhr.open("POST", "/../../back-office/ajax/admin/right.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("valueProfile=" + valueProfile + "&valueDelete=" + valueDelete);
}

function readDataProfileNot(sData) {
    var cible = document.getElementById("collapseUnlockProfile");
    cible.innerHTML=sData;
    cible.style.visibility="visible";
}

function readProfile(sData) {
    $('#contentToSupp').remove();
    var cible = document.getElementById("contentToAdd");
    cible.innerHTML=sData;
    cible.style.visibility="visible";
}

$('#importLDAP').on('click',function(){
    var button=$(this);
    button.prop('disabled',1);button.button('loading');
    $.post('/back-office/ajax/admin/user.php',{action:'import'},function(data){
        notification.notify(data.message,'info');
        button.text(data.message);
        button.prop('disabled',0)
    },'json')
});


//NOT USED / Update rights
/*function requestUpdateRight(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }else if (xhr.status === 500) {
            notification.notify("ERROR 500", 'danger');
        } else if (xhr.status === 404) {
            notification.notify("ERROR 404", 'danger');
        }
    };

    var i=0;
    var valueToUpdate=[];
    $("#lstRightsToUpdate input[type='text']").each(
        function() {
            valueToUpdate[i]=$(this).val();
            i++;
        });
    i=0;
    var idToUpdate=[];
    $("#lstRightsToUpdate input[type='hidden']").each(
        function() {
            idToUpdate[i]=$(this).val();
            i++;
        });
    var i=0;
    var descriptionToUpdate=[];
    $("#lstDescriptionRight input[type='hidden']").each(
        function() {
            descriptionToUpdate[i]=$(this).val();
            i++;
        });

    var valueProfile = document.getElementById("adminLstProfile").value;
    xhr.open("POST", "../../back-office/ajax/admin/right.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("valueProfile=" + valueProfile + "&valueToUpdate=" + valueToUpdate + "&idToUpdate="+ idToUpdate + "&descriptionToUpdate=" + descriptionToUpdate);
}*/