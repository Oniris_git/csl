function getXMLHttpRequest() {
    var xhr = null;
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
        } else {
            xhr = new XMLHttpRequest();
        }
    } else {
        alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
        return null;
    }
    return xhr;
}

function requestAuth(callback){

    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        } else if (xhr.status === 500) {
            notification.notify("ERROR 500", 'danger');
        } else if (xhr.status === 404) {
            notification.notify("ERROR 404", 'danger');
        }
    };

    var select = document.getElementById("secureSMTP");
    var choice = select.selectedIndex;

    var valueAuth = select.options[choice].value;

    xhr.open("POST", "../../../back-office/ajax/admin/auth.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("valueAuth=" + valueAuth);
}

function readAuth(sData){
    var target = document.getElementById("targetAuth");
    var DSLScript  = document.createElement("script");
    DSLScript.src  = "../../../dist/js/admin.js";
    DSLScript.type = "text/javascript";
    document.body.appendChild(DSLScript);
    target.innerHTML= sData;
}