function requestNameDown(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }
    };

    var order = document.getElementById("name_down");
    var valueOrder = order.value;
    xhr.open("POST", "../back-office/ajax/order.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("order=" + valueOrder);
}

function requestNameUp(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }
    };

    var order = document.getElementById("name_up");
    var valueOrder = order.value;
    xhr.open("POST", "../back-office/ajax/order.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("order=" + valueOrder);
}

function requestProfileDown(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }
    };

    var order = document.getElementById("profile_down");
    var valueOrder = order.value;
    xhr.open("POST", "../back-office/ajax/order.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("order=" + valueOrder);
}

function requestProfileUp(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }
    };

    var order = document.getElementById("profile_up");
    var valueOrder = order.value;
    xhr.open("POST", "../back-office/ajax/order.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("order=" + valueOrder);
}

function readSortFilter(sData) {
    var cible = document.getElementById("cible");
    cible.innerHTML=sData;
    cible.style.visibility="visible";
}

function readSort(sData) {
    $('#lstUserToSupp').remove();
    var cible = document.getElementById("cible");
    cible.innerHTML=sData;
    cible.style.visibility="visible";
}

function requestAccount(callback){
    var xhr = new getXMLHttpRequest();

    xhr.onreadystatechange = function(){
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)){
            callback(xhr.responseText);
        }
    };

    var select = document.getElementById("selectAccount");
    var choice = select.selectedIndex;

    var valueAccount = select.options[choice].value;
    xhr.open("POST", "../../../back-office/ajax/admin/user.php", true);
    xhr.setRequestHeader("Content-type", 'application/x-www-form-urlencoded');
    xhr.send("showHTML=" + valueAccount);
}

function readAccount(sData){
    $('#defaultForm').remove();
    var targetAccount = document.getElementById("newForm");
    targetAccount.style.visibility="visible";
    targetAccount.innerHTML = sData;
    var DSLScript  = document.createElement("script");
    DSLScript.src  = "../../../dist/js/admin.js";
    DSLScript.type = "text/javascript";
    document.body.appendChild(DSLScript);
}