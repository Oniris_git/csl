function getXMLHttpRequest() {
    var xhr = null;
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
        } else {
            xhr = new XMLHttpRequest();
        }
    } else {
        alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
        return null;
    }
    return xhr;
}

//request to catch user choice
function requestFilter(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
            document.getElementById("loader").style.display = "none";
        } else if (xhr.status === 500) {
            notification.notify("ERROR 500", 'danger');
        } else if (xhr.status === 404) {
            notification.notify("ERROR 404", 'danger');
        } else if (xhr.readyState < 4) {
            document.getElementById("loader").style.display = "inline";
        }
    };

    var select = document.getElementById("select");
    var choice = select.selectedIndex;

    var valueFilter = select.options[choice].value;

    xhr.open("POST", "../back-office/ajax/filter.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("filter=" + valueFilter);
}

//request to catch user input
function requestSearchFilter(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status == 0)) {
            callback(xhr.responseText);
            document.getElementById("loader").style.display = "none";
        } else if (xhr.status === 500) {
            notification.notify("ERROR 500", 'danger');
        } else if (xhr.status === 404) {
            notification.notify("ERROR 404", 'danger');
        } else if (xhr.readyState < 4) {
            document.getElementById("loader").style.display = "inline";
        }
    };

    var search = document.getElementById("search");
    var valueSearch = search.value;
    if (valueSearch == ""){
        notification.notify("Search Empty", 'danger');
    } else {
        xhr.open("POST", "../back-office/ajax/search.php", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send("search=" + valueSearch);
    }
}

//request to catch user input
function requestSearch(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
            //document.getElementById("loader").style.display = "none";
        } else if (xhr.status === 500) {
            notification.notify("ERROR 500", 'danger');
        } else if (xhr.status === 404) {
            notification.notify("ERROR 404", 'danger');
        } else if (xhr.readyState < 4) {
            document.getElementById("loader").style.display = "inline";
        }
    };

    var search = document.getElementById("search");
    var valueSearch = search.value;
    if (valueSearch == ""){
        notification.notify("Search Empty", 'danger');
    } else{
        xhr.open("POST", "../back-office/ajax/search.php", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send("search2=" + valueSearch);
    }
}

//request to catch user choice to display scales
function requestScale(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        } else if (xhr.status === 500) {
            notification.notify("ERROR 500", 'danger');
        } else if (xhr.status === 404) {
            notification.notify("ERROR 404", 'danger');
        }
    };
    var select = document.getElementById("scale");
    var valueScale = select.value;
    xhr.open("POST", "/back-office/ajax/scale_type.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("scale=" + valueScale);
}

//request to display scales
function requestShowScale(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }
    };
    var showGrade = document.getElementById("showGrade");
    var valueShow = showGrade.value;
    xhr.open("POST", "../back-office/ajax/scale_type.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("showGrade=" + valueShow);
}

//display into target
function readData(sData) {
    var cible = document.getElementById("cible");
    cible.innerHTML = sData;
    cible.style.visibility = "visible";
}

//display into target
function readDataFilterUser(sData) {
    var cible = document.getElementById("cible");
    cible.innerHTML = sData;
    cible.style.visibility = "visible";
}

//display into target
function readUserSearch(sData) {
    $('#lstUserToSupp').remove();
    $('.pagination').remove();
    var cible = document.getElementById("cible");
    cible.visibility = 'visible';
    cible.innerHTML = sData;
    cible.style.visibility = "visible";
}

