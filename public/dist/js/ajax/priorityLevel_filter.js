/**
 *
 * @returns {xhr object}
 * use for ajax
 */
function getXMLHttpRequest() {
    var xhr = null;
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
        } else {
            xhr = new XMLHttpRequest();
        }
    } else {
        alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
        return null;
    }
    return xhr;
}

/**
 *
 * @param callback
 * get the user choice to display workshop attempt
 */
function RequestPriorityLevel(callback, idUser){
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }else if(xhr.status === 500){
            notification.notify("ERROR 500", 'danger');
        }else if(xhr.status === 404){
            notification.notify("ERROR 404", 'danger');
        }
    }

    var choice = document.getElementById("priorityLevelSelected").value;
    xhr.open("POST", "../../back-office/ajax/priorityLevel_filter.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("choice=" + choice + "&idUser=" + idUser);
}

/**
 *
 * @param sData
 * callback function to send html block to view
 * display workshop attempt
 */
function readDataWA(sData) {
    $('#waAjax').remove();
    var targetNewWa = document.getElementById("targetNewWa");
    targetNewWa.innerHTML = sData;
    targetNewWa.style.visibility = "visible";
}
