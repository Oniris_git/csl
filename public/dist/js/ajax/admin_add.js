/**
 *
 * @returns {xhr object}
 * use for ajax
 */
function getXMLHttpRequest() {
    var xhr = null;
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
        } else {
            xhr = new XMLHttpRequest();
        }
    } else {
        alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
        return null;
    }
    return xhr;
}

/**
 *
 * @param callback
 * request to add category
 */
function requestAddCategory(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }else if(xhr.status === 500){
            notification.notify("ERROR 500", 'danger');
        }else if(xhr.status === 404){
            notification.notify("ERROR 404", 'danger');
        }
    };

    var title = document.getElementById("title").value;
    var description = document.getElementById("description").value;
    var action = "add-category";
    xhr.open("POST", "../../../back-office/ajax/admin/category.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("title=" + title + "&description=" + description + "&action=" + action);
}

/**
 *
 * @param callback
 * request to add profile
 */
function requestAddProfile(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }else if(xhr.status === 500){
            notification.notify("ERROR 500", 'danger');
        }else if(xhr.status === 404){
            notification.notify("ERROR 404", 'danger');
        }
    };

    var label = document.getElementById("profile-label").value;
    var description = document.getElementById("profile-description").value;
    var action = "add-profile";
    xhr.open("POST", "../../../back-office/ajax/admin/profile.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("label=" + label + "&description=" + description + "&action=" + action);
}

/**
 *
 * @param callback
 * request to add sub category
 */
function requestAddSubCategory(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }else if(xhr.status === 500){
            notification.notify("ERROR 500", 'danger');
        }else if(xhr.status === 404){
            notification.notify("ERROR 404", 'danger');
        }
    };

    var title = document.getElementById("title").value;
    var description = document.getElementById("description").value;
    var action = "add-subcategory";
    xhr.open("POST", "../../../back-office/ajax/admin/sub_category.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("title=" + title + "&description=" + description + "&action=" + action);
}

/**
 *
 * @param callback
 * request to add discipline
 */
function requestAddDiscipline(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }else if(xhr.status === 500){
            notification.notify("ERROR 500", 'danger');
        }else if(xhr.status === 404){
            notification.notify("ERROR 404", 'danger');
        }
    };

    var title = document.getElementById("title").value;
    var description = document.getElementById("description").value;
    var action = "add-discipline";
    xhr.open("POST", "../../../back-office/ajax/admin/discipline.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("title=" + title + "&description=" + description + "&action=" + action);
}

/**
 *
 * @param callback
 * request to add priority level
 */
function requestAddPriorityLevel(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }else if(xhr.status === 500){
            notification.notify("ERROR 500", 'danger');
        }else if(xhr.status === 404){
            notification.notify("ERROR 404", 'danger');
        }
    };

    var title = document.getElementById("title").value;
    var description = document.getElementById("description").value;
    var action = "add-priority_level";
    xhr.open("POST", "../../../back-office/ajax/admin/priority_level.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("title=" + title + "&description=" + description + "&action=" + action);
}

/**
 *
 * @param callback
 * request to add specie
 */
function requestAddSpecie(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }else if(xhr.status === 500){
            notification.notify("ERROR 500", 'danger');
        }else if(xhr.status === 404){
            notification.notify("ERROR 404", 'danger');
        }
    };

    var title = document.getElementById("title").value;
    var description = document.getElementById("description").value;
    var action = "add-species";
    xhr.open("POST", "../../../back-office/ajax/admin/species.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("title=" + title + "&description=" + description + "&action=" + action);
}

/**
 *
 * @param callback
 * request to add scale type
 */
function requestAddScaleType(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }else if(xhr.status === 500){
            notification.notify("ERROR 500", 'danger');
        }else if(xhr.status === 404){
            notification.notify("ERROR 404", 'danger');
        }
    };

    var scale_type_label = document.getElementById("scale_type-label").value;
    var action = "create-scale_type";
    xhr.open("POST", "../../../back-office/ajax/admin/scale_type.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("scale_type-label=" + scale_type_label + "&action=" + action);
}

/**
 *
 * @param callback
 * request to add room
 */
function requestAddRoom(callback) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }else if(xhr.status === 500){
            notification.notify("ERROR 500", 'danger');
        }else if(xhr.status === 404){
            notification.notify("ERROR 404", 'danger');
        }
    };

    var roomLabel = document.getElementById("room-label-input").value;
    var action = "add-room";
    xhr.open("POST", "../../../back-office/ajax/admin/room.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("room-label=" + roomLabel + "&action=" + action);
}

/**
 *
 * @param sData
 * callback function to send html block to view
 */
function readDataAdd(sData) {
    $('#addToTab').remove();
    var DSLScript  = document.createElement("script");
    DSLScript.src  = "../../../dist/js/admin.js";
    DSLScript.type = "text/javascript";
    document.body.appendChild(DSLScript);
    var cibleNew = document.getElementById("cibleNew");
    cibleNew.innerHTML = sData;
    cibleNew.style.visibility = "visible";
}