<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Vespula\Auth\Auth;
use CSLManager\Administration\Helper\Config;
use CSLManager\Hook\Hook;
use CSLManager\Administration\Mapper\WorkshopMapper;
use CSLManager\Administration\Mapper\WorkshopAttemptMapper;
use CSLManager\Administration\Mapper\UserMapper;

// Autoloader
require __DIR__ . '/../vendor/autoload.php';

// Enregistrement du tableau de configuration
$config = require __DIR__ . '/../config.php';
$config = new Config($config);

// Chargement des dépendences dans le container
require __DIR__ . '/../lib/bootstrap.php';


//$logger = $container->get('logger');
$session = $container->get('session');
$con = $container->get('db');

$args = [
    'action' => FILTER_SANITIZE_STRING,
    'scale' => FILTER_SANITIZE_STRING,
    'comment' => FILTER_SANITIZE_STRING,
    'workshop' => FILTER_SANITIZE_STRING,
    'cancel' => FILTER_SANITIZE_STRING,
    'token' => FILTER_DEFAULT
];

$ERROR = [];
$WARN = [];
$SUCCESS = [];

$POST = filter_input_array(INPUT_POST, $args, false);

if (isset($POST['cancel'])){
    try{
        $waMapper = new WorkshopAttemptMapper($con);
        $waMapper->cancelWorkshopAttemptById($POST['workshopAttempt']);
        $WARN = [
            "message"=>TXT_CANCELED_WORKSHOP_ATTEMPT
        ];
    }catch(Exception $e){
        $WARN = [
            "message"=>TXT_ERROR_CANCELED_WORKSHOP_ATTEMPT
        ];
    }

}else {
    try {
        $session = $container->get('session');
        $adapter = $container->get('adapter');

        $auth = new Auth($adapter, $session);
    } catch (Exception $e) {
        exit($e->getMessage());
    }

    if (!$auth->isValid()) {
        header('Location: login.php');
        exit();
    }


    $hook = new Hook(['after_validation' => true]);
    $hook->setHookAvailable($config->get('hook'));

    $wsMapper = new WorkshopMapper($con);
    $waMapper = new WorkshopAttemptMapper($con);
    $uMapper = new UserMapper($con);

    try {
        $session->verifyToken('form', $POST['token']);
        $user = $uMapper->getUserByUpn($session->getUsername());
    } catch (\Exception $e) {
        /*$logger->critical(TXT_LOG_BAD_TOKEN, ['message' => $e->getMessage()]);*/
        echo $e->getMessage();
        exit();
    }

    $requestedWorkshop = $session->getValue('workshop');
    $wa_id = $session->getValue('id-workshop_attempt');

    //$logger->info(TXT_LOG_START_VALIDATION);
    /*$logger->info(TXT_LOG_REQUEST_WORKSHOP, [
        'user' => $user->getFirstName() . " " . $user->getName(),
        'workshop' => $requestedWorkshop]);*/

    try {
        $workshop = $wsMapper->getWorkshopByLabel($requestedWorkshop);
        $wsMapper->setScaleType($workshop);
        $wa = $waMapper->getWorkshopAttemptByIdUser($user, $workshop);
        $waMapper->checkTime($workshop, $wa);
        $wsMapper->checkScale($workshop, $POST['scale']);

    } catch (\Exception $e) {
        /*$logger->error(TXT_LOG_ERROR, ['line' => __LINE__, 'message' => $e->getMessage()]);*/
        $ERROR[] = $e->getMessage();
    }

    if (empty($ERROR) && $waMapper->finish($wa->getId(), $POST['scale'])) {
        /*$logger->info(TXT_LOG_VALIDATION, [
            'workshop' => $wa->getIdWorkshop(),
            'user' => $user->getFirstName() . " " . $user->getName()]);*/
        try {
            if (!empty($POST['comment'])) {
                /*$logger->info(TXT_LOG_UPDATE_COMMENT, [
                    'comment' => $POST['comment']]);*/
                $waMapper->updateComment($user->getId(), $wa->getIdWorkshop(), $POST['comment']);
            }
        } catch (\Exception $e) {
            /*$logger->warning(TXT_LOG_ERROR_UPDATE_COMMENT,
                ['message' => $e->getMessage()]);*/
            $WARN[] = TXT_LOG_ERROR_UPDATE_COMMENT . $e->getMessage();
        }

        $session->unsetValue('workshop');
        $session->unsetValue('id-workshop_attempt');
        $session->unsetValue('form_token');

        $wa = $waMapper->getWorkshopAttemptById($wa->getId());

        $SUCCESS[] = TXT_LOG_WORKSHOP_COMPLETE . $wa->getFullLabel() . ' »<br/>' .
            TXT_LOG_WORKSHOP_COMPLETE_DURATION . date_diff(new \DateTime($wa->getDebut()),
                new \DateTime($wa->getFin()))->format('%H heure et %I minute(s), %s secondes') . '<br/>' .
            TXT_LOG_WORKSHOP_COMPLETE_ANSWER . $wa->getScaleLabel();
        if ($hook->isHookAvailable() && $configDB['domainMoodle'] != "") {
            //$logger->info(TXT_LOG_WORKSHOP_EXECUTION);
            try {
                $hookdata = [
                    'email' => $user->getEmail(),
                    'devoir' => $workshop->getIdMoodle(),
                    'grade' => $wa->getOrderScale(),
                    'comment' => $waMapper->getCommentsByWorkShopAttempt($wa, $user)
                ];
                $SUCCESS[] = $hook->emit_signal('after_validation', $hookdata);
                //$logger->info(TXT_LOG_WORKSHOP_EXECUTED);
                //$logger->dump($hookdata);
            } catch (\Exception $e) {
                //$logger->warning(TXT_LOG_WORKSHOP_ERROR_COMMUNICATION, ['message' => $e->getMessage()]);
                $WARN[] = TXT_LOG_WORKSHOP_ERROR_COMMUNICATION . PHP_EOL . $e->getMessage() . $wa->getIdScaleType() . " " . $workshop->getIdMoodle();
            }

        }
    } else {
        //$logger->error(TXT_LOG_WORKSHOP_OCCURRED);
        $ERROR[] = TXT_LOG_WORKSHOP_OCCURRED;
    }
}
    //$logger->info(TXT_LOG_WORKSHOP_END);
    $session->setValue('V_SUCCESS', $SUCCESS);
    $session->setValue('V_WARN', $WARN);
    $session->setValue('V_ERROR', $ERROR);

    header('Location: index.php');
