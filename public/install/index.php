<?php

if (file_exists('./../../config.php')) {
    header('Location: ./../../index.php');
}

if(isset($_GET['lang'])){
    switch ($_GET['lang']){
        case "fr":
            $smtpLink = "/fr/configuration-smtp/";
            $moodleLink = "/3x/fr/FAQ_App_Moodle_mobile";
            $ldapLink = "/pub/printers/mfps/ews_help/help/fr/help_LdapAuth2.html";
            include "../../lang/lang-fr.inc.php";
            break;
        case "en":
            $smtpLink = "/smtp-settings/";
            $moodleLink = "/36/en/Using_web_services";
            $ldapLink = "/pub/printers/mfps/ews_help/help/en/help_LdapAuth2.html";
            include "../../lang/lang-en.inc.php";
            break;
        case "de":
            $moodleLink = "/36/en/Using_web_services";
            $smtpLink = "/de/mailserver-einrichten-smtp/";
            $ldapLink = "/pub/printers/mfps/ews_help/help/de/help_LdapAuth2.html";
            include "../../lang/lang-de.inc.php";
            break;
        default:
            $smtpLink = "/smtp-settings/";
            $moodleLink = "/36/en/Using_web_services";
            $ldapLink = "/pub/printers/mfps/ews_help/help/en/help_LdapAuth2.html";
            include "../../lang/lang-en.inc.php";
            break;
    }
}else{
    $smtpLink = "/smtp-settings/";
    $moodleLink = "/36/en/Using_web_services";
    $ldapLink = "/pub/printers/mfps/ews_help/help/en/help_LdapAuth2.html";
    include "../../lang/lang-en.inc.php";
}

require "install.phtml";

