<?php

function validateSMTP($dataSMTP, $mail){
    try{
        $mail->isSMTP();
        $mail->MailerDebug = 2;
        $mail->Host = $dataSMTP['host'];
        $mail->Port = $dataSMTP['port'];
        $mail->SMTPSecure = $dataSMTP['secure'];
        $mail->SMTPAuth = true;
        $mail->Username = $dataSMTP['user_name'];
        $mail->Password = $dataSMTP['password'];
        $mail->setFrom($dataSMTP['user_name'], 'Mailer Test');
        $mail->addAddress($dataSMTP['user_name'], 'Administrateur Technique');
        $mail->Subject = 'Test SMTP';
        $mail->Body = 'Check SMTP Valid';
        $mail->CharSet = 'utf8';
        $mail->Send();
        return true;
    } catch (Exception $e) {
        return $e->errorMessage();
    }
}

function generateToken($form){
    $token = sha1(uniqid(microtime(), true));
    return $token;
}

function createBDD($dataBDD){
    try{
        $bdd = new PDO("mysql:host=".$dataBDD['host'].";DBName=".$dataBDD['db'], $dataBDD['login'], $dataBDD['pass']);

        //files sql to create BDD
        $fileSql = '../../script/migration/install.sql';

        function executeQueryFile($fileSql, $bdd) {
            $query = file_get_contents($fileSql);
            $bdd->query($query);
        }
        executeQueryFile($fileSql, $bdd);

        return "true";
//if values are not valid throw exception to display
    }catch(Exception $e){
        return "Error :  ".$e->getMessage();
    }
}

function validateMoodle($dataMoodle){
    $post_data = ['courseids' => [$dataMoodle['course']]];
    $url = $dataMoodle['domain'].'/webservice/rest/server.php?wstoken='.$dataMoodle['token']. '&wsfunction=mod_assign_get_assignments&moodlewsrestformat=json';
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post_data));
    $post = json_decode(curl_exec($curl));
    curl_close($curl);
    if(isset($post->exception)):
        return $post->message;
    else:
        return "true";
    endif;
    curl_close($curl);

}

function checkImage($file){
    $target_dir = "../dist/images/";
    $target_file = $target_dir . basename($file["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    $check = getimagesize($file["tmp_name"]);
    if($check !== false) {
        $uploadOk = 1;
    } else if ($check === false) {
        return TXT_IMAGE_INVALID;
    }else if ($file["size"] > 5000000) {
        return TXT_IMAGE_SIZE;
    }else if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {
        return TXT_IMAGE_FORMAT;
    }
    // error
    if ($uploadOk == 0) {
        return TXT_IMAGE_ERROR;
    } else {
        if (move_uploaded_file($file["tmp_name"], $target_file)) {
            $logo= $file['name'];
            return "true";
        }else{
            return TXT_IMAGE_ERROR;
        }
    }
}

function checkLdap($dataLDAP){
    $ldaprdn  = $dataLDAP['binddn'];     // ldap rdn or dn
    $ldappass = $dataLDAP['bindpw'];  // associated password

    // connect to ldap server
    $ldapconn = ldap_connect($dataLDAP['uri']);

    // Set some ldap options for talking to
    ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);

    if ($ldapconn) {
        // binding to ldap server
        $ldapbind = @ldap_bind($ldapconn, $ldaprdn, $ldappass);
        if($ldapbind == true){
            return "true";
        }else{
            return "Error : Check LDAP values";
        }
    }else{
        return "Error : Check LDAP values";
    }
}

function createUser($userMapper, $credentials, $email){
    $exist = stripos($credentials['username'], ".");
    if($exist != false){
        $firstName_Name=explode(".",$credentials['username']);
        $user=[
            'upn'=> $credentials['username'],
            'firstName'=>$firstName_Name[0],
            'name'=>$firstName_Name[1],
            'id_profile'=> 1,
            'email'=>$email,
            "auth" => "manual",
            "password" => password_hash($credentials['password'], PASSWORD_DEFAULT),
            "request" => null,
            "open" => 0,
            "token" => generateToken("form")
        ];
        try{
            $result = $userMapper->create($user);
            return true;
        }catch (Exception $e){
            return "Error : ".TXT_LOG_LOGIN_ERROR_CREATE.$e->getMessage();
        }


    }else{
        return "Error : lidentifiant doit être dans le format prenom.nom";
    }

}

function createConfigFile($dataBDD){
    $db = $dataBDD['db'];
    $host = $dataBDD['host'];
    $login = $dataBDD['login'];
    $pass = $dataBDD['pass'];
    $file = fopen("../../config.php", "w");
    fwrite($file, "<?php\r\n
        return [\r\n
        \t\"db\" =>[\r\n
        \t\t\"type\" =>     \"mysql\",\r\n
        \t\t\"base\" =>     \"$db\",\r\n
        \t\t\"server\" =>   \"$host\",\r\n
        \t\t\"user\" =>     \"$login\",\r\n
        \t\t\"password\" => \"$pass\",\r\n
        \t],\r\n
        \t\"view\" => [\r\n
        \t\t\"anonymousAccess\" => [\r\n
        \t\t\t\"/index.php\", \"/about.php\", \"/login.php\", \"/legal.php\", \"/back-office/feedback.php\",\"/request.php\"\r\n
        \t\t],\r\n
        \t],\r\n
        \r\n
        \t\"hook\" => true,\r\n
        \t\"debug\" => true, \r\n
        \t\"base\" => __DIR__.\"/\",\r\n
        \t\"log\" => [ \r\n
        \t\"logdir\" => __DIR__.\"/logs\" \r\n
        \t],\r\n
        \t\"version\" => \"22.04\" \r\n
        \r\n
        ];\r\n
        ?>"
    );
    //Close configuration:
    fclose($file);
}
