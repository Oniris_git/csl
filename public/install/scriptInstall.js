function checkLdap(){
    var check = document.getElementById("valueCheck").value;
    var input = document.getElementsByClassName("disableLdap");
    if(check == "on"){
        document.getElementById("valueCheck").value = "off";
        for($i = 0; $i < 5; $i++){
            input[$i].disabled=true;
        }
    }else if(check == "off"){
        document.getElementById("valueCheck").value = "on";
        for($i = 0; $i < 5; $i++){
            input[$i].disabled=false;
        }
    }
}

function checkMoodle(){
    var check = document.getElementById("valueCheckMoodle").value;
    var input = document.getElementsByClassName("disableMoodle");
    if(check == "on"){
        document.getElementById("valueCheckMoodle").value = "off";
        for($i = 0; $i < 3; $i++){
            input[$i].disabled=true;
        }
    }else if(check == "off"){
        document.getElementById("valueCheckMoodle").value = "on";
        for($i = 0; $i < 3; $i++){
            input[$i].disabled=false;
        }
    }
}