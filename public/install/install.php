<?php

/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 04/12/2018
 * Time: 14:23
 */

use PHPMailer\PHPMailer\PHPMailer;
include "../../lang/lang-en.inc.php";
include "install_functions.php";

use CSLManager\Administration\Mapper\PermissionMapper;
use CSLManager\Administration\Mapper\UserMapper;
use CSLManager\Administration\Mapper\ConfigMapper;
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../vendor/phpmailer/phpmailer/src/SMTP.php';
require __DIR__ . '/../../vendor/phpmailer/phpmailer/src/PHPMailer.php';
require __DIR__ . '/../../vendor/phpmailer/phpmailer/src/Exception.php';
require __DIR__ . '/../../vendor/phpmailer/phpmailer/src/OAuth.php';

$bool = false;
$credentials = [];
$smtpLink = "/fr/configuration-smtp/";
$moodleLink = "/3x/fr/FAQ_App_Moodle_mobile";
$ldapLink = "/pub/printers/mfps/ews_help/help/fr/help_LdapAuth2.html";

$args = [
    'host' =>FILTER_SANITIZE_STRING,
    'db' =>FILTER_SANITIZE_STRING,
    'login' =>FILTER_SANITIZE_STRING,
    'pass' =>FILTER_SANITIZE_STRING,
    'portSMTP' =>FILTER_VALIDATE_INT,
    'hostSMTP' =>FILTER_SANITIZE_STRING,
    'secureSMTP' =>FILTER_SANITIZE_STRING,
    'emailSMTP' =>FILTER_SANITIZE_STRING,
    'passwordSMTP' =>FILTER_SANITIZE_STRING,
    'tokenMoodle' =>FILTER_SANITIZE_STRING,
    'domainMoodle' =>FILTER_SANITIZE_STRING,
    'courseMoodle' =>FILTER_VALIDATE_INT,
    'appTitle' =>FILTER_SANITIZE_STRING,
    'emailSupport' =>FILTER_SANITIZE_STRING,
    'domain' =>FILTER_SANITIZE_STRING,
    'valueCheck' =>FILTER_SANITIZE_STRING,
    'uriLDAP' =>FILTER_SANITIZE_STRING,
    'baseDnLDAP' =>FILTER_SANITIZE_STRING,
    'bindDnLDAP' =>FILTER_SANITIZE_STRING,
    'bindPwLDAP' =>FILTER_SANITIZE_STRING,
    'fieldLDAP' =>FILTER_SANITIZE_STRING,
    'portLDAP' =>FILTER_VALIDATE_INT,
    'adminName' =>FILTER_SANITIZE_STRING,
    'adminPwConfirm' =>FILTER_SANITIZE_STRING,
    'adminPw' =>FILTER_SANITIZE_STRING,
    'valueCheckMoodle' =>FILTER_SANITIZE_STRING,
];


$POST = filter_input_array(INPUT_POST, $args, false);

//connexion BDD / inject data for tables
$dataBDD = [
    'host' =>$POST['host'],
    'db' =>"cslmanager",
    'login' =>$POST['login'],
    'pass' =>$POST['pass'],
];

try {
    $bdd = new PDO("mysql:host=".$dataBDD['host'], $dataBDD['login'], $dataBDD['pass']);
} catch (Exception $e) {
    $ERROR['bdd'] = $e->getMessage();
}
// $resultBDD = createBDD($dataBDD);
// if($resultBDD != "true"){
//     $ERROR = [
//         "message" => $resultBDD
//     ];
//     require "install.phtml";
//     exit;
// }
//create bdd instance
// $bdd = new PDO("mysql:host=".$dataBDD['host'].";dbname=".$dataBDD['db'],$dataBDD['login'],$dataBDD['pass']);
//put into variable values if exist / else null
//variables for SMTP
$dataSMTP = [
    'host' =>$POST['hostSMTP'],
    'secure' =>$POST['secureSMTP'],
    'port' =>$POST['portSMTP'],
    'user_name' =>$POST['emailSMTP'],
    'password' =>$POST["passwordSMTP"]
];

//Create a new PHPMailer instance / check data = true
$mail = new PHPMailer(true);
$resultSMTP = validateSMTP($dataSMTP, $mail);
if($resultSMTP !== true){
    $ERROR ["smtp"] = $resultSMTP;
    // require "install.phtml";
    // exit;
}

//variables for connexion with Moodle
$dataMoodle = [
    'token' => isset($POST['tokenMoodle'])? $POST['tokenMoodle'] : "",
    'domain' => isset($POST["domainMoodle"])? $POST['domainMoodle'] : "",
    'course' => isset($POST["courseMoodle"])? $POST['courseMoodle'] : ""
];
if(isset($POST['valueCheckMoodle']) && $POST['valueCheckMoodle'] == "on"):
    $resultMoodle = validateMoodle($dataMoodle);
    if($resultMoodle != "true"){
        $ERROR["moodle"] = $resultMoodle;
        // require "install.phtml";
        // exit;
    }
endif;

$appTitle = $POST["appTitle"];
$emailSupport = $POST['emailSupport'];
$domain = $POST['domain'];

//check image format
$resultImage = checkImage($_FILES["fileSelect"]);
if($resultImage != true){
    $ERROR["image"] = $resultImage;
    // require "install.phtml";
    // exit;
}
$target_dir = "../dist/images/";
$target_file = $target_dir . basename($_FILES["fileSelect"]["name"]);
$dataView = array(
    'title' => $POST["appTitle"],
    'footer' => "&copy; Env Alfort / Oniris Nantes",
    'technique' => $POST["emailSupport"],
    'logo' => $target_file,
    'legal' => "",
    'lang' => "en",
    'domain' => $_POST['domain']
);

//check if user want to use ldap or not
if(isset($POST['valueCheck']) && $POST['valueCheck'] == "on"){
    //variables for LDAP
    $dataLDAP = [
        'uri' =>$POST["uriLDAP"],
        'basedn' =>$POST["baseDnLDAP"],
        'binddn' =>$_POST["bindDnLDAP"],
        'bindpw' =>$_POST["bindPwLDAP"],
        'filter' =>$POST['fieldLDAP']."=%s",
        'port' =>$POST["portLDAP"],
        'attribute1' => "cn",
        'attribute2' => "sn",
        'attribute3' => "givenName",
        'attribute4' => "mail",
        'attribute5' => "description",
        'attribute6' => $POST['fieldLDAP'],
        'filterImport' => "(mail=%s)",
        'escapeChars' => '\\&!|=<>,+"\';()',
    ];

    $ldapAttributes = [
        $dataLDAP['attribute1'],
        $dataLDAP['attribute2'],
        $dataLDAP['attribute3'],
        $dataLDAP['attribute4'],
        $dataLDAP['attribute5'],
        $dataLDAP['attribute6']
    ];

    try{
        $resultLdap = checkLdap($dataLDAP);
    }catch (Exception $e){
        $resultLdap = $e->getMessage();
        // require "install.phtml";
        // exit;
    }

    if ($resultLdap != "true") {
        $ERROR["ldap"] = $resultLdap;
    }

    // try{
    //     //insert type auth into config table
    //     $bool = $configMapper->insert('auth', "type", "ldap");
    // }catch (Exception $e){
    //     $ERROR = [
    //         "message" => new Exception(TXT_NOTIFICATION_CHECK_VALUES." LDAP = ".$e->getMessage())
    //     ];
    //     require "install.phtml";
    //     exit;
    // }

}

//start to insert into BDD
// try{
    //check Ldap data / insert user / insert Ldap data
    $credentials = [
        'username' =>$POST["adminName"],
        'password' => $POST["adminPw"]
    ];
    //insert admin user
    $checkPassword = strcmp($POST['adminPwConfirm'], $POST['adminPw']);
    if (strlen($POST['adminPw']) <= 4):
        $ERROR["user"] = TXT_INSTALLATION_ERROR_PASSWORD_SIZE;
        // require "install.phtml";
        // exit;
    elseif($checkPassword === 0):
        // $resultUser = createUser($userMapper, $credentials);
        // if($resultUser != "true"){
        //     $ERROR = [
        //         "message" => $resultUser
        //     ] ;
        //     require "install.phtml";
        //     exit;
        // };
    else:
        $ERROR ["user"]= TXT_INSTALLATION_ERROR_PASSWORD_CHECK;
        // require "install.phtml";
        // exit;
    endif;
// }catch(Exception $e){
//     $ERROR = [
//         "message" => new Exception(TXT_LOG_LOGIN_ERROR_CREATE.$e->getMessage())
//     ];
//     require "install.phtml";
//     exit;
// }


if (!isset($ERROR) || count($ERROR) == 0) {
    try{
        $resultBDD = createBDD($dataBDD);
        //instantiation mappers
        $bdd = new PDO("mysql:host=".$dataBDD['host'].";dbname=".$dataBDD['db'],$dataBDD['login'],$dataBDD['pass']);
        $configMapper = new ConfigMapper($bdd);
        $userMapper = new UserMapper($bdd);
        $resultUser = createUser($userMapper, $credentials, $POST['emailSupport']);
        if ($resultUser !== true) {
            $ERROR['user'] = $resultUser;
            require "install.phtml";
            exit;
        }
        if(isset($POST['valueCheck']) && $POST['valueCheck'] == "on"){
            $auth = "ldap";
            foreach ($dataLDAP as $key => $value) {
                $bool = $configMapper->insert('auth_ldap', $key, $value);
            }
        } 
        else{
            $auth = "manual";
        }
        //insert type auth into config table for manual
        $bool = $configMapper->insert('auth', "type", $auth);
        //insert SMTP data into config table
        foreach ($dataSMTP as $key => $value) {
            $bool = $configMapper->insert('smtp', $key, $value);
        }
        //insert into BDD Moodle data
        foreach ($dataMoodle as $key => $value){
            $bool = $configMapper->insert('moodle', $key, $value);
        }
        foreach ($dataView as $key => $value){
            $bool = $configMapper->insert('view', $key, $value);
        }                
        //creation of file "config.php"
        createConfigFile($dataBDD);

        require "success.phtml";
    }catch (Exception $e){
        $ERROR = [
            "message" => $e->getMessage()
        ];
        require "install.phtml";
        exit;
    }
} else {
    require "install.phtml";
    exit;
}



// try{
//     //insert SMTP data into config table
//     foreach ($dataSMTP as $key => $value) {
//         $bool = $configMapper->insert('smtp', $key, $value);
//     }
// }catch (Exception $e){
//     $ERROR = [
//         "message" => new Exception("Error SMTP = ".$e->getMessage())
//     ];
//     require "install.phtml";
//     exit;
// }
// try{
//     //insert into BDD Moodle data
//     foreach ($dataMoodle as $key => $value){
//         $bool = $configMapper->insert('moodle', $key, $value);
//     }
// }catch (Exception $e){
//     $ERROR = [
//         "message" => new Exception("Error Moodle = ".$e->getMessage())
//     ];
//     require "install.phtml";
//     exit;
// }
// try{
//     //insert into BDD View data
//     foreach ($dataView as $key => $value){
//         $bool = $configMapper->insert('view', $key, $value);
//     }
// }catch (Exception $e){
//     $ERROR = [
//         "message" => new Exception("Error values = ".$e->getMessage())
//     ];
//     require "install.phtml";
//     exit;
// }

// if(isset($POST['valueCheck']) && $POST['valueCheck'] == "on") {
//     try {
//         //insert into BDD config data
//         foreach ($dataLDAP as $key => $value) {
//             $bool = $configMapper->insert('auth_ldap', $key, $value);
//         }
//     } catch (Exception $e) {
//         $ERROR = [
//             "message" => new Exception("Error LDAP = ".$e->getMessage())
//         ];
//         require "install.phtml";
//         exit;
//     }
// }

//Delete install files
/*if(isset($_POST["sup"])){
    unlink("install.php");
    unlink("index.php");
    unlink("install.phtml");
    unlink("success.phtml");
}*/
