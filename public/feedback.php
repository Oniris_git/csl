<?php

use CSLManager\Administration\Mappers\LanguageMapper;

require '../lib/bootstrap.php';

$langMapper = new LanguageMapper($connector);
$languages = $langMapper->selectLanguages();

//$contentPage = 'feedback.phtml';
include '../view/front/feedback.phtml';
