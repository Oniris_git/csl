<?php

use CSLManager\Administration\Mapper\ConfigMapper;
use CSLManager\Administration\Mapper\LanguageMapper;

require __DIR__ . '/../lib/bootstrap.php';


$langMapper = new LanguageMapper($connector);
$languages = $langMapper->selectLanguages();

$configMapper = new ConfigMapper($connector);

$legalContent = $configMapper->selectConfigByName("view", "legal");


include __DIR__ . '/../view/front/legal.phtml';
