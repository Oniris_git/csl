
//on display le nom du fichier dans le label de l'input file
/*$('input[type="file"]').change(function(e){
    var fileName = e.target.files[0].name;
    $('.custom-file-label').html(fileName);
});*/

function getXMLHttpRequest() {
    var xhr = null;
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
        } else {
            xhr = new XMLHttpRequest();
        }
    } else {
        alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
        return null;
    }
    return xhr;
}

function requestExercice(callback, categorie) {
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        } else if (xhr.status === 500) {
            notification.notify("ERROR 500", 'danger');
        } else if (xhr.status === 404) {
            notification.notify("ERROR 404", 'danger');
        }
    };

    if(categorie !== "Je ne sais pas"){
        xhr.open("POST", "index.php", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send("categorie=" + categorie);
    }
    
}

//on display le choix de l'utilisateur
function changeValue(value, classButton){
    if(value == "Je ne sais pas"){
        $('#cibleExercices  ').replaceWith("<div id='cibleExercices'></div>");
        $("#exercice").val("Je ne sais pas");
    }else if(classButton == "#category"){
        $('#exercice').removeAttr('disabled');
        requestExercice(readExercice, value);
    }
    $(classButton).text(value);
    $(classButton).val(value);
}



//on modifie le style du boutton en fonction du choix
function changeComment(value){
    if(value == "OUI"){
        $("#commentNON").removeClass("btn-success").addClass("btn-outline-dark");
        $("#commentOUI").removeClass("btn-outline-dark").addClass("btn-success");
        $("#comment").val(value);
    }else{
        $("#commentOUI").removeClass("btn-success").addClass("btn-outline-dark");
        $("#commentNON").removeClass("btn-outline-dark").addClass("btn-success");
        $("#comment").val(value);
    }
}

function changeNumber(value){
    if(value == "moins"){
        $("#plus").removeClass("btn-success").addClass("btn-outline-dark");
        $("#moins").removeClass("btn-outline-dark").addClass("btn-success");
        $("#numberUser").val(value);
    }else{
        $("#moins").removeClass("btn-success").addClass("btn-outline-dark");
        $("#plus").removeClass("btn-outline-dark").addClass("btn-success");
        $("#numberUser").val(value);
    }
}

function changeConnexion(value){
    if(value == "wifi"){
        $("#eduroam").removeClass("btn-success").addClass("btn-outline-dark");
        $("#wifi").removeClass("btn-outline-dark").addClass("btn-success");
        $("#connexion").val(value);
    }else{
        $("#wifi").removeClass("btn-success").addClass("btn-outline-dark");
        $("#eduroam").removeClass("btn-outline-dark").addClass("btn-success");
        $("#connexion").val(value);
    }
}

//display into target
function readExercice(sData) {
    $('#cibleExercices').replaceWith(sData);
}
