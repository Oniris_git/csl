<?php

require __DIR__ . '/../../lib/bootstrap.php';

use CSLManager\Administration\Mapper\WorkshopMapper;
use CSLManager\Administration\Mapper\CategoryMapper;
use CSLManager\Administration\Mapper\ConfigMapper;
use CSLManager\Administration\Mapper\LanguageMapper;

$wsMapper = new WorkshopMapper($connector);
$catMapper = new CategoryMapper($connector);
$configMapper = new ConfigMapper($connector);
$langMapper = new LanguageMapper($connector);
$data = [];

include "bug_reportFunctions.php";

$args = [
    'categorie' => FILTER_SANITIZE_STRING,
    'action' => FILTER_SANITIZE_STRING,
    'idUser' => FILTER_SANITIZE_STRING,
    'numberUser' => FILTER_SANITIZE_STRING,
    'connexion' => FILTER_SANITIZE_STRING,
    'comment' => FILTER_SANITIZE_STRING,
    'action' => FILTER_SANITIZE_STRING,
    'exercice' => FILTER_SANITIZE_STRING,
    'heure' => FILTER_SANITIZE_STRING,
    'min' => FILTER_SANITIZE_STRING,
    'note' => FILTER_SANITIZE_STRING,
    'marqueTel' => FILTER_SANITIZE_STRING,
];
$POST = filter_input_array(INPUT_POST, $args, false);
$languages = $langMapper->selectLanguages();

if(isset($POST['categorie'])){
    try{
        $categorie = $catMapper->getCategorieByName($POST['categorie']);
        $listExercices = [];
        $listExercices = $wsMapper->getWorkshopsByCategory($categorie->getId());
        require "exercice_ajax.phtml";
        exit;
    }catch(PDOException $e){
        $data = [
            "message" => $e->getMessage()
        ];
        echo json_encode($data);
    }
}else if(isset($POST['action'])){
    try{
        $file = "";
        if(isset($_FILES['screenShot'])){
            $file_img = checkFileImg($_FILES['screenShot']);
            if($file_img == "OK"){
                $file = $_FILES['screenShot'];
            }else{
                $file = "erreur";
                $ERROR = [
                    "message" => $file_img['message']
                ];
            }
        }
        if($file != "erreur"){
            sendMailreport($configMapper, $POST, $file);
            $SUCCESS = [
                "message" => "Rapport envoyé. Merci pour votre aide"
            ];
        }

    }catch(Exception $e){
        $ERROR = [
            "message" => $e->getMessage()
        ];
    }
}

$categories = [];
$categories = $catMapper->getCategories();


include '../../public/bug-report/bug_form.phtml';


