<?php

use PHPMailer\PHPMailer\PHPMailer;

function sendMailreport($configMapper, $POST, $file){

    $mail = new PHPMailer(true);

    $smtp = [
        'host' => $configMapper->selectConfigByName("smtp",'host')['value'],
        'secure' => $configMapper->selectConfigByName("smtp",'secure')['value'],
        'port' => $configMapper->selectConfigByName("smtp",'port')['value'],
        'user_name' => $configMapper->selectConfigByName("smtp",'user_name')['value'],
        'password' => $configMapper->selectConfigByName("smtp",'password')['value'],
    ];

    $to = $configMapper->selectConfigByName("view",'technique')['value'];

    try {
        //Server settings
        //$mail->SMTPDebug = 2;                                       // Enable verbose debug output
        $mail->isSMTP(true);                                            // Set mailer to use SMTP
        $mail->Host       = $smtp['host'];  // Specify main and backup SMTP servers
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = $smtp['user_name'];                     // SMTP username
        $mail->Password   = $smtp['password'];                               // SMTP password
        $mail->SMTPSecure = $smtp['secure'];                                  // Enable TLS encryption, `ssl` also accepted
        $mail->Port       = $smtp['port']; 

        //Recipients
        $mail->setFrom($POST['idUser'].'@bugreport.com', $POST['idUser']);
        $mail->addAddress($to, $smtp['user_name']);     // Add a recipient
        if($file != ""){
            // Attachments
            $mail->addAttachment($file["tmp_name"]);   
        }

        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Rapport d\'erreur';
        $mail->Body    = mailTemplate($POST);
        $mail->AltBody = 'Retour utilisateur';

        $mail->send();
    } catch (Exception $e) {
        throw new Exception( "Message could not be sent. Mailer Error: {$mail->ErrorInfo}");
    }
}

function mailTemplate($POST){

        $result = ' <body>
                        <div>
                            <h2 style="text-align: center">Rapport de bug CSLManager</h2>
                        </div>
                        <center><table style="margin-top:5rem;width:60%;border:solid;border-width:1px;border-color: #f0f0f0">
                            <caption style="margin:1rem">Erreur sur le profil de '.$POST['idUser'].'</caption>
                            <thead style="border:solid;border-width:1px;border-color: #f0f0f0">
                                <tr>
                                    <th style="text-align: left">Champ</th>
                                    <th style="text-align: left">Reponse</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Exercice</td>
                                    <td>'.$POST['exercice'].'</td>
                                </tr>
                                <tr>
                                    <td>Note</td>
                                    <td>'.$POST['note'].'</td>
                                </tr>
                                <tr>
                                    <td>Commentaire</td>
                                    <td>'.$POST['comment'].'</td>
                                </tr>
                                <tr>
                                    <td>Heure</td>
                                    <td>'.$POST['heure'].$POST['min'].'</td>
                                </tr>
                                <tr>
                                    <td>Nombre d\'utilisateurs</td>
                                    <td>'.$POST['numberUser'].'</td>
                                </tr>
                                <tr>
                                    <td>Connection</td>
                                    <td>'.$POST['connexion'].'</td>
                                </tr>
                                <tr>
                                    <td>Heure</td>
                                    <td>'.$POST['marqueTel'].'</td>
                                </tr>
                            </tbody>
                        </table></center>
                    </body>';
        return $result;
    }

function checkFileImg($file){
    $path_info = pathinfo($file["name"],PATHINFO_EXTENSION);
    //file image?
    $check = getimagesize($file["tmp_name"]);
    if ($check !== false) {
        $uploadOk = 1;
    } else {
        return $ERROR = [
            "message" => TXT_IMAGE_INVALID
        ];
    }
    if ($file["size"] > 500000) {
        return $ERROR = [
            "message" => TXT_IMAGE_SIZE
        ];
    }elseif ($path_info != "jpg" && $path_info != "JPG"&& $path_info != "png" && $path_info != "PNG" && $path_info != "jpeg" && $path_info != "JPEG" ) {
        return $ERROR = [
            "message" => TXT_IMAGE_FORMAT
        ];
    }elseif ($uploadOk == 0) {
        return $ERROR = [
            "message" => TXT_FILE_INVALID
        ];
    }else {
        return 'OK';
    }
}
