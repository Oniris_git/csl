<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 16/01/2019
 * Time: 10:35
 */

use CSLManager\Administration\Mapper\UserMapper;

require "../lib/bootstrap.php";
$args = [
    "changeBtn" => FILTER_SANITIZE_STRING,
    "changePassword" => FILTER_SANITIZE_STRING,
    "confirmChangePassword" => FILTER_SANITIZE_STRING,
    "data" => FILTER_SANITIZE_STRING
];

$POST = filter_input_array(INPUT_POST, $args, false);
$GET = filter_input_array(INPUT_GET, $args, false);

$userMapper = new UserMapper($connector);
$user = $userMapper->selectUserByToken($GET['data']);

//if token exist in url / check if the request is still available
if(isset($GET['data'])):
    $user = $userMapper->selectUserByToken($GET['data']);
    if($user != null):
        if($user->getOpen() == 1):
            $request = strtotime ($user->getRequest());
            $timeRequest = (mktime(date("H"), date("i"), date("s"), date("n"), date("j"), date("Y"))- $request);
            if($timeRequest > 900):
                header("Location: index.php?error=expired") ;
            exit;
            endif;
        endif;
        include "../view/front/request.phtml";
    else:
        header("Location: index.php?error=token") ;
    endif;
endif;

//if change request / check password and record new data
if(isset($POST['changeBtn'])):
    if(strlen ($POST['changePassword']) < 6):
        $user = $userMapper->getUserById($POST['changeBtn']);
        $regMessage = TXT_REGISTER_MDP_ERROR_LENGTH;
        include "../view/front/request.phtml";
    else:
        $checkPassword = strcmp($POST['changePassword'], $POST['confirmChangePassword']);
        if($checkPassword === 0):
            $hash = password_hash($POST['confirmChangePassword'], PASSWORD_DEFAULT);
            $user = [
                "password" => $hash
            ];
            try {
                $userMapper->updatePassword($POST['changeBtn'],$hash);
                header ('Location: index.php');
                exit;
            } catch (Exception $e) {
                $user = $userMapper->getUserById($POST['changeBtn']);
                $regMessage = TXT_REGISTER_ERROR;
                include "../view/front/request.phtml";
            }
        else:
            if ($checkPassword !== 0):
                $user = $userMapper->getUserById($POST['changeBtn']);
                $regMessage= TXT_REGISTER_MDP_ERROR;
                include "../view/front/request.phtml";
            endif;
        endif;
    endif;
endif;

