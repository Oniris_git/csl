<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use CSLManager\Administration\Helper\Config;
use CSLManager\Administration\Mapper\ConfigMapper;

$config = require __DIR__ . '/../../../config.php';
require __DIR__ . '/../../../vendor/autoload.php';

//access to BDD
$BASE = $config['db']['base'];
$IP = $config['db']['server'];
$USER = $config['db']['user'];
$PASSWORD = $config['db']['password'];

//create PDO object
try {
    $pdo = new PDO('mysql:host=' . $IP . ';dbname=' . $BASE . ';charset=UTF8', $USER, $PASSWORD);
} catch (PDOException $e) {
    exit(TXT_LOG_ERROR_CONNECTION . $e->getMessage());
}

if (!extension_loaded('curl')) {
    exit(TXT_LOG_ERROR_ADD_ON);
}

//start time
$start = microtime(true);

//Moodle values in BDD
$configMapper = new ConfigMapper($pdo);
$token = $configMapper->selectConfigByName('moodle', 'token')['value'];
$lang = $configMapper->selectConfigByName('view', 'lang')['value'];
$urlExterne = $configMapper->selectConfigByName('moodle', 'domain')['value'];
$courseId = $configMapper->selectConfigByName('moodle', 'course')['value'];
$post_data = ['courseids' => [$courseId]];

//include lang file
include(__DIR__.'/../../../lang/lang-'.$lang.'.inc.php');

//access to Moodle
$serverUrl = $urlExterne . '/webservice/rest/server.php?wstoken=' . $token
    . '&wsfunction=mod_assign_get_assignments'
    . '&moodlewsrestformat=json';

$curl = curl_init($serverUrl);

//access to data and decode
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post_data));
$post = json_decode(curl_exec($curl));
curl_close($curl);

if (isset($post->exception)) {
    $data = [
        'success' => false,
        'message' => TXT_LOG_ERROR_MOODLE_NOT_FOUND
    ];
    echo  json_encode($data);
    exit;
};

//data in tab
$assignments = [];
foreach ($post->courses as $course) {
    foreach ($course->assignments as $assignment) {
        $assignments[] = $assignment;
    }
}


//SQL request
$sql =
'INSERT INTO moodle_mod (module_id, devoir_id) VALUES (:mid, :did)
ON DUPLICATE KEY UPDATE devoir_id = :did2;';

//insert into
$stmt = $pdo->prepare($sql);
$i = 1;
foreach ($assignments as $a) {
    if (!$stmt->execute(['mid' => $a->id, 'did' => $a->cmid, 'did2' => $a->cmid])) {
        $data = [
            'success' => false,
            'message' => TXT_LOG_ERROR_COURSE.$a->cmid.PHP_EOL
        ];
        echo  json_encode($data);
        exit;
    }
    $i++;
}

//send message to the user
$data = [
    'success' => true,
    'message' => $i.TXT_LOG_SUCCESS_MOODLE_LINK . (microtime(true) - $start) . ' secondes' . PHP_EOL
];

echo  json_encode($data);
