<?php

use CSLManager\Administration\Mapper\ScaleMapper;
use CSLManager\Administration\Stat;
use CSLManager\Administration\Session\Session;
use CSLManager\Administration\Mapper\UserMapper;

require '../../../lib/bootstrap.php';

$upn=null;
$order=null;
$ajaxJSchart = "";

$stat = new Stat($connector);
$uMapper = new UserMapper($connector);
$stats = new StdClass;
$sMapper = new Session();
$scaleMapper = new ScaleMapper($connector);
$scalesType = $scaleMapper->getScaleTypes();


$show=(isset($_POST["showGrade"])) ? $_POST["showGrade"] : NULL;
if($show){
    $chartType = 'user';
    $user= $uMapper->getUserById($show);
    $upn= $user->getUpn();
    $stats->scales = $stat->getScales($upn);
    $userSelected = $show;
}


//check the user choice to display information for global
$filter = (isset($_POST["scale"])) ? $_POST["scale"] : NULL;

//we want to know if the request is global or for user
//$requestFilter[0] / Global G or User U
//$requestFilter[1] / 0 no id / else id
//$requestFilter[2] / Global G or Distinct D
//second case $requestFilter[3] / user Id selected
$requestFilter = explode("-", $filter);
if($requestFilter[0] == "G"):
    $chartType = 'global';
    if($filter == "G-0-G"):
        $title = TXT_GLOBAL;
        $stats->scales = $stat->getScales();
    elseif ($filter == "G-0-D"):
        $title = TXT_DISTINCT;
        $stats->scales = $stat->getScalesDistinct();
    else:
        if($requestFilter[2] == "G"):
            $stats->scales = $stat->getScales($upn, $order, $requestFilter[1]);
        elseif ($requestFilter[2] == "D"):
            $stats->scales = $stat->getScalesDistinct($upn, $requestFilter[1]);
        endif;
        $scaleType = $scaleMapper->getScaleTypeById( $requestFilter[1]);
        $title = $scaleType['label'];
    endif;
elseif ($requestFilter[0] == "U"):
    $chartType = 'user';
    $userSelected = $requestFilter[3];
    $user = $uMapper->getUserById($userSelected);
    $upn = $user->getUpn();
    $filter = substr($filter, 0,5);

    if($filter == "U-0-G"):
        $title = TXT_GLOBAL;
        $stats->scales = $stat->getScales($upn);
    elseif ($filter == "U-0-D"):
        $title = TXT_DISTINCT;
        $stats->scales = $stat->getScalesDistinct($upn);
    else:
        if($requestFilter[2] == "G"):
            $stats->scales = $stat->getScales($upn,$order,$requestFilter[1]);
        elseif ($requestFilter[2] == "D"):
            $stats->scales = $stat->getScalesDistinct($upn ,$requestFilter[1]);
        endif;
        $scaleType = $scaleMapper->getScaleTypeById( $requestFilter[1]);
        $title = $scaleType['label'];
    endif;
endif;

require '../../../view/back-office/blocks/ajax/list_priority_level.phtml';


