<?php

use CSLManager\Administration\Mapper\ScaleMapper;
use CSLManager\Administration\Stat;
use CSLManager\Administration\Mapper\WorkshopMapper;
use CSLManager\Administration\Mapper\UserMapper;

require '../../../lib/bootstrap.php';

//instantiation of variables
$ajaxJSchart = "";
$charType= 'global';

//check the user choice to display informations
$filter = (isset($_POST["filter"])) ? $_POST["filter"] : NULL;
if (isset($filter)) {
    switch ($filter){
        case 2:
            $wMapper = new WorkshopMapper($connector);
            $workshops=$wMapper->getWorkshops();
            require '../../../view/back-office/blocks/ajax/list_workshop_attempt.phtml';
            break;
        case 3:
            $uMapper = new UserMapper($connector);
            $users=$uMapper->getUsers();
            require '../../../view/back-office/blocks/ajax/list_user_filter.phtml';
            break;
        case 4:
            $chartType = 'global';
            $stat = new Stat($connector);
            $stats = new StdClass;
            $scaleMapper = new ScaleMapper($connector);
            $scalesType = $scaleMapper->getScaleTypes();
            $stats->scales = $stat->getScales();
            require '../../../view/back-office/blocks/ajax/list_priority_level.phtml';
            break;
    }
} 
