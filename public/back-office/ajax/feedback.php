<?php

require '../../../lib/bootstrap.php';

use CSLManager\Administration\Mapper\UserMapper;
use CSLManager\Administration\Mappers\ConfigMapper;

$data = [];
$configMapper = new ConfigMapper($connector);
$userMapper = new UserMapper($connector);

$subject = filter_input(INPUT_POST, 'subject', FILTER_SANITIZE_STRING);
$body = filter_input(INPUT_POST, 'body', FILTER_SANITIZE_STRING);

$sender = $session->getValue('upn');
$body = <<<DOC
	Message envoyé de : $sender
	$body
DOC;

if (empty($body) || empty($subject)) {
    $data['success'] = false;
    $data['message'] = TXT_ADMIN_MAILER_EMPTY;

    echo json_encode($data);
    exit();
}

$request = "feedback";
$user = $userMapper->getUserByUpn($sender);
$resultMail = sendMail($user , $connector, $request, $body);


if ($resultMail == null || $resultMail == true) {
    $data['success'] = true;
    $data['message'] = TXT_ADMIN_MAILER_SEND_SUCCESS;
} else {
    $data['success'] = false;
    $data['message'] = TXT_ADMIN_MAILER_SEND_ERROR ;
}

echo json_encode($data);
