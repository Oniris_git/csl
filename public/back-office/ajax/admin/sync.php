<?php


$config = require __DIR__ . '/../../../../config.php';
require __DIR__ . '/../../../../vendor/autoload.php';
require '../../../../lib/bootstrap.php';

use CSLManager\Hook\Hook;

//start time
$start = microtime(true);

$hook = new Hook(['sync_departments' => true]);
$hook->setHookAvailable($config->get('hook'));

if ($hook->isHookAvailable() && $permission->check('edit:users')) {
    $count = $hook->emit_signal('sync_departments', []);
    //send message to the user
    $data = [
        'success' => true,
        'message' => $count.TXT_LOG_SUCCESS_MOODLE_LINK.(microtime(true) - $start) . ' secondes' . PHP_EOL
    ];

    echo  json_encode($data);
}
