<?php

use CSLManager\Administration\Mapper\SpecieMapper;

require __DIR__ . '/../../../../lib/bootstrap.php';

$data = [];

$args = [
    'action' => FILTER_SANITIZE_STRING,
    'idSpecies' => FILTER_VALIDATE_INT,
    'title' => FILTER_SANITIZE_STRING,
    'description' => FILTER_SANITIZE_STRING
];

$POST = filter_input_array(INPUT_POST, $args, false);

$speciesMapper = new SpecieMapper($connector);

switch ($POST['action']) {
    case 'add-species':
        if (! $permission->check('create:species')) {
            $ERROR = [
                'message' => TXT_NOTIFICATION_NO_RIGHT
            ];
        }

        else if (!isset($POST['title'], $POST['description'])) {
            $ERROR = [
                'message' => TXT_NOTIFICATION_CHECK_VALUES
            ];
        }else{
            try{
                $result = $speciesMapper->create([
                    'title' => $POST['title'],
                    'description' => $POST['description']
                ]);
                if($result != true){
                    $ERROR= [
                        "message"=>TXT_NOTIFICATION_ERROR . $speciesMapper->getPDOError()[2]
                    ];
                }
            }catch (Exception $e){
                $ERROR= [
                    "message"=>TXT_NOTIFICATION_ERROR . $speciesMapper->getPDOError()[2]
                ];
            }
        }

        $species= $speciesMapper->getSpecies();
        require '../../../../view/back-office/admin/blocks/admin_species_block.phtml';
        break;

    case 'alter-species':
        if (! $permission->check('edit:species')) {
            $data = [
                'success' => false,
                'message' => TXT_NOTIFICATION_NO_RIGHT
            ];
            break;
        }

        if (! isset($POST['title'], $POST['description'],
            $POST['idSpecies'])
        ) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
            break;
        }

        $result = $speciesMapper->alter([
            'title' => $POST['title'],
            'description' => $POST['description'],
            'id' => $POST['idSpecies']
        ]);

        if ($result !== false) {
            $data['success'] = true;
            $data['message'] = TXT_NOTIFICATION_ALTER_SPECIE;
        } else {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_ERROR .
                $speciesMapper->getPDOError()[2];
        }
        echo json_encode($data);
        break;

    case 'del':
        if (! $permission->check('edit:species')) {
            $data = [
                'success' => false,
                'message' => TXT_NOTIFICATION_NO_RIGHT
            ];
            break;
        }

        if (! isset($POST['idSpecies'])) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
            break;
        }
        $result = $speciesMapper->delete($POST['idSpecies']);
        if ($result !== false) {
            $data['success'] = true;
            $data['message'] = TXT_NOTIFICATION_DEL_SPECIE;
        } else {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_ERROR .
                $speciesMapper->getPDOError()[2];
        }
        echo json_encode($data);
        break;

    default:
        $data = ['success' => false, 'message' => TXT_NOTIFICATION_UNKNOWN_ACTION];
        echo json_encode($data);
        break;
}


