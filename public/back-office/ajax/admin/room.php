<?php

use CSLManager\Administration\Mapper\RoomMapper;

require '../../../../lib/bootstrap.php';

$data = [];

$args = [
    'action' => FILTER_SANITIZE_STRING,
    'room-id' => FILTER_VALIDATE_INT,
    'room-label' => FILTER_SANITIZE_STRING,
];

$roomMapper = new RoomMapper($connector);
$POST = filter_input_array(INPUT_POST, $args, false);

switch ($POST['action']) {
    case 'add-room':
        if (!$permission->check('create:room')) {
            $ERROR = [
                'message' => TXT_NOTIFICATION_NO_RIGHT
            ];
        }
       else if (!isset($POST['room-label'])) {
            $ERROR = [
                'message' => TXT_NOTIFICATION_CHECK_VALUES
            ];
       }else{
           try {
               $result = $roomMapper->createRoom($POST['room-label']);
           } catch (\Exception $e) {
               $ERROR= [
                   "message"=>TXT_NOTIFICATION_ERROR . $roomMapper->getPDOError()[2]
               ];
           }
       }
        $rooms= $roomMapper->getRooms();
        require '../../../../view/back-office/admin/blocks/admin_room_block.phtml';
        break;
    case 'alter-room':
        if (!$permission->check('edit:room')) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_NO_RIGHT;
            break;
        }

        if (!isset($POST['room-id']) || !isset($POST['room-label'])) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
            break;
        }

        try {
            $roomMapper->editRoom($POST['room-id'], $POST['room-label']);
            $data['success'] = true;
            $data['message'] = TXT_NOTIFICATION_ALTER_ROOM;
        } catch (\Exception $e) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_ERROR . $e->getMessage();
        }
        echo json_encode($data);
        break;
    case 'del-room':
        if (!$permission->check('edit:room')) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_NO_RIGHT;
            break;
        }

        if (!isset($POST['room-id'])) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
            break;
        }

        try {
            $roomMapper->deleteRoom($POST['room-id']);
            $data['success'] = true;
            $data['message'] = TXT_NOTIFICATION_DEL_ROOM;
        } catch (\Exception $e) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_ERROR . $e->getMessage();
        }
        echo json_encode($data);
        break;
    default:
        $data = ['success' => false, 'message' => TXT_NOTIFICATION_NOT_IMPLEMENTED];
        echo json_encode($data);
        break;
}


