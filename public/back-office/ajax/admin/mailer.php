<?php

use CSLManager\Administration\Mappers\ConfigMapper;
use PHPMailer\PHPMailer\PHPMailer;

require __DIR__ . '/../../../../lib/bootstrap.php';

$data = [];

$args = [
    "action" =>  FILTER_SANITIZE_STRING,
    "hostSMTP" => FILTER_SANITIZE_STRING,
    "secureSMTP" => FILTER_SANITIZE_STRING,
    "portSMTP" => FILTER_VALIDATE_INT,
    "emailSMTP" => FILTER_SANITIZE_STRING,
    "passwordSMTP" => FILTER_SANITIZE_STRING
];

$POST = filter_input_array(INPUT_POST, $args, false);

$configMapper = new ConfigMapper($connector);

switch ($POST['action']){
    case "alterSMTP":
        if(!$permission->check("edit:mailer")):
            $data = [
                "success" => false,
                "message" => TXT_NOTIFICATION_NO_RIGHT
            ];
            break;
        endif;

        try{
            //Create a new PHPMailer instance / check data = true
            $mail = new PHPMailer(true);
            $mail->SMTPDebug = false;
            $mail->do_debug = 0;
            try{
                $mail->isSMTP();
                $mail->MailerDebug = false;
                $mail->Host = $POST['hostSMTP'];
                $mail->Port = $POST['portSMTP'];
                $mail->SMTPSecure = $POST['secureSMTP'];
                $mail->SMTPAuth = true;
                $mail->Username = $POST['emailSMTP'];
                $mail->Password = $POST['passwordSMTP'];
                $mail->setFrom($POST['emailSMTP'], 'Mailer Test');
                $mail->addAddress($POST['emailSMTP'], 'Administrateur Technique');
                $mail->Subject = 'Test SMTP';
                $mail->Body = 'Check SMTP Valid';
                $mail->CharSet = 'utf8';
                $mail->Send();
            }catch (phpmailerException $e) {
                $data = [
                    "message" => $e->errorMessage()
                ] ;
                break;
            } catch (Exception $e) {
                $data = [
                    "message" => $e->errorMessage()
                ] ;
                break;
            }
            $configMapper->alterSMTP("host", $POST['hostSMTP']);
            $configMapper->alterSMTP("secure", $POST['secureSMTP']);
            $configMapper->alterSMTP("port", $POST['portSMTP']);
            $configMapper->alterSMTP("user_name", $POST['emailSMTP']);
            $configMapper->alterSMTP("password", $POST['passwordSMTP']);
            $data = [
                "success" => true,
                "message" => TXT_ADMIN_NOTIFICATION_SUCCESS
            ];
        }catch(Exception $e){
            $data = [
                "success" => false,
                "message" => $e->getMessage()
            ];
        }
}


echo json_encode($data);
