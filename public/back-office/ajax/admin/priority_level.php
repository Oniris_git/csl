<?php

use CSLManager\Administration\Mapper\PriorityLevelMapper;

require '../../../../lib/bootstrap.php';

$data = [];

$args = [
    'action' => FILTER_SANITIZE_STRING,
    'idPriority_level' => FILTER_VALIDATE_INT,
    'title' => FILTER_SANITIZE_STRING,
    'description' => FILTER_SANITIZE_STRING
];

$POST = filter_input_array(INPUT_POST, $args, false);

switch ($POST['action']) {
    case 'add-priority_level':
        $plMapper = new PriorityLevelMapper($connector);
        if (!$permission->check('create:rank')) {
            $ERROR = [
                'message' => TXT_NOTIFICATION_NO_RIGHT
            ];
        }

        else if (!isset($POST['title']) || !isset($POST['description'])) {
            $ERROR = [
                'message' => TXT_NOTIFICATION_CHECK_VALUES
            ];
        }else{
            try {
                $result = $plMapper->create([
                    'title' => $POST['title'],
                    'description' => $POST['description']
                ]);
                if($result != true){
                    $ERROR= [
                        "message"=>TXT_NOTIFICATION_ERROR . $plMapper->getPDOError()[2]
                    ];
                }
            } catch (Exception $e) {
                $ERROR= [
                    "message"=>TXT_NOTIFICATION_ERROR . $plMapper->getPDOError()[2]
                ];
            }
        }

        $priority_levels= $plMapper->getPriorityLevels();
        require '../../../../view/back-office/admin/blocks/admin_priority_level_block.phtml';
        break;

    case 'alter-priority_level':
        if (!$permission->check('edit:rank')) {
            noRights($data);
            break;
        }
        $GET = filter_input_array(INPUT_GET, $args, false);
        if (!isset($POST['title']) || !isset($POST['description'])) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
            break;
        }

        $plMapper = new PriorityLevelMapper($connector);
        try {
            $plMapper->alter([
                'title' => $POST['title'],
                'description' => $POST['description'],
                'id' => $POST['idPriority_level']
            ]);

            $data['success'] = true;
            $data['message'] = TXT_NOTIFICATION_ALTER_PRIORITY_LEVEL;
        } catch (\Exception $e) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_ERROR . $e->getMessage();
        }
        echo json_encode($data);
        break;


    case 'del':
        if (! $permission->check('edit:rank')) {
            $data = [
                'success' => false,
                'message' => TXT_NOTIFICATION_NO_RIGHT
            ];
            break;
        }

        if (! isset($POST['idPriority_level'])) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
            break;
        }
        $plMapper = new PriorityLevelMapper($connector);
        $result = $plMapper->delete($POST['idPriority_level']);
        if ($result !== false) {
            $data['success'] = true;
            $data['message'] = TXT_NOTIFICATION_DEL_PRIORITY_LEVEL;
        } else {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_ERROR . $plMapper->getPDOError()[2];
        }
        echo json_encode($data);
        break;
}


