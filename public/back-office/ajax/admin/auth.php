<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 03/01/2019
 * Time: 13:45
 */

use Vespula\Auth\Adapter\Ldap;
use CSLManager\Administration\Mapper\ConfigMapper;

require __DIR__ . '/../../../../lib/bootstrap.php';

$args = [
    "valueAuth" =>  FILTER_SANITIZE_STRING,
    "uriLDAP" => FILTER_SANITIZE_STRING,
    "baseDnLDAP" => FILTER_SANITIZE_STRING,
    "bindDnLDAP" => FILTER_SANITIZE_STRING,
    "bindPwLDAP" => FILTER_SANITIZE_STRING,
    "portLDAP" => FILTER_VALIDATE_INT,
    "auth" => FILTER_SANITIZE_STRING
];

$POST = filter_input_array(INPUT_POST, $args, false);

$configMapper = new ConfigMapper($connector);

if(isset($POST['valueAuth'])):
    switch ($POST['valueAuth']){
        case 'ldap' :
            require '../../../../view/back-office/admin/blocks/admin_auth_block_ldap.phtml';
            break;
        case 'ad' :
            require '../../../../view/back-office/admin/blocks/admin_auth_block_ad.phtml';
            break;
        default:
            include "../../../../view/back-office/admin/blocks/admin_auth_block_ldap.phtml";
            break;
    }
endif;

if(isset($POST['auth'])):
    switch ($POST['auth']):
        case 'ldap':
            if(!$permission->check("edit:auth")):
                $data = [
                    "success" => false,
                    "meessage" => TXT_NOTIFICATION_NO_RIGHT
                ];
                break;
            endif;
            try{
                $authOld = $configMapper->selectType("auth");
                // check ldap / using ldap bind
                $ldaprdn  = $POST['bindDnLDAP'];     // ldap rdn or dn
                $ldappass = $POST['bindPwLDAP'];  // associated password

                // connect to ldap server
                $ldapconn = ldap_connect($POST['uriLDAP']);

                // Set some ldap options for talking to
                ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
                ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);

                if ($ldapconn) {
                    // binding to ldap server
                    $ldapbind = @ldap_bind($ldapconn, $ldaprdn, $ldappass);
                }
                if($ldapbind):
                    switch ($authOld[0]['value']):
                        case "ldap":
                            $configMapper->alterConfig("auth_ldap", "uri", $POST['uriLDAP']);
                            $configMapper->alterConfig("auth_ldap", "basedn", $POST['baseDnLDAP']);
                            $configMapper->alterConfig("auth_ldap", "binddn", $POST['bindDnLDAP']);
                            $configMapper->alterConfig("auth_ldap", "bindpw", $POST['bindPwLDAP']);
                            $configMapper->alterConfig("auth_ldap", "port", $POST['portLDAP']);
                            $data = [
                                "success" => true,
                                "message" => TXT_ADMIN_AUTH_SUCCESS
                            ];
                            break;
                        case 'manual':
                            $configMapper->alterConfig("auth","type","ldap");
                            $ldapAttributes = [
                                "attribute1" => "cn",
                                "attribute2" => "sn",
                                "attribute3" => "givenName",
                                "attribute4" => "mail",
                                "attribute5" => "description",
                                "attribute6" => "supannaliaslogin",
                            ];
                            $configMapper->insert("auth_ldap", "uri", $POST['uriLDAP']);
                            $configMapper->insert("auth_ldap", "basedn", $POST['baseDnLDAP']);
                            $configMapper->insert("auth_ldap", "binddn", $POST['bindDnLDAP']);
                            $configMapper->insert("auth_ldap", "bindpw", $POST['bindPwLDAP']);
                            $configMapper->insert("auth_ldap", "port", $POST['portLDAP']);
                            $configMapper->insert("auth_ldap", "filter", "supannaliaslogin=%s");
                            $configMapper->insert("auth_ldap", "filterImport", "(mail=%s)");
                            $configMapper->insert("auth_ldap", "escapeChars", "\&!|=<>,+';()");

                            foreach ($ldapAttributes as $key => $value):
                                $configMapper->insert("auth_ldap", $key, $value);
                            endforeach;

                            $data = [
                                "success" => true,
                                "message" => TXT_ADMIN_AUTH_SUCCESS
                            ];
                            break;

                    endswitch;
                else:
                    $data = [
                        "success" => false,
                        "message" => TXT_NOTIFICATION_CHECK_VALUES
                    ];
                    break;
                endif;
            }catch (Exception $e){
                $data = [
                    "success" => false,
                    "message" => $e->getMessage()
                ];
            }
            break;
        case "ad":
            $authOld = $configMapper->selectType("auth");
            switch ($authOld):
                case "ldap":
                    break;
                case "ad":
                    break;
            endswitch;
            break;
        default:

            break;
    endswitch;
    echo json_encode($data);
endif;

