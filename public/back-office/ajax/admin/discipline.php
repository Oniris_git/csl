<?php

use CSLManager\Administration\Mapper\DisciplineMapper;

require '../../../../lib/bootstrap.php';

$data = [];

$args = [
    'action' => FILTER_SANITIZE_STRING,
    'idDiscipline' => FILTER_VALIDATE_INT,
    'title' => FILTER_SANITIZE_STRING,
    'description' => FILTER_SANITIZE_STRING,
    'id' => FILTER_VALIDATE_INT,
];

$POST = filter_input_array(INPUT_POST, $args, false);

switch ($POST['action']) {
    case 'add-discipline':
        $disciplineMapper = new DisciplineMapper($connector);
        if (!$permission->check('create:discipline')) {
            $ERROR = [
                'message' => TXT_NOTIFICATION_NO_RIGHT
            ];
        }
        if (!isset($POST['title']) || !isset($POST['description'])) {
            $ERROR = [
                'message' => TXT_NOTIFICATION_CHECK_VALUES
            ];
        }

        try {
            $result = $disciplineMapper->create([
                'title' => $POST['title'],
                'description' => $POST['description']
            ]);
            if($result != true){
                $ERROR= [
                    "message"=>TXT_NOTIFICATION_ERROR . $disciplineMapper->getPDOError()[2]
                ];
            }
        } catch (Exception $e) {
            $ERROR= ["error"=>TXT_NOTIFICATION_ERROR.$disciplineMapper->getPDOError()[2]];
        }

        $view="list";
        $disciplines= $disciplineMapper->getDisciplines();
        require '../../../../view/back-office/admin/blocks/admin_discipline_block.phtml';
        break;

    case 'alter-discipline':
        if (!$permission->check('edit:discipline')) {
            noRights($data);
            break;
        }
        $GET = filter_input_array(INPUT_GET, $args, false);
        if (!isset($POST['title']) || !isset($POST['description'])) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
            break;
        }

        $disciplineMapper = new DisciplineMapper($connector);
        try {
            $disciplineMapper->alter([
                'title' => $POST['title'],
                'description' => $POST['description'],
                'id' => $POST['idDiscipline']
            ]);

            $data['success'] = true;
            $data['message'] = TXT_NOTIFICATION_ALTER_DISCIPLINE;
        } catch (\Exception $e) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_ERROR . $e->getMessage();
        }
        echo json_encode($data);
        break;

    case 'del':
        if (! $permission->check('edit:discipline')) {
            $data = [
                'success' => false,
                'message' => TXT_NOTIFICATION_NO_RIGHT
            ];
            break;
        }

        if (! isset($POST['idDiscipline'])) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
            break;
        }
        $disciplineMapper = new DisciplineMapper($connector);
        $result = $disciplineMapper->delete($POST['idDiscipline']);
        if ($result !== false) {
            $data['success'] = true;
            $data['message'] = TXT_NOTIFICATION_DEL_DISCIPLINE;
        } else {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_ERROR.
                $disciplineMapper->getPDOError()[2];
        }
        echo json_encode($data);
        break;
}


