 <?php

use CSLManager\Administration\Mapper\PermissionMapper;
use CSLManager\Administration\Mapper\ProfileMapper;
require '../../../../lib/bootstrap.php';

$view="alter";
$rightMapper = new PermissionMapper($connector);
$profileMapper = new ProfileMapper($connector);

//check posts / if does not exist return null
$valueProfile = (isset($_POST["valueProfile"])) ? $_POST["valueProfile"] : NULL;
$valueAdd = (isset($_POST["valueAdd"])) ? $_POST["valueAdd"] : NULL;
$valueDelete = (isset($_POST["valueDelete"])) ? $_POST["valueDelete"] : NULL;

//explode posts to have values in table
$lstValues=explode(",",$valueAdd);
$lstValuesToDel=explode(",",$valueDelete);

//check if user request is to add profile values
if(($valueProfile) != null && ($valueAdd) !== null) {
    try {
        foreach ($lstValues as $val){
            $rightMapper->insertPermission($valueProfile,$val);
        }
        $profile = $profileMapper->getProfileById($valueProfile);
    } catch (Exception $e) {
        $ERROR= [
            'message'=> TXT_NOTIFICATION_ERROR . $e->getMessage()
        ] ;
    }

    $rights =$rightMapper->getByProfile($valueProfile);
    require '../../../../view/back-office/admin/admin_profile.phtml';

//check if user request is to delete profile values
}else if(($valueProfile) != null && ($valueDelete) !== null){

    try {
        foreach ($lstValuesToDel as $val){
            $rightMapper->delete($val,$valueProfile);
        }
        $profile = $profileMapper->getProfileById($valueProfile);
    } catch (Exception $e) {
        $ERROR=[
            'message' => TXT_NOTIFICATION_ERROR . $e->getMessage()
        ];
    }

    $rights =$rightMapper->getByProfile($valueProfile);
    require '../../../../view/back-office/admin/admin_profile.phtml';

//check if just value profile display information
}else if(($valueProfile) != null){

    $rights = $rightMapper->getRightsNotIn($valueProfile);
    require '../../../../view/back-office/blocks/ajax/list_profile.phtml';
}