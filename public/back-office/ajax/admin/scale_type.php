<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use CSLManager\Administration\Mapper\ScaleMapper;

require '../../../../lib/bootstrap.php';

function noRights(&$data){
    $data['success'] = false;
    $data['message'] = TXT_NOTIFICATION_NO_RIGHT;
}

function missingArgs(&$data){
    $data['success'] = false;
    $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
}

$data = [];

$args = [
    'action' => FILTER_SANITIZE_STRING,
    'scale-id' => FILTER_SANITIZE_NUMBER_INT,
    'scale_type-id' => FILTER_VALIDATE_INT,
    'scale_type-label' => FILTER_SANITIZE_STRING,
    'scale-label' => FILTER_SANITIZE_STRING,
    'scale-code' => FILTER_SANITIZE_STRING,
    'order' => [
        'filter' => FILTER_VALIDATE_INT,
        'flags' => FILTER_REQUIRE_ARRAY,
    ]
];

$POST = filter_input_array(INPUT_POST, $args, false);

switch ($POST['action']) {
    case 'alter-order':
        if (!$permission->check('edit:evals')) {
            noRights($data);
            break;
        }
        if (!isset($POST['scale-id']) || !isset($POST['order'])) {
            missingArgs($data);
            break;
        }

        $scaleMapper = new ScaleMapper($connector);
        try {
            $scaleMapper->setOrderByGroup(
                $POST['scale-id'],
                $POST['order']
            );
            $data = ['success' => true, 'message' => TXT_NOTIFICATION_ALTER_ORDER];
        } catch (\Exception $e) {
            $data = ['success' => false, 'message' => $e->getMessage()];
        }
        echo json_encode($data);
        break;

    case 'del-scale':
        if (!$permission->check('edit:evals')) {
            noRights($data);
            break;
        }
        if (!isset($POST['scale-id'])) {
            missingArgs($data);
            break;
        }
        $scaleMapper = new ScaleMapper($connector);
        try {
            $scale = $scaleMapper->getScaleById($POST['scale-id']);
            $scaleMapper->setStatus($scale, 1);
            $data = ['success' => true, 'message' => TXT_NOTIFICATION_DEL_SCALE];
        } catch (\Exception $e) {
            $data = ['success' => false, 'message' => $e->getMessage()];
        }
        echo json_encode($data);
        break;

    case 'add-scale':
        if (!$permission->check('edit:evals')) {
            noRights($data);
            break;
        }
        if (!isset($POST['scale-id'])) {
            missingArgs($data);
            break;
        }
        $scaleMapper = new ScaleMapper($connector);
        try {
            $scale = $scaleMapper->getScaleById($POST['scale-id']);
            $scaleMapper->setStatus($scale, 0);
            $data = ['success' => true, 'message' => TXT_NOTIFICATION_AVAILABLE_SCALE];
        } catch (\Exception $e) {
            $data = ['success' => false, 'message' => $e->getMessage()];
        }
        echo json_encode($data);
        break;

    case 'alter-scale':
        if (!$permission->check('edit:evals')) {
            noRights($data);
            break;
        }
        if (!isset($POST['scale-id']) || !isset($POST['scale-label'])) {
            missingArgs($data);
            break;
        }
        $scaleMapper = new ScaleMapper($connector);
        try {
            $scale = $scaleMapper->getScaleById($POST['scale-id']);
            $scaleMapper->alter($scale, $POST['scale-label']);
            $data = ['success' => true, 'message' => TXT_NOTIFICATION_ALTER_SCALE];
        } catch (\Exception $e) {
            $data = ['success' => false, 'message' => $e->getMessage()];
        }
        echo json_encode($data);
        break;

    case 'create-scale':
        if (!$permission->check('edit:evals')) {
            noRights($data);
            break;
        }
        if (!isset($POST['scale-code']) || !isset($POST['scale-label'])) {
            missingArgs($data);
            break;
        }
        $scale = [
            'scale-code' => $POST['scale-code'],
            'scale-label' => $POST['scale-label'],
        ];
        $scaleMapper = new ScaleMapper($connector);
        try {
            $scale = $scaleMapper->create($scale);
            $data = ['success' => true, 'message' => TXT_NOTIFICATION_ADD_SCALE];
            $data['scale'] = [
                'scale-label' => $scale['scale-label'],
                'scale-code' => $scale['scale-code']
            ];
            $data['request'] = $POST;
        } catch (\Exception $e) {
            $data = ['message' => $e->getMessage(), 'success' => false];
        }
        echo json_encode($data);
        break;

    case 'ungroup':
        if (!$permission->check('edit:evals')) {
            noRights($data);
            break;
        }
        if (!isset($POST['scale-id'])) {
            missingArgs($data);
            break;
        }
        $scaleMapper = new ScaleMapper($connector);
        try {
            $scale = $scaleMapper->getScaleById($POST['scale-id']);
            $scaleMapper->unGroup($scale);
            $data = ['success' => true, 'message' => TXT_NOTIFICATION_UNGROUP_SCALE];
        } catch (\Exception $e) {
            $data = ['success' => false, 'message' => $e->getMessage()];
        }
        echo json_encode($data);
        break;

    case 'create-scale_type':
        $scaleMapper = new ScaleMapper($connector);
        if (!$permission->check('edit:evals')) {
            $ERROR = [
                'message' => TXT_NOTIFICATION_NO_RIGHT
            ];
        }
        else if (!isset($POST['scale_type-label'])) {
            $ERROR = [
                'message' => TXT_NOTIFICATION_CHECK_VALUES
            ];
        }else{
            try {
                $scale_type = ['label' => $POST['scale_type-label']];
                $result = $scaleMapper->createScaleType($scale_type);
                if($result != true){
                    $ERROR= [
                        "message"=>TXT_NOTIFICATION_ERROR . $scaleMapper->getPDOError()[2]
                    ];
                }
            } catch (\Exception $e) {
                $ERROR = [
                    'message' =>TXT_NOTIFICATION_ERROR . $scaleMapper->getPDOError()[2]
                ];
            }
        }

        $scale_types= $scaleMapper->getScaleTypes();
        require '../../../../view/back-office/admin/blocks/admin_scale_type_block.phtml';
        break;

    case 'del-scale_type':
        if (!$permission->check('edit:evals')) {
            noRights($data);
            break;
        }
        if (!isset($POST['scale_type-id'])) {
            missingArgs($data);
            break;
        }

        try {
            $scaleMapper = new ScaleMapper($connector);
            $scaleMapper->delete($POST['scale_type-id']);
            $data = ['success' => true, 'message' => TXT_NOTIFICATION_DEL_GROUP];
        } catch (\Exception $e) {
            $data = ['message' => $e->getMessage(), 'success' => false];
        }
        echo json_encode($data);
        break;
    default:
        $data = ['success' => false, 'message' => TXT_NOTIFICATION_NOT_IMPLEMENTED];
        echo json_encode($data);
        break;
}

//header('Content-Type: application/json');

