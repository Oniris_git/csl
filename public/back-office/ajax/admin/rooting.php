<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 03/01/2019
 * Time: 16:29
 */

use CSLManager\Administration\Mappers\ConfigMapper;

require __DIR__ . '/../../../../lib/bootstrap.php';

$data = [];

$args = [
    "action" =>  FILTER_SANITIZE_STRING,
    "tokenMoodle" => FILTER_SANITIZE_STRING,
    "domainMoodle" => FILTER_SANITIZE_STRING,
    "courseMoodle" => FILTER_VALIDATE_INT,
];

$POST = filter_input_array(INPUT_POST, $args, false);

$configMapper = new ConfigMapper($connector);

if(isset($POST['action'])):
    switch ($POST['action']):
        case 'alterMoodle':
            if(!$permission->check("edit:root")):
                $data = [
                    "success" => false,
                    "message" => TXT_NOTIFICATION_NO_RIGHT
                ];
                break;
            endif;
            try{
                if($POST['tokenMoodle'] != ""):
                    $configMapper->alterMoodle("token", $POST['tokenMoodle']);
                endif;
                if ($POST['domainMoodle'] != ""):
                    $configMapper->alterMoodle("domain", $POST['domainMoodle']);
                endif;
                if ($POST['courseMoodle'] != ""):
                    $configMapper->alterMoodle("course", $POST['courseMoodle']);
                endif;
                $data = [
                    "success" => true,
                    "message" => TXT_ADMIN_ROOTING_SUCCESS
                ];
            }catch (Exception $e){
                $data = [
                    "success" => false,
                    "message" => $e->getMessage()
                ];
            }

            break;
        default:
            break;
    endswitch;
endif;

echo json_encode($data);