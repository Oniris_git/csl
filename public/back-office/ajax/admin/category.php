<?php

use CSLManager\Administration\Mapper\CategoryMapper;

require __DIR__ . '/../../../../lib/bootstrap.php';

$data = [];

$args = [
    'action' => FILTER_SANITIZE_STRING,
    'idCategory' => FILTER_VALIDATE_INT,
    'title' => FILTER_SANITIZE_STRING,
    'description' => FILTER_SANITIZE_STRING
];

$POST = filter_input_array(INPUT_POST, $args, false);

$categoryMapper = new CategoryMapper($connector);

switch ($POST['action']) {
    case 'add-category':
        if (! $permission->check('create:category')) {
            $ERROR = [
                'message' => TXT_NOTIFICATION_NO_RIGHT
            ];
        }

        else if (! isset($POST['title'], $POST['description'])) {
            $ERROR = [
                'message' => TXT_NOTIFICATION_CHECK_VALUES
            ];
        }else{
            try{
                $result = $categoryMapper->create([
                    'title' => $POST['title'],
                    'description' => $POST['description']
                ]);
                if($result != true){
                    $ERROR= [
                        "message"=>TXT_NOTIFICATION_ERROR . $categoryMapper->getPDOError()[2]
                    ];
                }
            }catch (Exception $e){
                $ERROR= ["message"=>TXT_NOTIFICATION_ERROR.$categoryMapper->getPDOError()[2]];
            }
        }

        //send the updated list to ihm
        $vue="list";
        $categories= $categoryMapper->getCategories();
        require '../../../../view/back-office/admin/blocks/admin_category_block.phtml';
        break;

    case 'alter-category':
        if (! $permission->check('edit:category')) {
            $data = [
                'success' => false,
                'message' => TXT_NOTIFICATION_NO_RIGHT
            ];
            break;
        }
        if (! isset($POST['title'], $POST['description'],
            $POST['idCategory'])
        ) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
            break;
        }

        $result = $categoryMapper->alter([
            'title' => $POST['title'],
            'description' => $POST['description'],
            'id' => $POST['idCategory']
        ]);

        if ($result !== false) {
            $data['success'] = true;
            $data['message'] = TXT_NOTIFICATION_ALTER_CATEGORY;
        } else {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_ERROR .
                $categoryMapper->getPDOError()[2];
        }
        echo json_encode($data);
        break;

    case 'del':
        if (! $permission->check('edit:category')) {
            $data = [
                'success' => false,
                'message' => TXT_NOTIFICATION_NO_RIGHT
            ];
            break;
        }

        if (! isset($POST['idCategory'])) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
            break;
        }

        $result = $categoryMapper->delete($POST['idCategory']);
        if ($result !== false) {
            $data['success'] = true;
            $data['message'] = TXT_NOTIFICATION_DEL_CATEGORY;
        } else {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_ERROR .
                $categoryMapper->getPDOError()[2];
        }
        echo json_encode($data);
        break;

    default:
        $data = ['success' => false, 'message' => TXT_NOTIFICATION_UNKNOWN_ACTION];
        echo json_encode($data);
        break;
}


