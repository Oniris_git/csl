<?php

use Vespula\Ldap\LdapSearch;
use Vespula\Ldap\LdapWrapper;
use CSLManager\Administration\Entity\User;
use CSLManager\Administration\Mapper\ProfileMapper;
use CSLManager\Administration\Mapper\UserMapper;

require '../../../../lib/bootstrap.php';

$uMapper = new UserMapper($connector);

$data = [];

$args = [
    'action' => FILTER_SANITIZE_STRING,
    'user-id' => FILTER_VALIDATE_INT,
    'user-upn' => FILTER_SANITIZE_STRING,
    'showHTML' => FILTER_SANITIZE_STRING,
    'email' => FILTER_SANITIZE_STRING,
    'user-profile' => FILTER_VALIDATE_INT,
];

$POST = filter_input_array(INPUT_POST, $args, false);

// to display form
if(isset($POST['showHTML'])){
    $profileMapper = new ProfileMapper($connector);
    $profiles = $profileMapper->getProfiles();
    switch ($POST['showHTML']):
        case "manual":
            include "../../../../view/back-office/admin/blocks/admin_user_block_manual.phtml";
            exit;
        case "ldap":
            include "../../../../view/back-office/admin/blocks/admin_user_block_ldap.phtml";
            exit;
        default:
            include "../../../../view/back-office/admin/blocks/admin_user_block_manual.phtml";
            exit;
    endswitch;
}

switch ($POST['action']) {
    case 'import':
        if (!$permission->check('create:users')) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_NO_RIGHT;
            break;
        }

        $ldap = new LdapSearch(new LdapWrapper(),
            $configLdap['ldapUri'],
            $configLdap['ldapBindOptions'],
            $configLdap['ldapOptions'],
            $configLdap['ldapPort']
        );

        //$ldap->setDefaultAttributes($configLdap['ldapAttributes']);
        //$ldap->setSortBy('cn');
        //$ldap->setTimestampFormat('ymd');
        $searchfilter = '(|(mail=*))';
        $ldapResults = $ldap->findAll($searchfilter);
        $usersToImport = [];
        foreach ($ldapResults as $ldapResult) {
            // Skip existing users
            $i=0;
            if ($uMapper->userExist($ldapResult['mail'])) {
                $i++;
                continue;
            }
            try {
                $usersToImport[] = new User([
                    'upn' => $ldapResult['uid'],
                    //'id_profile' => ($ldapResult['postofficebox'] == 'Enseignant') ? 'gest' : 'etu',
                    'id_profile' => 3,
                    'max_difficulty' => 1,
                    'email' => $ldapResult['mail']
                ]);
            } catch (Exception $e) {
                $data['success'] = false;
                $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
                break;
            }
        }

        try {
            $result  = $uMapper->import($usersToImport);
            echo ($result);
        } catch (\Exception $e) {
        }

        $data['success'] = true;
        $data['message'] = TXT_NOTIFICATION_IMPORT_SUCCESS;
        break;
    case 'user-add-manual':
        if (!$permission->check('create:users')) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_NO_RIGHT;
            break;
        }

        if (!isset($POST['user-upn']) || !isset($POST['user-profile'])) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
            break;
        }

        try {
            $firstNameName=explode(".",$POST['user-upn']);
            if(sizeof($firstNameName) != 1){
                $password = createPassword();
                $passwordHash = password_hash($password, PASSWORD_DEFAULT);
                $token = generateToken('form');
                $user=[
                    'upn'=> strtolower ($POST['user-upn']),
                    'firstName'=> strtoupper(substr( $firstNameName[0], 0, 1)).strtolower(substr($firstNameName[0], 1)),
                    'name'=>strtoupper(substr( $firstNameName[1], 0, 1)).strtolower(substr($firstNameName[1], 1)),
                    'id_profile'=> $POST['user-profile'],
                    'auth' => "manual",
                    'password' => $passwordHash,
                    'max_difficulty' => 1,
                    'request' => date("Y-m-d H:i:s",time()),
                    'open' => 1,
                    'token' => $token,
                    'email' => $POST['email']
                ];
                try{
                    $userEntity = new User($user);
                    $uMapper->create($user);
                    $request = 'init';
                    sendMail($userEntity, $connector, $request);
                    $data['success'] = true;
                    $data['message'] = TXT_NOTIFICATION_ADD_USER;
                    break;
                }catch(Exception $e){
                    $data['success'] = false;
                    $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
                    break;
                }
            }else{
                $data['success'] = false;
                $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
                break;
            }

        } catch (Exception $e) {
            $data['success'] = false;
            $data['message'] = $e->getMessage();
            break;
        }

    case 'user-add-ldap':
        if (!$permission->check('create:users')) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_NO_RIGHT;
            break;
        }

        if (!isset($POST['user-upn']) || !isset($POST['user-profile'])) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
            break;
        }

        try {

            $ldap = new LdapSearch(new LdapWrapper(),
                $configLdap['ldapUri'],
                $configLdap['ldapBindOptions'],
                $configLdap['ldapOptions'],
                $configLdap['ldapPort']
            );

            //$ldap->setDefaultAttributes($configLdap['ldapAttributes']);
            //$ldap->setSortBy('cn');
            //$ldap->setTimestampFormat('ymd');
            $searchfilter = '(|('.$configLdap['ldapAttributes'][5].'='.$POST['user-upn'].'))';

            $result = $ldap->find($searchfilter);

            $firstNameName=explode(".",$POST['user-upn']);
            if(sizeof($firstNameName) != 1){
                $user=[
                    'upn'=> $POST['user-upn'],
                    'firstName'=>$firstNameName[0],
                    'name'=>$firstNameName[1],
                    'id_profile'=> $POST['user-profile'],
                    'auth' => "ldap",
                    'email' => $result['mail']
                ];
                $uMapper->create($user);
                $data['success'] = true;
                $data['message'] = TXT_NOTIFICATION_ADD_USER;
            }else{
                $data['success'] = false;
                $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
                break;
            }

        } catch (Exception $e) {
            var_dump($e);die();
            $data['success'] = false;
            $data['message'] = $e->getMessage();
        }
        break;
    default:
        $data = ['success' => false, 'message' => TXT_NOTIFICATION_UNKNOWN_ACTION];
        break;
}

echo json_encode($data);
