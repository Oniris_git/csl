<?php

use CSLManager\Administration\Mapper\SubCategoryMapper;

require '../../../../lib/bootstrap.php';

$data = [];

$args = [
    'action' => FILTER_SANITIZE_STRING,
    'idSubcategory' => FILTER_VALIDATE_INT,
    'title' => FILTER_SANITIZE_STRING,
    'description' => FILTER_SANITIZE_STRING,
];

$POST = filter_input_array(INPUT_POST, $args, false);

switch ($POST['action']) {
    case 'add-subcategory':
        $subCategoryMapper = new subCategoryMapper($connector);
        if (!$permission->check('create:subcategory')) {
            $ERROR = [
                'message' => TXT_NOTIFICATION_NO_RIGHT
            ];
        }

        else if (!isset($POST['title']) || !isset($POST['description'])) {
            $ERROR = [
                'message' => TXT_NOTIFICATION_CHECK_VALUES
            ];
        }

        try {
            $result = $subCategoryMapper->create([
                'title' => $POST['title'],
                'description' => $POST['description']
            ]);
            if($result != true){
                $ERROR= [
                    "message"=>TXT_NOTIFICATION_ERROR . $subCategoryMapper->getPDOError()[2]
                ];
            }
        } catch (Exception $e) {
            $ERROR = [
                'message' => $subCategoryMapper->getPDOError()[2]
            ];
        }

        $subcategories= $subCategoryMapper->getSubCategories();
        require '../../../../view/back-office/admin/blocks/admin_sub_category_block.phtml';

        break;

    case 'alter-subcategory':
        if (!$permission->check('edit:subcategory')) {
            noRights($data);
            break;
        }
        $GET = filter_input_array(INPUT_GET, $args, false);
        if (!isset($POST['title']) || !isset($POST['description'])) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
            break;
        }

        $subCategoryMapper = new SubCategoryMapper($connector);
        try {
            $subCategoryMapper->alter([
                'title' => $POST['title'],
                'description' => $POST['description'],
                'id' => $POST['idSubcategory']
            ]);

            $data['success'] = true;
            $data['message'] = TXT_NOTIFICATION_ALTER_SUB_CATEGORY;
        } catch (\Exception $e) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_ERROR . $e->getMessage();

        }
        echo json_encode($data);
        break;

    case 'del':
        if (! $permission->check('edit:subcategory')) {
            $data = [
                'success' => false,
                'message' => TXT_NOTIFICATION_NO_RIGHT
            ];
            break;
        }

        if (! isset($POST['idSubcategory'])) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
            break;
        }
        $subCategoryMapper = new SubCategoryMapper($connector);
        $result = $subCategoryMapper->delete($POST['idSubcategory']);
        if ($result !== false) {
            $data['success'] = true;
            $data['message'] = TXT_NOTIFICATION_DEL_SUB_CATEGORY;
        } else {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_ERROR .
            $subCategoryMapper->getPDOError()[2];

        }
        echo json_encode($data);
        break;
}


