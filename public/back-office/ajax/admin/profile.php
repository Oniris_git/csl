<?php

use CSLManager\Administration\Mapper\ProfileMapper;

require '../../../../lib/bootstrap.php';

$data = [];

$args = [
    'action' => FILTER_SANITIZE_STRING,
    'profile-id' => FILTER_VALIDATE_INT,
    'label' => FILTER_SANITIZE_STRING,
    'description' => FILTER_SANITIZE_STRING
];

$POST = filter_input_array(INPUT_POST, $args, false);

switch ($POST['action']) {
    case 'add-profile':
        $profileMapper = new ProfileMapper($connector);
        if (!$permission->check('create:profiles')) {
            $ERROR = [
                'message' => TXT_NOTIFICATION_NO_RIGHT
            ];
        }
        else if (!isset($POST['label']) || !isset($POST['description'])) {
            $ERROR = [
                'message' => TXT_NOTIFICATION_CHECK_VALUES
            ];
        }else {
            try {
                $result = $profileMapper->create([
                    'label' => $POST['label'],
                    'description' => $POST['description']
                ]);
            } catch (\Exception $e) {
                $ERROR = [
                    'message' => $profileMapper->getPDOError()[2]
                ];
            }
        }
        $profiles= $profileMapper->getProfiles();
        require '../../../../view/back-office/admin/blocks/admin_profile_block.phtml';
        break;
    default:
        $data = ['success' => false, 'message' => TXT_NOTIFICATION_UNKNOWN_ACTION];
        break;

    case 'alter-profile':
        if (!$permission->check('create:profiles')) {
            noRights($data);
            break;
        }
        $GET = filter_input_array(INPUT_GET, $args, false);
        if (!isset($POST['title']) || !isset($POST['description'])) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
            break;
        }

        $profileMapper = new ProfileMapper($connector);
        try {
            $profileMapper->alter([
                'title' => $POST['title'],
                'description' => $POST['description'],
                'id' => $POST['idProfile']
            ]);

            $data['success'] = true;
            $data['message'] = TXT_NOTIFICATION_ALTER_PROFILE;
        } catch (\Exception $e) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_ERROR . $e->getMessage();
        }
        echo json_encode($data);
        break;


    case 'del-profile':
        if (! $permission->check('create:profiles')) {
            $data = [
                'success' => false,
                'message' => TXT_NOTIFICATION_NO_RIGHT
            ];
            break;
        }

        if (! isset($POST['profile-id'])) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
            break;
        }
        $profileMapper = new ProfileMapper($connector);
        $result = $profileMapper->delete($POST['profile-id']);
        if ($result !== false) {
            $data['success'] = true;
            $data['message'] = TXT_NOTIFICATION_DEL_PROFILE;
        } else {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_ERROR.
                $profileMapper->getPDOError()[2];
        }
        echo json_encode($data);
        break;
}

