<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use CSLManager\Administration\Mapper\UserMapper;
use CSLManager\Administration\Stat;

require '../../../lib/bootstrap.php';

$args = [
    'chart' => FILTER_SANITIZE_STRING,
    'global' => FILTER_VALIDATE_INT,
    'user' => FILTER_VALIDATE_INT
];

$GET = filter_input_array(INPUT_GET, $args, false);

$stat = new Stat($connector);
$data = [];
$upn = $session->getValue('upn');

if (isset($GET['global']) && $GET['global'] == 1
    && $permission->check('print:stats')
) {
    $upn = null;
}

if (isset($GET['user']) && $permission->check('print:others_stats')) {
    $uMapper = new UserMapper($connector);
    try {
        $user = $uMapper->getUserById($GET['user']);
        $upn = $user->getUpn();
    } catch (\Exception $e) {
    }
}

switch ($GET['chart']) {
    case 'chartwapermonth':
        $data = $stat->getWorkshopAttemptPerMonth($upn);
        //$data = array_column($data, 'count', 'month');
        break;
    default:
        break;
}

header('Content-Type: application/json;charset=utf8');
echo json_encode($data);
