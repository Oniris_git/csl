<?php

use CSLManager\Administration\Mapper\WorkshopMapper;
use CSLManager\Administration\Mapper\CategoryMapper;

require '../../../lib/bootstrap.php';

$wMapper = new WorkshopMapper($connector);

$filter = (isset($_POST["filter"])) ? $_POST["filter"] : NULL;
if ($filter) {
   $i=0;
    $workshops = $wMapper->getWorkshopsByFilter($filter);
    switch ($filter){
        case 'id_room':
            $tri = TXT_LIST_WORKSPACE_ROOM;
            break;
        case 'id_category':
            $tri= TXT_LIST_WORKSPACE_CATEGORY;
            break;
        case 'id_sub_category':
            $tri = TXT_LIST_WORKSPACE_SUB_CATEGORY;
            break;
        case 'id_species':
            $tri = TXT_LIST_WORKSPACE_SPECIE;
            break;
        case 'count_to_validate':
            $tri = TXT_LIST_WORKSPACE_COUNT_TO_VALIDATE;
            break;
        case 'id_difficulty':
            $tri = TXT_LIST_WORKSPACE_DIFFICULTY;
            break;
        case 'id_priority_level':
            $tri = TXT_LIST_WORKSPACE_RANK;
            break;
        case 'id_discipline':
            $tri = TXT_LIST_WORKSPACE_DISCIPLINE;
            break;
    }

    require '../../../view/back-office/blocks/ajax/list_workshop.phtml';
}