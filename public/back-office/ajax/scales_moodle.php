<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use CSLManager\Administration\Helper\Config;
use CSLManager\Administration\Mapper\ScaleMapper;
use CSLManager\Administration\Mappers\ConfigMapper;

require '../../../lib/bootstrap.php';

$args = [
    'idScaleMoodle' => FILTER_SANITIZE_STRING
];

$POST = filter_input_array(INPUT_POST, $args, false);


if(isset($_POST['idScaleMoodle'])){
    //start time
    $start = microtime(true);

//Moodle values in BDD
    $configMapper = new ConfigMapper($connector);
    $lang = $configMapper->selectConfigByName('view', 'lang')['value'];
    $post_data = ['scaleid' => $POST['idScaleMoodle']];

//access to Moodle
    $serverUrl = $configDB['domainMoodle'] . '/webservice/rest/server.php?wstoken=' . $configDB['tokenMoodle']
        . '&wsfunction=core_competency_get_scale_values'
        . '&moodlewsrestformat=json';

    $curl = curl_init($serverUrl);

//access to data and decode
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post_data));
    $post = json_decode(curl_exec($curl));
    curl_close($curl);

    if (isset($post->exception)) {
        $data = [
            'success' => false,
            'message' => TXT_LOG_ERROR_MOODLE_NOT_FOUND
        ];
        echo  json_encode($data);
        exit;
    };

    $scaleMapper = new ScaleMapper($connector);
    try {
        foreach ($post as $value){
            $string = $value->name;
            $data = [
                'scale-code' =>initiales($string),
                'scale-label' =>$value->name
            ];
            $scaleMapper->create($data);
        }

        $data = [
            "success" => true,
            "message" => TXT_ADMIN_EVALUATION_MOODLE_ADD_SUCCESS
        ];
    } catch (Exception $e) {
        $data = [
            'success' => false,
            'message' => $e->getMessage()
        ];
    }
    echo  json_encode($data);
}else{
    $data = [
        'success' => false,
        'message' => TXT_ADMIN_EVALUATION_MOODLE_ADD_ERROR
    ];
}

