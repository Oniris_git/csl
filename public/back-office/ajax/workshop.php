<?php

/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use CSLManager\Administration\Entity\Workshop;
use CSLManager\Administration\Mapper\WorkshopAttemptMapper;
use CSLManager\Administration\Mapper\WorkshopMapper;

require '../../../lib/bootstrap.php';

$data = [];

$args = [
    'action' => FILTER_SANITIZE_STRING,
    // Add
    'add-label' => FILTER_SANITIZE_STRING,
    'add-full_label' => FILTER_SANITIZE_STRING,
    'add-description' => FILTER_SANITIZE_STRING,
    'add-room' => FILTER_VALIDATE_INT,
    'add-idMoodle' => FILTER_VALIDATE_INT,
    'add-difficulty' => FILTER_VALIDATE_INT,
    'add-count' => FILTER_VALIDATE_INT,
    'add-duration' => FILTER_VALIDATE_INT,
    'add-ceiling_duration' => FILTER_VALIDATE_INT,
    'add-scale_type' => FILTER_VALIDATE_INT,
    'add-category' => FILTER_VALIDATE_INT,
    'add-sub-category' => FILTER_VALIDATE_INT,
    'add-discipline' => FILTER_VALIDATE_INT,
    'add-priority_level' => FILTER_VALIDATE_INT,
    'add-species' => FILTER_VALIDATE_INT,
    // Alter
    'alter-label' => FILTER_SANITIZE_STRING,
    'alter-full_label' => FILTER_SANITIZE_STRING,
    'alter-description' => FILTER_SANITIZE_STRING,
    'alter-id' => FILTER_VALIDATE_INT,
    'alter-room' => FILTER_VALIDATE_INT,
    'alter-idMoodle' => FILTER_VALIDATE_INT,
    'alter-difficulty' => FILTER_VALIDATE_INT,
    'alter-count_to_validate' => FILTER_VALIDATE_INT,
    'alter-duration_min' => FILTER_VALIDATE_INT,
    'alter-duration_max' => FILTER_VALIDATE_INT,
    'alter-scale_type' => FILTER_VALIDATE_INT,
    'alter-category' => FILTER_VALIDATE_INT,
    'alter-sub_category' => FILTER_VALIDATE_INT,
    'alter-discipline' => FILTER_VALIDATE_INT,
    'alter-priority_level' => FILTER_VALIDATE_INT,
    'alter-species' => FILTER_VALIDATE_INT,
    'note-tp' => FILTER_SANITIZE_STRING,
    // Delete
    'del_id' => FILTER_VALIDATE_INT
    //'duration-workshop_attempt' => FILTER_VALIDATE_INT,

];

$POST = filter_input_array(INPUT_POST, $args, false);
$POST = array_map('trim', $POST);

$wMapper = new WorkshopMapper($connector);

switch ($POST['action']) {
    case 'alter':
        if (!$permission->check('edit:workspace')) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_NO_RIGHT;
            break;
        }
        if (!isset($POST['alter-id']) || !isset($POST['alter-label']) || !isset($POST['alter-description'])) {
            $data['message'] = TXT_WORKSHOP_PARAMETER_MISSING;
            $data['success'] = false;
            break;
        }

        $wData = [
            'id' => $POST['alter-id'],
            'label' => $POST['alter-label'],
            'full_label' => $POST['alter-full_label'],
            'description' => $POST['alter-description'],
            'id_moodle' => (isset($POST['alter-idMoodle'])) ? $POST['alter-idMoodle'] : '',
            'id_room' => (isset($POST['alter-room'])) ? $POST['alter-room'] : '',
            'id_difficulty' => (isset($POST['alter-difficulty'])) ? $POST['alter-difficulty'] : '',
            'count_to_validate' => (isset($POST['alter-count_to_validate'])) ? $POST['alter-count_to_validate'] : '',
            'duration' => (isset($POST['alter-duration_min'])) ? $POST['alter-duration_min'] : '',
            'ceiling_duration' => (isset($POST['alter-duration_max'])) ? $POST['alter-duration_max'] : '',
            'deleted' => '', // On ne modifie pas le témoin suppression
            'available' => '', // On ne modifie pas le témoin disponible
            'postes_requis' => '',
            'id_scale_type' => (isset($POST['alter-scale_type'])) ? $POST['alter-scale_type'] : '',
            'id_category' => intval($POST['alter-category']),
            'id_sub_category' => $POST['alter-sub_category'],
            'id_discipline' => $POST['alter-discipline'],
            'id_priority_level' => $POST['alter-priority_level'],
            'id_species' => $POST['alter-species']
        ];

        $alteredWorkstation = new Workshop($wData);

        try {
            $wMapper->alter($alteredWorkstation);
            $data['success'] = true;
            $data['message'] = TXT_WORKSHOP_ALTERED;
        } catch (\Exception $e) {
            $data['success'] = false;
            $data['message'] = $e->getMessage();
        }
        break;
////////////////////////////////////////////////////////////////////////
    // Création d'un poste de travail
    case 'add':
        if (!$permission->check('create:workspace')) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_NO_RIGHT;
            break;
        }

        if (!isset($POST['add-label']) || !isset($POST['add-full_label']) || !isset($POST['add-description']) || !isset($POST['add-scale_type'])
        ) {
            $data['message'] = TXT_WORKSHOP_PARAMETER_MISSING;
            $data['success'] = false;
            break;
        }

        $wData = [
            'label' => $POST['add-label'],
            'full_label' => $POST['add-full_label'],
            'description' => $POST['add-description'],
            'deleted' => 0,
            'available' => 1,
            'id_moodle' => (isset($POST['add-idMoodle'])) ? $POST['add-idMoodle'] : '',
            'id_room' => (isset($POST['add-room'])) ? $POST['add-room'] : '',
            'id_difficulty' => (isset($POST['add-difficulty'])) ? $POST['add-difficulty'] : '',
            'duration' => (isset($POST['add-duration'])) ? $POST['add-duration'] : '',
            'count_to_validate' => (isset($POST['add-count'])) ? $POST['add-count'] : '',
            'id_scale_type' => $POST['add-scale_type'],
            'id_category' => $POST['add-category'],
            'id_sub_category' => $POST['add-sub-category'],
            'id_discipline' => $POST['add-discipline'],
            'id_priority_level' => $POST['add-priority_level'],
            'id_species' => $POST['add-species'],
            'ceiling_duration' => (isset($POST['add-ceiling_duration'])) ? $POST['add-ceiling_duration'] : '',
            'postes_requis' => '',
        ];

        $newWorkshop = new Workshop($wData);
        try {
            $wMapper->store($newWorkshop);
            $data['success'] = true;
            $data['message'] = TXT_WORKSHOP_CREATED;
        } catch (\Exception $e) {
            $data['success'] = false;
            $data['message'] = $e->getMessage();
        }

        break;
////////////////////////////////////////////////////////////////////////
    // Fin de tp
    ///
    /*case 'fin-tp':
        if (!$permission->check('edit:tp')) {
            $data['success'] = false;
            $data['message'] = 'Vous n\'avez pas les droits';
            break;
        }

        if (!isset($POST['mod-id']) || !isset($POST['duree-tp']) || !isset($POST['note-tp'])) {
            $data['message'] = 'Paramètres manquants';
            $data['success'] = false;
            break;
        }

        $tpMapper = new WorkshopAttemptMapper($connector);

        if (!is_numeric($POST['mod-id']) || !is_numeric($POST['duree-tp']) || $POST['duree-tp'] <= 0
        ) {
            $data['success'] = false;
            $data['message'] = 'Valeurs incorrectes';
        } else {
            if ($tpMapper->setFin($POST['mod-id'], $POST['duree-tp'], $POST['note-tp'])) {
                $data['success'] = true;
                $data['message'] = "TP n° " . $POST['mod-id'] . " terminé avec succès.";
            } else {
                $data['success'] = false;
                $data['message'] = 'Échec de la mise à jour';
            }
        }
        break;*/

    case 'del':
        if (!$permission->check('edit:workspace')) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_NO_RIGHT;
            break;
        }

        if (!isset($POST['del_id'])) {
            $data['success'] = false;
            $data['message'] = TXT_WORKSHOP_PARAMETER_MISSING;
            break;
        }

        try {
            $wMapper->delete($POST['del_id']);
            $data['success'] = true;
            $data['message'] = TXT_WORKSHOP_DELETED;
        } catch (\Exception $e) {
            $data['success'] = false;
            $data['message'] = $e->getMessage();
        }

        break;
    default:
        $data['success'] = false;
        $data['message'] = TXT_NOTIFICATION_UNKNOWN_ACTION;
        break;
}

echo json_encode($data);
