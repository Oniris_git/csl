<?php 

use CSLManager\Administration\Mapper\UserMapper;

require '../../../lib/bootstrap.php';

$search = (isset($_POST["search"])) ? $_POST["search"] : NULL;
$search2 = (isset($_POST["search2"])) ? $_POST["search2"] : NULL;

if($search){
    $uMapper = new UserMapper($connector);
    $users=$uMapper->search($search);
    $viewable=false;
    require '../../../view/back-office/blocks/ajax/list_user_filter.phtml';
}elseif ($search2){
    $uMapper = new UserMapper($connector);
    $users=$uMapper->search($search2);

    require '../../../view/back-office/blocks/list_user.phtml';
}