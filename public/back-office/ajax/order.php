<?php

use CSLManager\Administration\Mapper\UserMapper;

require '../../../lib/bootstrap.php';


$page = filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT);
$page = ($page) ?: 1;

$totalUsers = 1;

$order = (isset($_POST["order"])) ? $_POST["order"] : NULL;
if($order){
    $uMapper = new UserMapper($connector);
    switch ($order){
        case 'filter_name_up_filter':
            $order= " ORDER BY u.name ASC ";
            $users=$uMapper->getUsers($order);
            $totalUsers = $uMapper->getTotalUsers();
            require '../../../view/back-office/blocks/ajax/list_user_filter.phtml';
            break;
        case 'filter_name_down_filter':
            $order= ' ORDER BY u.name DESC ';
            $users=$uMapper->getUsers($order);
            $totalUsers = $uMapper->getTotalUsers();
            require '../../../view/back-office/blocks/ajax/list_user_filter.phtml';
            break;
        case 'filter_profile_up_filter':
            $order= ' ORDER BY p.label ASC ';
            $users=$uMapper->getUsers($order);
            $totalUsers = $uMapper->getTotalUsers();
            require '../../../view/back-office/blocks/ajax/list_user_filter.phtml';
            break;
        case 'filter_profile_down_filter':
            $order= ' ORDER BY p.label DESC ';
            $users=$uMapper->getUsers($order);
            $totalUsers = $uMapper->getTotalUsers();
            require '../../../view/back-office/blocks/ajax/list_user_filter.phtml';
            break;
        case 'filter_name_up':
            $order= " ORDER BY u.name ASC ";
            $users=$uMapper->_paginate()->_offset(($page - 1) * 25)->getUsers($order);
            $totalUsers = $uMapper->getTotalUsers();
            $totalPages = ceil($totalUsers / 25);
            require '../../../view/back-office/blocks/list_user.phtml';
            break;
        case 'filter_name_down':
            $order= ' ORDER BY u.name DESC ';
            $users=$uMapper->_paginate()->_offset(($page - 1) * 25)->getUsers($order);
            $totalUsers = $uMapper->getTotalUsers();
            $totalPages = ceil($totalUsers / 25);
            require '../../../view/back-office/blocks/list_user.phtml';
            break;
        case 'filter_profile_up':
            $order= ' ORDER BY p.label ASC ';
            $users=$uMapper->_paginate()->_offset(($page - 1) * 25)->getUsers($order);
            $totalUsers = $uMapper->getTotalUsers();
            $totalPages = ceil($totalUsers / 25);
            require '../../../view/back-office/blocks/list_user.phtml';
            break;
        case 'filter_profile_down':
            $order= ' ORDER BY p.label DESC ';
            $users=$uMapper->_paginate()->_offset(($page - 1) * 25)->getUsers($order);
            $totalUsers = $uMapper->getTotalUsers();
            $totalPages = ceil($totalUsers / 25);
            require '../../../view/back-office/blocks/list_user.phtml';
            break;
    }


}