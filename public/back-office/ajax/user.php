<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use CSLManager\Administration\Mapper\ProfileMapper;
use CSLManager\Administration\Mapper\UserMapper;

require '../../../lib/bootstrap.php';

$data = [];

$args = [
    'action' => FILTER_SANITIZE_STRING,
    // Action modification
    'alter-profile' => FILTER_VALIDATE_INT,
    'alter-upn' => FILTER_VALIDATE_EMAIL,
    'alter-id' => FILTER_VALIDATE_INT,
    //Search
    'q' => FILTER_SANITIZE_STRING
];

$POST = filter_input_array(INPUT_POST, $args, false);

$uMapper = new UserMapper($connector);

switch ($POST['action']) {
    case 'alter':
        if (!$permission->check('edit:users')) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_NO_RIGHT;
            break;
        }

        if (!isset($POST['alter-upn']) || !isset($POST['alter-id']) || !isset($POST['alter-profile'])) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_CHECK_VALUES;
            break;
        }

        $profileMapper = new ProfileMapper($connector);

        if (!in_array($POST['alter-profile'], array_column($profileMapper->getProfiles(), 'id'))) {
            $data['success'] = false;
            $data['message'] = TXT_LOG_LOGIN_UNKNOWN_USER;
            break;
        }

        try {
            $user = $uMapper->getUserById($POST['alter-id']);
            $user->setIdProfile($POST['alter-profile']);
            $uMapper->alter($user);
            $data['success'] = true;
            $data['message'] = TXT_NOTIFICATION_ALTER_SAVED;
        } catch (\Exception $e) {
            $data['success'] = false;
            $data['message'] = $e->getMessage();
        }

        break;

    case 'search':
        if (!$permission->check('print:users')) {
            $data['success'] = false;
            $data['message'] = TXT_NOTIFICATION_NO_RIGHT;
            break;
        }

        // On retire les caractères spéciaux « wildcard » SQL
        $q = trim(str_replace(['%', '_'], '', $POST['q']));

        if (empty($q)) {
            $data['success'] = false;
            $data['message'] = TXT_ADMIN_MAILER_EMPTY;
            break;
        }

        try {
            $data['users'] = $uMapper->search($q);
            foreach ($data['users'] as &$u) {
                $u = (array)$u;
            }
            unset($u);
            $data['total_users'] = count($data['users']);
            $data['success'] = true;
            $data['message'] = TXT_USER_SEARCH_DONE_ON.$q;
        } catch (\Exception $e) {
            $data['success'] = false;
            $data['message'] = $e->getMessage();
        }
        break;

    default:
        $data['success'] = false;
        $data['message'] = TXT_NOTIFICATION_UNKNOWN_ACTION;
        break;
}

echo str_replace(['\\u0000', '*'], '', json_encode($data));
