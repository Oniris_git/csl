<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 21/11/2018
 * Time: 11:28
 */

use CSLManager\Administration\Mapper\UserMapper;
use CSLManager\Administration\Mapper\WorkshopAttemptMapper;
use CSLManager\Administration\Mapper\WorkshopMapper;
use CSLManager\Administration\Mapper\PriorityLevelMapper;
use CSLManager\Administration\Stat;

require '../../../lib/bootstrap.php';
require __DIR__.'/../statFunctions.php';

$filter = (isset($_POST["choice"])) ? $_POST["choice"] : NULL;
$order = null;
$i=0;

$stat = new Stat($connector);
$uMapper = new UserMapper($connector);
$waMapper = new WorkshopAttemptMapper($connector);
$wsMapper = new WorkshopMapper($connector);
$plMapper = new PriorityLevelMapper($connector);
$stats = new StdClass;

//to catch the user
if(isset($_POST['idUser'])){
    $idUser = $_POST['idUser'];
    $user = $uMapper->getUserById($idUser);
    $upn = $user->getUpn();
    $chartType = 'user';
}else if(isset($_GET['user'])){
    $idUser = $_GET['user'];
    $chartType = 'user';
}else{
    $idUser=[];
    $upn=$session->getValue('upn');;
    $user=$uMapper->getUserByUpn($upn);
    $idUser=$user->getId();
    $chartType = '';
}

$stats->total = $stat->getCountWorkshopAttempt($upn);
$stats->done = $stat->getWorkshopAttemptDone($upn);
$workstations = $wsMapper->getWorkshops();

$workshopAttemptList= $waMapper->selectWorkshopAttemptByUser($idUser, $filter);

foreach ($workshopAttemptList as $wa) {
    $timeByWa[$i] = $stat->getTotalTimeByUserByWorkshopAttempt($idUser, $wa->getId());
    $i++;
}

$idWaDistinct = $stat->getDistinctWorkshopAttemptByUser($idUser, $filter);
$countWaDistinct = count($idWaDistinct);

//get the workshop name for each workshop attempt to do in modal
$WSTodo = $stat->selectWorkshopNotDoneByUser($idUser, $filter);
$WSTodoSize = sizeof($WSTodo);

//get the curent user's total time for the workshop
$totalTime = $stat->getTotalTime($type = 'user', $champs = 'id', $value = $idUser);
if ($stats->total !== 0) {
    $averageTime = round(($totalTime / $stats->total));
} else {
    $averageTime = 0;
}
//More details in stat_functions
$distinctWa = getDistinctWa($stat, $idUser, $idWaDistinct,$wsMapper);
$lstWa = getWorkshopAttempt($waMapper, $idWaDistinct);
$lstWaByWs = getLstWaByWs($waMapper, $lstWa, $idUser);
$lstBestScale = getBestScale($lstWaByWs, $idUser, $stat);
$lstTime = getLstTime($lstBestScale,$stat,$idUser);

//get priority levels
$priorityLevels = $plMapper->getPriorityLevels();


require '../../../view/back-office/blocks/ajax/priorityLevel_filter.phtml';
