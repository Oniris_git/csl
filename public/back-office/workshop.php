<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use CSLManager\Administration\Mapper\CategoryMapper;
use CSLManager\Administration\Mapper\DisciplineMapper;
use CSLManager\Administration\Mapper\ScaleMapper;
use CSLManager\Administration\Mapper\PromoMapper;
use CSLManager\Administration\Mapper\PriorityLevelMapper;
use CSLManager\Administration\Mapper\SpecieMapper;
use CSLManager\Administration\Mapper\SubCategoryMapper;
use CSLManager\Administration\Mapper\WorkshopMapper;
use CSLManager\Administration\Mapper\RoomMapper;


require '../../lib/bootstrap.php';

$workshops = [];
$printable = false;
$viewable = false;

if (!$permission->check('print:workspace')) {
    include __DIR__.'/../403.html';
    exit();
}

$wMapper = new WorkshopMapper($connector);

$w_id = (int)filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

if ($w_id !== 0) {
    try {
        $workshop = $wMapper->getWorkshopById($w_id);

        $scheme = (!empty($_SERVER['HTTPS'])) ? 'https://' : 'http://';
        $url = $scheme . $_SERVER['SERVER_NAME']
            . '/index.php?poste='
            . $workshop->getLabel();

        $QRCode = new \Endroid\QrCode\QrCode($url);

        $workshop->setQrcode(
            $QRCode->writeDataUri()
        );
        unset($QRCode);

        $scaleMapper = new ScaleMapper($connector);
        $scale = $scaleMapper->getScaleTypes($workshop->getIdScaleType());

        $printable = true;
        $viewable = true;
    } catch (\Exception $e) {
        $printable = false;
        $viewable = false;
        $ERROR[] = $e->getMessage();
        $workshops = $wMapper->getWorkshops();
    }
} else {
    $workshops = $wMapper->getWorkshops();
}

$roomMapper= new RoomMapper($connector);
$roomList = $roomMapper->getRooms();

$difficultyMapper = new PromoMapper($connector);
$difficulty = $difficultyMapper->getPromos();

$scaleTypeMapper = new ScaleMapper($connector);
$scaleTypes = $scaleTypeMapper->getScaleTypes();

$categoryMapper= new CategoryMapper($connector);
$category = $categoryMapper->getCategories();

$subCategoryMapper= new SubCategoryMapper($connector);
$subCategory = $subCategoryMapper->getSubCategories();

$disciplineMapper= new DisciplineMapper($connector);
$discipline = $disciplineMapper->getDisciplines();

$plMapper= new PriorityLevelMapper($connector);
$priorityLevel = $plMapper->getPriorityLevels();

$speciesMapper= new SpecieMapper($connector);
$species = $speciesMapper->getSpecies();


$contentPage = 'workshop.phtml';

include '../../view/skel.phtml';
