<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use CSLManager\Administration\CSV;
use CSLManager\Administration\Mapper\PriorityLevelMapper;
use CSLManager\Administration\Mapper\ScaleMapper;
use CSLManager\Administration\Mapper\UserMapper;
use CSLManager\Administration\Stat;
use CSLManager\Administration\Mapper\WorkshopMapper;
use CSLManager\Administration\Mapper\WorkshopAttemptMapper;

require __DIR__ . '/../../lib/bootstrap.php';
require __DIR__.'/statFunctions.php';

//instantiation of mappers
$stat = new Stat($connector);
$uMapper = new UserMapper($connector);
$wsMapper = new WorkshopMapper($connector);
$waMapper = new WorkshopAttemptMapper($connector);
$plMapper = new PriorityLevelMapper($connector);
$stats = new StdClass;
$scaleMapper = new ScaleMapper($connector);

//request variables in table
$args = [
    'csv' => FILTER_VALIDATE_INT,
    'chart' => FILTER_SANITIZE_STRING,
    'global' => FILTER_VALIDATE_INT,
    'user' => FILTER_VALIDATE_INT
];
$GET = filter_input_array(INPUT_GET, $args, false);

$upn = $session->getValue('upn');

$order = null;
$filter= null;

if ($permission->check('print:others_stats') && isset($GET['user'])) {
    $user = $uMapper->getUserById($GET['user']);
    $upn = $user->getUpn();
    $idUser = $_GET['user'];
}else if ($permission->check('print:others_stats')){
    $upn = $session->getValue('upn');
    $user = $uMapper->getUserByUpn($upn);
    $idUser = $user->getId();
    $ajaxJSChart = '&user='.$idUser;
}

if (isset($GET['csv']) && $GET['csv'] === 1) {
    $filename = 'csv_';
    if (isset($GET['global']) && $GET['global'] === 1 && $permission->check('print:stats')) {
        $upn = null;
        $filename .= 'global_';
    }

    if (!isset($GET['global']) && isset($GET['user']) && $permission->check('print:stats')) {
        $filename .= $GET['user'] . '_';
    }

    $data = [];
    $csv = new CSV($filename . date('Y-m-d'));
    switch ($GET['chart']) {
        case 'wsPerMonth':
            $results = $stat->getWorkshopAttemptPerMonth($upn);

            if(isset($results['datasets']['labels']) && $results['datasets']['labels'] != null):
                $data[] = $results['labels'];
                array_unshift($data[0], 'Promos');
                $head = array_pop($data);
                foreach ($results['datasets'] as $result) {
                    array_unshift($result['data'], $result['label']);
                    $data[] = $result['data'];
                }
                $csv->setHead($head);
            else:
                $csv->setHead($data);
                $data= [];
            endif;
            break;
        case 'full':
            $data = $stat->getWorkshopAttemptTotal();
            $csv->setHead(['Date', TXT_START, 'Id', TXT_NAME_FIELD,
                TXT_FIRST_NAME, 'Promo', 'URL', TXT_EXERCICE, TXT_WORKSPACE_FILTER_ROOM, TXT_STAT_TIME,
                TXT_SCALE, '']);
            break;
        case 'userFull':
            $data = $stat->getWorkshopAttemptTotalByUser($upn);
            $csv->setHead(['Date', TXT_START,
                'Id', TXT_FIRST_NAME,TXT_NAME_FIELD, 'Promo',
                'URL', TXT_EXERCICE, TXT_WORKSPACE_FILTER_ROOM, TXT_DURATION, TXT_SCALE]);
            break;
        default:
            $data = [];
            break;
    }

    $csv->setData($data);
    $csv->output();
    exit();
}

if (isset($GET['global']) && $GET['global'] == 1 && $permission->check('print:stats')){
    $ajaxJSChart = '&global=1';
    $chartType = 'global';
    $upn = null;
}else if (!isset($GET['global']) && isset($GET['user']) && $permission->check('print:others_stats')){
    $ajaxJSChart = '&user=' . $GET['user'];
    $chartType = 'user';
}else{
    $ajaxJSChart = "";
    $chartType = 'user';
}

$stats->time = $stat->getTotalTime($chartType, 'upn', $upn);
$stats->total = $stat->getCountWorkshopAttempt($upn);
$stats->scales = $stat->getScales( $upn, $order);
$stats->done = $stat->getWorkshopAttemptDone($upn);
$workstations = $wsMapper->getWorkshops();
$users = $uMapper->getUsers();

//get the count of distinct workshop attempt
if(isset($_GET['global'])){
    $countWaDistinct=$stats->total;
    $WSTodo = $stat->selectWorkshopNotDone();
    $WSTodoSize = sizeof($WSTodo);
}else{
    //select the workshop attempt and get the time by workshop attempt
    if(isset($_GET['user'])){
        $idUser=$_GET['user'];
    }else{
        $idUser=[];
        if($idUser == null){
            $upn=$session->getValue('upn');
            $user=$uMapper->getUserByUpn($upn);
            $idUser=$user->getId();
        }
    }
    $workshopAttemptList= $waMapper->selectWorkshopAttemptByUser($idUser, $filter);
    $i=0;
    $timeByWa = [];
    foreach ($workshopAttemptList as $wa) {
        $timeByWa[$i] = $stat->getTotalTimeByUserByWorkshopAttempt($idUser, $wa->getId());
        $i++;
    }
    $idWaDistinct = $stat->getDistinctWorkshopAttemptByUser($idUser, $filter);
    $countWaDistinct = count($idWaDistinct);
    //get the workshop name for each workshop attempt to do in modal
    $WSTodo = $stat->selectWorkshopNotDoneByUser($idUser, $filter);
    $WSTodoSize = sizeof($WSTodo);

    //get the curent user's total time for the workstation
    $totalTime = $stat->getTotalTime($type = 'user', $champs = 'id', $value = $idUser);
    if ($stats->total !== 0) {
        $averageTime = round(($totalTime / $stats->total));
    } else {
        $averageTime = 0;
    }
    //More details in stat_functions
    $distinctWa = getDistinctWa($stat, $idUser, $idWaDistinct,$wsMapper);
    $lstWa = getWorkshopAttempt($waMapper, $idWaDistinct);
    $lstWaByWs = getLstWaByWs($waMapper, $lstWa, $idUser);
    $lstBestScale = getBestScale($lstWaByWs, $idUser, $stat, $scaleMapper);
    $lstTime = getLstTime($lstBestScale,$stat,$idUser);

    //get priority levels
    $priorityLevels = $plMapper->getPriorityLevels();
}

$contentPage = 'stat.phtml';
include '../../view/skel.phtml';
