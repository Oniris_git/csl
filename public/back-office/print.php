<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use CSLManager\Administration\Mapper\WorkshopMapper;

require '../../lib/bootstrap.php';

if (!$permission->check('print:qrcode')) {
    include __DIR__.'/../403.html';
    exit();
}

$id = filter_input(INPUT_GET, 'id');

if ($id !== 0) {
    $wMapper = new WorkshopMapper($connector);
    try {
        $workshop = $wMapper->getWorkshopById($id);
        $scheme = (!empty($_SERVER['HTTPS'])) ? 'https://' : 'http://';
		$url = $scheme . $_SERVER['SERVER_NAME']
			. '/index.php?poste='
			. $workshop->getLabel();

        // On génère le QRCode
        $QRCode = new \Endroid\QrCode\QrCode($url);
        
        $workshop->setQrcode(
			$QRCode->writeDataUri()
		);
		unset($QRCode);

    } catch (\Throwable $e) {
        header('Content-Type: text/plain');
        echo $e->getMessage();
        die();
    }
}

$contentPage = '../../view/back-office/qrCode.phtml';
include '../../view/skel.phtml';
