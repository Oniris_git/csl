<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Interface d'administration de la plateforme VetSims
 * Vue d'ensemble
 */

use CSLManager\Administration\Mapper\ScaleMapper;
use CSLManager\Administration\Mapper\WorkshopAttemptMapper;

require '../../lib/bootstrap.php';

$workspaces = [];
$last = [];
$stat = [];

// Workshop attempt in progress //
if ($permission->check('edit:tp')) {
    if (!isset($wsMapper)) {
        $waMapper = new WorkshopAttemptMapper($connector);
    }

    $scaleMapper = new ScaleMapper($connector);
    $active = $waMapper->active();
    foreach ($active as $item) {
        $item->getScales()(
            $scaleMapper->getScalesByTypeId($item->getNoteGroupe())
        );
    }
}

$contentPage = 'install.phtml';
include '../../view/skel.phtml';
