<?php

use CSLManager\Administration\Mapper\WorkshopMapper;
use CSLManager\Administration\Stat;
use CSLManager\Administration\Mapper\CategoryMapper;
use CSLManager\Administration\Mapper\SubCategoryMapper;
use CSLManager\Administration\Mapper\SpecieMapper;
use CSLManager\Administration\Mapper\UserMapper;
use CSLManager\Administration\Mapper\PromoMapper;
use CSLManager\Administration\Mapper\WorkshopAttemptMapper;
require __DIR__ . '/../../lib/bootstrap.php';

if (!$permission->check('print:workspace')) {
    include __DIR__.'/../403.html';
    exit();
}

//instantiation variables to affect a color to a difficulty
$easy= '#80C363';
$middleLow = '#4F90C8';
$middle = '#F84949';
$hard = '#424242';

//get the workshop by the id to display information
if(isset($_GET['workshop_attempt'])){
    
    //Instantiation of different mappers
    $wMapper = new WorkshopMapper($connector);
    $stMapper = new Stat($connector);
    $cMapper = new CategoryMapper($connector);
    $subMapper = new SubCategoryMapper($connector);
    $spMapper = new SpecieMapper($connector);
    $uMapper = new UserMapper($connector);
    $pMapper = new PromoMapper($connector);
    $waMapper = new WorkshopAttemptMapper($connector);
    
    //get the workshop by the id
    $workshopId= $_GET['workshop_attempt'];
    $workshop=$wMapper->getWorkshopById($workshopId);

    $lstUserGlobal= $waMapper->selectUserByWorkshop($workshopId);
    $globalCount = sizeof($lstUserGlobal);
    
    //get the difficulty of the workshop attempt and assign a color to display
    $idDifficulty= $workshop->getIdDifficulty();
    $difficulty= $pMapper->getPromoById($idDifficulty);
    switch ($idDifficulty){
        case 1:
            $idDifficulty = $easy;
            break;
        case 2:
            $idDifficulty = $middleLow;
            break;
        case 3:
            $idDifficulty = $middle;
            break;
        case 4:
            $idDifficulty = $hard;
            break;
    }
    
    //get the count of distinct users for the workshop
    $lstIdUser=$waMapper->getDistinctIdUserByWorkshop($workshopId);
    $countUsers= count($lstIdUser);
    if(strlen($countUsers == 0) ){
        $countUsers = 0;
    }
    
    //get the user list to display in modal
    $i= 0;
    $lstUser= [];
    foreach ($lstIdUser as $row){
        $lstUser[$i]= $uMapper->getUserById($row['id_user']);
        $i++;
    }
    
    //get the total time for the workshop
    $totalTime= $stMapper->getTimeByWorkshop($workshopId);
    
    //get the average time by workshop attempt
    if($countUsers == 0){
        $averageTime = 0;
    }else{
        $averageTime= ($totalTime/$globalCount);
    }
    
    //get the number of users who have the status granted for exercice
    $granted= $stMapper->getCountGrantedByWorkshop($workshop);

    //get the coments and the number of coments by workstation
    $comments=$stMapper->selectCommentsByWorkShop($workshopId);
    if(sizeof($comments) !== 0){
        $countComments = count($comments);
    }else{
        $countComments = 0;
    }    
    
    //get the information to display into description
    //get the category title
    $categoryId = $workshop->getIdCategory();
    $categoryW= $cMapper->getCategoryById($categoryId);
    if($categoryW){
        $categoryW = $categoryW->getTitle();
    }else{
        $categoryW= TXT_NOT_COMPLETE;

    }
    //get the specie title
    $specieId = $workshop->getIdSpecies();
    $specieW = $spMapper->getSpecie($specieId);
    if($specieW){
        $specieW= $specieW->getTitle();
    }else{
        $specieW = TXT_NOT_COMPLETE;
    }
    
    //workshop attempt average
    if($granted !== null && $granted != 0){
        $sizeList= $stMapper->getDistinctGrantedByWorkshop($workshop);
        $resultAverage=round(($countUsers/$sizeList),2);
    }else{
        $resultAverage =0;
        $granted = 0;
        $sizeList=0;
    }
}

//put the content of the page into a variable to include into the template
$contentPage= 'blocks/technicalsheet_workshop.phtml';

//include the template
include '../../view/skel.phtml';
