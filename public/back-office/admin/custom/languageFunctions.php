<?php

//check file format
function checkFileImg($file){
    //directory image
    $path = "../../../dist/images/";
    $target_file_img = $path . basename($file["name"]);
    $uploadOk = 1;
    //bring back extension
    $imageFileType_img = pathinfo($target_file_img, PATHINFO_EXTENSION);
    //file image?
    $check = getimagesize($file["tmp_name"]);
    if ($check !== false) {
        $uploadOk = 1;
    } else {
        return $ERROR = [
            "message" => TXT_IMAGE_INVALID
        ];
    }
    if (file_exists($target_file_img)) {
        return $ERROR = [
            "message" => TXT_FILE_EXIST
        ];
    }elseif ($file["size"] > 500000) {
        return $ERROR = [
            "message" => TXT_IMAGE_SIZE
        ];
    }elseif ($imageFileType_img != "jpg" &&$imageFileType_img != "JPG"&& $imageFileType_img != "png" && $imageFileType_img != "PNG" && $imageFileType_img != "jpeg" && $imageFileType_img != "JPEG" ) {
        return $ERROR = [
            "message" => TXT_IMAGE_FORMAT
        ];
    }elseif ($uploadOk == 0) {
        return $ERROR = [
            "message" => TXT_FILE_INVALID
        ];
    }else {
        return $target_file_img;
    }
}

//check file format
function checkFilePHP($file){
    //directory php file
    $path = "../../../../lang/";
    //directory php file
    $target_file_php = $path . basename($file["name"]);
    //bring back extension
    $phpFileType_php = pathinfo($target_file_php, PATHINFO_EXTENSION);

    if (file_exists(file_exists($target_file_php))) {
        return $ERROR = [
            "message" => TXT_FILE_EXIST
        ];
    }elseif ($phpFileType_php != "php") {
        return $ERROR = [
            "message" => TXT_IMAGE_FORMAT
        ];
    }else {
        return $target_file_php;
    }
}