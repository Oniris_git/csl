<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 30/11/2018
 * Time: 11:59
 */

use CSLManager\Administration\Mapper\ConfigMapper;
require '../../../../lib/bootstrap.php';

/*$arg = [
    'html' => FILTER_SANITIZE_STRING
];

$POST = filter_input_array(INPUT_POST, $arg, false);*/

$configMapper = new ConfigMapper($connector);

if(isset($_POST['html'])){
    try {
        $configMapper->alterConfig("view", "legal", $_POST['html']);
        echo TXT_LEGAL_SUCCESS;
    } catch (Exception $e) {
        echo($e->getMessage());
    }
}else{
    $legalContent = $configMapper->selectConfigByName("view", "legal");
    $contentPage = 'admin/admin_legal.phtml';
    include '../../../../view/skel.phtml';
}

