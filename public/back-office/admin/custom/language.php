<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 30/11/2018
 * Time: 11:59
 */

use CSLManager\Administration\Entity\Language;
use CSLManager\Administration\Mapper\ConfigMapper;
use CSLManager\Administration\Mapper\LanguageMapper;
require '../../../../lib/bootstrap.php';
include "languageFunctions.php";

//check $_POST
$argsGet = [
    'action' => FILTER_SANITIZE_STRING,
    'newLanguage' => FILTER_SANITIZE_STRING
];

$argsPost = [
    'action' => FILTER_SANITIZE_STRING,
    'lang-selected' => FILTER_SANITIZE_STRING,
];

$GET = filter_input_array(INPUT_GET, $argsGet, false);
$POST = filter_input_array(INPUT_POST, $argsPost, false);

//check permission
if (!$permission->check("edit:lang")):
    include __DIR__ . '/../../../403.html';
    exit();
else :
    //instantiation Mappers
    $configMapper = new ConfigMapper($connector);
    $langMapper = new LanguageMapper($connector);

    if (isset($GET['action'])):
        switch ($GET['action']) :
            case 'alterLanguage':
                if(!isset($GET['newLanguage'])):
                    $ERROR = [
                        'message' => 'Error request'
                    ];
                endif;

                try{
                    $configMapper->alterView("lang", $GET['newLanguage']);
                }catch (Exception $e){
                    $ERROR = [
                        'message' => $e->getMessage()
                    ];
                }
        endswitch;
    endif;

    if(isset($POST['action'])){
        switch ($POST['action']):
            case "add-lang":
                if (isset($_FILES["fileLangIMG"]) && $_FILES["fileLangIMG"]['name'] != null && isset($_FILES["fileLangPHP"]) && $_FILES["fileLangPHP"]['name'] != null) {
                    $resultImg = checkFileImg($_FILES["fileLangIMG"]);
                    $resultPHP = checkFilePHP($_FILES["fileLangPHP"]);
                    if(isset($resultImg['message'])):
                        $ERROR =  $resultImg;
                    elseif(isset($resultPHP['message'])):
                        $ERROR = $resultPHP;
                    else:
                        try {
                            $titleStep1 = str_replace("../../../../lang/lang-", "",$resultPHP);
                            $titleStep2 = str_replace(".inc.php", "",$titleStep1);
                            $data = [
                                'title' => $titleStep2,
                                'path_img' => $resultImg,
                                'path_file' => $resultPHP
                            ];
                            $newLanguage = new Language($data);
                            $langMapper->insertLanguage($newLanguage);
                            move_uploaded_file($_FILES["fileLangIMG"]["tmp_name"], $resultImg);
                            move_uploaded_file($_FILES["fileLangPHP"]["tmp_name"], $resultPHP);
                        } catch (Exception $e) {
                            $ERROR = [
                                "message" => $e->getMessage()
                            ];
                        }
                    endif;
                }
                case "alter-lang":
                    if (isset($_FILES["fileLangIMG"]) && $_FILES["fileLangIMG"]['name'] != null && isset($_FILES["fileLangPHP"]) && $_FILES["fileLangPHP"]['name'] != null && isset($_FILES["lang-selected"]) && $POST['lang-selected'] != null ) {
                        $resultImg = checkFileImg($_FILES["fileLangIMG"]);
                        $resultPHP = checkFilePHP($_FILES["fileLangPHP"]);
                        if(isset($resultImg['message']) || isset($resultPHP['message'])):
                            break;
                        else:
                            try {
                                $titleStep1 = str_replace("../../../../lang/lang-", "",$resultPHP);
                                $titleStep2 = str_replace(".inc.php", "",$titleStep1);
                                $data = [
                                    'title' => $titleStep2,
                                    'path_img' => $resultImg,
                                    'path_file' => $resultPHP
                                ];
                                $newLanguage = new Language($data);
                                $oldLang = $langMapper->selectLanguageByTitle($POST['lang-selected']);
                                $langMapper->deleteLanguageByTitle($oldLang->getTitle());
                                unlink($oldLang->getPathFile());
                                unlink($oldLang->getPathImg());
                                $langMapper->insertLanguage($newLanguage);
                                move_uploaded_file($_FILES["fileLangIMG"]["tmp_name"], $resultImg);
                                move_uploaded_file($_FILES["fileLangPHP"]["tmp_name"], $resultPHP);
                            } catch (Exception $e) {
                                $ERROR = [
                                    "message" => $e->getMessage()
                                ];
                            }
                        endif;
                    }
         endswitch;
    }

    try {
        $languages = $langMapper->selectLanguages();
    } catch (Exception $e) {
        $ERROR = [
            'message' => $e->getMessage()
        ];
    }

    try{
        $configLang = $configMapper->selectConfigByName("view", "lang")['value'];
        $defaultLangEntity = $langMapper->selectLanguageByTitle($configLang);
        $defaultLangPath = $defaultLangEntity->getPathImg();
        $defaultLangTitle = $defaultLangEntity->getTitle();
    }catch(Exception $e){
        $ERROR = [
            'message' => $e->getMessage()
        ];
        $defaultLangPath = "";
    }
endif;

$contentPage = 'admin/admin_language.phtml';
include '../../../../view/skel.phtml';
