<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 30/11/2018
 * Time: 11:59
 */

require '../../../../lib/bootstrap.php';
include 'languageFunctions.php';
use CSLManager\Administration\Mapper\ConfigMapper;

$configMapper = new ConfigMapper($connector);

$logo = $configDB['logo'];

$args = [
    'title' => FILTER_SANITIZE_STRING,
    'supportTech' => FILTER_SANITIZE_STRING,
    'domain' => FILTER_SANITIZE_STRING,
];

$POST = filter_input_array(INPUT_POST, $args, false);

if (!$permission->check("edit:view")){
    include __DIR__ . '/../../../403.html';
    exit();
}else{
    if (isset($_FILES["logoFile"]) && $_FILES["logoFile"]['name'] != null){
        $resultCheck = checkFileImg($_FILES["logoFile"]);
        if (isset($resultCheck['message'])) {
            $ERROR = $resultCheck;
        } else {
            try{
                $oldLogo = $logo;
                $configMapper->alterView("logo", $resultCheck);
                if (move_uploaded_file($_FILES["logoFile"]["tmp_name"], $resultCheck)) {
                    $logo = $configMapper->selectConfigByName("view",'logo')['value'];
                    unlink($oldLogo);
                }
                $SUCCESS = [
                    'message' => TXT_NOTIFICATION_ALTER_SAVED
                ];
            }catch (Exception $e){
                $ERROR = [
                    "message" => TXT_IMAGE_ERROR
                ] ;
            }
        }
    }
    if(isset($POST['title']) && $POST['title'] != null ){
        try {
            $configMapper->alterView("title", $POST['title']);
            $SUCCESS = [
                'message' => TXT_NOTIFICATION_ALTER_SAVED
            ];
        } catch (\Exception $e) {
            $ERROR= [
                "message"=>TXT_NOTIFICATION_ERROR . $roomMapper->getPDOError()[2]
            ];
        }
    }

    if(isset($POST['supportTech']) && $POST['supportTech'] != null ){
        try {
            $configMapper->alterView("technique", $POST['supportTech']);
            $SUCCESS = [
                'message' => TXT_NOTIFICATION_ALTER_SAVED
            ];
        } catch (\Exception $e) {
            $ERROR= [
                "message"=>TXT_NOTIFICATION_ERROR . $roomMapper->getPDOError()[2]
            ];
        }
    }

    if(isset($POST['domain']) && $POST['domain'] != null ){
        try {
            $configMapper->alterView("domain", $POST['domain']);
            $SUCCESS = [
                'message' => TXT_NOTIFICATION_ALTER_SAVED
            ];
        } catch (\Exception $e) {
            $ERROR= [
                "message"=>TXT_NOTIFICATION_ERROR . $roomMapper->getPDOError()[2]
            ];
        }
    }

    $title = $configMapper->selectConfigByName("view",'title')['value'];
    $support = $configMapper->selectConfigByName("view",'technique')['value'];
    $domain =  $configMapper->selectConfigByName("view",'domain')['value'];

    $contentPage = 'admin/admin_view.phtml';
    include '../../../../view/skel.phtml';
}


