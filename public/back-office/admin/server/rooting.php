<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 03/01/2019
 * Time: 09:19
 */

use CSLManager\Administration\Mapper\ConfigMapper;

require "../../../../lib/bootstrap.php";

if(!$permission->check("print:root")){
    include __DIR__ . '/../../../403.html';
    exit;
}

$configMapper = new ConfigMapper($connector);

$dataMoodle = $configMapper->selectType("moodle");

$contentPage = "admin/admin_rooting.phtml";

include "../../../../view/skel.phtml";
