<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 03/01/2019
 * Time: 09:19
 */

use CSLManager\Administration\Mapper\ConfigMapper;

require "../../../../lib/bootstrap.php";

if(!$permission->check("print:mailer")){
    include __DIR__ . '/../../../403.html';
    exit;
}

//get actual mailer information
$configMapper = new ConfigMapper($connector);
$smtpData = [
    "host" => $configMapper->selectConfigByName("smtp", "host")['value'],
    "secure" => $configMapper->selectConfigByName("smtp", "secure")['value'],
    "port" => $configMapper->selectConfigByName("smtp", "port")['value'],
    "user_name" => $configMapper->selectConfigByName("smtp", "user_name")['value'],
    "password" => $configMapper->selectConfigByName("smtp", "password")['value']
];

$contentPage = "admin/admin_mailer.phtml";

include "../../../../view/skel.phtml";
