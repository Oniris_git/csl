<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 03/01/2019
 * Time: 09:18
 */

use CSLManager\Administration\Mapper\ConfigMapper;

require "../../../../lib/bootstrap.php";

if(!$permission->check("print:auth")){
    include __DIR__ . '/../../../403.html';
    exit;
}

$configMapper = new ConfigMapper($connector);

$typeAuth = $configMapper->selectConfigByName("auth", "type")['value'];

switch ($typeAuth){
    case "ldap":
        $data = [
            'uri' => $configLdap['ldapUri'],
            'basedn' => $configLdap['ldapBindOptions']['basedn'],
            'binddn' => $configLdap['ldapBindOptions']['binddn'],
            'filter' => $configLdap['ldapBindOptions']['filter'],
            'port' => $configLdap['ldapPort']
        ];
        break;
    default :
        $data = [];
        break;
}

$contentPage = "admin/admin_auth.phtml";

include "../../../../view/skel.phtml";
