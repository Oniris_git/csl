<?php

use CSLManager\Administration\Mapper\UserMapper;

require '../../../../lib/bootstrap.php';

if (!$permission->check('edit:profiles')) {
    include __DIR__ . '/../../../403.html';
    exit();
}

$userMapper = new UserMapper($connector);

$all = $userMapper->getUsers();
$emails = [];

foreach ($all as $user) {
    $emails[$user->getEmail()] = $user;
}

//
// GET USERS FROM API AND STOCK IN $post_data
//

$post_data = [];

foreach ($post_data as $mdl_user) {
    $csl_user = $emails[$mdl_user->email];
    // $csl_user->setDescription($mdl_user->department); IMPLEMENT THIS IN BO
    $userMapper->alter($csl_user); // IMPLEMENT alter METHOD SO IT ALTER DESCRIPTION FIELD
}
