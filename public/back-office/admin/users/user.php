<?php

use CSLManager\Administration\Entity\User;
use CSLManager\Administration\Mapper\ProfileMapper;
use CSLManager\Administration\Mapper\UserMapper;

require '../../../../lib/bootstrap.php';

if (!$permission->check('edit:users')) {
    include __DIR__ . '/../../../403.html';
    exit();
}

$args = [
    'action' => FILTER_SANITIZE_STRING,
    'csvImport' => FILTER_SANITIZE_STRING,
];

$POST = filter_input_array(INPUT_POST, $args, false);
$userMapper = new UserMapper($connector);

switch ($POST['action']) {
    case 'csvImport':
        if(!isset($_FILES['csv']['name'])):
            $ERROR = [
                'message' => "Import failed"
            ];
        else:
            $fp = fopen($_FILES['csv']['tmp_name'], "r");
            $i= 0;
            while(!feof($fp)):
                $line = fgets($fp, 4096);
                $list = explode(";", $line);
                $password = createPassword();
                $token = generateToken('form');
                if(sizeof($list) === 5 ):
                    switch ($list[4]):
                        case "student":
                            $profile = 3;
                            break;
                        case "admin":
                            $profile = 1;
                            break;
                        case "manag":
                            $profile = 2;
                            break;
                        default:
                            $profile = 3;
                            break;
                    endswitch;
                    $tab[$i] = [
                        "upn" => strtolower ($list[2]),
                        "firstName" => strtolower ($list[0]),
                        "name" => strtolower ($list[1]),
                        "id_profile" => $profile,
                        "max_difficulty" => 1,
                        "auth" => "manual",
                        "email" => $list[3],
                        "password" => $password,
                        "token" => $token,
                        "open" => 1,
                        "request" => date("Y-m-d H:i:s",time())
                    ];
                else:
                    $ERROR = [
                        "Check values line ".$i
                    ];
                endif;
                $i++;
                endwhile;
                $count = 0;
                foreach ($tab as $row):
                    $row['password']=  password_hash($row['password'], PASSWORD_DEFAULT);
                    try {
                        $user = new User($row);
                        $userMapper->create($row);
                        $request = "init";
                        $resultMail = sendMail($user, $connector, $request);
                    } catch (Exception $e) {
                        $ERROR = [
                            "message" => $e->getMessage()." line ".$count
                        ];
                    }
                    $count++;
                endforeach;
            $SUCCESS = [
                "message" => TXT_NOTIFICATION_IMPORT_SUCCESS
            ];
        endif;
}

$profileMapper = new ProfileMapper($connector);
$profiles = $profileMapper->getProfiles();


$contentPage = 'admin/admin_user.phtml';
include '../../../../view/skel.phtml';




