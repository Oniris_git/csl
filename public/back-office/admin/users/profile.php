<?php

use CSLManager\Administration\Mapper\ProfileMapper;

require '../../../../lib/bootstrap.php';

if (!$permission->check('edit:profiles')) {
    include __DIR__ . '/../../../403.html';
    exit();
}

$args = [
    'profile' => FILTER_VALIDATE_INT,
];

$GET = filter_input_array(INPUT_GET, $args, false);

$profileMapper = new ProfileMapper($connector);

if (isset($GET['profile'])) {
    $view = 'profile';
    try {
        $profile = $profileMapper->getProfileById($GET['profile']);
        $rights = $permission->getByProfile($profile['id']);
    } catch (Exception $e) {
        $ERROR[] = $e->getMessage();
    }
} else {
    $view = 'list';
    try {
        $profiles = $profileMapper->getProfiles();
    } catch (Exception $e) {
        $ERROR[] = $e->getMessage();
    }
}

$contentPage = 'admin/admin_profile.phtml';
include '../../../../view/skel.phtml';
