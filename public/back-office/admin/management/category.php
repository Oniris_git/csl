<?php

use CSLManager\Administration\Mapper\CategoryMapper;

require __DIR__ . '/../../../../lib/bootstrap.php';

if (! $permission->check('edit:category')) {
    include __DIR__ . '/../../../403.html';
    exit();
}

$args = [
    'category' => FILTER_VALIDATE_INT,
];

$GET = filter_input_array(INPUT_GET, $args, false);

$categoryMapper = new CategoryMapper($connector);
$view = 'list';

if (isset($GET['category'])) {
    $category = $categoryMapper->getCategoryById($GET['category']);
    if ($category !== false ) {
        $view = 'category';
    } else {
        $ERROR[] = (count($category) > 0)
            ? TXT_ADMIN_CATEGORY_ERROR_UNKNOWN
            : $categoryMapper->getPDOStatementError()[2];
        $categories = $categoryMapper->getCategories();
    }
} else {
    $categories = $categoryMapper->getCategories();
}

$contentPage = 'admin/admin_category.phtml';
include '../../../../view/skel.phtml';
