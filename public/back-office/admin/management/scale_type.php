<?php

use CSLManager\Administration\Mapper\ScaleMapper;

require '../../../../lib/bootstrap.php';

if (!$permission->check('edit:evals')) {
    include __DIR__ . '/../../../403.html';
    exit();
}

$args = [
    'scale' => FILTER_VALIDATE_INT,
];

$GET = filter_input_array(INPUT_GET, $args, false);

$scaleMapper = new ScaleMapper($connector);

if (isset($GET['scale'])) {
    $view = 'scale';
    try {
        $scale_type = $scaleMapper->getScaleTypeById($GET['scale']);
        $scales = $scaleMapper->getScalesByTypeId($GET['scale']);
        $notgrouped = $scaleMapper->getScalesNotInType();
    } catch (Exception $e) {
        $ERROR[] = $e->getMessage();
        $scale_types = $scaleMapper->getScales();
        $view = 'list';
    }
} else {
    $view = 'list';
    try {
        $scale_types = $scaleMapper->getScaleTypes();
    } catch (Exception $e) {
        $ERROR[] = $e->getMessage();
    }
}

$contentPage = 'admin/admin_scale_type.phtml';
include '../../../../view/skel.phtml';
