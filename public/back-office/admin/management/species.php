<?php

use CSLManager\Administration\Mapper\SpecieMapper;

require __DIR__ . '/../../../../lib/bootstrap.php';

if (!$permission->check('edit:species')) {
    include __DIR__ . '/../../../403.html';
    exit();
}

$args = [
    'specie' => FILTER_VALIDATE_INT,
];

$GET = filter_input_array(INPUT_GET, $args, false);

$speciesMapper = new SpecieMapper($connector);
$view = 'list';

if (isset($GET['specie'])) {
    $specie = $speciesMapper->getSpecie($GET['specie']);
    if ($specie !== false) {
        $view = 'specie';
    } else {
        $ERROR[] = (count($specie) > 0)
            ? TXT_ADMIN_SPECIES_ERROR_UNKNOWN
            : $speciesMapper->getPDOStatementError()[2];

        $species = $speciesMapper->getSpecies();
    }
} else {
    $species = $speciesMapper->getSpecies();
}

$contentPage = 'admin/admin_species.phtml';
include '../../../../view/skel.phtml';
