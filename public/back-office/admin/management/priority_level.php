<?php

use CSLManager\Administration\Mapper\PriorityLevelMapper;

require '../../../../lib/bootstrap.php';

if (!$permission->check('edit:rank')) {
    include __DIR__ . '/../../403.html';
    exit();
}

$args = [
    'priority_level' => FILTER_VALIDATE_INT,
];

$GET = filter_input_array(INPUT_GET, $args, false);

$plMapper = new PriorityLevelMapper($connector);

if (isset($GET['priority_level'])) {
    $view = 'priority_level';
    try {
        $priority_level= $plMapper->getPriorityLevelById($GET['priority_level']);
    } catch (Exception $e) {
        $ERROR[] = $e->getMessage();
    }
} else {
    $view = 'list';
    try {
        $priority_levels = $plMapper->getPriorityLevels();
    } catch (Exception $e) {
        $ERROR[] = $e->getMessage();
    }
}

$contentPage = 'admin/admin_priority_level.phtml';
include '../../../../view/skel.phtml';
