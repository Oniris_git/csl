<?php

use CSLManager\Administration\Mapper\DisciplineMapper;

require '../../../../lib/bootstrap.php';

if (!$permission->check('edit:discipline')) {
    include __DIR__ . '/../../403.html';
    exit();
}

$args = [
    'discipline' => FILTER_VALIDATE_INT,
];

$GET = filter_input_array(INPUT_GET, $args, false);

$disciplineMapper = new DisciplineMapper($connector);

if (isset($GET['discipline'])) {
    $view = 'discipline';
    try {
        $discipline= $disciplineMapper->getDisciplineById($GET['discipline']);
    } catch (Exception $e) {
        $ERROR[] = $e->getMessage();
    }
} else {
    $view = 'list';
    try {
        $disciplines = $disciplineMapper->getDisciplines();
    } catch (Exception $e) {
        $ERROR[] = $e->getMessage();
    }
}

$contentPage = 'admin/admin_discipline.phtml';
include '../../../../view/skel.phtml';
