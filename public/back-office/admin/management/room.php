<?php

use CSLManager\Administration\Mapper\RoomMapper;

require '../../../../lib/bootstrap.php';

if (!$permission->check('edit:room')) {
    include __DIR__ . '/../../../403.html';
    exit();
}

$args = [

];

$roomMapper = new RoomMapper($connector);
$rooms = $roomMapper->getRooms();

$contentPage = 'admin/admin_room.phtml';
include '../../../../view/skel.phtml';
