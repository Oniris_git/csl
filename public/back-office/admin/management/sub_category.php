<?php

use CSLManager\Administration\Mapper\SubCategoryMapper;

require '../../../../lib/bootstrap.php';

if (!$permission->check('edit:subcategory')) {
    include __DIR__ . '/../../../403.html';
    exit();
}

$args = [
    'subcategory' => FILTER_VALIDATE_INT,
];

$GET = filter_input_array(INPUT_GET, $args, false);

$subcategoryMapper = new SubCategoryMapper($connector);
$view = 'list';

if (isset($GET['subcategory'])) {
    $view = 'subcategory';
    try {
        $subcategory= $subcategoryMapper->getSubCategory($GET['subcategory']);
    } catch (Exception $e) {
        $ERROR[] = $e->getMessage();
    }
} else {
    $view = 'list';
    try {
        $subcategories = $subcategoryMapper->getSubCategories();
    } catch (Exception $e) {
        $ERROR[] = $e->getMessage();
    }
}

$contentPage = 'admin/admin_sub_category.phtml';
include '../../../../view/skel.phtml';
