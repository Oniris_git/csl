<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Gestion des utilisateurs
 */

use CSLManager\Administration\Mapper\ProfileMapper;
use CSLManager\Administration\Mapper\UserMapper;

require '../../lib/bootstrap.php';

if (!$permission->check('print:users')) {
    include __DIR__.'/../403.html';
    exit();
}

$uMapper = new UserMapper($connector);

$users = [];
$viewable = false;

$u_id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
$page = filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT);
$order = filter_input(INPUT_POST, 'order');
$page = ($page) ?: 1;

$totalUsers = 1;

if ($u_id !== null && $u_id !== false) {
    try {
        $viewable = true;
        $user = $uMapper->getUserById($u_id);
        $profileMapper = new ProfileMapper($connector);
        $profile = $profileMapper->getProfiles();
    } catch (Exception $e) {
        $viewable = false;
        $ERROR[] = $e->getMessage();
        $users = $uMapper->_paginate()->_offset(($page - 1) * 25)->getUsers();
        $totalUsers = $uMapper->getTotalUsers();
    }
}else if(($order)!== null){
    $viewable = false;
    $users = $uMapper->_paginate()->_offset(($page - 1) * 25)->getUsers($order);
    $totalUsers = $uMapper->getTotalUsers();

}
else {
    $users = $uMapper->_paginate()->_offset(($page - 1) * 25)->getUsers();
    $totalUsers = $uMapper->getTotalUsers();
}

$totalPages = ceil($totalUsers / 25);

$contentPage = 'user.phtml';
include '../../view/skel.phtml';
