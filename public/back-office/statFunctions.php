<?php

//get list with the distinct workshop attempt and the count of same workshop attempt (Table)
function getDistinctWa($stat, $idUser, $idTpDistinct, $wMapper){
    $i=0;
    $result= [];
    foreach($idTpDistinct as $id){
        $result[$i]= $stat->getCountWorkshopAttemptByUserForWorkshop($idUser, $id['id_workshop']);
        $result[$i][$i]= $wMapper->getWorkshopById($id['id_workshop']);
        $i++;
    }
    return $result;
} 

//get list with the distinct workshop attempts (object)
function getWorkshopAttempt($waMapper, $idWaDistinct){
    $result=[];
    $i=0;
    foreach ($idWaDistinct as $id){
        $result[$i]= $waMapper->selectUserByWorkshop($id['id_workshop']);
        $i++;
    }
   return $result;
}

//get list of all workshop attempt by workshop(Table)
function getLstWaByWs($waMapper, $lstWa, $idUser){
    $u=0;
    $data=[];
    foreach($lstWa as $ws){
        $data[$u]=$waMapper->selectWorkshopAttemptByUserByWorkshop($idUser, $ws[0]->getIdWorkshop());
        $u++;
    }
    
    return $data;
}

//get list of best time for each scale
function getLstTime($lstBestScale,$stat,$idUser){
    $i=0;
    $data=[];
    foreach ($lstBestScale as $scale){
        $time=$stat->getTotalTimeByUserByWorkshopAttempt ($idUser, $scale[0]->getId());
        $timeFormat= sprintf('%dh, %dm, %ds',(($time) / 3600), (($time) / 60 % 60), ($time) % 60);
        $nbHour= substr($timeFormat, 0,1);
        if($nbHour == 0){
            $timeFormat= sprintf('%dm, %ds',(($time) / 60 % 60), ($time) % 60);
        }
        $data[$i]=$timeFormat;
        $i++;
    }
    return $data;
}

//get a workshop attempt list with the best scale and best time
function getBestScale($lstWaByWs, $idUser, $stat){
    $data=[];
    $i=0;
    foreach($lstWaByWs as $row):
        if(sizeof($row) > 1):
            $scaleStocked=NULL;
            foreach ($row as $scale):
               if($scaleStocked !== null):
                   if($scale->getOrderScale() > $scaleStocked->getOrderScale()):
                       $scaleStocked = $scale;
                   elseif ($scale->getOrderScale() == $scaleStocked->getOrderScale()):
                       if ( ($stat->getTotalTimeByUserByWorkshopAttempt ($idUser, $scaleStocked->getId()) ) > ( $stat->getTotalTimeByUserByWorkshopAttempt ($idUser, $scale->getId()) )):
                           $scaleStocked=$scale;
                       endif;
                   endif;
               else:
                    $scaleStocked = $scale;
                endif;
            endforeach;
            $data[$i][0]=$scaleStocked;
            $i++;
        elseif(sizeof($row) == 1):
            $data[$i]=$row;
            $i++;
        endif;
    endforeach;
    return $data;
}

