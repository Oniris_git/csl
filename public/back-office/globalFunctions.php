<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 17/01/2019
 * Time: 10:06
 */

use PHPMailer\PHPMailer\PHPMailer;

require __DIR__.'/../../vendor/phpmailer/phpmailer/src/SMTP.php';
require __DIR__.'/../../vendor/phpmailer/phpmailer/src/PHPMailer.php';
require __DIR__.'/../../vendor/phpmailer/phpmailer/src/OAuth.php';
use CSLManager\Administration\Mapper\ConfigMapper;

function sendMail($user , $connector, $request, $body = null){
    $configMapper = new ConfigMapper($connector);

    $smtp = [
        'host' => $configMapper->selectConfigByName("smtp",'host'),
        'secure' => $configMapper->selectConfigByName("smtp",'secure'),
        'port' => $configMapper->selectConfigByName("smtp",'port'),
        'user_name' => $configMapper->selectConfigByName("smtp",'user_name'),
        'password' => $configMapper->selectConfigByName("smtp",'password'),
    ];

    $domain = $configMapper->selectConfigByName("view",'domain');

    switch ($request){
        case "init":
            $body = "<h3>".TXT_MAIL_INIT_TITLE."</h3>
                <p>".TXT_MAIL_INIT_CONTENT1."<a href='".$domain['value']."/request.php?data=".$user->getToken()."'>".TXT_LINK."</a></p>";
            $subject = '[CSLManager] Init Password';
            $from = $smtp['user_name']['value'];
            $to = $user->getEmail();
            break;
        case "changeMDP":
            $body = "<h3>".TXT_MAIL_CHANGE_TITLE."</h3>
                <p>".TXT_MAIL_INIT_CONTENT1."<a href='".$domain['value']."/request.php?data=".$user->getToken()."'>".TXT_LINK."</a></p>
                <p>".TXT_MAIL_INIT_CONTENT."</p>";
            $subject = '[CSLManager] Change Password';
            $from = $smtp['user_name']['value'];
            $to = $user->getEmail();
            break;
        case "feedback":
            $subject = '[CSLManager] Feedback';
            $from = $user->getEmail();
            $to = $smtp['user_name']['value'];
    }

    //Create a new PHPMailer instance
    $mail = new PHPMailer();
    //Tell PHPMailer to use SMTP
    $mail->isSMTP();
    //Enable SMTP debugging
    $mail->SMTPDebug = 0;
    //Set the hostname of the mail server
    $mail->Host = $smtp['host']['value'];
    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    $mail->Port = $smtp['port']['value'];
    //Set the encryption system to use - ssl (deprecated) or tls
    $mail->SMTPSecure = $smtp['secure']['value'];
    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;
    //Username to use for SMTP authentication - use full email address for gmail
    $mail->Username = $smtp['user_name']['value'];
    //Password to use for SMTP authentication
    $mail->Password = $smtp['password']['value'];
    $mail->IsHTML(true);
    //Set who the message is to be sent from
    $mail->setFrom($from, 'Mailer CSLManager');
    //Set who the message is to be sent to
    $mail->addAddress($to, 'New User');
    //Set the subject line
    $mail->Subject = $subject;
    //Replace the plain text body with one created manually
    $mail->Body = $body;
    //send the message, check for errors
    $mail->CharSet = 'utf8';

    try{
       $result = $mail->send() ;
    } catch(Exception $e) {
        throw new Exception("Impossible to send mail");
    }
}

function createPassword()
{
    $password = "";
    for($i = 0; $i <= 6; $i++)
    {
        $random = rand(97,122);
        $password .= chr($random);
    }

    return $password;
}

function generateToken($form)
{
    $token = sha1(uniqid(microtime(), true));
    return $token;
}

function cryptEmail($email){
    $emailExplode = explode("@", $email);
    $emailSplit = str_split ($emailExplode[0]);
    $size = sizeof($emailSplit);
    $idCrypt = "";

    for ($i=0; $i < $size ; $i++):
        if($i != 0 && $i != $size-1 && $emailSplit[$i] != "."):
            $emailSplit[$i] = "*";
        endif;
        $idCrypt .= $emailSplit[$i];
    endfor;

    $idCrypt .= "@".$emailExplode[1];
    return $idCrypt;
}

function initiales($chaine)
{
    $words = explode(" ", $chaine);
    $initiale = '';

    foreach($words as $init){
        $initiale .= $init{0};
    }
    return strtoupper($initiale);
}
