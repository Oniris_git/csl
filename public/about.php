<?php

use CSLManager\Administration\Helper\Config;
use CSLManager\Administration\Mapper\LanguageMapper;

require __DIR__ . '/../lib/bootstrap.php';

$langMapper = new LanguageMapper($connector);
$languages = $langMapper->selectLanguages();

include __DIR__ . '/../view/front/about.phtml';
