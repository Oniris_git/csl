 <?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Vespula\Auth\Auth;
use CSLManager\Administration\Helper\Config;
use CSLManager\Administration\Mapper\ScaleMapper;
use CSLManager\Administration\Mapper\WorkshopAttemptMapper;
use CSLManager\Administration\Mapper\WorkshopMapper;
use CSLManager\Administration\Mapper\UserMapper;
use CSLManager\Administration\Mapper\LanguageMapper;

require __DIR__ . '/../lib/bootstrap.php';

if (!file_exists('./../config.php')) {
    header('Location: ./install/index.php');
}

$Gargs = [
    'poste' => FILTER_SANITIZE_STRING,
    'register' => FILTER_SANITIZE_STRING,
    "error" => FILTER_SANITIZE_STRING
];

$GET = filter_input_array(INPUT_GET, $Gargs);

$ERROR = [];
$WARN = [];


$etape = 'list';
if(isset($GET['error'])):
    switch ($GET['error']):
        case "expired":
            $ERROR = [
                "message" => TXT_LOG_LOGIN_TOKEN_EXPIRED
            ];
            break;
        case "token":
            $ERROR = [
                "message" => TXT_LOG_LOGIN_INVALID_TOKEN
            ];
            break;
    endswitch;
endif;

if (!empty($session->getValue('V_ERROR'))) {
    $ERROR = $session->getValue('V_ERROR');
}

if (!empty($session->getValue('V_WARN'))) {
    $WARN = $session->getValue('V_WARN');
}

if (!empty($session->getValue('V_SUCCESS'))) {
    $SUCCESS = $session->getValue('V_SUCCESS');
}

if($GET['register']):
    $SUCCESS = [
        "message" => TXT_REGISTER_SUCCESS
    ];
endif;

$session->unsetValue('V_ERROR');
$session->unsetValue('V_WARN');
$session->unsetValue('V_SUCCESS');
$requestedWorkshop = ($GET['poste'] !== null) ? $GET['poste'] : $session->getValue('poste');

if($auth->getUsername()){
    $uMapper = new UserMapper($connector);
    try{
        $user = $uMapper->getUserByUpn($auth->getUsername());
        $tokenPassword = generateToken('form');
        $uMapper->requestPassword($tokenPassword, $user->getId());
        $user->setToken($tokenPassword);
    }catch (Exception $e){
        $ERROR= [
            "message" => $uMapper->getPDOError()
        ];
        exit();
    }
}

$langMapper = new LanguageMapper($connector);
$languages = $langMapper->selectLanguages();

if ($requestedWorkshop) {

    if (!$auth->isValid()) {
        header('Location: login.php?poste=' . urlencode($requestedWorkshop));
        exit();
    }

    $wsMapper = new WorkshopMapper($connector);
    $waMapper = new WorkshopAttemptMapper($connector);

    try {
        $workshop = $wsMapper->getWorkshopByLabel($requestedWorkshop);
        $user = $uMapper->getUserByUpn($auth->getUsername());
    } catch (Exception $e) {
        // $logger->alert(
        //     TXT_LOG_ERROR_USER,
        //     ['user' => $auth->getUsername(),
        //      'workshop' => $requestedWorkshop]
        // );
        //$logger->debug($e->getMessage());
        $ERROR[] = $e->getMessage() . TXT_LOG_ERROR_CONTACT;
        require __DIR__.'/../view/front/index.phtml';
        exit();
    }

    /*$logger->info(
        TXT_LOG_REQUEST_WORKSHOP,
        [
            'user' => $user->getName() . " " . $user->getName(),
            'workshop' => $workshop->getFullLabel()
        ]
    );*/

    // WorkshopAttempt active
    $idActiveWa = $waMapper->getActive($user);

    try {
        if (!$idActiveWa || ($waMapper->getWorkshopAttemptByIdUser($user, $workshop) !== false && $waMapper->getWorkshopAttemptByIdUser($user, $workshop)->getIdWorkshop() === $workshop->getId())) {
            // No workshop attempt
            $wsMapper->setScaleType($workshop);

            if (!$idActiveWa) {
                try {
                    $waMapper->start($user, $workshop);
                    /*$logger->info(
                        TXT_LOG_REQUEST_WORKSHOP,
                        [
                             'user' => $user->getFirstName() . " " . $user->getName(),
                            'workshop' => $workshop->getFullLabel()
                        ]
                    );*/
                } catch (\Exception $e) {
                    //$logger->error(TXT_LOG_ERROR_START);
                    //$logger->debug($e->getMessage());
                    $WARN[] = $e->getMessage();
                }
            }

            $workshopAttempt = $waMapper->getWorkshopAttemptByIdUser($user, $workshop);

            if ($waMapper->checkCount($user, $workshop) < $workshop->getCountToValidate()) {
                /*$logger->info(
                    TXT_LOG_ERROR_VALIDATION,
                    [
                        'user' => $user->getFirstName() . " " . $user->getName(),
                        'workshop' => $workshop->getFullLabel()
                    ]
                );*/
                $WARN[] = TXT_LOG_ERROR_VALIDATION_OPERATION.' : '.$workshop->getFullLabel();
            }

            $session->setValue('workshop', $workshop->getLabel());
            $session->setValue('id-workshop_attempt', $workshopAttempt->getId());

            $token = $session->generateToken('form');

            $comment = $waMapper->getCommentsByWorkShopAttempt($workshopAttempt, $user);

            $etape = 'start';
        } else {
            $waMapper->start($user, $workshop);
            $workshopAttempt = $waMapper->getWorkshopAttemptByIdUser($user, $workshop);
            $workshop = $wsMapper->getWorkshopById($workshopAttempt->getIdWorkshop());
            /*$logger->info(
                TXT_LOG_ERROR_USER_VALIDATION,
                [
                    'user' => $user->getFirstName() . " " . $user->getName(),
                    'workshop' => $poste->getLabel()
                ]
            );*/
            $ERROR[] = TXT_LOG_ERROR_LINK.' <a href="?poste=' . $workshop->getLabel() . '">' . $workshop->getFullLabel() . '</a>';
        }
    } catch (\Exception $e) {
        $ERROR[] = $e->getMessage();
    }
}

if ($permission->check('edit:tp')) {
    if (!isset($waMapper)) {
        $waMapper = new WorkshopAttemptMapper($connector);
    }

    $scaleMapper = new ScaleMapper($connector);
    $active = $waMapper->active();
}

if ($permission->check('edit:tp')) {
    if (!isset($waMapper)) {
        $waMapper = new WorkshopAttemptMapper($connector);
    }

    $scaleMapper = new ScaleMapper($connector);
    $active = $waMapper->active($user);

    if($active != null){
        $i=0;
        foreach ($active as $link){
            if(isset($GET['poste'])){
                //check exercice active not equal $GET['poste']
                if($GET['poste'] != $link->getLabel()){
                    $WARN[$i]= "<a href='index.php?poste=".$link->getLabel()."'>".TXT_LOG_WORKSHOP_ATTEMPT_IN_PROGRESS.' : '.$link->getFullLabel()."</a>";
                }
            }else{
                //display list exercices active
                $WARN[$i]= "<a href='index.php?poste=".$link->getLabel()."'>".TXT_LOG_WORKSHOP_ATTEMPT_IN_PROGRESS.' : '.$link->getFullLabel()."</a>";
            }
            $i++;
        }
    }
}

require __DIR__.'/../view/front/index.phtml';

