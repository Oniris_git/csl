<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Vespula\Auth\Adapter\Ldap;
use Vespula\Ldap\LdapSearch;
use Vespula\Ldap\LdapWrapper;
use Vespula\Auth\Auth;
use Vespula\Auth\Exception;
use CSLManager\Administration\Helper\Config;
use CSLManager\Administration\Mapper\PermissionMapper;
use CSLManager\Administration\Entity\User;
use CSLManager\Administration\Mapper\UserMapper;
use CSLManager\Administration\Mapper\ConfigMapper;
require __DIR__ . '/../lib/bootstrap.php';

//put post into variables
$url = urldecode(filter_input(INPUT_GET, 'url', FILTER_SANITIZE_ENCODED));
$poste = urldecode(filter_input(INPUT_GET, 'poste', FILTER_SANITIZE_ENCODED));

$args = [
    "btnChange" => FILTER_SANITIZE_STRING,
    "idToChange" =>FILTER_SANITIZE_STRING
];

$POST = filter_input_array(INPUT_POST, $args, false);

//if user want to change his password
if(isset($POST['btnChange'])):
    if($POST['idToChange']!= null):
        try{
            $userMapper = new UserMapper($connector);
            $userToUpdate = $userMapper->getUserByUpn($POST['idToChange']);
            if($userToUpdate->getAuth() != "manual"):
                $authMessage = TXT_LOG_LOGIN_CONTACT_ADMIN;
            else:
                //we crypt mail to secure and we change token which is available for 15 minutes
                $email = cryptEmail($userToUpdate->getEmail());
                $newToken = generateToken('form');
                $userMapper->requestPassword($newToken, $userToUpdate->getId());
                $userUpdated = $userMapper->getUserById($userToUpdate->getId());
                $request = "changeMDP";
                sendMail($userUpdated, $connector, $request);
                $mailSendMessage = TXT_LOG_LOGIN_CHANGE_MAIL_SEND;
            endif;
        }catch(\Exception $e){
            $authMessage = $e->getMessage();
        }
    else:
        $authMessage = TXT_LOG_LOGIN_INPUT_EMPTY;
    endif;
else:
    //put config into variables / LDAP
    $configMapper = new ConfigMapper($connector);
    $authType = $configMapper->selectConfigByName("auth", 'type')['value'];

    if (!$auth->isValid() && $_SERVER['REQUEST_METHOD'] === "POST") {
        /**
         * Check if user exist / authenticate / rights list
         */
        $uMapper = new UserMapper($connector);
        $permission = new PermissionMapper($connector);

        //input post into credential tab
        $credentials = [
            'username' => filter_input(INPUT_POST, 'loginUsername'),
            'password' => filter_input(INPUT_POST, 'loginPassword')
        ];
        $registerType = $uMapper->getRegisterType($credentials['username']);
        $token = filter_input(INPUT_POST, 'token');

        /*$logger->info(
            TXT_LOG_LOGIN_CONNECTION,
            ['user' => $credentials['username']]
        );*/
        //if user does not exist in BDD / check if exist in LDAP
        if (!$session->verifyToken('login_admin', $token)) {
            $authMessage = TXT_LOG_LOGIN_INVALID_TOKEN;
        } else if (!$uMapper->exist($credentials['username'])) {
            if($configDB['authType'] == "ldap"){
                try{
                    $ldap = $container->get('adapter');
                    $result=$ldap->authenticate($credentials);
                } catch (Exception $e) {
                    $authMessage = TXT_LOG_LOGIN_INVALID_LDAP;
                }
                //if user exist in LDAP / create user in BDD and try login
                if($result !== true){
                    $authMessage = TXT_LOG_LOGIN_UNKNOWN_USER;
                }else{
//                    $ldap = new LdapSearch(new LdapWrapper(),
//                        $configLdap['ldapUri'],
//                        $configLdap['ldapBindOptions'],
//                        $configLdap['ldapOptions'],
//                        $configLdap['ldapPort']
//                    );
//
//                    //$ldap->setDefaultAttributes($configLdap['ldapAttributes']);
//                    //$ldap->setSortBy('cn');
//                    //$ldap->setTimestampFormat('ymd');
//                    $searchfilter = '(|('.$configLdap['ldapAttributes'][5].'='.$credentials['username'].'))';
//
//                    $result = $ldap->find($searchfilter);

                    $result = $ldap->looksupUserData($credentials['username']);

                    var_dump($result);die();

                    $firstName_Name = strtolower ($credentials['username']);
                    $firstName_Name = explode(".",$firstName_Name);
                    $user=[
                        'upn'=> strtolower ($credentials['username']),
                        'firstName'=>strtoupper(substr( $firstName_Name[0], 0, 1)).strtolower(substr($firstName_Name[0], 1)),
                        'name'=>strtoupper(substr( $firstName_Name[1], 0, 1)).strtolower(substr($firstName_Name[1], 1)),
                        'id_profile'=> 3,
                        'auth' => "ldap",
                        'email' => $result['mail'],
                        'open' => 0,
                        'request' => null,
                        'token' => null,
                    ];
                    try {
                        $uMapper->create($user);
                        $registerType = $uMapper->getRegisterType($credentials['username']);
                        $auth->login($credentials);
                    } catch (\Exception $e) {
                        /*$logger->alert(TXT_LOG_ERROR,[
                                'line' => __LINE__,
                                'message' => $e->getMessage()
                            ]
                        );
                        $authMessage = TXT_LOG_LOGIN_ERROR_CREATE;*/
                    }
                }
            }
        } else {
            //check if user account is manual and check password if true
             if($registerType['auth'] == "manual"):
                 $manual = $container->get('manual');
                 $checkPw = $manual->authenticate($credentials);
                if($checkPw == true):
                    $user = $manual->lookupUserData($credentials['username']);
                    $session->setStatus(Auth::VALID);
                    $session->setUsername($credentials['username']);
                else:
                    $session->setStatus("ANON");
                endif;
            elseif ($registerType['auth'] == "ldap"):
                try {
                    $auth->login($credentials);
                } catch (Exception $e) {
                    /*$logger->alert(
                        TXT_LOG_ERROR,
                        [
                            'line' => __LINE__,
                            'message' => $e->getMessage()
                        ]
                    );*/
                    $authMessage = $e->getMessage();
                    $auth->logout();
                }
            endif;
        }

        if (!$auth->isValid()) {
            if (empty($authMessage)) {
                // wrong input
                $authMessage = (!empty($adapter->getError()))
                    ? $adapter->getError()
                    : TXT_LOG_LOGIN_ERROR_INPUT;
            }
            //$logger->warning(TXT_LOG_LOGIN_ERROR_CONNECTION, ['message' => $authMessage]);
        }
        // if valid / update user
        if ($auth->isValid() && $registerType['auth'] == "ldap") {
            $session->setValue('upn', $auth->getUserdata($configLdap['ldapAttributes'][5]));
            $session->setValue('rights', $permission->load($credentials['username']));
            $session->setValue('email', $auth->getUserData('mail'));
            $session->unsetValue('login_admin_token');
            $permission->loadProfile($auth->getUserData($configLdap['ldapAttributes'][5]));
            $session->setValue('profile', $permission->profile);
            $session->setValue('upn', $auth->getUserData($configLdap['ldapAttributes'][5]));
            $session->setValue('email', $auth->getUserData('mail'));
            $session->unsetValue('login_admin_token');
            //$logger->info(TXT_LOG_LOGIN_SUCCESS_CONNECTION, ['user' => $credentials['username']]);
            if (true) { //idk who wrote this, i'm so tired
                    try{
                        $user = new User([
                            'name' => $configLdap['ldapAttributes'][1],
                            'first_name' => $configLdap['ldapAttributes'][0],
                            'description' => $configLdap['ldapAttributes'][4],
                            'upn' => $configLdap['ldapAttributes'][5],
                            'email' => $configLdap['ldapAttributes'][3],
                            'auth' => 'ldap',
                            'max_difficulty'=>1,
                            'id_profile'=>3
                        ]);
                        $uMapper->alter($user);
                    }catch (\Exception $e){
                        throw new \Exception($e);
                    }
                //$logger->info(TXT_LOG_LOGIN_UPDATE_USER, ['user' => $credentials['username']]);
            }
            //if valid / put user into session
        }else if($auth->isValid() && $registerType['auth'] == "manual"){
            $session->setValue('upn', $user->getUpn());
            $permission->loadProfile($user->getUpn());
            $session->setValue('rights', $permission->load($credentials['username']));
            $session->setValue('profile', $permission->profile);
            $session->unsetValue('login_admin_token');
            //$logger->info(TXT_LOG_LOGIN_SUCCESS_CONNECTION, ['user' => $credentials['username']]);
        }
    }
endif;
//redirect
if ($auth->isValid()) {
    if ($poste !== "") {
        header('Location: /index.php?poste=' . $poste);
    } elseif ($url !== "") {
        header('Location: ' . $url);
    } else {
       header('Location: index.php');
    }
} else {
    $token = $session->generateToken('login_admin');
    include '../view/login.phtml';
}
