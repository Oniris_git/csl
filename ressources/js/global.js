$('input[type="file"]').change(function(e){
    var fileName = e.target.files[0].name;
    $('.custom-file-label').html(fileName);
});


$(document).ready(function() {
    var loadingPic = $('.loading');
    $("#tables-note a:first").tab('show');
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled")
        $("#wrapperHome").toggleClass("toggled")
    });
});

//show popover
$(function () {
    $('[data-toggle="popover"]').popover()
});

//in side bar show or hide arrow
function arrowUser(){
    var target = document.getElementById("userDropDwn");
    if (target.style.visibility === "hidden"){
        target.style.visibility="visible";
    } else{
        target.style.visibility="hidden";
    }
};

//in side bar show or hide arrow
function arrowManagement(){
    var target = document.getElementById("managementDropDwn");
    if (target.style.visibility === "hidden"){
        target.style.visibility="visible";
    } else{
        target.style.visibility="hidden";
    }
};

//in side bar show or hide arrow
function arrowCustom(){
    var target = document.getElementById("customDropDwn");
    if (target.style.visibility === "hidden"){
        target.style.visibility="visible";
    } else{
        target.style.visibility="hidden";
    }
};

function arrowServer(){
    var target = document.getElementById("serverDropDwn");
    if(target.style.visibility === "hidden"){
        target.style.visibility = "visible";
    }else{
        target.style.visibility = "hidden";
    }
}

$('#myModal').on('shown.bs.modal', function () {
    $('#myInput').trigger('focus')
});

function showLang(){
    var flag = document.getElementsByClassName("flag");
    var size = flag.length;
    for ($i=0;$i<size;$i++){
        if(flag[$i].style.visibility == "hidden" ){
            flag[$i].style.visibility = "visible";
        }else{
            flag[$i].style.visibility = "hidden";
        }
    }
}

