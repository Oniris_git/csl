//instantiation of qr code reader

let scanner = new Instascan.Scanner({
    video: document.getElementById('preview')
});

scanner.addListener('scan', function (content) {
    var targetLink = document.getElementById("targetLink");
    targetLink.innerHTML = "<div style='text-align: center'><a href='"+content+"' >"+content+"</a></div>";
});


Instascan.Camera.getCameras().then(function (cameras) {
    var container = document.getElementById('camera-container')
    if (cameras.length > 0) {
        var select = document.createElement('select')
        select.onchange = function(){scanner.start(cameras[this.value])}
        container.appendChild(select)
        for (let i = 0; i < cameras.length; i++) {
            let option = document.createElement('option')
            option.value = i
            option.innerHTML = cameras[i].name  
            option.id = 'camera'+i   
            select.appendChild(option)       
        }
        
    } else {
        var targetLink = document.getElementById("targetLink");
        targetLink.innerHTML = "<h4 style='color: #d43f3a; text-align: center'>No Camera detected</h4>";
        console.error('No cameras found.');
    }
}).catch(function (e) {
    console.error(e);
});
