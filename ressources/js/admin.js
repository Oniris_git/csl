//JQuery to select the scale type  to delete and send to .php and return response in pop up
$('.del-scale_type').on('click',function(){
    if(!confirm('Delete ?'))return;
    var b=$(this);
    var idScale_type=$(this).data('id-scale_type');
    $.post('/back-office/ajax/admin/scale_type.php',{
        action:'del-scale_type','scale_type-id':idScale_type
    },function(data){
        if(!data.success){
            notification.notify(data.message, 'danger')
        }else{notification.notify(data.message, 'success');
            $(b).closest("tr").remove()
        }
    },'json')
});

//JQuery to select the room  to delete and send to .php and return response in pop up
$('.del-room').on('click',function(){
    if(!confirm('Delete ?'))return;
    var b=$(this);
    var roomId=$(this).data('room-id');
    $.post('/back-office/ajax/admin/room.php',{
        action:'del-room','room-id':roomId
    },function(data){
        if(!data.success){
            notification.notify( data.message, 'danger')
        } else{
            notification.notify(data.message, 'success');
            $(b).closest('.card-room').remove()
        }
    },'json')
});

//JQuery to select the category  to delete and send to .php and return response in pop up
$('.del-category').on('click',function(){
    if(!confirm('Delete ?'))return;
    var b=$(this);
    var idCategory=$(this).data('category-id');
    $.post('/back-office/ajax/admin/category.php',{
        action:'del','idCategory':idCategory
    },function(data){
        if(!data.success){
            notification.notify(data.message, 'danger')
        } else{
            notification.notify(data.message, 'success');
            $(b).closest('.categoryLine').remove()
        }
    },'json')
});

//JQuery to select the sub category  to delete and send to .php and return response in pop up
$('.del-sub-category').on('click',function(){
    if(!confirm('Delete ?'))return;
    var b=$(this);
    var idSubCategory=$(this).data('sub-category-id');
    $.post('/back-office/ajax/admin/sub_category.php',{
        action:'del','idSubcategory':idSubCategory
    },function(data){
        if(!data.success){
            notification.notify(data.message, 'danger')
        } else{
            notification.notify(data.message, 'success');
            $(b).closest('.subCategoryLine').remove()}
    },'json')
});

//JQuery to select the discipline  to delete and send to .php and return response in pop up
$('.del-discipline').on('click',function(){
    if(!confirm('Delete ?'))return;
    var b=$(this);
    var idDiscipline=$(this).data('discipline-id');
    $.post('/back-office/ajax/admin/discipline.php',{
        action:'del','idDiscipline':idDiscipline},function(data){
        if(!data.success){
            notification.notify(data.message, 'danger')
        } else{
            notification.notify(data.message, 'success');
            $(b).closest('.disciplineLine').remove()}
    },'json')
});

//JQuery to select the priority level to delete and send to .php and return response in pop up
$('.del-priority_level').on('click',function(){
    if(!confirm('Delete ?'))return;
    var b=$(this);
    var idPriorityLevel=$(this).data('priority_level-id');
    $.post('/back-office/ajax/admin/priority_level.php',{
        action:'del','idPriority_level':idPriorityLevel
    },function(data){
        if(!data.success){notification.notify(data.message, 'danger')
        } else{
            notification.notify(data.message, 'success');
            $(b).closest('.priorityLevelLine').remove()}
    },'json')
});

//JQuery to select the specie to delete and send to .php and return response in pop up
$('.del-specie').on('click',function(){
    if(!confirm('Delete ?'))return;
    var b=$(this);
    var idSpecies=$(this).data('specie-id');
    $.post('/back-office/ajax/admin/species.php',{
        action:'del','idSpecies':idSpecies},function(data){
        if(!data.success){notification.notify(data.message, 'danger')
        } else{
            notification.notify(data.message, 'success');
            $(b).closest('.specieLine').remove()}
    },'json')
});

//JQuery to select the profile  to delete and send to .php and return response in pop up
$('.del-profile').on('click',function(){
    if(!confirm('Delete ?'))return;
    var b=$(this);
    var idProfile=$(this).data('profile-id');
    console.log(idProfile);
    $.post('/back-office/ajax/admin/profile.php',{
        action:'del-profile','profile-id':idProfile},function(data){
        if(!data.success){
            notification.notify(data.message, 'danger')
        } else{
            notification.notify(data.message, 'success');
            $(b).closest('.profileLine').remove()}
    },'json')
});

//JQuery to select the workshop  to delete and send to .php and return response in pop up
$('.del-workshop').on('click',function(e){
    e.preventDefault();
    var poste=$(this);
    var poste_id=poste.attr('data-id-workshop');
    console.log(poste_id);
    var icon='';
    if(isNaN(poste_id)){
        poste.button('error');return!1
    }
    $.post('/back-office/ajax/workshop.php',{
        action:'del',del_id:poste_id
    },function(data){
        if(!data.success){
            icon="fa-exclamation";poste.prop('disabled',!1)
        }
        else{
            icon="fa-check"
        }
        var message='<i class="fa '+icon+'"></i> '+data.message;
        notification.notify(data.message,'info')
    },'json')
});

$('#importLDAP').on('click',function(){
    var button=$(this);
    button.prop('disabled',1);button.button('loading');
    $.post('/back-office/ajax/admin/user.php',{action:'import'},function(data){
        notification.notify(data.message,'info');
        button.text(data.message);
        button.prop('disabled',0)
    },'json')
});

//use to change submit action PHP to XHR request
$('.auto-submit-item').change(function(e){
    e.preventDefault();
    var form=$(this).parents('form');
    var formData=form.serialize();
    $.ajax({
        method:form.attr('method'),url:form.attr('action'),data:formData,dataType:'json',encode:!0
    }).done(function(data){
        var success;
        var icon;
        if(data.success){
            success='success';
            icon='fa-check'
        }else{
            success='danger';
            icon='fa-warning'
        }
        notification.notify('<i class="fa '+icon+'"></i> '+data.message,success)
    }).fail(function(){
        notification.notify('<i class="fa fa-warning"></i> Une erreur est survenue lors de la requête','warning')
    })
}).on('keyup keypress',function(e){
    var keyCode=e.keyCode||e.which;
    if(keyCode===13){
        e.preventDefault();
        return!1
    }
});

$('form').not('.ajax-search').not('#syncform').submit(function(event){
    event.preventDefault();
    var icon="";
    var success="";
    var form=$(this);
    var formData=form.serialize();
    $.ajax({
        method:form.attr('method'),url:form.attr('action'),data:formData,dataType:'json',encode:!0
    }).done(function(data){
        if(!data.success){
            icon="fa-exclamation";success="danger"
        } else{
            icon="fa-check";
            success="success";
            /*if(typeof data.request!=='undefined'&& data.request.action=='create-scale_type'){
                tr=document.createElement('tr');
                tr.innerHTML='<td>'+data.groupe["scale_type-label"]+'</td>'+
                    '<td><a href="?groupe='+data.groupe["scale_type-id"]+
                    '"><i class="fa fa-cog"></i></td>'+
                    '<td><span class="suppr-group" data-id="'+data.groupe["scale_type-id"]+
                    '"><i class="fa fa-times"></i></span></td>';
            document.getElementById('table-groupe').appendChild(tr)
            }*/
        }
        var message='<span class="text-'+success+'"><i class="fa '+icon+'"></i> '+data.message+'</span>';
        notification.notify(message,success)
    })
});


/*$('.btn-fin-tp').on('click',function(event){
    var el=$(event.target);
    var input=$(el).closest(".input-group").find('input');
    var tp_id=input.data('tp-id');
    var tp_note=el.data('eval');
    if(input.val()<=0||input.val()===''){
        input.parent().addClass("has-error");
        return!1
    }
    var formData={
        'action':'fin-tp','duree-tp':input.val(),'mod-id':tp_id,'note-tp':tp_note
    };
    input.parent().removeClass("has-error");
    var jqXHR=$.post('/back-office/ajax/workspace.php',formData,null,'json');
    jqXHR.done(function(response){
        notification.notify(response.message,'info');
        if(response.success){
            el.closest('.panel').remove()
        }else{
            input.parent().addClass("has-error")}
    });
    jqXHR.fail(function(){
        notification.notify('Erreur de communication avec le serveur.','warning')
    })
});*/

$('#syncform').submit(function (event) {
    event.preventDefault();
    var icon = "";
    var success = "";
    var form = $(this);
    var formData = form.serialize();
    $.ajax({
        method: form.attr('method'),
        url: form.attr('action'),
        data: formData,
        dataType: 'json',
        encode: !0,
        beforeSend: function(){
            $("#loading-overlay").show();
        },
    }).done(function (data) {
        $("#loading-overlay").hide();
        if (!data.success) {
            icon = "fa-exclamation";
            success = "danger"
        } else {
            icon = "fa-check";
            success = "success"
        }
        var message = '<span class="text-' + success + '"><i class="fa ' + icon + '"></i> ' + data.message + '</span>';
        notification.notify(message, success)
    })
})





