var notification = (function () {
    var config = {
        id: 'notif-box',
        duration: 5000,
        timeoutid: 0
    };

    // Cache DOM
    var body = document.body;
    var template = document.querySelector('#notification');
    var content = template.content;

    function _render() {
        body.appendChild(document.importNode(content, true));
    }

    function _destroy() {
        var el = document.getElementById(config.id);
        if (el !== null)
            el.parentNode.removeChild(el);
    }

    function notify(value, level) {
        var lvl = typeof level !== 'undefined' ? level : 'info';

        clearTimeout(config.timeoutid);
        _destroy();
        content.querySelector('div').classList.add('bg-' + lvl);
        content.querySelector('p').innerHTML = value;
        _render();
        config.timeoutid = setTimeout(function () {
            _destroy();
        }, config.duration);
    }

    return {
        notify: notify
    };
})();