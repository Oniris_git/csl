<?php

//----------- Fichiers de langue en français de VirtualVet--------------------
define('TXT_LABEL', 'Libellé');
define('TXT_ACCOUNT', 'Type de compte');
define('TXT_MANUAL_ACCOUNT', 'Manuel');
define('TXT_LDAP_ACCOUNT', 'LDAP');
define('TXT_FULL_LABEL', 'Libellé long');
define('TXT_DESCRIPTION', 'Description :');
define('TXT_EDIT', 'Modifier');
define('TXT_ADD', 'Ajouter');
define('TXT_DELETE', ' Supprimer');
define('TXT_PRINT', 'Imprimer');
define('TXT_ACTIVE', 'Actif');
define('TXT_SCALE', 'Barême');
define('TXT_DOMAIN', 'Domaine');
define('TXT_BROWSE', 'Parcourir');
define('TXT_ROOM', 'Salle ');
define('TXT_PROFILE', 'Profil');
define('TXT_ADMIN', 'Administrateur');
define('TXT_MANAGER', 'Gestionnaire');
define('TXT_STUDENT', 'Etudiant');
define('TXT_ID', 'Code');
define('TXT_NOTREADY', 'Code non encore développé');
define('TXT_ID_HELP', 'Attention, cet identifiant est unique et sera ensuite verrouillé.');
define('TXT_LOGIN', 'Identifiant');
define('TXT_PASSWORD', 'Mot de passe');
define('TXT_NEW_PASSWORD', 'Nouveau mot de passe');
define('TXT_PASSWORD_CONFIRM', 'Confirmer mot de passe');
define('TXT_NAME', 'Nom');
define('TXT_NAME_MIN', 'nom');
define('TXT_FORENAME', 'Prénom');
define('TXT_ENTER', 'Entrer');
define('TXT_DISCONNECT', 'Déconnexion');
define('TXT_CONNECT', 'Connexion');
define('TXT_WARNING', 'Attention !');
define('TXT_ERROR', 'Erreur');
define('TXT_APPLY', ' Envoyer');
define('TXT_CANCEL', ' Annuler l\'exercice');
define('TXT_IMPORT', ' Importer');
define('TXT_PARAMETERS', ' Paramétrer');
define('TXT_MOODLE', ' Lien vers Moodle');
define('TXT_WORKSHOP_DIFFICULTY', 'Niveau');
define('TXT_EXERCICE', 'Exercice');
define('TXT_LINK','Lien');
define('TXT_START','Début');
define('TXT_DURATION','Durée');
define('TXT_WELCOME','Bienvenue ');
define('TXT_FIRST_NAME','Prénom ');
define('TXT_NAME_FIELD','Nom ');
define('TXT_REGISTER','Inscription ');
define('TXT_REGISTER_FIELD','prenom.nom ');
define('TXT_REGISTER_MAIL','Email ');
define('TXT_REGISTER_SUCCESS','Inscription complété avec succès');
define('TXT_REGISTER_ERROR','Erreur dans la crétion de l\'utilisateur');
define('TXT_REGISTER_USERNAME_ERROR','L\'identifiant n\'est pas formé correctement');
define('TXT_REGISTER_MDP_ERROR','Les mots de passe doivent être identiques');
define('TXT_REGISTER_MDP_ERROR_LENGTH','Le mot de passe doit comprendre 6 caractères au minimum');
define('TXT_HELP','Aide');
define('TXT_FILTER','Trier');
define('TXT_CANCELED_WORKSHOP_ATTEMPT','Exercice annulé');
define('TXT_ERROR_CANCELED_WORKSHOP_ATTEMPT','Impossible d\'annuler l\'exercice contactez un responsable');

//--------------------Page install.phtml---------------------------------
define('TXT_INDEX_PAGE_OVERVIEW', 'Vue générale');

//--------------------Page header.phtml---------------------------------
define('TXT_HEADER_ADMIN_PANEL', 'Panel d\'administration');

//--------------------Page login.phtml---------------------------------
define('TXT_LOGIN_PAGE_CONNECTION', 'Page de connexion');

//--------------------Page navbar.phtml---------------------------------
define('TXT_NAVBAR_FEEDBACK', 'Contactez-nous');
define('TXT_NAVBAR_TOGGLE', 'Toggle navigation');
define('TXT_NAVBAR_HOME', 'Home');

//--------------------Page side_navbar.phtml---------------------------------
define('TXT_SIDE_NAVBAR_DASHBOARD', 'Tableau de bord');
define('TXT_SIDE_NAVBAR_WORKSHOPS', 'Exercices');
define('TXT_SIDE_NAVBAR_USERS', ' Utilisateurs');
define('TXT_SIDE_NAVBAR_PREFERENCES', 'Préférences');
define('TXT_SIDE_NAVBAR_ADMINISTRATION', 'Administration');
define('TXT_SIDE_NAVBAR_PROFILES', 'Profils');
define('TXT_SIDE_NAVBAR_RANK', 'Rangs');
define('TXT_SIDE_NAVBAR_SPECIES', 'Espèces');
define('TXT_SIDE_NAVBAR_CATEGORY', 'Catégories');
define('TXT_SIDE_NAVBAR_SUB_CATEGORY', 'Sous catégories');
define('TXT_SIDE_NAVBAR_DISCIPLINE', 'Disciplines');
define('TXT_SIDE_NAVBAR_SCALES', 'Barêmes');
define('TXT_SIDE_NAVBAR_STATS', 'Statistiques');
define('TXT_SIDE_NAVBAR_ROOMS', 'Salles');
define('TXT_SIDE_NAVBAR_HOME', 'Accueil');
define('TXT_SIDE_MANAGEMENT', ' Gestion');
define('TXT_SIDE_USER', ' Utilisateur');
define('TXT_SIDE_CUSTOM', ' Personnalisation');
define('TXT_SIDE_LOGO', ' Logo');
define('TXT_SIDE_LANG', ' Langues');
define('TXT_SIDE_LEGAL', ' Mentions légales');
define('TXT_SIDE_SERVER', ' Serveur');
define('TXT_SIDE_AUTH', ' Authentification');
define('TXT_SIDE_MAILER', ' Mailer');
define('TXT_SIDE_ROOTING', ' Chemins Moodle');

//--------------------Page skel.phtml---------------------------------
define('TXT_SKEL_NOSCRIPT', 'JavaScript est requis pour bénéficier de toutes les fonctionnalités !');

//--------------------Page feedback.phtml---------------------------------
define('TXT_FEEDBACK', 'Feedback');
define('TXT_FEEDBACK_HELP', 'Une remarque ? Une suggestion ? Un bug ?<br/>Vous pouvez contacter l\'administrateur du site via ce formulaire.');
define('TXT_FEEDBACK_ABUSE', 'Merci de ne pas abuser de ce service');
define('TXT_FEEDBACK_SUBJECT', 'Objet');
define('TXT_FEEDBACK_COMMENT', 'Remarque');
define('TXT_FEEDBACK_SUGGESTION', 'Suggestion');
define('TXT_FEEDBACK_GLITCH', 'Bug');
define('TXT_FEEDBACK_MESSAGE', 'Message');

//--------------------Page stat.phtml---------------------------------
define('TXT_STAT_USERSTAT', ' Statistiques de l\'utilisateur ');
define('TXT_STAT_CSV_USERSTAT', ' Export CSV statistiques de l\'utilisateur ');
define('TXT_STAT_GLOBALSTAT', 'Statistiques globales');
define('TXT_STAT_CSVSTAT', 'Export global en CSV');
define('TXT_STAT_EXERCICE_LIST', 'Liste des exercices effectués ');
define('TXT_STAT_AVERAGE', ' moyenne');
define('TXT_STAT_WORDING', 'Libellé');
define('TXT_STAT_EVALUATION', 'Scale');
define('TXT_STAT_TIME', 'Temps');
define('TXT_STAT_DATE', 'Date');
define('TXT_STAT_CLOSE', 'Fermer');
define('TXT_STAT_REMAINING_EXERCICE', ' Exercices &agrave; faire');
define('TXT_STAT_DISTINCTS_EXERCICE', ' Exercices distincts');
define('TXT_STAT_MODAL_TITLE', 'Liste des exercices restants &agrave; faire');
define('TXT_STAT_COUNT_TRY', 'Nombre de tentatives');
define('TXT_STAT_BEST_TIME', 'Meilleur temps');
define('TXT_STAT_BEST_GRADE', 'Meilleure note');
define('TXT_STAT_SHOW_GRADES', 'Répartition des notes de ');

//--------------------Page stat_filter.phtml---------------------------------
define('TXT_STAT_FILTER_TITLE_FILTER',' Trier les statistiques');
define('TXT_STAT_FILTER_CHOOSE','Choisissez une option');
define('TXT_STAT_FILTER_ACTION','Filtrer');
define('TXT_STAT_FILTER_EXERCICE','Exercice');
define('TXT_STAT_FILTER_USER','Utilisateur');
define('TXT_STAT_FILTER_GRADE','Grade');

//--------------------Page workshop_filter.phtml---------------------------------
define('TXT_WORKSPACE_FILTER_BUTTON',' Trier les exercices');
define('TXT_WORKSPACE_FILTER_ROOM','Salle');
define('TXT_WORKSPACE_FILTER_CATEGORY','Cat&eacute;gorie');
define('TXT_WORKSPACE_FILTER_SUB_CATEGORY','Sous cat&eacute;gorie');
define('TXT_WORKSPACE_FILTER_SPECIE','Espece');
define('TXT_WORKSPACE_FILTER_COUNT_TO_VALIDATE','Nombre  de tentatives');
define('TXT_WORKSPACE_FILTER_COUNT_DIFFICULTY','Niveau');
define('TXT_WORKSPACE_FILTER_RANK','Rang');
define('TXT_WORKSPACE_DISCIPLINE','Discipline');

//--------------------Page list_priority_level.phtml---------------------------------
define('TXT_GRADE_GLOBAL_T','Global ternaire');
define('TXT_GLOBAL_Q','Global quaternaire');
define('TXT_GLOBAL','Global');
define('TXT_DISTINCTS_T','Distinct ternaire');
define('TXT_DISTINCT_Q','Distinct quaternaire');
define('TXT_DISTINCT','Distinct');

//--------------------Page list-woorkspace.phtml---------------------------------
define('TXT_LIST_WORKSPACE_ROOM','salle');
define('TXT_LIST_WORKSPACE_CATEGORY','cat&eacute;gorie');
define('TXT_LIST_WORKSPACE_SUB_CATEGORY','sous cat&eacute;gorie');
define('TXT_LIST_WORKSPACE_SPECIE','espèce');
define('TXT_LIST_WORKSPACE_COUNT_TO_VALIDATE','nombre de tentatives');
define('TXT_LIST_WORKSPACE_DIFFICULTY','niveau');
define('TXT_LIST_WORKSPACE_RANK','bar&ecirc;me');
define('TXT_LIST_WORKSPACE_DISCIPLINE','discipline');
define('TXT_NOT_COMPLETE','Non renseign&eacute;');
define('TXT_ROOM_NUMBER','Salle n&deg; : ');
define('TXT_LIST_WORKSPACE_CATEGORY_TITLE','Cat&eacute;gorie : ');
define('TXT_LIST_WORKSPACE_SUB_CATEGORY_TITLE','Sous cat&eacute;gorie : ');
define('TXT_LIST_WORKSPACE_SPECIE_TITLE','Espèce :');
define('TXT_LIST_WORKSPACE_COUNT_TO_VALIDATE_TITLE','Nombre de tentatives : ');
define('TXT_LIST_WORKSPACE_DIFFICULTY_TITLE','Niveau : ');
define('TXT_LIST_WORKSPACE_RANK_TITLE','Bar&ecirc;me : ');
define('TXT_LIST_WORKSPACE_DISCIPLINE_TITLE','Discipline : ');
define('TXT_LIST_WORKSPACE_FILTER_BY',' Filtre par : ');

//--------------------Page list_user_filter.phtml---------------------------------
define('TXT_USER_FILTER_USERS',' Utilisateurs');
define('TXT_USER_FILTER_SEARCH','Lancer la recherche');
define('TXT_USER_SEARCH_DONE_ON','Recherche effectu&eacute; sur le champ ');
define('TXT_USER_FILTER_SEARCH_PLACEHOLDER','Rechercher par nom ou pr&eacute;nom...');
define('TXT_USER_FILTER_NAME','Nom');
define('TXT_USER_FILTER_FIRSTNAME','Pr&eacute;nom');
define('TXT_USER_FILTER_UPN','UPN');
define('TXT_USER_FILTER_PROFIL','Profil');

//--------------------Page list_priority_level.phtml---------------------------------
define('TXT_LIST_GRADE_USER','Utilisateur');
define('TXT_LIST_GRADE_WORDING','Libell&eacute;');
define ('TXT_LIST_GRADE_DATE','Date');
define('TXT_LIST_GRADE_MODAL_TITLE','Aide');
define('TXT_LIST_GRADE_DESCRIPTION','Le tri est effectu&eacute; de la fa&#231;on suivante :');
define ('TXT_LIST_GRADE_GLOBAL_TERNAIRE','"Global ternaire" affiche tous les exercices effectu&eacute;s selon le bar&#234;me de notation ternaire.');
define ('TXT_LIST_GRADE_GLOBAL_QUATERNAIRE','"Global quaternaire" affiche tous les exercices effectu&eacute;s selon le bar&#234;me de notation quaternaire.');
define ('TXT_LIST_GRADE_GLOBAL','"Global" affiche tous les exercices effectu&eacute;s.');
define ('TXT_LIST_GRADE_DISTINCT_TERNAIRE','"Distinct ternaire" affiche les exercices effectu&eacute;s par des utilisateurs diff&eacute;rents selon le bar&#234;me de notation ternaire.');
define ('TXT_LIST_GRADE_DISTINCT_QUATERNAIRE','"Distinct quaternaire" affiche tous les exercices effectu&eacute;s par des utilisateurs diff&eacute;rents selon le bar&#234;me de notation quaternaire.');
define ('TXT_LIST_GRADE_DISTINCT','"Distinct" affiche tous les exercices effectu&eacute;s par des utilisateurs diff&eacute;rents.');

//--------------------Page list_workshop_attempt.phtml---------------------------------
define('TXT_LIST_EXERCICE_WORDING','Libell&eacute;');
define('TXT_LIST_EXERCICE_USER','Utilisateur');
define('TXT_LIST_EXERCICE_DURATION','Dur&eacute;e');

//--------------------Page technicalsheet_exercice.phtml---------------------------------
define('TXT_TECHNICALSHEET_TITLE',' Données résumées de l\'exercice');
define('TXT_TECHNICALSHEET_TITLE_MODAL_USERS','Liste des utilisateurs');
define('TXT_TECHNICALSHEET_DESCRIPTION','Description');
define('TXT_TECHNICALSHEET_MODAL_TITLE','Description de l\'exercice');
define('TXT_TECHNICALSHEET_ROOM','Salle');
define('TXT_TECHNICALSHEET_SPECIE','Esp&egrave;ce');
define('TXT_TECHNICALSHEET_CATEGORY','Cat&eacute;gorie');
define('TXT_TECHNICALSHEET_TIME','Temps imparti');
define('TXT_CLOSE','Fermer');
define('TXT_TECHNICALSHEET_NUMBER_USER','Nombre d\'utilisateurs');
define('TXT_TECHNICALSHEET_AVERAGE_DURATION','Durée moyenne (minutes)');
define('TXT_TECHNICALSHEET_AVERAGE_TRY','Nombre moyen de tentatives');
define('TXT_TECHNICALSHEET_STATUS','Barême Acquis ou plus');
define('TXT_TECHNICALSHEET_DISTINCT','Utilisateurs distincts');
define('TXT_TECHNICALSHEET_COMENTS','Commentaires');
define('TXT_TECHNICALSHEET_COMENTS_TITLE','Commentaires des &eacute;tudiants');
define('TXT_TECHNICALSHEET_NO_COMENTS','Aucun commentaire pour cet exercice');
define('TXT_UPN','UPN');

//--------------------Page user.phtml---------------------------------
define('TXT_USER_EDIT_USER', 'Modifier les propriétés de l\'utilisateur');
define('TXT_USER_STAT_USER', 'Voir les statistiques de l\'utilisateur');
define('TXT_USER_MY_STAT', 'Voir vos statistiques');

//--------------------Page legal.phtml---------------------------------
define('TXT_LEGAL_SUCCESS', 'Enregistrement réussi');

//--------------------Page workshop.phtml---------------------------------
define('TXT_WORKSHOP_ADD_WORKSHOP', 'Ajouter un exercice');
define('TXT_WORKSHOP_ID', 'Libellé court');
define('TXT_WORKSHOP_LMS_COURSE', 'Cours Moodle');
define('TXT_WORKSHOP_LMS_ID', 'Moodle ID de l\'activité');
define('TXT_WORKSHOP_LMS_URL', 'Lien vers Moodle');
define('TXT_WORKSHOP_LEVEL', 'Niveau');
define('TXT_WORKSHOP_ATTEMPTS', 'Nb de tentatives pour valider');
define('TXT_WORKSHOP_NUMBER', 'Nb');
define('TXT_WORKSHOP_DURATION', 'Durée minimale');
define('TXT_WORKSHOP_DURATION_MAX', 'Durée maximale');
define('TXT_WORKSHOP_EDIT', 'Modifier les attributs de l\'exercice');
define('TXT_WORKSHOP_QRCODE', 'Le QRCode est généré en fonction de la racine du site et du libellé court.');
define('TXT_WORKSHOP_SYNCHRONISATION', ' Synchronisation Moodle');
define('TXT_WORKSHOP_QRCODE2', 'Si vous changez le libellé court, il faudra ré-imprimer le QRCode.');
define('TXT_WORKSHOP_PARAMETER_MISSING', 'Paramètres manquants');
define('TXT_WORKSHOP_ALTERED', "Poste modifié");
define('TXT_WORKSHOP_CREATED', "Poste créé");
define('TXT_WORKSHOP_DELETED', "Poste supprimé");
define('TXT_CATEGORY', 'Catégorie');
define('TXT_SUBCATEGORY', 'Sous-catégorie');
define('TXT_DISCIPLINE', 'Discipline');
define('TXT_RANK', 'Rang');
define('TXT_SPECIES', 'Espèce');

//--------------------Page admin/admin_scale_type.phtml---------------------------------
define('TXT_ADMIN_EVALUATION_SCALE_LIST', ' Liste des barêmes');
define('TXT_ADMIN_EVALUATION_SCALE_NB', 'Nombre de barêmes');
define('TXT_ADMIN_EVALUATION_ADD_SCALE', 'Ajouter un barême');
define('TXT_ADMIN_EVALUATION_HELP', 'Vous pouvez glisser-déposer les valeurs disponibles vers le barême');
define('TXT_ADMIN_EVALUATION_ADD_SCALE_ITEM', 'Ajouter un item au barême');
define('TXT_ADMIN_EVALUATION_AVAILABLE', 'Disponible');
define('TXT_ADMIN_EVALUATION_EDIT_SCALE', 'Modifier un barême');
define('TXT_ADMIN_EVALUATION_MOODLE_SCALES', ' Récupération des barêmes Moodle par l\'id');
define('TXT_ADMIN_EVALUATION_MOODLE_ADD_SUCCESS', ' Bareme moodle ajouté');
define('TXT_ADMIN_EVALUATION_MOODLE_ADD_ERROR', "Vous devez renseigné l'id du barême Moodle");

//--------------------Page front/install.phtml---------------------------------
define('TXT_FRONT_INDEX_MYSTATS', 'Mes statistiques');
define('TXT_FRONT_INDEX_WELCOME', 'Bienvenue à ');
define('TXT_FRONT_INDEX_BEGIN', 'Pour commencer, vous devez flasher le QR-code de l\'un des exercices');
define('TXT_FRONT_INDEX_MORNING', 'Bonjour');
define('TXT_FRONT_INDEX_THANKS', 'Merci');
define('TXT_FRONT_INDEX_CONNECTED', 'vous êtes connecté à l\'exercice');
define('TXT_FRONT_INDEX_SESSION_BEGIN', 'Début de session');
define('TXT_FRONT_INDEX_LMS_DOC', 'Documentation Moodle');
define('TXT_FRONT_INDEX_LMS_DOC_LINK', ' Lien vers la documentation');
define('TXT_FRONT_INDEX_WORKSHOP_DESCRIPTION', 'Description de l\'exercice');
define('TXT_FRONT_INDEX_REMINDER', 'À la fin de votre session, merci de renseigner votre évaluation');
define('TXT_FRONT_INDEX_EVALUATION', 'Évaluez-vous');
define('TXT_FRONT_INDEX_OPTION_LABEL', 'Sélectionnez');
define('TXT_FRONT_INDEX_COMMENT', 'Commentaire éventuel');
define('TXT_FRONT_INDEX_COMMENT_LENGTH', '200 caractères maximum');
define('TXT_FRONT_INDEX_ABOUT_PAGE', ' À propos');
define('TXT_FRONT_INDEX_DOMAIN', ' Domaine');

//--------------------Page blocks/list_user.phtml---------------------------------
define('TXT_BLOCKS_LIST_USER_MAIL', 'Mail');
define('TXT_BLOCKS_LIST_USER_FILTER', 'Filtrer');
define('TXT_DEPARTMENT', 'Promotion');

//--------------------Page blocks/stats.phtml---------------------------------
define('TXT_BLOCKS_STATS_PERMONTH', 'Par mois');
define('TXT_BLOCKS_STATS_TIME', 'Temps total');
define('TXT_BLOCKS_STATS_WORKSHOPS', ' Exercices faits ');
define('TXT_BLOCKS_STATS_WORKSHOPS_DISTINCTS', 'Exercices distincts');
define('TXT_BLOCKS_STATS_SPAN', 'Répartition');
define('TXT_BLOCKS_STATS_GRADES', 'Grades');
define('TXT_BLOCKS_STATS_COMMENT', 'Commentaire');
define('TXT_BLOCKS_STATS_CSV_EXPORT', 'Exporter en CSV');
define('TXT_BLOCKS_STATS_WORKSHOPS_MODAL_TITLE', 'Liste des exercices effectués');

//--------------------Page blocks/workshopAttemptInProgress.phtml---------------------------------
define('TXT_BLOCKS_PROGRESS_HEADER', 'Exercices en cours d\'utilisation');
define('TXT_BLOCKS_PROGRESS_BEGINBY', 'Commencé par');
define('TXT_BLOCKS_PROGRESS_BEGINDATE', 'Débuté le');

//--------------------Page admin/admin_profile.phtml---------------------------------
define('TXT_ADMIN_PROFILE_LIST', ' Liste des profils');
define('TXT_ADMIN_PROFILE_NB', 'Nombre de profils');
define('TXT_ADMIN_PROFILE_NAME', 'Nom du profil');
define('TXT_ADMIN_PROFILE_RIGHTS', 'Droits du profil');
define('TXT_ADMIN_ADD_PROFILE', 'Ajouter un profil');
define('TXT_ADMIN_PROFILE_RIGHTS_HELP', 'Cliquer pour une description');

//--------------------Page admin/admin_room.phtml---------------------------------
define('TXT_ADMIN_MANAGE_ROOM', ' Gestion des salles');
define('TXT_ADMIN_ADD_ROOM', 'Ajouter une salle');

//--------------------Page admin/admin_mailer.phtml---------------------------------
define('TXT_ADMIN_MAILER_TITLE', ' Paramètres du mailer');
define('TXT_ADMIN_NOTIFICATION_SUCCESS', ' Paramètres du mailer modifiés, un mail test vous a été envoyé dans votre boite mail');
define('TXT_ADMIN_MAILER_EMPTY', 'Le contenu ne peut être vide.');
define('TXT_ADMIN_MAILER_SEND_SUCCESS', 'Votre message à bien été envoyé.');
define('TXT_ADMIN_MAILER_SEND_ERROR', 'Une erreur est survenue dans l\'envoi du message. ');

//--------------------Page admin/admin_auth.phtml---------------------------------
define('TXT_ADMIN_AUTH_TITLE', ' Système d\'authentification');
define('TXT_ADMIN_AUTH_SUCCESS', ' Paramètres d\'authentification modifiés');
define('TXT_ADMIN_AUTH_ACTUAL', ' Informations actuellement enregistrées');
define('TXT_ADMIN_AUTH_AUTH', 'Authentification ');
define('TXT_ADMIN_AUTH_ATTRIBUTE', 'Attribut');
define('TXT_ADMIN_AUTH_VALUE', 'Valeur');
define('TXT_ADMIN_AUTH_ALTER', ' Modifier l\'authentification');
define('TXT_ADMIN_AUTH_LDAP', 'LDAP');
define('TXT_ADMIN_AUTH_AD', 'Active Directory');
define('TXT_ADMIN_AUTH_PARAMETERS', 'Paramêtres LDAP');

//--------------------Page admin/admin_rooting.phtml---------------------------------
define('TXT_ADMIN_ROOTING_SUCCESS', ' Paramètres Moodle modifiés');

//--------------------Page admin/admin_user.phtml---------------------------------
define('TXT_ADMIN_ADD_USER', ' Ajout d\'un utilisateur');
define('TXT_ADMIN_IMPORT_USER_LDAP', 'Import d\'utilisateurs via LDAP');
define('TXT_ADMIN_IMPORT_USER_CSV', 'Import d\'utilisateurs via CSV Compte Manuel');
define('TXT_ADMIN_IMPORT_USER_HELP', 'Les champs sont délimités par le <code>;</code>');
define('TXT_ADMIN_SYNC_USERS', 'Synchroniser les promotions');

//--------------------Page admin/admin_category.phtml-------------------------------
define('TXT_ADMIN_ADD_CATEGORY', 'Ajouter une catégorie');
define('TXT_ADMIN_CATEGORY_LIST', ' Liste des catégories');
define('TXT_ADMIN_CATEGORY_NB', 'Nombre de catégories');
define('TXT_ADMIN_CATEGORY_LIBELLE', 'Libellé ');
define('TXT_ADMIN_CATEGORY_DESCRIPTION', 'Description');
define('TXT_ADMIN_CATEGORY_MODIFY', 'Modifier');
define('TXT_ADMIN_CATEGORY_FORM_LIBELLE', 'Libellé ');
define('TXT_ADMIN_CATEGORY_FORM_DESCRIPTION', 'Description');
define('TXT_ADMIN_CATEGORY_BUTTON_ADD', 'Ajouter');
define('TXT_ADMIN_CATEGORY', 'Catégorie');
define('TXT_ADMIN_CATEGORY_BUTTON_ALTER', 'Modifier');
define('TXT_ADMIN_CATEGORY_ID', 'Identifiant');
define('TXT_ADMIN_CATEGORY_ERROR_UNKNOWN', 'Catégorie introuvable');

//--------------------Page admin/admin_sub_category.phtml-------------------------------
define('TXT_ADMIN_ADD_SUB_CATEGORY', 'Ajouter une sous-catégorie');
define('TXT_ADMIN_SUB_CATEGORY_LIST', ' Liste des sous-catégories');
define('TXT_ADMIN_SUB_CATEGORY_NB', 'Nombre de sous-catégories');
define('TXT_ADMIN_SUB_CATEGORY_LIBELLE', 'Libellé ');
define('TXT_ADMIN_SUB_CATEGORY_DESCRIPTION', 'Description');
define('TXT_ADMIN_SUB_CATEGORY_MODIFY', 'Modifier');
define('TXT_ADMIN_SUB_CATEGORY_FORM_LIBELLE', 'Libellé ');
define('TXT_ADMIN_SUB_CATEGORY_FORM_DESCRIPTION', 'Description');
define('TXT_ADMIN_SUB_CATEGORY_BUTTON_ADD', 'Ajouter');
define('TXT_ADMIN_SUB_CATEGORY', 'Catégorie');
define('TXT_ADMIN_SUB_CATEGORY_BUTTON_ALTER', 'Modifier');
define('TXT_ADMIN_SUB_CATEGORY_ID', 'Identifiant');

//--------------------Page admin/admin_species.phtml-------------------------------
define('TXT_ADMIN_ADD_SPECIES', 'Ajouter une espèce');
define('TXT_ADMIN_SPECIES_LIST', ' Liste des espèces');
define('TXT_ADMIN_SPECIES_NB', 'Nombre d\'espèces');
define('TXT_ADMIN_SPECIES_LIBELLE', 'Libellé ');
define('TXT_ADMIN_SPECIES_DESCRIPTION', 'Description');
define('TXT_ADMIN_SPECIES_MODIFY', 'Modifier');
define('TXT_ADMIN_SPECIES_FORM_LIBELLE', 'Libellé ');
define('TXT_ADMIN_SPECIES_FORM_DESCRIPTION', 'Description');
define('TXT_ADMIN_SPECIES_BUTTON_ADD', 'Ajouter');
define('TXT_ADMIN_SPECIES', 'Espèce');
define('TXT_ADMIN_SPECIES_BUTTON_ALTER', 'Modifier');
define('TXT_ADMIN_SPECIES_ID', 'Identifiant');
define('TXT_ADMIN_SPECIES_ERROR_UNKNOWN', 'Espèce introuvable');

//--------------------Page admin/admin_discipline.phtml-------------------------------
define('TXT_ADMIN_ADD_DISCIPLINE', 'Ajouter une discipline');
define('TXT_ADMIN_DISCIPLINE_LIST', ' Liste des disciplines');
define('TXT_ADMIN_DISCIPLINE_NB', 'Nombre de disciplines');
define('TXT_ADMIN_DISCIPLINE_LIBELLE', 'Libellé ');
define('TXT_ADMIN_DISCIPLINE_DESCRIPTION', 'Description');
define('TXT_ADMIN_DISCIPLINE_MODIFY', 'Modifier');
define('TXT_ADMIN_DISCIPLINE_FORM_LIBELLE', 'Libellé ');
define('TXT_ADMIN_DISCIPLINE_FORM_DESCRIPTION', 'Description');
define('TXT_ADMIN_DISCIPLINE_BUTTON_ADD', 'Ajouter');
define('TXT_ADMIN_DISCIPLINE', 'Discipline');
define('TXT_ADMIN_DISCIPLINE_BUTTON_ALTER', 'Modifier');
define('TXT_ADMIN_DISCIPLINE_ID', 'Identifiant');

//--------------------Page admin/admin_priority_level.phtml-------------------------------
define('TXT_ADMIN_ADD_RANK', 'Ajouter un rang');
define('TXT_ADMIN_RANK_LIST', ' Liste des rangs');
define('TXT_ADMIN_RANK_NB', 'Nombre de rang');
define('TXT_ADMIN_RANK_LIBELLE', 'Libelle ');
define('TXT_ADMIN_RANK_DESCRIPTION', 'Description');
define('TXT_ADMIN_RANK_MODIFY', 'Modifier');
define('TXT_ADMIN_RANK_FORM_LIBELLE', 'Libellé ');
define('TXT_ADMIN_RANK_FORM_DESCRIPTION', 'Description');
define('TXT_ADMIN_RANK_BUTTON_ADD', 'Ajouter');
define('TXT_ADMIN_RANK', 'Rang');
define('TXT_ADMIN_RANK_BUTTON_ALTER', 'Modifier');
define('TXT_ADMIN_RANK_ID', 'Identifiant');
define('TXT_ADMIN_RANK_FILTER', ' Trier par rang');

//--------------------Page admin/admin_poste.phtml-------------------------------
define('TXT_ADMIN_ADD_RIGHT', ' Ajouter un droit');

//--------------------Page block/list_profile.phtml-------------------------------
define('TXT_ADMIN_ADD_NEW_RIGHT', ' Ajouter des droits au profil');
define('TXT_ADMIN_DELETE_RIGHT', ' Retirer des droits au profil');
define('TXT_ADMIN_UPDATE_RIGHT', ' Modifier les droits du profil');

//--------------------Page language.phtml-------------------------------
define('TXT_LANG_MANAGE', ' Gestion des langues');
define('TXT_LANG_AVAILABLE', ' Langues disponibles');
define('TXT_LANG_ADD_FILE', ' Ajouter un fichier de langue en trois étapes');
define('TXT_LANG_ALTER_FILE', ' Modifier un fichier de langue en trois étapes');
define('TXT_LANG_DEFAULT', ' Langue par défault');
define('TXT_LANG_STEP_1', ' Etape 1 : Téléchargez un fichier de variables');
define('TXT_LANG_ALTER_STEP_1', ' Etape 1 : Téléchargez le fichier de variables');
define('TXT_LANG_STEP_2', ' Etape 2 : Renommez, Renseignez, Déposez');
define('TXT_LANG_STEP_ALTER_2', ' Etape 2 : Renseignez, Déposez');
define('TXT_LANG_STEP_3', ' Etape 3 : Déposez l\'image du drapeau');
define('TXT_LANG_CHANGE_DEFAULT', ' Modifier la langue par défault');
define('TXT_LANG_MODAL_STEP1', ' Téléchargez l\'un des fichiers de base pour ensuite modifier le contenu des variables');
define('TXT_LANG_MODAL_STEP2', ' Renommez le fichier, renseignez les variables puis redéposez votre fichier');
define('TXT_LANG_MODAL_STEP3', ' Télécharger l\'image du drapeau au format .png via le lien ci-dessous et déposez le' );

//--------------------Page view.phtml-------------------------------
define('TXT_VIEW_LOGO', ' Logo actuel');
define('TXT_VIEW_TITLE', ' Titre de l\'application');
define('TXT_VIEW_SUPPORT', ' Mail support technique');
define('TXT_VIEW_ALTER_LOGO', ' Modifier le logo');
define('TXT_VIEW_SAVE_CHANGE', ' Enregistrer les modifications');
define('TXT_CUSTOM_APPLI', "Personnalisez votre application");
define('TXT_CUSTOM_FRONT', " Visuel");
define('TXT_CUSTOM_LOGO', " Logo obligatoire");

//--------------------To display in notification-------------------------------
define('TXT_COLOPHON_TITLE', 'À propos');
define('TXT_COLOPHON_P1', ' Manager est une web-application développée conjointement entre les équipes de l\'Ecole nationale vétérinaire d\'Alfort(');
define('TXT_COLOPHON_P2', ', Maisons-Alfort) et de l\'Ecole nationale vétérinaire, agroalimentaire et de l’alimentation ');
define('TXT_COLOPHON_P3', 'Cette application est destinée aux salles de simulation médicale vétérinaire, constituées d’un grand nombre de postes de travail sur lesquels les étudiants en formation initiale s’exercent à l’acquisition de gestes techniques et de procédures en mode libre accès.');
define('TXT_COLOPHON_P4', 'Elle permet aux étudiants de s’auto-évaluer et de générer des statistiques exportables sur les exercices réalisés ou restant à faire.');
define('TXT_COLOPHON_P5', 'Elle permet aussi au gestionnaire des salles, par une interface d\'administration, de gérer la liste des postes de travail (création / modification / suppression), de suivre la progression des étudiants et de réaliser un ensemble de statistiques sur l’activité des étudiants.');
define('TXT_COLOPHON_P6', ' Elle s\'interface aussi avec les instances Moodle au moyen de WebServices, ce qui permet de créer un lien vers un espace de cours où sont déposés des documents complémentaires (vidéos, notices etc.) et où les acquis de l’étudiant peuvent alimenter son parcours de formation.');
define('TXT_COLOPHON_TITLE_2', 'Colophon');
define('TXT_COLOPHON_DESIGNER', 'Concepteurs :');
define('TXT_COLOPHON_DEV', 'Développeurs :');
define('TXT_COLOPHON_TRANSLATORS', 'Traducteurs :');
define('TXT_COLOPHON_LIB', 'Librairies :');
define('TXT_COLOPHON_LIB_TITLE', ' Manager fait usage de ces différentes librairies open sources :');
define('TXT_COLOPHON_DISTRIBUTION', ' Manager est distribué sous licence GPLv3. Il est ainsi protégé par le copyleft : vous pouvez librement modifier le code, mais tout usage commercial sous quelque forme que ce soit est prohibé.');

//--------------------To display in notification-------------------------------
define('TXT_NOTIFICATION_NO_RIGHT', 'Vous n\'avez pas les droits.');
define('TXT_NOTIFICATION_IMPORT_SUCCESS', 'Import terminé');
define('TXT_NOTIFICATION_CHECK_VALUES', 'Vérifiez vos valeurs');
define('TXT_NOTIFICATION_ADD_USER', 'Utilisateur ajouté');
define('TXT_NOTIFICATION_UNKNOWN_ACTION', 'Action inconnue');
define('TXT_NOTIFICATION_ERROR', 'Une erreur est survenue : ');
define('TXT_NOTIFICATION_ALTER_CATEGORY', 'Catégorie modifiée');
define('TXT_NOTIFICATION_DEL_CATEGORY', 'Categorie supprimée');
define('TXT_NOTIFICATION_DEL_DISCIPLINE', 'Discipline supprimée');
define('TXT_NOTIFICATION_ALTER_DISCIPLINE', 'Discipline modifiée');
define('TXT_NOTIFICATION_ADD_PRIORITY_LEVEL', 'Rang créé');
define('TXT_NOTIFICATION_ALTER_PRIORITY_LEVEL', 'Rang modifié');
define('TXT_NOTIFICATION_DEL_PRIORITY_LEVEL', 'Rang supprimé');
define('TXT_NOTIFICATION_ADD_PROFILE', 'Profil créé');
define('TXT_NOTIFICATION_ALTER_PROFILE', 'Profil modifié');
define('TXT_NOTIFICATION_DEL_PROFILE', 'Profil supprimé');
define('TXT_NOTIFICATION_ADD_ROOM', 'Salle créée');
define('TXT_NOTIFICATION_DEL_ROOM', 'Salle supprimée');
define('TXT_NOTIFICATION_ALTER_ROOM', 'Salle modifiée');
define('TXT_NOTIFICATION_ALTER_ORDER', 'Ordre mis à jour.');
define('TXT_NOTIFICATION_ALTER_SCALE', 'Évaluation modifiée');
define('TXT_NOTIFICATION_DEL_SCALE', 'Evaluation supprimée ');
define('TXT_NOTIFICATION_AVAILABLE_SCALE', 'Evaluation disponible ');
define('TXT_NOTIFICATION_ADD_SCALE', 'Évaluation créée');
define('TXT_NOTIFICATION_UNGROUP_SCALE', 'Évaluation dégroupée.');
define('TXT_NOTIFICATION_ADD_GROUP', 'Groupe créé');
define('TXT_NOTIFICATION_DEL_GROUP', 'Groupe supprimé');
define('TXT_NOTIFICATION_NOT_IMPLEMENTED', 'Non implémenté');
define('TXT_NOTIFICATION_ADD_SPECIE', 'Espece créée');
define('TXT_NOTIFICATION_ALTER_SPECIE', 'Espèce modifiée');
define('TXT_NOTIFICATION_DEL_SPECIE', 'Espèce supprimée');
define('TXT_NOTIFICATION_ADD_SUB_CATEGORY', 'Sous Catégorie créée');
define('TXT_NOTIFICATION_ALTER_SUB_CATEGORY', 'Sous catégorie modifiée');
define('TXT_NOTIFICATION_DEL_SUB_CATEGORY', 'Sous catégorie supprimée');
define('TXT_NOTIFICATION_ALTER_SAVED', 'Modification enregistré');

//--------------------To display in log-------------------------------
define('TXT_LOG_ERROR_USER', 'Error. Utilisateur {user} ou poste {poste} inconnu.');
define('TXT_LOG_ERROR_CONTACT', " Contacter un responsable.");
define('TXT_LOG_ERROR_UNKNOWN', ' inconnu.');
define('TXT_LOG_REQUEST_WORKSHOP', 'L\'utilisateur {user} a demandé le poste {workshop}');
define('TXT_LOG_ERROR_START', 'Impossible de démarrer le TP.');
define('TXT_LOG_ERROR_VALIDATION', 'L\'utilisateur {user} n\'a pas encore validé le poste {workshop}');
define('TXT_LOG_ERROR_VALIDATION_OPERATION', 'Vous n\'avez pas encore validé cette opération.');
define('TXT_LOG_ERROR_USER_VALIDATION', 'L\'utilisateur {user} a une absence de pointage sur le poste {workshop}');
define('TXT_LOG_ERROR_LINK', '[ERR02] Vous avez une absence de pointage sur le poste :');
define('TXT_LOG_BAD_TOKEN', 'Mauvais token CRSF ou utilisateur inconnu. Message : {message}');
define('TXT_LOG_START_VALIDATION', 'Début de la validation...');
define('TXT_LOG_ERROR', 'Une erreur est survenue à la ligne {line} avec le message : {message}');
define('TXT_LOG_VALIDATION', 'WorkshopAttempt {workshop} validé pour l\'utilisateur {user}');
define('TXT_LOG_UPDATE_COMMENT', 'Mis à jour du commentaire... [{comment}]');
define('TXT_LOG_ERROR_UPDATE_COMMENT', 'Impossible de mettre à jour le commentaire : {message}');
define('TXT_LOG_WORKSHOP_COMPLETE', 'Vous avez complété votre session sur le poste « ');
define('TXT_LOG_WORKSHOP_COMPLETE_ANSWER', "Ce poste est considéré comme ");
define('TXT_LOG_WORKSHOP_EXECUTION', 'Exécution des crochets...');
define('TXT_LOG_WORKSHOP_EXECUTED', 'Crochets exécutés.');
define('TXT_LOG_WORKSHOP_COMPLETE_DURATION', 'Pour une durée de ');
define('TXT_LOG_WORKSHOP_ERROR_COMMUNICATION', 'Erreur de communication entre CslManager et Moodle : {message}');
define('TXT_LOG_WORKSHOP_OCCURRED', 'Une erreur est survenue lors de l\'enregistrement.');
define('TXT_LOG_WORKSHOP_END', 'Fin de la validation...');
define('TXT_LOG_WORKSHOP_ATTEMPT_IN_PROGRESS', "Vous n'avez pas validé ce poste de travail");
define('TXT_LOG_ERROR_CONNECTION', "Connexion échouée : ");
define('TXT_LOG_ERROR_ADD_ON', "Extension curl non chargée");
define('TXT_LOG_ERROR_MOODLE_NOT_FOUND', "EXCEPTION_MOODLE_NO_USER_FOUND");
define('TXT_LOG_SUCCESS_MOODLE_LINK', " entrées. Terminé en ");
define('TXT_LOG_ERROR_COURSE', "Erreur avec le devoir n° ");
define('TXT_LOG_LOGIN_CONNECTION', "Tentative de connexion de l'utilisateur {user}");
define('TXT_LOG_LOGIN_INVALID_TOKEN', "Token invalide");
define('TXT_LOG_LOGIN_INVALID_LDAP', "Données de connexion au ldap invalides");
define('TXT_LOG_LOGIN_UNKNOWN_USER', "Utilisateur inconnu");
define('TXT_LOG_LOGIN_ERROR_CREATE', "Création de l'utilisateur impossible");
define('TXT_LOG_LOGIN_ERROR_INPUT', "Mauvaise combinaison identifiant / mot de passe");
define('TXT_LOG_LOGIN_ERROR_CONNECTION', "Échec de la connexion avec le message : {message}");
define('TXT_LOG_LOGIN_SUCCESS_CONNECTION', "Connexion réussie pour l'utilisateur {user}");
define('TXT_LOG_LOGIN_UPDATE_USER', "L'utilisateur {user} a été mis à jour");
define('TXT_LOG_LOGIN_CHANGE_PASSWORD', "Vous avez oublié votre mot de passe ?");
define('TXT_LOG_LOGIN_CHANGE_MAIL_SEND', "Un mail vient de vous être envoyé pour modifier vôtre mot de passe à l'adresse ");
define('TXT_LOG_LOGIN_CONTACT_ADMIN',"L'identifiant ne correspond pas à un compte manuel, contactez l'administrateur");
define('TXT_LOG_LOGIN_INPUT_EMPTY',"Veuillez compléter le champ");
define('TXT_LOG_LOGIN_TOKEN_EXPIRED',"Vôtre demande de changement de mot de passe a expiré. Veuillez réitérer cette demande.");

//--------------------To display after validation-------------------------------
define('TXT_VALIDATION_SUCCESS', "Devoir mis à jour dans Moodle");

//--------------------To display information image-------------------------------
define('TXT_IMAGE_INVALID', "Le fichier n'est pas une image valide.");
define('TXT_IMAGE_EXIST', "Erreur! le fichier image existe déjà.");
define('TXT_IMAGE_SIZE', "Le fichier selectionné est trop volumineux.");
define('TXT_IMAGE_FORMAT', "Les images doivent etre au format : JPG, JPEG, PNG");
define('TXT_IMAGE_ERROR', "Erreur! impossible d'ajouter l'image.");

//--------------------To display information file-------------------------------
define('TXT_FILE_INVALID', "Le fichier n'est pas valide.");
define('TXT_FILE_EXIST', "Erreur! le fichier existe déjà.");

//--------------------Installation content-------------------------------
define('TXT_INSTALLATION_TITLE', "Installation de CslManager");
define('TXT_INSTALLATION', "Installation");
define('TXT_INSTALLATION_HASHTAG_1', "#Pédagogie");
define('TXT_INSTALLATION_HASHTAG2', "#Pratique");
define('TXT_INSTALLATION_HASHTAG3', "#Vétérinaire");
define('TXT_INSTALLATION_BDD', "Base de données");
define('TXT_INSTALLATION_FEEDBACK', "Feedback");
define('TXT_INSTALLATION_MOODLE', "Moodle");
define('TXT_INSTALLATION_ADMIN', "Administrateur");
define('TXT_INSTALLATION_ADMIN_NAME', "Nom de l'administrateur :");
define('TXT_INSTALLATION_CUSTOM', "Personnalisation");
define('TXT_INSTALLATION_AUTH', "Authentification");
define('TXT_INSTALLATION_MODAL_TITLE', "Informations Complémentaires");
define('TXT_INSTALLATION_SERVER_NAME', "Nom du serveur MYSQL :");
define('TXT_INSTALLATION_SERVER_NAME_EXAMPLE', "ex: localhost");
define('TXT_INSTALLATION_LOGIN_BDD', "Login :");
define('TXT_INSTALLATION_LOGIN_BDD_EXAMPLE', "ex: root");
define('TXT_INSTALLATION_PASSWORD', "Mot de passe :");
define('TXT_INSTALLATION_PASSWORD_WRONG', "Les mot de passe doivent être identiques");
define('TXT_INSTALLATION_BDD_NAME', "Nom de la base de données :");
define('TXT_INSTALLATION_BDD_NAME_EXAMPLE', "ex: cslmanager");
define('TXT_INSTALLATION_SMTP_HOST', "Hôte du SMTP :");
define('TXT_INSTALLATION_SMTP_HOST_EXAMPLE', "ex: smtps.exemple.fr");
define('TXT_INSTALLATION_SMTP_SECURE', "Clé de sécurité :");
define('TXT_INSTALLATION_OPTIONS', "Options");
define('TXT_INSTALLATION_SELECT_OPTION', "Selectionnez une option");
define('TXT_INSTALLATION_SMTP_SSL', "ssl");
define('TXT_INSTALLATION_SMTP_TLS', "tls");
define('TXT_INSTALLATION_SMTP_PORT', "Port :");
define('TXT_INSTALLATION_SMTP_HOST_EMAIL', "Email de l'hôte :");
define('TXT_INSTALLATION_SMTP_HOST_EMAIL_EXAMPLE', "ex: example@outlook.fr");
define('TXT_INSTALLATION_MOODLE_TOKEN', "Token : ");
define('TXT_INSTALLATION_MOODLE_URL', "URL de Moodle :");
define('TXT_INSTALLATION_MOODLE_URL_EXAMPLE', "ex: http://moodle.fr");
define('TXT_INSTALLATION_MOODLE_COURSE', "Numéro du cours dédié :");
define('TXT_INSTALLATION_TECHNIQUE_SUPPORT', "Adresse mail du support technique :");
define('TXT_INSTALLATION_LOGO', "Logo");
define('TXT_INSTALLATION_CHOOSE_FILE', "Choisissez votre fichier");
define('TXT_INSTALLATION_AUTH_URI', "Uri :");
define('TXT_INSTALLATION_AUTH_URI_EXAMPLE', "ex :srv-ldap-01.example.fr");
define('TXT_INSTALLATION_AUTH_BASEDN', "BaseDn :");
define('TXT_INSTALLATION_AUTH_BINDDN', "BindDn :");
define('TXT_INSTALLATION_AUTH_BINDPW', "BindPw :");
define('TXT_INSTALLATION_AUTH_BINDDN_EXAMPLE', "ex: uid=cslmanager,ou=people,dc=example,dc=fr");
define('TXT_INSTALLATION_AUTH_PORT', "Port :");
define('TXT_INSTALLATION_LAUNCH_INSTALL', "Lancer l'installation");
define('TXT_INSTALLATION_APPLICATION_TITLE', "Nom de l'application");
define('TXT_INSTALLATION_CONGRADULATION', "Félicitations");
define('TXT_INSTALLATION_BDD_SUCCESS', "La base de données à été créée avec succès");
define('TXT_INSTALLATION_START', "C'est parti !");
define('TXT_INSTALLATION_WARNING', "Attention si toutes les informations ne sont pas renseignées ou inexactes certaines fonctionnalitées peuvent être impactés");
define('TXT_INSTALLATION_MODAL_BDD_1', "Il vous faudra créer la base de données dans MYSQL au préalable");
define('TXT_INSTALLATION_MODAL_BDD_2', "Les informations sont obligatoires");
define('TXT_INSTALLATION_MODAL_FEEDBACK_1', "Les informations sont utilisées pour permettre à vos utilisateurs de vous faire des retours sur l'application");
define('TXT_INSTALLATION_MODAL_FEEDBACK_2', "Vous trouverez des informations complémentaires");
define('TXT_INSTALLATION_MODAL_MOODLE_1', "Les informations sont ici utilisées en lien avec Moodle. ");
define('TXT_INSTALLATION_MODAL_MOODLE_2', "Il vous faut au préalable créer un cours sur Moodle aucquel sera attaché un numéro ( identifiant ).");
define('TXT_INSTALLATION_MODAL_MOODLE_3', "Ensuite, il vous faut créer un jeton pour autoriser l'application à accéder à la zone dédiée( vous trouverez des informations complémentaires ");
define('TXT_INSTALLATION_MODAL_AUTH_1', "Les paramêtres du Ldap sont utilisés pour l'authentification des utilisateurs sur la plateforme pédagogique.");
define('TXT_INSTALLATION_MODAL_AUTH_2', "Les informations relatives au Ldap sont obligatoires. Vous trouverez des indications sur la manière de renseigner ces champs");
define('TXT_INSTALLATION_MODAL_ADMIN_1', "Les informations renseignées ici sont utilisées pour créer l'administrateur de l'application");
define('TXT_INSTALLATION_MODAL_ADMIN_2', "Les informations sont en lien avec l'annuaire utilisé lors de l'authentification. ");
define('TXT_INSTALLATION_HERE', "ici");
define('TXT_INSTALLATION_ERROR_PASSWORD_SIZE',  "Le mot de passe doit faire 6 caractères minimum");
define('TXT_INSTALLATION_ERROR_PASSWORD_CHECK', "Les mots de passe doivent être identiques");

//--------------------Mail content-------------------------------
define('TXT_MAIL_INIT_TITLE', 'Un compte CSLManager vient de vous être créé');
define('TXT_MAIL_INIT_CONTENT1', 'Pour modifier votre mot de passe cliquez sur ce ');
define('TXT_MAIL_CHANGE_TITLE', 'Vous venez de faire une demande de changement de mot de passe');
define('TXT_MAIL_INIT_CONTENT', 'Il ne sera valide que 15 minutes.');




