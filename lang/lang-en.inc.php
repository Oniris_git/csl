<?php

//----------- VirtualVet English translation file --------------------
define('TXT_LABEL', 'Label:');
define('TXT_ACCOUNT', 'Account type');
define('TXT_MANUAL_ACCOUNT', 'Manual');
define('TXT_LDAP_ACCOUNT', 'LDAP');
define('TXT_FULL_LABEL', 'Full label');
define('TXT_DESCRIPTION', 'Description:');
define('TXT_EDIT', 'Edit');
define('TXT_ADD', 'Add');
define('TXT_DELETE', ' Delete');
define('TXT_PRINT', 'Print');
define('TXT_ACTIVE', 'Active');
define('TXT_SCALE', 'Scale');
define('TXT_DOMAIN', 'Domain');
define('TXT_ROOM', 'Room');
define('TXT_BROWSE', 'Browse');
define('TXT_PROFILE', 'Profile');
define('TXT_ADMIN', 'Administrator');
define('TXT_MANAGER', 'Manager');
define('TXT_STUDENT', 'Student');
define('TXT_ID', 'ID');
define('TXT_NOTREADY', 'Code not implemented');
define('TXT_ID_HELP', 'Caution: this ID is unique and will be locked.');
define('TXT_LOGIN', 'Login');
define('TXT_PASSWORD', 'Password');
define('TXT_NEW_PASSWORD', 'New password');
define('TXT_PASSWORD_CONFIRM', 'Confirm password');
define('TXT_NAME', 'Name');
define('TXT_NAME_MIN', 'name');
define('TXT_FORENAME', 'Forename');
define('TXT_ENTER', 'Enter');
define('TXT_DISCONNECT', 'Logout');
define('TXT_CONNECT', 'Log in');
define('TXT_WARNING', 'Caution!');
define('TXT_ERROR', 'Error');
define('TXT_APPLY', 'Apply');
define('TXT_CANCEL', ' Cancel workshop attempt');
define('TXT_IMPORT', ' Import');
define('TXT_PARAMETERS', ' Parameters');
define('TXT_MOODLE', ' Link to Moodle');
define('TXT_WORKSHOP_DIFFICULTY', 'Difficulty level');
define('TXT_EXERCICE', 'Workshop');
define('TXT_LINK','Link');
define('TXT_WELCOME','Welcome ');
define('TXT_START','Start');
define('TXT_DURATION','Duration');
define('TXT_REGISTER','Register ');
define('TXT_FIRST_NAME','First name ');
define('TXT_NAME_FIELD','Name ');
define('TXT_REGISTER_FIELD','firstname.name');
define('TXT_REGISTER_MAIL','Email ');
define('TXT_REGISTER_SUCCESS','Registration successful');
define('TXT_REGISTER_ERROR','Error: user non created');
define('TXT_REGISTER_USERNAME_ERROR','Incorrect user ID');
define('TXT_REGISTER_MDP_ERROR','Passwords must be the same');
define('TXT_REGISTER_MDP_ERROR_LENGTH','Password must be at least 6 characters long');
define('TXT_HELP','Help');
define('TXT_FILTER','Scale');
define('TXT_CANCELED_WORKSHOP_ATTEMPT','Workshop attempt cancelled');
define('TXT_ERROR_CANCELED_WORKSHOP_ATTEMPT','Impossible to cancel workshop attempt, please contact manager');

//--------------------Page install.phtml---------------------------------
define('TXT_INDEX_PAGE_OVERVIEW', 'Overview');

//--------------------Page header.phtml---------------------------------
define('TXT_HEADER_ADMIN_PANEL', 'Admin panel');

//--------------------Page login.phtml---------------------------------
define('TXT_LOGIN_PAGE_CONNECTION', 'Connection Page');

//--------------------Page navbar.phtml---------------------------------
define('TXT_NAVBAR_FEEDBACK', 'Contact us');
define('TXT_NAVBAR_TOGGLE', 'Toggle navigation');
define('TXT_NAVBAR_HOME', 'Home');

//--------------------Page side_navbar.phtml---------------------------------
define('TXT_SIDE_NAVBAR_DASHBOARD', 'Dashboard');
define('TXT_SIDE_NAVBAR_WORKSHOPS', 'Workshops');
define('TXT_SIDE_NAVBAR_USERS', 'Users');
define('TXT_SIDE_NAVBAR_PREFERENCES', 'Preferences');
define('TXT_SIDE_NAVBAR_ADMINISTRATION', 'Administration');
define('TXT_SIDE_NAVBAR_PROFILES', 'Profiles');
define('TXT_SIDE_NAVBAR_RANK', 'Ranks');
define('TXT_SIDE_NAVBAR_SPECIES', 'Species');
define('TXT_SIDE_NAVBAR_CATEGORY', 'Category');
define('TXT_SIDE_NAVBAR_SUB_CATEGORY', 'Subcategory');
define('TXT_SIDE_NAVBAR_DISCIPLINE', 'Discipline');
define('TXT_SIDE_NAVBAR_SCALES', 'Scales');
define('TXT_SIDE_NAVBAR_STATS', 'Stats');
define('TXT_SIDE_NAVBAR_ROOMS', 'Rooms');
define('TXT_SIDE_NAVBAR_HOME', 'Home');
define('TXT_SIDE_MANAGEMENT', ' Management');
define('TXT_SIDE_USER', ' User');
define('TXT_SIDE_CUSTOM', ' Custom');
define('TXT_SIDE_LOGO', ' Logo');
define('TXT_SIDE_LANG', ' Language');
define('TXT_SIDE_LEGAL', ' Legal Notice');
define('TXT_SIDE_SERVER', ' Server');
define('TXT_SIDE_AUTH', ' Auth');
define('TXT_SIDE_MAILER', ' Mailer');
define('TXT_SIDE_ROOTING', ' Moodle root');

//--------------------Page skel.phtml---------------------------------
define('TXT_SKEL_NOSCRIPT', 'Javascript is required for full functionnality!');

//--------------------Page feedback.phtml---------------------------------
define('TXT_FEEDBACK', 'Feedback');
define('TXT_FEEDBACK_HELP', 'Any comment, suggestion, glitch?<br/>You can contact the site admin using this form.');
define('TXT_FEEDBACK_ABUSE', 'Please do not abuse');
define('TXT_FEEDBACK_SUBJECT', 'Subject');
define('TXT_FEEDBACK_COMMENT', 'Comment');
define('TXT_FEEDBACK_SUGGESTION', 'Suggestion');
define('TXT_FEEDBACK_GLITCH', 'Glitch');
define('TXT_FEEDBACK_MESSAGE', 'Message');

//--------------------Page stat.phtml---------------------------------
define('TXT_STAT_USERSTAT', ' User stats');
define('TXT_STAT_CSV_USERSTAT', ' User stats CSV export ');
define('TXT_STAT_GLOBALSTAT', 'Global stats');
define('TXT_STAT_CSVSTAT', 'Global CSV export');
define('TXT_STAT_EXERCICE_LIST', 'Exercice list ');
define('TXT_STAT_AVERAGE', ' average');
define('TXT_STAT_WORDING', 'Wording');
define('TXT_STAT_EVALUATION', 'Scale');
define('TXT_STAT_TIME', 'Time');
define('TXT_STAT_DATE', 'Date');
define('TXT_STAT_CLOSE', 'Close');
define('TXT_STAT_REMAINING_EXERCICE', ' Remaining workshops');
define('TXT_STAT_DISTINCTS_EXERCICE', ' Distincts workshops');
define('TXT_STAT_MODAL_TITLE', 'List of remaining workshops to do');
define('TXT_STAT_COUNT_TRY', 'number of attempts');
define('TXT_STAT_BEST_TIME', 'Best time');
define('TXT_STAT_BEST_GRADE', 'Best grade');
define('TXT_STAT_SHOW_GRADES', 'Show scales for ');

//--------------------Page stat_filter.phtml---------------------------------
define('TXT_STAT_FILTER_TITLE_FILTER',' Stats filter');
define('TXT_STAT_FILTER_CHOOSE','Choose an option');
define('TXT_STAT_FILTER_ACTION','Filter');
define('TXT_STAT_FILTER_EXERCICE','Workshop');
define('TXT_STAT_FILTER_USER','User');
define('TXT_STAT_FILTER_GRADE','Grade');

//--------------------Page workshop_filter.phtml---------------------------------
define('TXT_WORKSPACE_FILTER_BUTTON',' Workshop filter');
define('TXT_WORKSPACE_FILTER_ROOM','Room');
define('TXT_WORKSPACE_FILTER_CATEGORY','Category');
define('TXT_WORKSPACE_FILTER_SUB_CATEGORY','Subcategory');
define('TXT_WORKSPACE_FILTER_SPECIE','Species');
define('TXT_WORKSPACE_FILTER_COUNT_TO_VALIDATE','Count to validate');
define('TXT_WORKSPACE_FILTER_COUNT_DIFFICULTY','Difficulty level');
define('TXT_WORKSPACE_FILTER_RANK','Priority Level');
define('TXT_WORKSPACE_DISCIPLINE','Discipline');

//--------------------Page list_priority_level.phtml---------------------------------
define('TXT_GRADE_GLOBAL_T','Global ternary');
define('TXT_GLOBAL_Q','Global quaternary');
define('TXT_GLOBAL','Global');
define('TXT_DISTINCTS_T','Distinct ternary');
define('TXT_DISTINCT_Q','Distinct quaternary');
define('TXT_DISTINCT','Distinct');

//--------------------Page list_workshop.phtml---------------------------------
define('TXT_LIST_WORKSPACE_ROOM','room');
define('TXT_LIST_WORKSPACE_CATEGORY','category');
define('TXT_LIST_WORKSPACE_SUB_CATEGORY','subcategory');
define('TXT_LIST_WORKSPACE_SPECIE','species');
define('TXT_LIST_WORKSPACE_COUNT_TO_VALIDATE','count to validate');
define('TXT_LIST_WORKSPACE_DIFFICULTY','difficulty level');
define('TXT_LIST_WORKSPACE_RANK','rank');
define('TXT_LIST_WORKSPACE_DISCIPLINE','discipline');
define('TXT_NOT_COMPLETE','Not specified');
define('TXT_ROOM_NUMBER','Room n&deg;: ');
define('TXT_LIST_WORKSPACE_CATEGORY_TITLE','Category: ');
define('TXT_LIST_WORKSPACE_SUB_CATEGORY_TITLE','Subcategory: ');
define('TXT_LIST_WORKSPACE_SPECIE_TITLE','Species :');
define('TXT_LIST_WORKSPACE_COUNT_TO_VALIDATE_TITLE','count to validate: ');
define('TXT_LIST_WORKSPACE_DIFFICULTY_TITLE','Difficulty level: ');
define('TXT_LIST_WORKSPACE_RANK_TITLE','Priority level: ');
define('TXT_LIST_WORKSPACE_DISCIPLINE_TITLE','Discipline: ');
define('TXT_LIST_WORKSPACE_FILTER_BY',' Filter by: ');

//--------------------Page list_user_filter.phtml---------------------------------
define('TXT_USER_FILTER_USERS',' Users');
define('TXT_USER_FILTER_SEARCH','Search');
define('TXT_USER_SEARCH_DONE_ON','Searching on field ');
define('TXT_USER_FILTER_SEARCH_PLACEHOLDER','Search by name or profile');
define('TXT_USER_FILTER_NAME','Name');
define('TXT_USER_FILTER_FIRSTNAME','Firstname');
define('TXT_USER_FILTER_UPN','UPN');
define('TXT_USER_FILTER_PROFIL','Profile');

//--------------------Page list_priority_level.phtml---------------------------------
define('TXT_LIST_GRADE_USER','User');
define('TXT_LIST_GRADE_WORDING','Wording');
define ('TXT_LIST_GRADE_DATE','Date');
define('TXT_LIST_GRADE_MODAL_TITLE','Help');
define('TXT_LIST_GRADE_DESCRIPTION','Sorting is done in the following way:');
define ('TXT_LIST_GRADE_GLOBAL_TERNAIRE','"Global ternary" counts all workshops according to the ternary scale.');
define ('TXT_LIST_GRADE_GLOBAL_QUATERNAIRE','"Global quaternary" counts all workshops according to the quaternary scale.');
define ('TXT_LIST_GRADE_GLOBAL','"Global" counts all workshops.');
define ('TXT_LIST_GRADE_DISTINCT_TERNAIRE','"Distinct ternary" counts all workshops done by differents users according to the ternary scale.');
define ('TXT_LIST_GRADE_DISTINCT_QUATERNAIRE','"Distinct quaternary" counts all workshops done by differents users according to the quaternary scale.');
define ('TXT_LIST_GRADE_DISTINCT','"Distinct" counts all distinct workshops done by differents users.');

//--------------------Page list_workshop_attempt.phtml---------------------------------
define('TXT_LIST_EXERCICE_WORDING','Wording');
define('TXT_LIST_EXERCICE_USER','User');
define('TXT_LIST_EXERCICE_DURATION','Duration');

//--------------------Page technicalsheet_exercice.phtml---------------------------------
define('TXT_TECHNICALSHEET_TITLE','Data summary of the workshop');
define('TXT_TECHNICALSHEET_TITLE_MODAL_USERS','Users list');
define('TXT_TECHNICALSHEET_DESCRIPTION','Description');
define('TXT_TECHNICALSHEET_MODAL_TITLE','Workshop description');
define('TXT_TECHNICALSHEET_ROOM','Room');
define('TXT_TECHNICALSHEET_SPECIE','Species');
define('TXT_TECHNICALSHEET_CATEGORY','Category');
define('TXT_TECHNICALSHEET_TIME','Recommended duration');
define('TXT_CLOSE','Close');
define('TXT_TECHNICALSHEET_NUMBER_USER','Number of users');
define('TXT_TECHNICALSHEET_AVERAGE_DURATION','Average duration (minutes)');
define('TXT_TECHNICALSHEET_AVERAGE_TRY','Average number of attempts');
define('TXT_TECHNICALSHEET_STATUS','Acquired status or higher');
define('TXT_TECHNICALSHEET_DISTINCT','Distinct users');
define('TXT_TECHNICALSHEET_COMENTS','Comments');
define('TXT_TECHNICALSHEET_COMENTS_TITLE','Students comments');
define('TXT_TECHNICALSHEET_NO_COMENTS','No comment');
define('TXT_UPN','UPN');

//--------------------Page user.phtml---------------------------------
define('TXT_USER_EDIT_USER', 'Edit user');
define('TXT_USER_STAT_USER', 'Display user stats');
define('TXT_USER_MY_STAT', 'Display my stats');

//--------------------Page legal.phtml---------------------------------
define('TXT_LEGAL_SUCCESS', 'Content saved');

//--------------------Page workshop.phtml---------------------------------
define('TXT_WORKSHOP_ADD_WORKSHOP', 'Add workshop');
define('TXT_WORKSHOP_ID', 'Workshop ID');
define('TXT_WORKSHOP_LMS_COURSE', 'Moodle course');
define('TXT_WORKSHOP_LMS_ID', 'Moodle activity ID');
define('TXT_WORKSHOP_LMS_URL', 'Link to Moodle');
define('TXT_WORKSHOP_LEVEL', 'Level');
define('TXT_WORKSHOP_ATTEMPTS', 'Attempts to validate');
define('TXT_WORKSHOP_NUMBER', 'Nb');
define('TXT_WORKSHOP_DURATION', 'Minimal duration (min)');
define('TXT_WORKSHOP_DURATION_MAX', 'Maximal duration (mi)');
define('TXT_WORKSHOP_EDIT', 'Edit workshop');
define('TXT_WORKSHOP_QRCODE', 'A QR_code is linked to site root URL and workshop ID.');
define('TXT_WORKSHOP_QRCODE2', 'If you change workshop ID, you\'ll have to reprint the QR-Code.');
define('TXT_WORKSHOP_SYNCHRONISATION', ' Synchronize with Moodle');
define('TXT_WORKSHOP_PARAMETER_MISSING', 'Missing parameters');
define('TXT_WORKSHOP_ALTERED', "Workshop updated");
define('TXT_WORKSHOP_CREATED', "Workshop created");
define('TXT_WORKSHOP_DELETED', "Workshop deleted");
define('TXT_CATEGORY', 'Category');
define('TXT_SUBCATEGORY', 'Subcategory');
define('TXT_DISCIPLINE', 'Discipline');
define('TXT_RANK', 'Priority level');
define('TXT_SPECIES', 'Species');

//--------------------Page admin/admin_scale_type.phtml---------------------------------
define('TXT_ADMIN_EVALUATION_SCALE_LIST', ' Scales list');
define('TXT_ADMIN_EVALUATION_SCALE_NB', 'Scales number');
define('TXT_ADMIN_EVALUATION_ADD_SCALE', 'Add scale');
define('TXT_ADMIN_EVALUATION_HELP', 'You can drag and drop items to the scale');
define('TXT_ADMIN_EVALUATION_ADD_SCALE_ITEM', 'Add item to scale');
define('TXT_ADMIN_EVALUATION_AVAILABLE', 'Available');
define('TXT_ADMIN_EVALUATION_EDIT_SCALE', 'Edit scale');
define('TXT_ADMIN_EVALUATION_MOODLE_SCALES', 'Get Moodle scales');
define('TXT_ADMIN_EVALUATION_MOODLE_ADD_SUCCESS', ' Moodle scale added');
define('TXT_ADMIN_EVALUATION_MOODLE_ADD_ERROR', "You must fill Moodle ID in the field");

//--------------------Page front/install.phtml---------------------------------
define('TXT_FRONT_INDEX_MYSTATS', 'My stats');
define('TXT_FRONT_INDEX_WELCOME', 'Welcome to ');
define('TXT_FRONT_INDEX_BEGIN', 'Please flash any workshop QR-code to begin');
define('TXT_FRONT_INDEX_MORNING', 'Hello');
define('TXT_FRONT_INDEX_THANKS', 'Thanks');
define('TXT_FRONT_INDEX_CONNECTED', 'You are connected to workshop');
define('TXT_FRONT_INDEX_SESSION_BEGIN', 'Session begin');
define('TXT_FRONT_INDEX_LMS_DOC', 'Moodle docs');
define('TXT_FRONT_INDEX_LMS_DOC_LINK', 'Link to doc');
define('TXT_FRONT_INDEX_WORKSHOP_DESCRIPTION', 'Workshop description');
define('TXT_FRONT_INDEX_REMINDER', 'At the end of the session, you will have to assess your progression');
define('TXT_FRONT_INDEX_EVALUATION', 'Please assess yourself');
define('TXT_FRONT_INDEX_OPTION_LABEL', 'Select');
define('TXT_FRONT_INDEX_COMMENT', 'Comment');
define('TXT_FRONT_INDEX_COMMENT_LENGTH', '200 characters maximum');
define('TXT_FRONT_INDEX_ABOUT_PAGE', ' About');
define('TXT_FRONT_INDEX_DOMAIN', ' Domain');

//--------------------Page blocks/list_user.phtml---------------------------------
define('TXT_BLOCKS_LIST_USER_MAIL', 'Mail');
define('TXT_BLOCKS_LIST_USER_FILTER', 'Filter');
define('TXT_DEPARTMENT', 'Department');


//--------------------Page blocks/stats.phtml---------------------------------
define('TXT_BLOCKS_STATS_PERMONTH', 'Per month');
define('TXT_BLOCKS_STATS_TIME', 'Total time');
define('TXT_BLOCKS_STATS_WORKSHOPS', ' Workshops completed');
define('TXT_BLOCKS_STATS_WORKSHOPS_DISTINCTS', 'Distinct workshops');
define('TXT_BLOCKS_STATS_SPAN', 'Distribution');
define('TXT_BLOCKS_STATS_GRADES', 'Grades');
define('TXT_BLOCKS_STATS_COMMENT', 'Comment');
define('TXT_BLOCKS_STATS_CSV_EXPORT', 'CSV Export');
define('TXT_BLOCKS_STATS_WORKSHOPS_MODAL_TITLE', 'List workshops done');

//--------------------Page blocks/workshopAttemptInProgress.phtml---------------------------------
define('TXT_BLOCKS_PROGRESS_HEADER', 'Workshops in progress');
define('TXT_BLOCKS_PROGRESS_BEGINBY', 'Started by');
define('TXT_BLOCKS_PROGRESS_BEGINDATE', 'Date');

//--------------------Page admin/admin_profile.phtml---------------------------------
define('TXT_ADMIN_PROFILE_LIST', 'Profile list');
define('TXT_ADMIN_PROFILE_NB', 'Profile number');
define('TXT_ADMIN_PROFILE_NAME', 'Profile name');
define('TXT_ADMIN_PROFILE_RIGHTS', 'Profile permissions');
define('TXT_ADMIN_ADD_PROFILE', 'Add profile');
define('TXT_ADMIN_PROFILE_RIGHTS_HELP', 'Hover for a description');

//--------------------Page admin/admin_room.phtml---------------------------------
define('TXT_ADMIN_MANAGE_ROOM', ' Manage rooms');
define('TXT_ADMIN_ADD_ROOM', 'Add room');

//--------------------Page admin/admin_mailer.phtml---------------------------------
define('TXT_ADMIN_MAILER_TITLE', ' Mailer Parameters');
define('TXT_ADMIN_NOTIFICATION_SUCCESS', ' Parameters updated, you will receive an test email');
define('TXT_ADMIN_MAILER_EMPTY', 'Content can\'t be empty');
define('TXT_ADMIN_MAILER_SEND_SUCCESS', 'Message sent');
define('TXT_ADMIN_MAILER_SEND_ERROR', 'Mail sending error');

//--------------------Page admin/admin_auth.phtml---------------------------------
define('TXT_ADMIN_AUTH_TITLE', ' Auth system');
define('TXT_ADMIN_AUTH_SUCCESS', ' Auth parameters updated');
define('TXT_ADMIN_AUTH_ACTUAL', ' Actual parameters');
define('TXT_ADMIN_AUTH_AUTH', 'Auth : ');
define('TXT_ADMIN_AUTH_ATTRIBUTE', 'Attribute');
define('TXT_ADMIN_AUTH_VALUE', 'Value');
define('TXT_ADMIN_AUTH_ALTER', ' Update auth');
define('TXT_ADMIN_AUTH_LDAP', 'LDAP');
define('TXT_ADMIN_AUTH_AD', 'Active Directory');
define('TXT_ADMIN_AUTH_PARAMETERS', 'LDAP Parameters');

//--------------------Page admin/admin_rooting.phtml---------------------------------
define('TXT_ADMIN_ROOTING_SUCCESS', ' Moodle parameters updated');

//--------------------Page admin/admin_user.phtml---------------------------------
define('TXT_ADMIN_ADD_USER', ' Add user');
define('TXT_ADMIN_IMPORT_USER_LDAP', 'Import users via LDAP');
define('TXT_ADMIN_IMPORT_USER_CSV', 'Import users via CSV');
define('TXT_ADMIN_IMPORT_USER_HELP', 'Fields are separated by <code>;</code>, headings must be filled.');
define('TXT_ADMIN_SYNC_USERS', 'Sync departments');

//--------------------Page admin/admin_category.phtml-------------------------------
define('TXT_ADMIN_ADD_CATEGORY', 'Add category');
define('TXT_ADMIN_CATEGORY_LIST', ' List of categories');
define('TXT_ADMIN_CATEGORY_NB', 'Number of categories');
define('TXT_ADMIN_CATEGORY_LIBELLE', 'Wording');
define('TXT_ADMIN_CATEGORY_DESCRIPTION', 'Description');
define('TXT_ADMIN_CATEGORY_MODIFY', 'Edit');
define('TXT_ADMIN_CATEGORY_FORM_LIBELLE', 'Wording');
define('TXT_ADMIN_CATEGORY_FORM_DESCRIPTION', 'Description');
define('TXT_ADMIN_CATEGORY_BUTTON_ADD', 'Add');
define('TXT_ADMIN_CATEGORY', 'Category');
define('TXT_ADMIN_CATEGORY_BUTTON_ALTER', 'Edit');
define('TXT_ADMIN_CATEGORY_ID', 'ID');
define('TXT_ADMIN_CATEGORY_ERROR_UNKNOWN', 'Unknown category');

//--------------------Page admin/admin_sub_category.phtml-------------------------------
define('TXT_ADMIN_ADD_SUB_CATEGORY', 'Add subcategory');
define('TXT_ADMIN_SUB_CATEGORY_LIST', ' List of subcategories');
define('TXT_ADMIN_SUB_CATEGORY_NB', 'Number of subcategories');
define('TXT_ADMIN_SUB_CATEGORY_LIBELLE', 'Wording');
define('TXT_ADMIN_SUB_CATEGORY_DESCRIPTION', 'Description');
define('TXT_ADMIN_SUB_CATEGORY_MODIFY', 'Edit');
define('TXT_ADMIN_SUB_CATEGORY_FORM_LIBELLE', 'Wording');
define('TXT_ADMIN_SUB_CATEGORY_FORM_DESCRIPTION', 'Description');
define('TXT_ADMIN_SUB_CATEGORY_BUTTON_ADD', 'Add');
define('TXT_ADMIN_SUB_CATEGORY', 'Category');
define('TXT_ADMIN_SUB_CATEGORY_BUTTON_ALTER', 'Edit');
define('TXT_ADMIN_SUB_CATEGORY_ID', 'ID');

//--------------------Page admin/admin_species.phtml-------------------------------
define('TXT_ADMIN_ADD_SPECIES', 'Add species');
define('TXT_ADMIN_SPECIES_LIST', ' List of species');
define('TXT_ADMIN_SPECIES_NB', 'Number of species');
define('TXT_ADMIN_SPECIES_LIBELLE', 'Wording');
define('TXT_ADMIN_SPECIES_DESCRIPTION', 'Description');
define('TXT_ADMIN_SPECIES_MODIFY', 'Edit');
define('TXT_ADMIN_SPECIES_FORM_LIBELLE', 'Wording');
define('TXT_ADMIN_SPECIES_FORM_DESCRIPTION', 'Description');
define('TXT_ADMIN_SPECIES_BUTTON_ADD', 'Add');
define('TXT_ADMIN_SPECIES', 'Species');
define('TXT_ADMIN_SPECIES_BUTTON_ALTER', 'Edit');
define('TXT_ADMIN_SPECIES_ID', 'ID');
define('TXT_ADMIN_SPECIES_ERROR_UNKNOWN', 'Unknown species');

//--------------------Page admin/admin_discipline.phtml-------------------------------
define('TXT_ADMIN_ADD_DISCIPLINE', 'Add discipline');
define('TXT_ADMIN_DISCIPLINE_LIST', ' List of disciplines');
define('TXT_ADMIN_DISCIPLINE_NB', 'Number of disciplines');
define('TXT_ADMIN_DISCIPLINE_LIBELLE', 'Wording');
define('TXT_ADMIN_DISCIPLINE_DESCRIPTION', 'Description');
define('TXT_ADMIN_DISCIPLINE_MODIFY', 'Edit');
define('TXT_ADMIN_DISCIPLINE_FORM_LIBELLE', 'Wording');
define('TXT_ADMIN_DISCIPLINE_FORM_DESCRIPTION', 'Description');
define('TXT_ADMIN_DISCIPLINE_BUTTON_ADD', 'Add');
define('TXT_ADMIN_DISCIPLINE', 'Discipline');
define('TXT_ADMIN_DISCIPLINE_BUTTON_ALTER', 'Edit');
define('TXT_ADMIN_DISCIPLINE_ID', 'ID');

//--------------------Page admin/admin_priority_level.phtml-------------------------------
define('TXT_ADMIN_ADD_RANK', 'Add rank');
define('TXT_ADMIN_RANK_LIST', ' List of ranks');
define('TXT_ADMIN_RANK_NB', 'Number of ranks');
define('TXT_ADMIN_RANK_LIBELLE', 'Wording');
define('TXT_ADMIN_RANK_DESCRIPTION', 'Description');
define('TXT_ADMIN_RANK_MODIFY', 'Edit');
define('TXT_ADMIN_RANK_FORM_LIBELLE', 'Wording');
define('TXT_ADMIN_RANK_FORM_DESCRIPTION', 'Description');
define('TXT_ADMIN_RANK_BUTTON_ADD', 'Add');
define('TXT_ADMIN_RANK', 'PriorityLevel');
define('TXT_ADMIN_RANK_BUTTON_ALTER', 'Edit');
define('TXT_ADMIN_RANK_ID', 'ID');
define('TXT_ADMIN_RANK_FILTER', ' Filter by priority level');

//--------------------Page admin/admin_poste.phtml-------------------------------
define('TXT_ADMIN_ADD_RIGHT', ' Add right');

//--------------------Page block/list_profile.phtml-------------------------------
define('TXT_ADMIN_ADD_NEW_RIGHT', ' Add new right to profile');
define('TXT_ADMIN_DELETE_RIGHT', ' Remove right');
define('TXT_ADMIN_UPDATE_RIGHT', ' Update right');

//--------------------Page language.phtml-------------------------------
define('TXT_LANG_MANAGE', ' Manage languages');
define('TXT_LANG_AVAILABLE', ' Languages available');
define('TXT_LANG_ADD_FILE', ' Add language file by 3 steps');
define('TXT_LANG_ALTER_FILE', ' Update language file by 3 steps');
define('TXT_LANG_DEFAULT', ' Default language');
define('TXT_LANG_STEP_1', ' Step 1: Download empty variable file');
define('TXT_LANG_ALTER_STEP_1', ' Step 1 : download language file');
define('TXT_LANG_STEP_2', ' Step 2: Rename, Complete, Drop');
define('TXT_LANG_STEP_ALTER_2', ' Step 2: Complete, Drop');
define('TXT_LANG_STEP_3', ' Step 3: Drop flag picture');
define('TXT_LANG_CHANGE_DEFAULT', ' Change default language');
define('TXT_LANG_MODAL_STEP1', ' Download variable file to put your values into');
define('TXT_LANG_MODAL_STEP2', ' Rename file, change variables and drop file');
define('TXT_LANG_MODAL_STEP3', ' Download image file on the link behind and drop' );

//--------------------Page view.phtml-------------------------------
define('TXT_VIEW_LOGO', ' Actual logo');
define('TXT_VIEW_TITLE', ' Application title');
define('TXT_VIEW_SUPPORT', ' Technical support mail');
define('TXT_VIEW_ALTER_LOGO', ' Alter logo');
define('TXT_VIEW_SAVE_CHANGE', ' Save change');
define('TXT_CUSTOM_APPLI', "Custom your application");
define('TXT_CUSTOM_FRONT', " Display");
define('TXT_CUSTOM_LOGO', " Logo require");

//--------------------To display in notification-------------------------------
define('TXT_COLOPHON_TITLE', 'About');
define('TXT_COLOPHON_P1', ' Manager is a web-application developed jointly by the teams of the national veterinary school of Alfort (');
define('TXT_COLOPHON_P2', ', Maisons-Alfort) and the national school of veterinary medicine, food sciences and engineering ');
define('TXT_COLOPHON_P3', ' This application is intended for veterinary clinical skills labs, made up of a large number of workshops on which students in initial training are practicing the acquisition of technical gestures and procedures in open-access mode.');
define('TXT_COLOPHON_P4', ' It allows students to self-evaluate and generate exportable statistics on the exercises completed or still to be done.');
define('TXT_COLOPHON_P5', 'It also allows the manager of rooms, through an administration interface, to manage the list of workshops (creation / modification / deletion), to monitor the progress of students and to produce a set of statistics on student activity .');
define('TXT_COLOPHON_P6', 'It also interfaces with Moodle instances using WebServices, which allows to create a link to a course where additional documents (videos, records, etc.) are stored and where the student\'s learning can feed his Training courses.');
define('TXT_COLOPHON_TITLE_2', 'Colophon');
define('TXT_COLOPHON_DESIGNER', 'Designers  :');
define('TXT_COLOPHON_DEV', 'Developers :');
define('TXT_COLOPHON_TRANSLATORS', 'Translators :');
define('TXT_COLOPHON_LIB', 'Librairies :');
define('TXT_COLOPHON_LIB_TITLE', ' Manager is using the following open source softwares:');
define('TXT_COLOPHON_DISTRIBUTION', ' Manager is released under the terms of the GPLv3 licence.');

//--------------------To display in notification-------------------------------
define('TXT_NOTIFICATION_NO_RIGHT', 'You are not allowed');
define('TXT_NOTIFICATION_IMPORT_SUCCESS', 'Import over');
define('TXT_NOTIFICATION_CHECK_VALUES', 'Check your values');
define('TXT_NOTIFICATION_ADD_USER', 'User added');
define('TXT_NOTIFICATION_UNKNOWN_ACTION', 'Unknown action');
define('TXT_NOTIFICATION_ERROR', 'Error : ');
define('TXT_NOTIFICATION_ALTER_CATEGORY', 'Category updated');
define('TXT_NOTIFICATION_DEL_CATEGORY', 'Category deleted');
define('TXT_NOTIFICATION_DEL_DISCIPLINE', 'Discipline deleted');
define('TXT_NOTIFICATION_ALTER_DISCIPLINE', 'Discipline updated');
define('TXT_NOTIFICATION_ADD_PRIORITY_LEVEL', 'Priority level created');
define('TXT_NOTIFICATION_ALTER_PRIORITY_LEVEL', 'Priority level updated');
define('TXT_NOTIFICATION_DEL_PRIORITY_LEVEL', 'Priority level deleted');
define('TXT_NOTIFICATION_ADD_PROFILE', 'Profile created');
define('TXT_NOTIFICATION_ALTER_PROFILE', 'Profile updated');
define('TXT_NOTIFICATION_DEL_PROFILE', 'Profile deleted');
define('TXT_NOTIFICATION_ADD_ROOM', 'Room created');
define('TXT_NOTIFICATION_DEL_ROOM', 'Room deleted');
define('TXT_NOTIFICATION_ALTER_ROOM', 'Room updated');
define('TXT_NOTIFICATION_ALTER_ORDER', 'Order updated');
define('TXT_NOTIFICATION_ALTER_SCALE', 'Scale item updated');
define('TXT_NOTIFICATION_DEL_SCALE', 'Scale item deleted');
define('TXT_NOTIFICATION_AVAILABLE_SCALE', 'Scale item available ');
define('TXT_NOTIFICATION_ADD_SCALE', 'Scale item created');
define('TXT_NOTIFICATION_UNGROUP_SCALE', 'Scale off group');
define('TXT_NOTIFICATION_ADD_GROUP', 'Scale created');
define('TXT_NOTIFICATION_DEL_GROUP', 'Scale deleted');
define('TXT_NOTIFICATION_NOT_IMPLEMENTED', 'Not implemented');
define('TXT_NOTIFICATION_ADD_SPECIE', 'Species created');
define('TXT_NOTIFICATION_ALTER_SPECIE', 'Species updated');
define('TXT_NOTIFICATION_DEL_SPECIE', 'Species deleted');
define('TXT_NOTIFICATION_ADD_SUB_CATEGORY', 'Subcategory created');
define('TXT_NOTIFICATION_ALTER_SUB_CATEGORY', 'Subcategory updated');
define('TXT_NOTIFICATION_DEL_SUB_CATEGORY', 'Subcategory deleted');
define('TXT_NOTIFICATION_ALTER_SAVED', 'Update saved');

//--------------------To display in log-------------------------------
define('TXT_LOG_ERROR_USER', 'Error. User {user} or workshop {workshop} unknown.');
define('TXT_LOG_ERROR_CONTACT', " Contact manager");
define('TXT_LOG_ERROR_UNKNOWN', ' Unknown');
define('TXT_LOG_REQUEST_WORKSHOP', 'User {user} demand workshop {workshop}');
define('TXT_LOG_ERROR_START', 'Impossible to start workshop attempt');
define('TXT_LOG_ERROR_VALIDATION', 'User {user} didn\t validate workshop {workshop} yet');
define('TXT_LOG_ERROR_VALIDATION_OPERATION', 'You didn\'t validate this workshop yet');
define('TXT_LOG_ERROR_USER_VALIDATION', 'User {user} didn\'t validate workshop {workshop} yet');
define('TXT_LOG_ERROR_LINK', '[ERR02] You didn\'t validate this workshop yet:');
define('TXT_LOG_BAD_TOKEN', 'Bad token CRSF or user unknown. Message: {message}');
define('TXT_LOG_START_VALIDATION', 'Start to validate...');
define('TXT_LOG_ERROR', 'Error line {line} with message : {message}');
define('TXT_LOG_VALIDATION', 'WorkshopAttempt {workshop} validated for user {user}');
define('TXT_LOG_UPDATE_COMMENT', 'Comment update... [{comment}]');
define('TXT_LOG_ERROR_UPDATE_COMMENT', 'Impossible to update comment: {message}');
define('TXT_LOG_WORKSHOP_COMPLETE', 'You completed workshop « ');
define('TXT_LOG_WORKSHOP_COMPLETE_ANSWER', "This workshop is ");
define('TXT_LOG_WORKSHOP_EXECUTION', 'Execution...');
define('TXT_LOG_WORKSHOP_EXECUTED', 'Executed');
define('TXT_LOG_WORKSHOP_COMPLETE_DURATION', 'Duration');
define('TXT_LOG_WORKSHOP_ERROR_COMMUNICATION', 'communication error between CSL Manager and Scale: {message}');
define('TXT_LOG_WORKSHOP_OCCURRED', 'Error in record');
define('TXT_LOG_WORKSHOP_END', 'Validation over...');
define('TXT_LOG_WORKSHOP_ATTEMPT_IN_PROGRESS', "You didn't validate this workshop");
define('TXT_LOG_ERROR_CONNECTION', "Connection failed : ");
define('TXT_LOG_ERROR_ADD_ON', "Extension curl not loaded");
define('TXT_LOG_ERROR_MOODLE_NOT_FOUND', "EXCEPTION_MOODLE_NO_USER_FOUND");
define('TXT_LOG_SUCCESS_MOODLE_LINK', " entrance. Over in ");
define('TXT_LOG_ERROR_COURSE', "Error in course n° ");
define('TXT_LOG_LOGIN_CONNECTION', "Try to connect to user {user}");
define('TXT_LOG_LOGIN_INVALID_TOKEN', "Invalid token");
define('TXT_LOG_LOGIN_INVALID_LDAP', "Ldap connection data not valid");
define('TXT_LOG_LOGIN_UNKNOWN_USER', "Unknown user");
define('TXT_LOG_LOGIN_ERROR_CREATE', "Impossible to create user");
define('TXT_LOG_LOGIN_ERROR_INPUT', "Bad request id / password");
define('TXT_LOG_LOGIN_ERROR_CONNECTION', "Connection failed, message: {message}");
define('TXT_LOG_LOGIN_SUCCESS_CONNECTION', "Successful connection message {user}");
define('TXT_LOG_LOGIN_UPDATE_USER', "User {user} updated");
define('TXT_LOG_LOGIN_CHANGE_PASSWORD', "Forgot your password ?");
define('TXT_LOG_LOGIN_CHANGE_MAIL_SEND', "You just receive a mail to change password on ");
define('TXT_LOG_LOGIN_CONTACT_ADMIN',"The id is a manual account, contact administrator");
define('TXT_LOG_LOGIN_INPUT_EMPTY',"Complete field");
define('TXT_LOG_LOGIN_TOKEN_EXPIRED',"Your password change request has expired. Please try again");

//--------------------To display after validation-------------------------------
define('TXT_VALIDATION_SUCCESS', "Updated in Moodle");

//--------------------To display information image-------------------------------
define('TXT_IMAGE_INVALID', "File is not valid");
define('TXT_IMAGE_EXIST', "Error ! File already exist");
define('TXT_IMAGE_SIZE', "File size not valid");
define('TXT_IMAGE_FORMAT', "File format: JPG, JPEG, PNG");
define('TXT_IMAGE_ERROR', "Error ! Impossible to add");

//--------------------To display information file-------------------------------
define('TXT_FILE_INVALID', "File does not valid.");
define('TXT_FILE_EXIST', "Error File already exist.");

//--------------------Installation content-------------------------------
define('TXT_INSTALLATION_TITLE', "CslManager install");
define('TXT_INSTALLATION', "Installation");
define('TXT_INSTALLATION_HASHTAG_1', "#Training");
define('TXT_INSTALLATION_HASHTAG2', "#Simulation");
define('TXT_INSTALLATION_HASHTAG3', "#Veterinary");
define('TXT_INSTALLATION_BDD', "Database");
define('TXT_INSTALLATION_FEEDBACK', "Feedback");
define('TXT_INSTALLATION_MOODLE', "Moodle");
define('TXT_INSTALLATION_CUSTOM', "Personalization");
define('TXT_INSTALLATION_AUTH', "Authenticate");
define('TXT_INSTALLATION_ADMIN', "Administrator");
define('TXT_INSTALLATION_ADMIN_NAME', "Administrator name:");
define('TXT_INSTALLATION_MODAL_TITLE', "More Information");
define('TXT_INSTALLATION_SERVER_NAME', "MYSQL Server name:");
define('TXT_INSTALLATION_SERVER_NAME_EXAMPLE', "ex: localhost");
define('TXT_INSTALLATION_LOGIN_BDD', "Login:");
define('TXT_INSTALLATION_LOGIN_BDD_EXAMPLE', "ex: root");
define('TXT_INSTALLATION_PASSWORD', "Password:");
define('TXT_INSTALLATION_PASSWORD_WRONG', "Password must be equals");
define('TXT_INSTALLATION_BDD_NAME', "Database name:");
define('TXT_INSTALLATION_BDD_NAME_EXAMPLE', "ex: cslmanager");
define('TXT_INSTALLATION_SMTP_HOST', "SMTP Host:");
define('TXT_INSTALLATION_SMTP_HOST_EXAMPLE', "ex: smtps.exemple.fr");
define('TXT_INSTALLATION_SMTP_SECURE', "Secure key:");
define('TXT_INSTALLATION_OPTIONS', "Options");
define('TXT_INSTALLATION_SELECT_OPTION', "Select an option");
define('TXT_INSTALLATION_SMTP_SSL', "ssl");
define('TXT_INSTALLATION_SMTP_TLS', "tls");
define('TXT_INSTALLATION_SMTP_PORT', "Port:");
define('TXT_INSTALLATION_SMTP_HOST_EMAIL', "Host email:");
define('TXT_INSTALLATION_SMTP_HOST_EMAIL_EXAMPLE', "ex: example@outlook.fr");
define('TXT_INSTALLATION_MOODLE_TOKEN', "Token:");
define('TXT_INSTALLATION_MOODLE_URL', "Moodle URL :");
define('TXT_INSTALLATION_MOODLE_URL_EXAMPLE', "ex: http://moodle.fr");
define('TXT_INSTALLATION_MOODLE_COURSE', "Course:");
define('TXT_INSTALLATION_TECHNIQUE_SUPPORT', "Technical support:");
define('TXT_INSTALLATION_LOGO', "Logo");
define('TXT_INSTALLATION_CHOOSE_FILE', "Choose file");
define('TXT_INSTALLATION_AUTH_URI', "Uri:");
define('TXT_INSTALLATION_AUTH_URI_EXAMPLE',"ex :srv-ldap-01.example.fr");
define('TXT_INSTALLATION_AUTH_BASEDN', "BaseDn:");
define('TXT_INSTALLATION_AUTH_BINDDN', "BindDn:");
define('TXT_INSTALLATION_AUTH_BINDPW', "BindPw:");
define('TXT_INSTALLATION_AUTH_BINDDN_EXAMPLE', "ex: uid=vetsims,ou=people,dc=example,dc=fr");
define('TXT_INSTALLATION_AUTH_PORT', "Port:");
define('TXT_INSTALLATION_LAUNCH_INSTALL', "Launch application");
define('TXT_INSTALLATION_APPLICATION_TITLE', "Application name");
define('TXT_INSTALLATION_CONGRADULATION', "Congratulations");
define('TXT_INSTALLATION_BDD_SUCCESS', "Data base created");
define('TXT_INSTALLATION_START', "Let's start");
define('TXT_INSTALLATION_WARNING', "Caution, if all information is not given or if some is inaccurate, some features can be impacted");
define('TXT_INSTALLATION_MODAL_BDD_1', "You will need to create the database in MYSQL beforehand");
define('TXT_INSTALLATION_MODAL_BDD_2', "Information is required");
define('TXT_INSTALLATION_MODAL_FEEDBACK_1', "The information is used to allow your users to give you feedback on the application");
define('TXT_INSTALLATION_MODAL_FEEDBACK_2', "You will find further information ");
define('TXT_INSTALLATION_MODAL_MOODLE_1', "The information is used here in connection with Moodle.");
define('TXT_INSTALLATION_MODAL_MOODLE_2', "You must first create a course on Moodle which will be attached by its ID number (identifier).");
define('TXT_INSTALLATION_MODAL_MOODLE_3', "Then, you need to create a token to allow the application to access the dedicated zone (you will find additional information ");
define('TXT_INSTALLATION_MODAL_AUTH_1', "Ldap parameters are used for user authentication on the educational platform.");
define('TXT_INSTALLATION_MODAL_AUTH_2', "Ldap information is mandatory. You will find information on how to fill in these fields ");
define('TXT_INSTALLATION_MODAL_ADMIN_1', "The information provided here is used to create the application administrator");
define('TXT_INSTALLATION_MODAL_ADMIN_2', "The information is related to the directory used during authentication.");
define('TXT_INSTALLATION_HERE', "here");
define('TXT_INSTALLATION_ERROR_PASSWORD_SIZE',  "Password must count 6 letters minimum");
define('TXT_INSTALLATION_ERROR_PASSWORD_CHECK', "Password must be equals");

//--------------------Mail content-------------------------------
define('TXT_MAIL_INIT_TITLE', "An CSLManager account has just been created for you");
define('TXT_MAIL_INIT_CONTENT1', "To change your password click on this ");
define('TXT_MAIL_CHANGE_TITLE', "You just sent a password change request");
define('TXT_MAIL_INIT_CONTENT', "It will be available for 15 minutes.");
