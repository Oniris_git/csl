<?php

//----------- deutsche Sprachdatei --------------------
define('TXT_LABEL', 'Bezeichnung :');
define('TXT_ACCOUNT', 'Kontotyp');
define('TXT_MANUAL_ACCOUNT', 'Manuell');
define('TXT_LDAP_ACCOUNT', 'LDAP');
define('TXT_FULL_LABEL', 'Lange Bezeichnung');
define('TXT_DESCRIPTION', 'Beschreibung :');
define('TXT_EDIT', 'Ändern');
define('TXT_ADD', 'Anlegen');
define('TXT_DELETE', 'Löschen');
define('TXT_PRINT', 'Drucken');
define('TXT_ACTIVE', 'Mitwirkend');
define('TXT_SCALE', 'Bewertungsskala');
define('TXT_DOMAIN', 'Domain');
define('TXT_BROWSE', 'Reise');
define('TXT_ROOM', 'Raum');
define('TXT_PROFILE', 'Profil');
define('TXT_ADMIN', 'Administrator');
define('TXT_MANAGER', 'Manager');
define('TXT_STUDENT', 'Student');
define('TXT_ID', 'Kode');
define('TXT_NOTREADY', 'Computercode nicht bereit');
define('TXT_ID_HELP', 'Achtung, diese Identifikation ist eindeutig und wird später gesperrt.');
define('TXT_LOGIN', 'Login');
define('TXT_PASSWORD', 'Passwort');
define('TXT_NEW_PASSWORD', 'Neues Passwort');
define('TXT_PASSWORD_CONFIRM', 'Bestätigen');
define('TXT_NAME', 'Name');
define('TXT_FORENAME', 'Vorname');
define('TXT_NAME_MIN', 'vorname');
define('TXT_ENTER', 'Eingeben');
define('TXT_DISCONNECT', 'Abmelden');
define('TXT_CONNECT', 'Verbinding');
define('TXT_WARNING', 'Achtung !');
define('TXT_ERROR', 'Fehler');
define('TXT_APPLY', ' Senden');
define('TXT_CANCEL', ' Übung stornieren');
define('TXT_IMPORT', ' Einführen');
define('TXT_PARAMETERS', ' Einstellung');
define('TXT_MOODLE', ' Link zu Moodle');
define('TXT_WORKSHOP_DIFFICULTY', 'Schwierigkeitsgrad');
define('TXT_EXERCICE', 'Übung');
define('TXT_LINK','Link');
define('TXT_WELCOME','Willkommen ');
define('TXT_START','');
define('TXT_DURATION','');
define('TXT_FIRST_NAME','Vorname');
define('TXT_NAME_FIELD','Name');
define('TXT_REGISTER','Registrierung');
define('TXT_REGISTER_FIELD','vorname.name');
define('TXT_REGISTER_MAIL','Email');
define('TXT_REGISTER_SUCCESS','Vollständige Registrierung');
define('TXT_REGISTER_ERROR','Registrierungsfehler');
define('TXT_REGISTER_USERNAME_ERROR','Falsch Benutzer-ID');
define('TXT_REGISTER_MDP_ERROR','Passwörter müssen gleich sein');
define('TXT_REGISTER_MDP_ERROR_LENGTH','Das Passwort muss mindestens 6 Zeichen lang sein');
define('TXT_HELP','Hilfe');
define('TXT_FILTER','Sortieren');
define('TXT_CANCELED_WORKSHOP_ATTEMPT','übung storniert');
define('TXT_ERROR_CANCELED_WORKSHOP_ATTEMPT','Es ist nicht möglich, die Übung zu stornieren, bitte einen Manager kontactieren.');

//--------------------Page install.phtml---------------------------------
define('TXT_INDEX_PAGE_OVERVIEW', 'Übersicht');

//--------------------Page header.phtml---------------------------------
define('TXT_HEADER_ADMIN_PANEL', 'Adminpanel');

//--------------------Page login.phtml---------------------------------
define('TXT_LOGIN_PAGE_CONNECTION', 'Anmeldeseite');

//--------------------Page navbar.phtml---------------------------------
define('TXT_NAVBAR_FEEDBACK', 'Kontaktieren Sie uns');
define('TXT_NAVBAR_TOGGLE', 'Navigation umschalten');
define('TXT_NAVBAR_HOME', 'Startseite');

//--------------------Page side_navbar.phtml---------------------------------
define('TXT_SIDE_NAVBAR_DASHBOARD', 'Dashboard');
define('TXT_SIDE_NAVBAR_WORKSHOPS', 'Übungen');
define('TXT_SIDE_NAVBAR_USERS', ' Nutzer/innen');
define('TXT_SIDE_NAVBAR_PREFERENCES', 'Einstellungen');
define('TXT_SIDE_NAVBAR_ADMINISTRATION', 'Administration');
define('TXT_SIDE_NAVBAR_PROFILES', 'Profile');
define('TXT_SIDE_NAVBAR_RANK', 'Rangstufen');
define('TXT_SIDE_NAVBAR_SPECIES', 'Arten');
define('TXT_SIDE_NAVBAR_CATEGORY', 'Kategorien');
define('TXT_SIDE_NAVBAR_SUB_CATEGORY', 'Unterkategorien');
define('TXT_SIDE_NAVBAR_DISCIPLINE', 'Disziplin');
define('TXT_SIDE_NAVBAR_SCALES', 'Bewertungsskalen');
define('TXT_SIDE_NAVBAR_STATS', 'Statistiken');
define('TXT_SIDE_NAVBAR_ROOMS', 'Raüme');
define('TXT_SIDE_NAVBAR_HOME', 'Startseite');
define('TXT_SIDE_MANAGEMENT', ' Management');
define('TXT_SIDE_USER', ' Nutzer/in');
define('TXT_SIDE_CUSTOM', ' Anpassung');
define('TXT_SIDE_LOGO', ' Logo');
define('TXT_SIDE_LANG', ' Sprachen');
define('TXT_SIDE_LEGAL', ' Rechthinweise');
define('TXT_SIDE_SERVER', ' Server');
define('TXT_SIDE_AUTH', ' Authentifizierung');
define('TXT_SIDE_MAILER', ' Mailer');
define('TXT_SIDE_ROOTING', ' Moodlepfad');

//--------------------Page skel.phtml---------------------------------
define('TXT_SKEL_NOSCRIPT', 'JavaScript ist erforderlich, um alle Funktionen nutzen zu können !');

//--------------------Page feedback.phtml---------------------------------
define('TXT_FEEDBACK', 'Kommentar');
define('TXT_FEEDBACK_HELP', 'Eine Bemerkung ? Ein Vorschlag ? Ein Fehler ?<br/>Kontaktieren Sie uns über dieses Formular.');
define('TXT_FEEDBACK_ABUSE', 'Danke, dass Sie nicht missbrauchen');
define('TXT_FEEDBACK_SUBJECT', 'Ziel');
define('TXT_FEEDBACK_COMMENT', 'Bemerkung');
define('TXT_FEEDBACK_SUGGESTION', 'Vorschlag');
define('TXT_FEEDBACK_GLITCH', 'Fehler');
define('TXT_FEEDBACK_MESSAGE', 'Meldung');

//--------------------Page stat.phtml---------------------------------
define('TXT_STAT_USERSTAT', ' Benutzerstatistik ');
define('TXT_STAT_CSV_USERSTAT', 'CSV-Export von Benutzerstatistiken');
define('TXT_STAT_GLOBALSTAT', 'Umfassenden Statistiken');
define('TXT_STAT_CSVSTAT', 'Datenexport im CSV-Format');
define('TXT_STAT_EXERCICE_LIST', 'Aufstellung der durchgeführten Übungen ');
define('TXT_STAT_AVERAGE', ' Durchschnitt');
define('TXT_STAT_WORDING', 'Bezeichnung');
define('TXT_STAT_EVALUATION', 'Bewertungsskala');
define('TXT_STAT_TIME', 'Dauer');
define('TXT_STAT_DATE', 'Datum');
define('TXT_STAT_CLOSE', 'Abmelden');
define('TXT_STAT_REMAINING_EXERCICE', ' Verbleibenden Übungen');
define('TXT_STAT_DISTINCTS_EXERCICE', ' Verschieden Übungen');
define('TXT_STAT_MODAL_TITLE', 'Aufstellung der verbleibenden Übungen');
define('TXT_STAT_COUNT_TRY', 'Versuche');
define('TXT_STAT_BEST_TIME', 'Beste Zeit');
define('TXT_STAT_BEST_GRADE', 'Bestnote');
define('TXT_STAT_SHOW_GRADES', 'Verteilung der Noten von ');

//--------------------Page stat_filter.phtml---------------------------------
define('TXT_STAT_FILTER_TITLE_FILTER',' Statistiken sortieren');
define('TXT_STAT_FILTER_CHOOSE','Bitte wählen Sie eine Option');
define('TXT_STAT_FILTER_ACTION','Filtern');
define('TXT_STAT_FILTER_EXERCICE','Übung');
define('TXT_STAT_FILTER_USER','Nutzer/in');
define('TXT_STAT_FILTER_GRADE','Grad');

//--------------------Page workshop_filter.phtml---------------------------------
define('TXT_WORKSPACE_FILTER_BUTTON',' Übungen sortieren');
define('TXT_WORKSPACE_FILTER_ROOM','Raum');
define('TXT_WORKSPACE_FILTER_CATEGORY','Kategorie');
define('TXT_WORKSPACE_FILTER_SUB_CATEGORY','Unterkategorie');
define('TXT_WORKSPACE_FILTER_SPECIE','Art');
define('TXT_WORKSPACE_FILTER_COUNT_TO_VALIDATE','Versuche');
define('TXT_WORKSPACE_FILTER_COUNT_DIFFICULTY','Schwierigkeitsgrad');
define('TXT_WORKSPACE_FILTER_RANK','Rangstufe');
define('TXT_WORKSPACE_DISCIPLINE','Disziplin');

//--------------------Page list_priority_level.phtml---------------------------------
define('TXT_GRADE_GLOBAL_T','Umfassende ternär');
define('TXT_GLOBAL_Q','Umfassende quaternär');
define('TXT_GLOBAL','Umfassende');
define('TXT_DISTINCTS_T','Verschiedene ternär');
define('TXT_DISTINCT_Q','Verschiedene quaternär');
define('TXT_DISTINCT','Verschiedene');

//--------------------Page list-woorkspace.phtml---------------------------------
define('TXT_LIST_WORKSPACE_ROOM','Raum');
define('TXT_LIST_WORKSPACE_CATEGORY','Kategorie');
define('TXT_LIST_WORKSPACE_SUB_CATEGORY','Unterkategorie');
define('TXT_LIST_WORKSPACE_SPECIE','Art');
define('TXT_LIST_WORKSPACE_COUNT_TO_VALIDATE','Versuche');
define('TXT_LIST_WORKSPACE_DIFFICULTY','Schwierigkeitsgrad');
define('TXT_LIST_WORKSPACE_RANK','Skala');
define('TXT_LIST_WORKSPACE_DISCIPLINE','Disziplin');
define('TXT_NOT_COMPLETE','Nicht ausgefüllt;');
define('TXT_ROOM_NUMBER','Raum n&deg; : ');
define('TXT_LIST_WORKSPACE_CATEGORY_TITLE','Kategorie : ');
define('TXT_LIST_WORKSPACE_SUB_CATEGORY_TITLE','Unterkategorie : ');
define('TXT_LIST_WORKSPACE_SPECIE_TITLE','Art :');
define('TXT_LIST_WORKSPACE_COUNT_TO_VALIDATE_TITLE','Versuche : ');
define('TXT_LIST_WORKSPACE_DIFFICULTY_TITLE','Schwierigkeitsgrad : ');
define('TXT_LIST_WORKSPACE_RANK_TITLE','Skala : ');
define('TXT_LIST_WORKSPACE_DISCIPLINE_TITLE','Disziplin : ');
define('TXT_LIST_WORKSPACE_FILTER_BY',' Sortieren mit : ');

//--------------------Page list_user_filter.phtml---------------------------------
define('TXT_USER_FILTER_USERS',' Nutzer/innen');
define('TXT_USER_FILTER_SEARCH','Suchen');
define('TXT_USER_SEARCH_DONE_ON','Suchen nach ');
define('TXT_USER_FILTER_SEARCH_PLACEHOLDER','Suchen nach Name, Vorname...');
define('TXT_USER_FILTER_NAME','Name');
define('TXT_USER_FILTER_FIRSTNAME','Vorname');
define('TXT_USER_FILTER_UPN','UPN');
define('TXT_USER_FILTER_PROFIL','Profil');

//--------------------Page list_priority_level.phtml---------------------------------
define('TXT_LIST_GRADE_USER','Nutzer/in');
define('TXT_LIST_GRADE_WORDING','Bezeichnung ;');
define ('TXT_LIST_GRADE_DATE','Datum');
define('TXT_LIST_GRADE_MODAL_TITLE','Hilfe');
define('TXT_LIST_GRADE_DESCRIPTION','Die Daten werden so sortiert :');
define ('TXT_LIST_GRADE_GLOBAL_TERNAIRE','"Umfassende ternär" anzeigt alle Übungen mit einer ternäre Skala.');
define ('TXT_LIST_GRADE_GLOBAL_QUATERNAIRE','"Umfassende quaternär" anzeigt alle Übungen mit einer quaternäre Skala.');
define ('TXT_LIST_GRADE_GLOBAL','"Umfassende" anzeigt alle ausgefürhte Übungen.');
define ('TXT_LIST_GRADE_DISTINCT_TERNAIRE','"Verschiedene ternär" anzeigt alle verschiedene Übungen mit einer ternäre Skala.');
define ('TXT_LIST_GRADE_DISTINCT_QUATERNAIRE','"Verschiedene quaternär" anzeigt alle verschiedene Übungen mit einer quaternäre Skala.');
define ('TXT_LIST_GRADE_DISTINCT','"Verschiedene"anzeigt alle Übungen, dass bei verschiedene Benuzer/innen ausgeführt sind.');

//--------------------Page list_workshop_attempt.phtml---------------------------------
define('TXT_LIST_EXERCICE_WORDING','Bezeichnung;');
define('TXT_LIST_EXERCICE_USER','Nutzer/in');
define('TXT_LIST_EXERCICE_DURATION','Dauer');

//--------------------Page technicalsheet_exercice.phtml---------------------------------
define('TXT_TECHNICALSHEET_TITLE','Übung zusammengefassten Daten');
define('TXT_TECHNICALSHEET_TITLE_MODAL_USERS','Benutzerliste');
define('TXT_TECHNICALSHEET_DESCRIPTION','Beschreibung');
define('TXT_TECHNICALSHEET_MODAL_TITLE','Übung Beschreibung');
define('TXT_TECHNICALSHEET_ROOM','Raum');
define('TXT_TECHNICALSHEET_SPECIE','Art');
define('TXT_TECHNICALSHEET_CATEGORY','Kateagorie');
define('TXT_TECHNICALSHEET_TIME','Vorgegebene Zeit');
define('TXT_CLOSE','Abmelden');
define('TXT_TECHNICALSHEET_NUMBER_USER','Benutzerzahl');
define('TXT_TECHNICALSHEET_AVERAGE_DURATION','Durchschnittliche Dauer (Minuten)');
define('TXT_TECHNICALSHEET_AVERAGE_TRY','Durchschnittliche Anzahl der Versuche');
define('TXT_TECHNICALSHEET_STATUS','Grad erworben oder mehr');
define('TXT_TECHNICALSHEET_DISTINCT','Verschiedene Benutzer');
define('TXT_TECHNICALSHEET_COMENTS','Kommentaren');
define('TXT_TECHNICALSHEET_COMENTS_TITLE','Student Kommentaren');
define('TXT_TECHNICALSHEET_NO_COMENTS','Kein Kommentar für diese Übung');
define('TXT_UPN','UPN');

//--------------------Page user.phtml---------------------------------
define('TXT_USER_EDIT_USER', 'Benutzereigenschaften ändern');
define('TXT_USER_STAT_USER', 'Benutzerstatistiken anzeigen');
define('TXT_USER_MY_STAT', 'Ihre Statistiken anzeigen');

//--------------------Page legal.phtml---------------------------------
define('TXT_LEGAL_SUCCESS', 'Daten gespeichert');

//--------------------Page workshop.phtml---------------------------------
define('TXT_WORKSHOP_ADD_WORKSHOP', 'Übung hinzufügen');
define('TXT_WORKSHOP_ID', 'Bezeichnung');
define('TXT_WORKSHOP_LMS_COURSE', 'MoodleKurs');
define('TXT_WORKSHOP_LMS_ID', 'Moodle Aktivität ID');
define('TXT_WORKSHOP_LMS_URL', 'Link zu Moodle');
define('TXT_WORKSHOP_LEVEL', 'Schwierigkeitsgrad');
define('TXT_WORKSHOP_ATTEMPTS', 'Anzahl der Versuche zur Erwahrung');
define('TXT_WORKSHOP_NUMBER', 'Anzahl');
define('TXT_WORKSHOP_DURATION', 'Mindestdauer (min)');
define('TXT_WORKSHOP_DURATION_MAX', 'Maximale Dauer (min)');
define('TXT_WORKSHOP_EDIT', 'Übungeigenschaften ändern');
define('TXT_WORKSHOP_QRCODE', 'Der QR-code is basierend auf der Websitehauptverzeichnis und der Übungbezeichnung generiert.');
define('TXT_WORKSHOP_SYNCHRONISATION', ' Moodle Synchronisierung');
define('TXT_WORKSHOP_QRCODE2', 'Wenn Sie das Kurzetikett wechseln, müssen Sie den QR-Code erneut drucken.');
define('TXT_WORKSHOP_PARAMETER_MISSING', 'Fehlende Parameter');
define('TXT_WORKSHOP_ALTERED', "Übung geändert");
define('TXT_WORKSHOP_CREATED', "Übung erstellt");
define('TXT_WORKSHOP_DELETED', "Übung gelöscht");
define('TXT_CATEGORY', 'Kategorie');
define('TXT_SUBCATEGORY', 'Unterkategorie');
define('TXT_DISCIPLINE', 'Disziplin');
define('TXT_RANK', 'Rangstufe');
define('TXT_SPECIES', 'Art');

//--------------------Page admin/admin_scale_type.phtml---------------------------------
define('TXT_ADMIN_EVALUATION_SCALE_LIST', ' Liste der Bewertungsskalen');
define('TXT_ADMIN_EVALUATION_SCALE_NB', 'Bewertungsskalen Anzahl');
define('TXT_ADMIN_EVALUATION_ADD_SCALE', 'Eine Bewertungsskala hinzufügen');
define('TXT_ADMIN_EVALUATION_HELP', 'Sie können verfügbare Grade per Drag\'Drop hinzufügen');
define('TXT_ADMIN_EVALUATION_ADD_SCALE_ITEM', 'Ein Grad hinzufügen');
define('TXT_ADMIN_EVALUATION_AVAILABLE', 'Verfügbar');
define('TXT_ADMIN_EVALUATION_EDIT_SCALE', 'Bewertungsskala ändern');
define('TXT_ADMIN_EVALUATION_MOODLE_SCALES', ' Moodle Skalen mit ID zurückbekommen');
define('TXT_ADMIN_EVALUATION_MOODLE_ADD_SUCCESS', ' Moodle Skala hinzugefügt');
define('TXT_ADMIN_EVALUATION_MOODLE_ADD_ERROR', "Sie müssen den Moodle ID eingeben");

//--------------------Page front/install.phtml---------------------------------
define('TXT_FRONT_INDEX_MYSTATS', 'Meine Statistiken');
define('TXT_FRONT_INDEX_WELCOME', 'Willkommen auf ');
define('TXT_FRONT_INDEX_BEGIN', 'Um zu beginnen, müssen Sie den QR-Code einer der Übungen flashen');
define('TXT_FRONT_INDEX_MORNING', 'Hallo');
define('TXT_FRONT_INDEX_THANKS', 'Vielen Dank');
define('TXT_FRONT_INDEX_CONNECTED', 'Sie sind verbunden');
define('TXT_FRONT_INDEX_SESSION_BEGIN', 'Beginn der Sitzung');
define('TXT_FRONT_INDEX_LMS_DOC', 'Moodledocs');
define('TXT_FRONT_INDEX_LMS_DOC_LINK', ' Link zur Dokumentation');
define('TXT_FRONT_INDEX_WORKSHOP_DESCRIPTION', 'Übung Beschreibung');
define('TXT_FRONT_INDEX_REMINDER', 'Füllen Sie am Ende Ihrer Sitzung Ihre Selbstbewertung aus');
define('TXT_FRONT_INDEX_EVALUATION', 'Selbstbewertung');
define('TXT_FRONT_INDEX_OPTION_LABEL', 'Wählen Sie');
define('TXT_FRONT_INDEX_COMMENT', 'Möglicher Kommentar');
define('TXT_FRONT_INDEX_COMMENT_LENGTH', 'Maximal 200 Zeichen');
define('TXT_FRONT_INDEX_ABOUT_PAGE', ' Àpropos');
define('TXT_FRONT_INDEX_DOMAIN', 'Domain');

//--------------------Page blocks/list_user.phtml---------------------------------
define('TXT_BLOCKS_LIST_USER_MAIL', 'Mail');
define('TXT_BLOCKS_LIST_USER_FILTER', 'Filtern');
define('TXT_DEPARTMENT', 'Department');


//--------------------Page blocks/stats.phtml---------------------------------
define('TXT_BLOCKS_STATS_PERMONTH', 'Per Monat');
define('TXT_BLOCKS_STATS_TIME', 'Gesamtzeit');
define('TXT_BLOCKS_STATS_WORKSHOPS', ' Durchgeführten Übungen');
define('TXT_BLOCKS_STATS_WORKSHOPS_DISTINCTS', 'Verschiedene Übungen');
define('TXT_BLOCKS_STATS_SPAN', 'Verteilung');
define('TXT_BLOCKS_STATS_GRADES', 'Grade');
define('TXT_BLOCKS_STATS_COMMENT', 'Kommentar');
define('TXT_BLOCKS_STATS_CSV_EXPORT', 'CSV Export');
define('TXT_BLOCKS_STATS_WORKSHOPS_MODAL_TITLE', 'Liste den durchgeführten Übungen');

//--------------------Page blocks/workshopAttemptInProgress.phtml---------------------------------
define('TXT_BLOCKS_PROGRESS_HEADER', 'Laufende Übungen');
define('TXT_BLOCKS_PROGRESS_BEGINBY', 'Begonnen bei');
define('TXT_BLOCKS_PROGRESS_BEGINDATE', 'Begonnen am');

//--------------------Page admin/admin_profile.phtml---------------------------------
define('TXT_ADMIN_PROFILE_LIST', ' Liste der Profile');
define('TXT_ADMIN_PROFILE_NB', 'Profile Anzahl');
define('TXT_ADMIN_PROFILE_NAME', 'Profilname');
define('TXT_ADMIN_PROFILE_RIGHTS', 'Rechte des Profils');
define('TXT_ADMIN_ADD_PROFILE', 'Ein Profil hinzufügen');
define('TXT_ADMIN_PROFILE_RIGHTS_HELP', 'Klicken Sie für eine Beschreibung');

//--------------------Page admin/admin_room.phtml---------------------------------
define('TXT_ADMIN_MANAGE_ROOM', ' Raum Management');
define('TXT_ADMIN_ADD_ROOM', 'Einen Raum hinzufügen');

//--------------------Page admin/admin_mailer.phtml---------------------------------
define('TXT_ADMIN_MAILER_TITLE', ' Mailer Einstellungen');
define('TXT_ADMIN_NOTIFICATION_SUCCESS', ' Mailer Einstellungen verändert ; ein E-mail wurde gesendet');
define('TXT_ADMIN_MAILER_EMPTY', 'Der Inhalt darf nicht leer sein.');
define('TXT_ADMIN_MAILER_SEND_SUCCESS', 'Ihre E-mail wurde gesendet.');
define('TXT_ADMIN_MAILER_SEND_ERROR', 'Fehler beim Senden der E-Mail');
define('TXT_ADMIN_SYNC_USERS', 'Sync departments');

//--------------------Page admin/admin_auth.phtml---------------------------------
define('TXT_ADMIN_AUTH_TITLE', ' Authentifizierungssystem');
define('TXT_ADMIN_AUTH_SUCCESS', ' Authentifizierung Einstellungen verändert');
define('TXT_ADMIN_AUTH_ACTUAL', ' Gespeicherten Informationen');
define('TXT_ADMIN_AUTH_AUTH', 'Authentifizierung : ');
define('TXT_ADMIN_AUTH_ATTRIBUTE', 'Attribut');
define('TXT_ADMIN_AUTH_VALUE', 'Wert');
define('TXT_ADMIN_AUTH_ALTER', ' Authentifizierung ändern');
define('TXT_ADMIN_AUTH_LDAP', 'LDAP');
define('TXT_ADMIN_AUTH_AD', 'Active Directory');
define('TXT_ADMIN_AUTH_PARAMETERS', 'LDAP Parameter');

//--------------------Page admin/admin_rooting.phtml---------------------------------
define('TXT_ADMIN_ROOTING_SUCCESS', ' Moodle Einstellungen verändert');

//--------------------Page admin/admin_user.phtml---------------------------------
define('TXT_ADMIN_ADD_USER', ' Nutzer/in hinzufügen');
define('TXT_ADMIN_IMPORT_USER_LDAP', 'Benutzer/innen via LDAP importieren');
define('TXT_ADMIN_IMPORT_USER_CSV', 'Benutzer/innen via CSV importieren');
define('TXT_ADMIN_IMPORT_USER_HELP', 'Die Datenfelder sind mit </code> getrennt. Die Kolbzeilen müssen ausgefüllt sein.');

//--------------------Page admin/admin_category.phtml-------------------------------
define('TXT_ADMIN_ADD_CATEGORY', 'Eine Kategorie hinzufügen');
define('TXT_ADMIN_CATEGORY_LIST', ' Liste der Kategorien');
define('TXT_ADMIN_CATEGORY_NB', 'Kategorien Anzahl');
define('TXT_ADMIN_CATEGORY_LIBELLE', 'Bezeichnung');
define('TXT_ADMIN_CATEGORY_DESCRIPTION', 'Beschreibung');
define('TXT_ADMIN_CATEGORY_MODIFY', 'Ändern');
define('TXT_ADMIN_CATEGORY_FORM_LIBELLE', 'Bezeichnung');
define('TXT_ADMIN_CATEGORY_FORM_DESCRIPTION', 'Beschreibung');
define('TXT_ADMIN_CATEGORY_BUTTON_ADD', 'Fügen');
define('TXT_ADMIN_CATEGORY', 'Kategorie');
define('TXT_ADMIN_CATEGORY_BUTTON_ALTER', 'Ändern');
define('TXT_ADMIN_CATEGORY_ID', 'Kategorie ID');
define('TXT_ADMIN_CATEGORY_ERROR_UNKNOWN', 'Unauffindbare Kategorie');

//--------------------Page admin/admin_sub_category.phtml-------------------------------
define('TXT_ADMIN_ADD_SUB_CATEGORY', 'Eine Unterkategorie hinzufügen');
define('TXT_ADMIN_SUB_CATEGORY_LIST', ' Liste der Unterkategorien');
define('TXT_ADMIN_SUB_CATEGORY_NB', 'Unterkategorien Anzahl');
define('TXT_ADMIN_SUB_CATEGORY_LIBELLE', 'Bezeichnung');
define('TXT_ADMIN_SUB_CATEGORY_DESCRIPTION', 'Beschreibung');
define('TXT_ADMIN_SUB_CATEGORY_MODIFY', 'Ändern');
define('TXT_ADMIN_SUB_CATEGORY_FORM_LIBELLE', 'Bezeichnung');
define('TXT_ADMIN_SUB_CATEGORY_FORM_DESCRIPTION', 'Beschreibung');
define('TXT_ADMIN_SUB_CATEGORY_BUTTON_ADD', 'Fügen');
define('TXT_ADMIN_SUB_CATEGORY', 'Unterkategorie');
define('TXT_ADMIN_SUB_CATEGORY_BUTTON_ALTER', 'Ändern');
define('TXT_ADMIN_SUB_CATEGORY_ID', 'Unterkategorie ID');

//--------------------Page admin/admin_species.phtml-------------------------------
define('TXT_ADMIN_ADD_SPECIES', 'Eine Art hinzufügen');
define('TXT_ADMIN_SPECIES_LIST', ' Liste der Arten');
define('TXT_ADMIN_SPECIES_NB', 'Arten Anzahl');
define('TXT_ADMIN_SPECIES_LIBELLE', 'Bezeichnung');
define('TXT_ADMIN_SPECIES_DESCRIPTION', 'Beschreibung');
define('TXT_ADMIN_SPECIES_MODIFY', 'Ändern');
define('TXT_ADMIN_SPECIES_FORM_LIBELLE', 'Bezeichnung');
define('TXT_ADMIN_SPECIES_FORM_DESCRIPTION', 'Beschreibung');
define('TXT_ADMIN_SPECIES_BUTTON_ADD', 'Fügen');
define('TXT_ADMIN_SPECIES', 'Art');
define('TXT_ADMIN_SPECIES_BUTTON_ALTER', 'Ändern');
define('TXT_ADMIN_SPECIES_ID', 'Art ID');
define('TXT_ADMIN_SPECIES_ERROR_UNKNOWN', 'Unauffindbare Art');

//--------------------Page admin/admin_discipline.phtml-------------------------------
define('TXT_ADMIN_ADD_DISCIPLINE', 'Eine Disziplin hinzufügen');
define('TXT_ADMIN_DISCIPLINE_LIST', ' Liste der Disziplinen');
define('TXT_ADMIN_DISCIPLINE_NB', 'Disziplin Anzahl');
define('TXT_ADMIN_DISCIPLINE_LIBELLE', 'Bezeichnung');
define('TXT_ADMIN_DISCIPLINE_DESCRIPTION', 'Beschreibung');
define('TXT_ADMIN_DISCIPLINE_MODIFY', 'Ändern');
define('TXT_ADMIN_DISCIPLINE_FORM_LIBELLE', 'Bezeichnung');
define('TXT_ADMIN_DISCIPLINE_FORM_DESCRIPTION', 'Beschreibung');
define('TXT_ADMIN_DISCIPLINE_BUTTON_ADD', 'Fügen');
define('TXT_ADMIN_DISCIPLINE', 'Disziplin');
define('TXT_ADMIN_DISCIPLINE_BUTTON_ALTER', 'Ändern');
define('TXT_ADMIN_DISCIPLINE_ID', 'Disziplin ID');

//--------------------Page admin/admin_priority_level.phtml-------------------------------
define('TXT_ADMIN_ADD_RANK', 'Einen Prioritätsrang hinzufügen¨');
define('TXT_ADMIN_RANK_LIST', ' Liste den Prioritätsränge');
define('TXT_ADMIN_RANK_NB', 'Prioritätsränge Anzahl');
define('TXT_ADMIN_RANK_LIBELLE', 'Bezeichnung');
define('TXT_ADMIN_RANK_DESCRIPTION', 'Beschreibung');
define('TXT_ADMIN_RANK_MODIFY', 'Ändern');
define('TXT_ADMIN_RANK_FORM_LIBELLE', 'Bezeichnung');
define('TXT_ADMIN_RANK_FORM_DESCRIPTION', 'Beschreibung');
define('TXT_ADMIN_RANK_BUTTON_ADD', 'Fügen');
define('TXT_ADMIN_RANK', 'Prioritätsrang');
define('TXT_ADMIN_RANK_BUTTON_ALTER', 'Ändern');
define('TXT_ADMIN_RANK_ID', 'Prioritätsrang ID');
define('TXT_ADMIN_RANK_FILTER', ' Per Prioritätsrang filtern');

//--------------------Page admin/admin_poste.phtml-------------------------------
define('TXT_ADMIN_ADD_RIGHT', ' Ein Recht hinzufügen');

//--------------------Page block/list_profile.phtml-------------------------------
define('TXT_ADMIN_ADD_NEW_RIGHT', ' Ein Recht zu Profil hinzufügen');
define('TXT_ADMIN_DELETE_RIGHT', ' Profilrechte löschen');
define('TXT_ADMIN_UPDATE_RIGHT', ' Profilrechte ändern');

//--------------------Page language.phtml-------------------------------
define('TXT_LANG_MANAGE', ' Sprachenmanagement');
define('TXT_LANG_AVAILABLE', ' Verfügbaren Sprachen');
define('TXT_LANG_ADD_FILE', ' Sprachdatei in drei Schritten hinzufügen');
define('TXT_LANG_ALTER_FILE', '');
define('TXT_LANG_DEFAULT', ' Standardsprache');
define('TXT_LANG_STEP_1', ' Schritt 1: Sprachdatei herunterladen');
define('TXT_LANG_ALTER_STEP_1', '');
define('TXT_LANG_STEP_2', ' Schritt 2: Umbenennen, Ausfüllen, Ablegen');
define('TXT_LANG_STEP_ALTER_2', ' Schritt 2: Ausfüllen, Ablegen');
define('TXT_LANG_STEP_3', ' Schritt 3: Flaggesymbol ablegen');
define('TXT_LANG_CHANGE_DEFAULT', ' Standardsprache ändern');
define('TXT_LANG_MODAL_STEP1', ' Herunterladen Sie eine bestehende Sprachdatei um die Variables zu verändern');
define('TXT_LANG_MODAL_STEP2', ' Umbennen Sie die Sprachdatei, um die Variables in der neue Sprach auszufüllen.');
define('TXT_LANG_MODAL_STEP3', ' Herunterladen Sie ein Flaggesymbol in png-Format über nachstehenden Link und ablegen Sie es' );

//--------------------Page view.phtml-------------------------------
define('TXT_VIEW_LOGO', ' Aktuelle Logo');
define('TXT_VIEW_TITLE', ' Softwaretitel');
define('TXT_VIEW_SUPPORT', ' Unterstützung Mailadresse');
define('TXT_VIEW_ALTER_LOGO', ' Logo ändern');
define('TXT_VIEW_SAVE_CHANGE', ' Änderungen speichern');
define('TXT_CUSTOM_APPLI', "Software Anpassung");
define('TXT_CUSTOM_FRONT', "Anzeigen");
define('TXT_CUSTOM_LOGO', " Logo obligatorisch");

//--------------------To display in notification-------------------------------
define('TXT_COLOPHON_TITLE', 'Apropos');
define('TXT_COLOPHON_P1', ' Manager ist eine Webanwendung, die von den Teams der Nationalen Veterinärschule von Alfort(');
define('TXT_COLOPHON_P2', ', Maisons-Alfort) und der Nationalen Veterinärschule für Lebensmittel und Ernährung  entwickelt wurde');
define('TXT_COLOPHON_P3', 'Diese Anwendung ist für veterinärmedizinische Simulationsräume gedacht, die aus einer Vielzahl von Arbeitsplätzen bestehen, an denen Studenten in der Erstausbildung die Erfassung technischer Gesten und Verfahren im Open-Access-Modus üben.');
define('TXT_COLOPHON_P4', 'Es ermöglicht den Studenten, sich selbst auszuwerten und exportierbare Statistiken über die durchgeführten oder noch zu erledigenden Übungen zu erstellen.');
define('TXT_COLOPHON_P5', 'Der Manager kann über eine Administrationsoberfläche auch die Liste der Übungen verwalten (Erstellung / Änderung / Löschung), den Fortschritt der Studenten überwachen und Statistiken über die Student Aktivität erstellen .');
define('TXT_COLOPHON_P6', 'Es ist auch über WebServices mit Moodle-Instanzen verbunden, so dass Sie einen Link zu einem Kursbereich erstellen können, in dem zusätzliche Dokumente (Videos, Aufzeichnungen usw.) gespeichert werden und in denen der Lernende seine eigenen Dokumente speichern kann Schulung');
define('TXT_COLOPHON_TITLE_2', 'Colophon');
define('TXT_COLOPHON_DESIGNER', 'Designer  :');
define('TXT_COLOPHON_DEV', 'Entwickler  :');
define('TXT_COLOPHON_TRANSLATORS', 'Übersetzer: :');
define('TXT_COLOPHON_LIB', 'Libraries :');
define('TXT_COLOPHON_LIB_TITLE', ' Manager nutzt diese verschiedenen Open Source-Libraries:');
define('TXT_COLOPHON_DISTRIBUTION', ' Manager ist unter der GPLv3-Lizenz vertrieben.');

//--------------------To display in notification-------------------------------
define('TXT_NOTIFICATION_NO_RIGHT', 'Sie besitzen nicht die Rechte.');
define('TXT_NOTIFICATION_IMPORT_SUCCESS', 'Import abgeschlossen');
define('TXT_NOTIFICATION_CHECK_VALUES', 'Kontrollieren Sie die Werte');
define('TXT_NOTIFICATION_ADD_USER', 'Nutzer hinzugefügt');
define('TXT_NOTIFICATION_UNKNOWN_ACTION', 'Unbekannte Action');
define('TXT_NOTIFICATION_ERROR', 'Ein Fehler is aufgetreten : ');
define('TXT_NOTIFICATION_ALTER_CATEGORY', 'Kategorie verändert');
define('TXT_NOTIFICATION_DEL_CATEGORY', 'Kategorie gelöscht');
define('TXT_NOTIFICATION_DEL_DISCIPLINE', 'Disziplin gelöscht');
define('TXT_NOTIFICATION_ALTER_DISCIPLINE', 'Disziplin verändert');
define('TXT_NOTIFICATION_ADD_PRIORITY_LEVEL', 'Prioritätsrang hinzugefügt');
define('TXT_NOTIFICATION_ALTER_PRIORITY_LEVEL', 'Prioritätsrang verändert');
define('TXT_NOTIFICATION_DEL_PRIORITY_LEVEL', 'Prioritätsrang gelöscht');
define('TXT_NOTIFICATION_ADD_PROFILE', 'Profil hinzugefügt');
define('TXT_NOTIFICATION_ALTER_PROFILE', 'Profil verändert');
define('TXT_NOTIFICATION_DEL_PROFILE', 'Profil gelöscht');
define('TXT_NOTIFICATION_ADD_ROOM', 'Raum hinzugefügt');
define('TXT_NOTIFICATION_DEL_ROOM', 'Raum gelöscht');
define('TXT_NOTIFICATION_ALTER_ROOM', 'Raum verändert');
define('TXT_NOTIFICATION_ALTER_ORDER', 'Ordnung verändert.');
define('TXT_NOTIFICATION_ALTER_SCALE', 'Bewertungsskala Element verändert');
define('TXT_NOTIFICATION_DEL_SCALE', 'Bewertungsskala Element gelöscht ');
define('TXT_NOTIFICATION_AVAILABLE_SCALE', 'Bewertungsskala Element verfügbar');
define('TXT_NOTIFICATION_ADD_SCALE', 'Bewertungsskala Element hinzugefügt');
define('TXT_NOTIFICATION_UNGROUP_SCALE', 'Bewertungsskala Gruppe auflösen.');
define('TXT_NOTIFICATION_ADD_GROUP', 'Gruppe hinzugefügt');
define('TXT_NOTIFICATION_DEL_GROUP', 'Gruppe gelöscht');
define('TXT_NOTIFICATION_NOT_IMPLEMENTED', 'Nicht implementiert');
define('TXT_NOTIFICATION_ADD_SPECIE', 'Art hinzugefügt');
define('TXT_NOTIFICATION_ALTER_SPECIE', 'Art verändert');
define('TXT_NOTIFICATION_DEL_SPECIE', 'Art gelöscht');
define('TXT_NOTIFICATION_ADD_SUB_CATEGORY', 'Unterkategorie hinzugefügt');
define('TXT_NOTIFICATION_ALTER_SUB_CATEGORY', 'Unterkategorie verändert');
define('TXT_NOTIFICATION_DEL_SUB_CATEGORY', 'Unterkategoriee gelöscht');
define('TXT_NOTIFICATION_ALTER_SAVED', 'Veränderung gespeichert');

//--------------------To display in log-------------------------------
define('TXT_LOG_ERROR_USER', 'Fehler. Unbekannter Nutzer {user} oder Übung {poste}.');
define('TXT_LOG_ERROR_CONTACT', " Kontactieren Sie einen Manager.");
define('TXT_LOG_ERROR_UNKNOWN', ' unbekannt.');
define('TXT_LOG_REQUEST_WORKSHOP', 'Der Nutzer {user} hat die Übung {workshop} gefragt');
define('TXT_LOG_ERROR_START', 'Fehler : die übung wird nicht gestartet.');
define('TXT_LOG_ERROR_VALIDATION', 'Der Nutzer {user} hat die Übung {workshop} nicht validiert');
define('TXT_LOG_ERROR_VALIDATION_OPERATION', 'Sie haben diese Übung nicht validiert.');
define('TXT_LOG_ERROR_USER_VALIDATION', 'Der Benutzer {user} hat einen Moodlelink Fehler für die Übung {workshop}');
define('TXT_LOG_ERROR_LINK', '[ERR02] Moodlelink Fehler für diese Übung :');
define('TXT_LOG_BAD_TOKEN', 'Schlechtes CRSF Token oder unbekannter Benutzer. Meldung : {message}');
define('TXT_LOG_START_VALIDATION', 'Validierung Anfang...');
define('TXT_LOG_ERROR', 'Ein Fehler is aufgetreten an der Zeile {line}. Meldung : {message}');
define('TXT_LOG_VALIDATION', 'Übung {workshop} validiert für Nutzer/in {user}');
define('TXT_LOG_UPDATE_COMMENT', 'Kommentar verändert... [{comment}]');
define('TXT_LOG_ERROR_UPDATE_COMMENT', 'Fehler : der Kommentar wird nicht verändert. Meldung : {message}');
define('TXT_LOG_WORKSHOP_COMPLETE', 'Diese Übung ist validiert.');
define('TXT_LOG_WORKSHOP_COMPLETE_ANSWER', "Diese Übung ist ");
define('TXT_LOG_WORKSHOP_EXECUTION', 'Laufende Haken...');
define('TXT_LOG_WORKSHOP_EXECUTED', 'Haken ausgeführt.');
define('TXT_LOG_WORKSHOP_COMPLETE_DURATION', 'Dauer ');
define('TXT_LOG_WORKSHOP_ERROR_COMMUNICATION', 'Link Fehler zwischen CSL Manager und Moodle. Meldung : {message}');
define('TXT_LOG_WORKSHOP_OCCURRED', 'Ein Fehler is aufgetreten.');
define('TXT_LOG_WORKSHOP_END', 'Ausgefürhung fertig...');
define('TXT_LOG_WORKSHOP_ATTEMPT_IN_PROGRESS', "Sie haben diese Übung nicht validiert");
define('TXT_LOG_ERROR_CONNECTION', "Verbindungfehler : ");
define('TXT_LOG_ERROR_ADD_ON', "Curl Erweiterung nicht verfügbar");
define('TXT_LOG_ERROR_MOODLE_NOT_FOUND', "EXCEPTION_MOODLE_NO_USER_FOUND");
define('TXT_LOG_SUCCESS_MOODLE_LINK', " Einträge. Fertig in ");
define('TXT_LOG_ERROR_COURSE', "Fehler mit Moodle Aufgabe n° ");
define('TXT_LOG_LOGIN_CONNECTION', "Nutzer/in {user} Verbindungsversuch");
define('TXT_LOG_LOGIN_INVALID_TOKEN', "Invalid Token");
define('TXT_LOG_LOGIN_INVALID_LDAP', "Invalid LDAP Verbindungsdaten");
define('TXT_LOG_LOGIN_UNKNOWN_USER', "Unbekannter Nutzer");
define('TXT_LOG_LOGIN_ERROR_CREATE', "Unmögliche Nutzer Generiegung");
define('TXT_LOG_LOGIN_ERROR_INPUT', "Invalid Login/Passwort Kombination");
define('TXT_LOG_LOGIN_ERROR_CONNECTION', "Verbindungsfehler. Meldung : {message}");
define('TXT_LOG_LOGIN_SUCCESS_CONNECTION', "Erfolgreiche Verbindung für Nutzer/in {user}");
define('TXT_LOG_LOGIN_UPDATE_USER', "Nutzer/in {user} verändert");
define('TXT_LOG_LOGIN_CHANGE_PASSWORD', "Passwort vergessen?");
define('TXT_LOG_LOGIN_CHANGE_MAIL_SEND', "Es wurde eine E-Mail an Sie gesendet, um Ihr Passwort in diese Adresse zu ändern ");
define('TXT_LOG_LOGIN_CONTACT_ADMIN',"Die ID stimmt mit keinem manuellen Konto überein. Wenden Sie sich an den Administrator.");
define('TXT_LOG_LOGIN_INPUT_EMPTY',"Bitte füllen Sie aus");
define('TXT_LOG_LOGIN_TOKEN_EXPIRED',"Passwortänderung abgelaufen. Bitte wiederholen Sie diese Anfrage.");

//--------------------To display after validation-------------------------------
define('TXT_VALIDATION_SUCCESS', "Moodle Aufgabe verändert");

//--------------------To display information image-------------------------------
define('TXT_IMAGE_INVALID', "Invalid Bild Format.");
define('TXT_IMAGE_EXIST', "Fehler! Es bereits ein zugehöriges Bilddatei gibt.");
define('TXT_IMAGE_SIZE', "Die Datei zu groß ist.");
define('TXT_IMAGE_FORMAT', "Bilder müssen in JPG, JPEG, PNG sein.");
define('TXT_IMAGE_ERROR', "Fehler! Das Bild wird nicht hinzugefügt.");

//--------------------To display information file-------------------------------
define('TXT_FILE_INVALID', "Invalid Datei.");
define('TXT_FILE_EXIST', "Fehler! Die Datei bereits vorhanden ist.");

//--------------------Installation content-------------------------------
define('TXT_INSTALLATION_TITLE', "Software Installation.");
define('TXT_INSTALLATION', "Installation");
define('TXT_INSTALLATION_HASHTAG_1', "#Pädagogik");
define('TXT_INSTALLATION_HASHTAG2', "#Clinical skills lab");
define('TXT_INSTALLATION_HASHTAG3', "#Tierarzt");
define('TXT_INSTALLATION_BDD', "Datenbank");
define('TXT_INSTALLATION_FEEDBACK', "Kommentar");
define('TXT_INSTALLATION_MOODLE', "Moodle");
define('TXT_INSTALLATION_ADMIN', "Administrator");
define('TXT_INSTALLATION_ADMIN_NAME', "Administratorname :");
define('TXT_INSTALLATION_CUSTOM', "Anpassung");
define('TXT_INSTALLATION_AUTH', "Authentifizierung");
define('TXT_INSTALLATION_MODAL_TITLE', "Weitere Informationen");
define('TXT_INSTALLATION_SERVER_NAME', "MYSQL Servername:");
define('TXT_INSTALLATION_SERVER_NAME_EXAMPLE', "ex: localhost");
define('TXT_INSTALLATION_LOGIN_BDD', "Login :");
define('TXT_INSTALLATION_LOGIN_BDD_EXAMPLE', "ex: root");
define('TXT_INSTALLATION_PASSWORD', "Passwort :");
define('TXT_INSTALLATION_PASSWORD_WRONG', "Passwörter müssen gleich sein");
define('TXT_INSTALLATION_BDD_NAME', "Datenbankname :");
define('TXT_INSTALLATION_BDD_NAME_EXAMPLE', "ex: cslmanager");
define('TXT_INSTALLATION_SMTP_HOST', "SMTP Host:");
define('TXT_INSTALLATION_SMTP_HOST_EXAMPLE', "ex: smtps.exemple.fr");
define('TXT_INSTALLATION_SMTP_SECURE', "Siecherheitsschlüssel :");
define('TXT_INSTALLATION_OPTIONS', "Optionen");
define('TXT_INSTALLATION_SELECT_OPTION', "Wählen Sie eine Option");
define('TXT_INSTALLATION_SMTP_SSL', "ssl");
define('TXT_INSTALLATION_SMTP_TLS', "tls");
define('TXT_INSTALLATION_SMTP_PORT', "Datenport :");
define('TXT_INSTALLATION_SMTP_HOST_EMAIL', "Host Mailadresse :");
define('TXT_INSTALLATION_SMTP_HOST_EMAIL_EXAMPLE', "ex: example@outlook.fr");
define('TXT_INSTALLATION_MOODLE_TOKEN', "Token :");
define('TXT_INSTALLATION_MOODLE_URL', "Moodle URL :");
define('TXT_INSTALLATION_MOODLE_URL_EXAMPLE', "ex: http://moodle.fr");
define('TXT_INSTALLATION_MOODLE_COURSE', "Moodle Kursnummer :");
define('TXT_INSTALLATION_TECHNIQUE_SUPPORT', "Technische Unterstützung Mailadresse:");
define('TXT_INSTALLATION_LOGO', "Logo");
define('TXT_INSTALLATION_CHOOSE_FILE', "Wählen Sie die Datei");
define('TXT_INSTALLATION_AUTH_URI', "Uri :");
define('TXT_INSTALLATION_AUTH_URI_EXAMPLE', "ex :srv-ldap-01.example.fr");
define('TXT_INSTALLATION_AUTH_BASEDN', "BaseDn :");
define('TXT_INSTALLATION_AUTH_BINDDN', "BindDn :");
define('TXT_INSTALLATION_AUTH_BINDPW', "BindPw :");
define('TXT_INSTALLATION_AUTH_BINDDN_EXAMPLE', "ex: uid=vetsims,ou=people,dc=example,dc=fr");
define('TXT_INSTALLATION_AUTH_PORT', "Datenport :");
define('TXT_INSTALLATION_LAUNCH_INSTALL', "Installation starten");
define('TXT_INSTALLATION_APPLICATION_TITLE', "Softwaretitel");
define('TXT_INSTALLATION_CONGRADULATION', "Glückwünsche");
define('TXT_INSTALLATION_BDD_SUCCESS', "Die Datenbank wurd erfolgreich erstellt");
define('TXT_INSTALLATION_START', "Wir gehen hin  !");
define('TXT_INSTALLATION_WARNING', "Achtung, wenn nicht alle Informationen angegeben werden oder ungenau sind, einige Funktionen können beeinträchtigt werden");
define('TXT_INSTALLATION_MODAL_BDD_1', "Sie müssen die Datenbank zuvor in MYSQL erstellen");
define('TXT_INSTALLATION_MODAL_BDD_2', "Die Informationen sind obligatorisch");
define('TXT_INSTALLATION_MODAL_FEEDBACK_1', "Die Informationen werden verwendet, damit Ihre Benutzer Ihnen Feedback zur Anwendung geben können");
define('TXT_INSTALLATION_MODAL_FEEDBACK_2', "Weitere Informationen hierzu finden ");
define('TXT_INSTALLATION_MODAL_MOODLE_1', "Die Informationen sind mit Moodle verwendet. ");
define('TXT_INSTALLATION_MODAL_MOODLE_2', "Sie müssen zuerst einen Kurs für Moodle erstellen, dem eine Nummer (ID) hinzugefügt wird.");
define('TXT_INSTALLATION_MODAL_MOODLE_3', "Anschließend müssen Sie ein Token erstellen, damit die Anwendung auf die dedizierte Zone zugreifen kann (zusätzliche Informationen finden Sie ");
define('TXT_INSTALLATION_MODAL_AUTH_1', "Ldap/AD Parameter werden für die Benutzerauthentifizierung auf der Moodleplatform verwendet.");
define('TXT_INSTALLATION_MODAL_AUTH_2', "Ldap/AD-Informationen sind obligatorisch. Informationen zum Ausfüllen dieser Felder finden Sie hier.");
define('TXT_INSTALLATION_MODAL_ADMIN_1', "Die hier bereitgestellten Informationen werden zum Erstellen des Anwendungsadministrators verwendet");
define('TXT_INSTALLATION_MODAL_ADMIN_2', "Die Informationen beziehen sich auf das während der Authentifizierung verwendete Verzeichnis. ");
define('TXT_INSTALLATION_HERE', "hier");
define('TXT_INSTALLATION_ERROR_PASSWORD_SIZE', "Das Passwort muss mindestens 6 Zeichen lang sei");
define('TXT_INSTALLATION_ERROR_PASSWORD_CHECK', "Passwörter müssen gleich sein");

//--------------------Mail content-------------------------------
define('TXT_MAIL_INIT_TITLE', "Ein Konto wurde gerade erstellt");
define('TXT_MAIL_INIT_CONTENT1', "Um Ihr Passwort zu ändern, klicken Sie auf diesen ");
define('TXT_MAIL_CHANGE_TITLE', "Sie haben gerade eine Passwortänderung angefordert");
define('TXT_MAIL_INIT_CONTENT', "Es ist nur 15 Minuten gültig.");



