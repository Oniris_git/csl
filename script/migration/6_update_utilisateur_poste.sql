-- -----------------------------------------------------
-- UPDATE `cslmanager`.`poste_travail`
-- -----------------------------------------------------
ALTER TABLE poste_travail
ADD id_category INT(11) NULL DEFAULT NULL,
ADD id_sub_category INT(11) NULL DEFAULT NULL,
ADD id_discipline   INT(11) NULL DEFAULT NULL,
ADD id_rank         INT(11) NULL DEFAULT NULL,
ADD id_species      INT(11) NULL DEFAULT NULL,
ADD CONSTRAINT `poste_travail_category_id_fk`
	FOREIGN KEY (`id_category`)
	REFERENCES `cslmanager`.`category` (`id`)
		ON DELETE SET NULL
		ON UPDATE CASCADE,
ADD CONSTRAINT `poste_travail_sub_category_id_fk`
	FOREIGN KEY (`id_sub_category`)
	REFERENCES `cslmanager`.`sub_category` (`id`)
		ON DELETE SET NULL
		ON UPDATE CASCADE,
ADD CONSTRAINT `poste_travail_discipline_id_fk`
  FOREIGN KEY (`id_discipline`)
  REFERENCES `cslmanager`.`discipline` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
ADD CONSTRAINT `poste_travail_species_id_fk`
  FOREIGN KEY (`id_species`)
  REFERENCES `cslmanager`.`species` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
ADD CONSTRAINT `poste_travail_rank_id_fk`
  FOREIGN KEY (`id_rank`)
  REFERENCES `cslmanager`.`rank` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE ;

-- -----------------------------------------------------
-- UPDATE `cslmanager`.`utilisateur`
-- -----------------------------------------------------

ALTER TABLE utilisateur
ADD email VARCHAR(255) NOT NULL UNIQUE AFTER prenom;
