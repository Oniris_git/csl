INSERT INTO `rights`(`label`, `description`) VALUES
('edit:auth','Modifier l authorisation'),
('print:auth','Afficher l authorisation'),
('edit:root','Modifier le rooting Moodle'),
('print:root','Afficher le rooting Moodle'),
('edit:mailer','Modifier le mailer'),
('print:mailer','Afficher le mailer'),
('edit:view','Modifier les paramètres de la vue'),
('print:view','Afficher les paramètres de la vue'),
('edit:lang','Modifier la langue'),
('print:lang','Afficher les langues'),
('edit:legal','Modifier les mentions légales'),
('print:legal','Afficher les mentions légales');