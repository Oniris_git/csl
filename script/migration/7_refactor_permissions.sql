/** Table de droits */
CREATE TABLE droits
(
  id int primary key not null auto_increment,
  libelle varchar(100) not null unique,
  description varchar(100)
);

INSERT INTO droits (libelle, description)
SELECT DISTINCT droit, description
  FROM permissions
  ORDER BY droit;

/** Table permissions */
ALTER TABLE permissions
  DROP COLUMN description,
  ADD COLUMN id_profil int,
  ADD COLUMN id_droit int;

UPDATE permissions p, droits d, profils pr
SET p.id_profil = pr.id, p.id_droit = d.id
WHERE p.profil = pr.libelle AND p.droit = d.libelle;

ALTER TABLE permissions
  DROP COLUMN id,
  DROP COLUMN profil,
  DROP COLUMN droit;

/** Table utilisateur */
ALTER TABLE utilisateur
ADD COLUMN id_profil int;

UPDATE utilisateur u, profils p
SET u.id_profil = p.id
WHERE u.code_profil = p.libelle;

ALTER TABLE utilisateur
DROP COLUMN code_profil;

/** Table profils */
ALTER TABLE profils
  DROP COLUMN libelle,
  CHANGE libelle_long libelle varchar(100);
