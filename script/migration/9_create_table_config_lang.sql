CREATE TABLE config
(
  id INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(10) NOT NULL,
  `name` VARCHAR(20) NOT NULL,
  `value` text NOT NULL
);

INSERT INTO config (type, name, value) VALUES
('auth', 'type', 'ldap'),
('smtp', 'host', 'smtps.oniris-nantes.fr'),
('smtp', 'secure', 'ssl'),
('smtp', 'port', '465'),
('smtp', 'user_name', 'remi.andre@oniris-nantes.fr'),
('smtp', 'password', 'fpq6i742'),
('auth', 'type', 'ldap'),
('auth_ldap', 'uri', 'srv-ldap-01.oniris-nantes.fr'),
('auth_ldap', 'basedn', 'ou=people,dc=oniris-nantes,dc=fr'),
('auth_ldap', 'binddn', 'uid=vetsims,ou=people,dc=oniris-nantes,dc=fr'),
('auth_ldap', 'bindpw', 'v3ts1ms'),
('auth_ldap', 'filter', 'supannaliaslogin=%s'),
('auth_ldap', 'port', '389'),
('auth_ldap', 'attribute1', 'cn'),
('auth_ldap', 'attribute2', 'sn'),
('auth_ldap', 'attribute3', 'givenName'),
('auth_ldap', 'attribute', 'mail'),
('auth_ldap', 'attribute', 'description'),
('auth_ldap', 'attribute', 'supannaliaslogin'),
('auth_ldap', 'filterImport', '(mail=%s)'),
('view', 'title', 'CSL Manager'),
('view', 'footer', '&copy; Env Alfort / Oniris Nantes'),
('view', 'logo', 'dist/images/Logo Virtual Vet.jpg'),
('view', 'technique', 'remi.andre@oniris-nantes.fr'),
('view', 'lang', 'fr'),
('moodle', 'token', '507f679b2adb420b5df9d573a5a81d55'),
('moodle', 'domain', 'https://connect.oniris-nantes.fr/'),
('moodle', 'course', '304'),
('view', 'domain', 'http://cslmanager'),
('view', 'legal', ''),
('auth_ldap', 'escapeChars', '\\&!|=<>,+"\;()');

CREATE TABLE IF NOT EXISTS `cslmanager`.`lang` (
  `id`               INT(11)      NOT NULL AUTO_INCREMENT,
  `title`            VARCHAR(50)  NOT NULL,
  `path_img`      VARCHAR(100) NULL     DEFAULT NULL,
  `path_file`      VARCHAR(100) NULL     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

INSERT INTO lang (title, path_img, path_file) values
('en', 'dist/images/United-Kingdom.png', '../../../../lang/lang-en.inc.php'),
('fr', 'dist/images/France.png', '../../../../lang/lang-fr.inc.php'),
('de', 'dist/images/germany.png', '../../../../lang/lang-de.inc.php');