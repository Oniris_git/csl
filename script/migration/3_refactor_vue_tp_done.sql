CREATE OR REPLACE VIEW `V_STAT_TP_DONE` AS
  SELECT
    evaluation.libelle       AS note,
    utilisateur.upn          AS upn,
    tp.debut                 AS debut,
    poste_travail.libelle    AS libelle,
    commentaires.commentaire AS commentaire,
    tp.id_utilisateur        AS idUser,
    poste_travail.id         AS idposte_travail,
    tp.id                    AS idTp,
    evaluation.groupe        AS scale 
  FROM tp
    LEFT JOIN evaluation ON evaluation.code_evaluation = tp.evaluation
    LEFT JOIN utilisateur ON utilisateur.id = tp.id_utilisateur
    LEFT JOIN poste_travail ON poste_travail.id = tp.id_poste_travail
    LEFT JOIN commentaires
      ON commentaires.id_poste_travail = poste_travail.id AND commentaires.id_utilisateur = utilisateur.id
  WHERE tp.fin IS NOT NULL
    AND poste_travail.deleted != 1
  ORDER BY evaluation.ordre DESC, poste_travail.id ;