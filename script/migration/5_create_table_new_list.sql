-- -----------------------------------------------------
-- Table `cslmanager`.`species`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cslmanager`.`species` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`title` VARCHAR(50) NOT NULL UNIQUE,
`description` VARCHAR(255) NULL DEFAULT NULL,
PRIMARY KEY (`id`)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `cslmanager`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cslmanager`.`category` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`title` VARCHAR(50) NOT NULL UNIQUE,
`description` VARCHAR(255) NULL DEFAULT NULL,
PRIMARY KEY (`id`)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `cslmanager`.`sub_category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cslmanager`.`sub_category` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`title` VARCHAR(50) NOT NULL UNIQUE,
`description` VARCHAR(255) NULL DEFAULT NULL,
PRIMARY KEY (`id`)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `cslmanager`.`discipline`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cslmanager`.`discipline` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`title` VARCHAR(50) NOT NULL UNIQUE,
`description` VARCHAR(255) NULL DEFAULT NULL,
PRIMARY KEY (`id`)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `cslmanager`.`rank`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cslmanager`.`rank` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`title` VARCHAR(50) NOT NULL UNIQUE,
`description` VARCHAR(255) NULL DEFAULT NULL,
PRIMARY KEY (`id`)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;






