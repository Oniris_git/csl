CREATE OR REPLACE VIEW `V_STAT_TP_PER_MONTH` AS
  SELECT
    `u`.`id`                                      AS `uid`,
    `u`.`upn`                                     AS `upn`,
    `u`.`description`                             AS `promo`,
    count(`tp`.`id`)              AS `count`,
    date_format(`tp`.`fin`, '%M') AS `month`,
    month(`tp`.`fin`)             AS `nmonth`,
    year(`tp`.`fin`)              AS `year`
  FROM (`tp`
    INNER JOIN `utilisateur` `u`)
  WHERE ((`tp`.`fin` IS NOT NULL) AND (`tp`.`id_utilisateur` = `u`.`id`))
  GROUP BY `u`.`upn`, `u`.`id`, date_format(`tp`.`fin`, '%M'), year(`tp`.`fin`)
  ORDER BY year, month(`tp`.`fin`);