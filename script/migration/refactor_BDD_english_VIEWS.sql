
CREATE OR REPLACE VIEW `V_WORKSHOP_ATTEMPT_DONE` AS
  SELECT
    scale.label       AS scale,
    user.upn          AS upn,
    workshop_attempt.debut                 AS start,
    workshop.full_label    AS label,
    workshop_comment.content AS comment,
    workshop_attempt.id_user        AS idUser,
    workshop.id         AS idWorkshop,
    workshop_attempt.id                    AS idWorkshopAttempt,
    scale.id_scale_type        AS scale_type
  FROM workshop_attempt
    LEFT JOIN scale ON scale.scale_item_code = workshop_attempt.scale
    LEFT JOIN user ON user.id = workshop_attempt.id_user
    LEFT JOIN workshop ON workshop.id = workshop_attempt.id_workshop
    LEFT JOIN workshop_comment
      ON workshop_comment.id_workshop = workshop.id AND workshop_comment.id_user = user.id
  WHERE workshop_attempt.fin IS NOT NULL
        AND workshop.deleted != 1
  ORDER BY scale.order_scale DESC, workshop.id ;



CREATE OR REPLACE VIEW `V_WORKSHOP_ATTEMPT_PER_MONTH` AS
  SELECT
    `u`.`id`                                      AS `uid`,
    `u`.`upn`                                     AS `upn`,
    `u`.`description`                             AS `promo`,
    count(`workshop_attempt`.`id`)              AS `count`,
    date_format(`workshop_attempt`.`fin`, '%M') AS `month`,
    month(`workshop_attempt`.`fin`)             AS `nmonth`,
    year(`workshop_attempt`.`fin`)              AS `year`
  FROM (`workshop_attempt`
    INNER JOIN `user` `u`)
  WHERE ((`workshop_attempt`.`fin` IS NOT NULL) AND (`workshop_attempt`.`id_user` = `u`.`id`))
  GROUP BY `u`.`upn`, `u`.`id`, date_format(`workshop_attempt`.`fin`, '%M'), year(`workshop_attempt`.`fin`)
  ORDER BY year, month(`workshop_attempt`.`fin`);



CREATE OR REPLACE VIEW `V_WORKSHOP_ATTEMPT` AS
  SELECT
    date_format(`workshop_attempt`.`debut`, '%d %b %Y')                                            AS `Date`,
    time_format(`workshop_attempt`.`debut`, '%H:%i')                                               AS `Debut`,
    `user`.`upn`                                     AS `upn`,
    ifnull(`user`.`name`,
           substring_index(substring_index(`user`.`upn`, '@', 1), '.', -(1))) AS `name`,
    ifnull(`user`.`first_name`,
           substring_index(substring_index(`user`.`upn`, '@', 1), '.', 1))    AS `FirstName`,
    `user`.`description`                                                      AS `Promo`,
    `workshop`.`label`                                                  AS `Workshop`,
    `workshop`.`full_label`                                                        AS `WorkshopFull`,
    `room`.`description`                                                            AS `Room`,
    time_format(timediff(`workshop_attempt`.`fin`, `workshop_attempt`.`debut`), '%Hh, %im, %ss') AS `Duration`,
    `scale`.`label`                                                           AS `Scale`

  FROM (`room`
    JOIN (((`workshop_attempt`
      JOIN `user`
        ON ((`workshop_attempt`.`id_user` = `user`.`id`))) JOIN
      `workshop`
        ON ((`workshop`.`id` = `workshop_attempt`.`id_workshop`))) JOIN
      `scale`
        ON ((`scale`.`scale_item_code` = `workshop_attempt`.`scale`))))
  WHERE ((`room`.`id` = `workshop`.`id_room`) AND
         (`workshop_attempt`.`fin` IS NOT NULL))
  ORDER BY `workshop_attempt`.`debut`;



CREATE OR REPLACE VIEW `V_WORKSHOP_ATTEMPT_VALIDATED` AS
  SELECT
    `u`.`id`                         AS `uid`,
    `u`.`upn`                        AS `upn`,
    `ws`.`id`                        AS `pid`,
    `ws`.`full_label`                   AS `label`,
    `ws`.`count_to_validate`         AS `count_to_validate`,
    count(`workshop_attempt`.`id`) AS `count`
  FROM ((`user` `u`
    JOIN `workshop` `ws`) JOIN `workshop_attempt`)
  WHERE
    ((`workshop_attempt`.`id_workshop` = `ws`.`id`) AND (`workshop_attempt`.`id_user` = `u`.`id`))
  GROUP BY `u`.`id`, `ws`.`id`
  HAVING (`count` > `ws`.`count_to_validate`)
  ORDER BY `u`.`upn`, `ws`.`label`;
