CREATE TABLE commentaires
(
  id_utilisateur INT NOT NULL,
  id_poste_travail INT NOT NULL,
  commentaire VARCHAR(200) NOT NULL,
  CONSTRAINT commentaires_id_utilisateur_id_poste_travail_pk PRIMARY KEY (id_utilisateur, id_poste_travail),
  CONSTRAINT commentaires_utilisateur_id_fk FOREIGN KEY (id_utilisateur) REFERENCES utilisateur (id),
  CONSTRAINT commentaires_poste_travail_id_fk FOREIGN KEY (id_poste_travail) REFERENCES poste_travail (id)
);

INSERT INTO commentaires
(id_utilisateur, id_poste_travail, commentaire)
  SELECT tp.id_utilisateur, tp.id_poste_travail, tp.comment from tp
  WHERE tp.comment <> '' and tp.comment is not NULL
  ORDER BY tp.id
ON DUPLICATE KEY UPDATE commentaire = tp.comment;

ALTER TABLE tp DROP comment;

CREATE OR REPLACE VIEW `V_TP` AS
  SELECT
    date_format(`tp`.`debut`, '%Y')                                                  AS `Année`,
    date_format(`tp`.`debut`, '%b')                                                  AS `Mois`,
    date_format(`tp`.`debut`, '%d')                                                  AS `Jour`,
    time_format(`tp`.`debut`, '%H:%i')                                               AS `Debut`,
    ifnull(`utilisateur`.`nom`,
           substring_index(substring_index(`utilisateur`.`upn`, '@', 1), '.', -(1))) AS `Nom`,
    ifnull(`utilisateur`.`prenom`,
           substring_index(substring_index(`utilisateur`.`upn`, '@', 1), '.', 1))    AS `Prenom`,
    `utilisateur`.`description`                                                      AS `Promo`,
    `poste_travail`.`libelle_court`                                                  AS `Poste`,
    `poste_travail`.`libelle`                                                        AS `PosteL`,
    `salle`.`description`                                                            AS `Salle`,
    time_to_sec(timediff(`tp`.`fin`, `tp`.`debut`))                  AS `Duree`,
    `evaluation`.`libelle`                                                           AS `Evaluation`
  FROM (`salle`
    JOIN (((`tp`
      JOIN `utilisateur`
        ON ((`tp`.`id_utilisateur` = `utilisateur`.`id`))) JOIN
      `poste_travail`
        ON ((`poste_travail`.`id` = `tp`.`id_poste_travail`))) JOIN
      `evaluation`
        ON ((`evaluation`.`code_evaluation` = `tp`.`evaluation`))))
  WHERE ((`salle`.`id` = `poste_travail`.`id_salle`) AND
         (`tp`.`fin` IS NOT NULL))
  ORDER BY `tp`.`debut`