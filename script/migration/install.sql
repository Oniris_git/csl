DROP DATABASE IF EXISTS cslmanager;
CREATE DATABASE cslmanager;
USE cslmanager;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET NAMES 'utf8';
SET time_zone = "+00:00";

--
-- Base de données :  `cslmanager`
--

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `config`
--

DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `difficulty`
--

DROP TABLE IF EXISTS `difficulty`;
CREATE TABLE IF NOT EXISTS `difficulty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `difficulty_level` int(11) NOT NULL COMMENT 'Niveau de difficultÃ©',
  `title` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `promo_id_uindex` (`id`),
  UNIQUE KEY `promo_pid_uindex` (`difficulty_level`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Contenu de la table `difficulty`
--

INSERT INTO `difficulty` (`id`, `difficulty_level`, `title`, `description`) VALUES
(1, 1, 'Facile', 'Difficulté de 1ère année'),
(2, 2, 'Moyen', 'Difficulté de 2ème année'),
(3, 3, 'Difficile', 'Difficulté de 3ème année'),
(4, 4, 'Très difficile', '');


-- --------------------------------------------------------

--
-- Structure de la table `discipline`
--

DROP TABLE IF EXISTS `discipline`;
CREATE TABLE IF NOT EXISTS `discipline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `moodle_mod`
--

DROP TABLE IF EXISTS `moodle_mod`;
CREATE TABLE IF NOT EXISTS `moodle_mod` (
  `module_id` int(11) NOT NULL,
  `devoir_id` int(11) NOT NULL,
  UNIQUE KEY `mod_moodle_module_id_uindex` (`module_id`),
  UNIQUE KEY `mod_moodle_devoir_id_uindex` (`devoir_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lien entre id interne du module et id affiche';

-- --------------------------------------------------------

--
-- Structure de la table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id_profile` int(11) DEFAULT NULL,
  `id_right` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `permissions`
--

INSERT INTO `permissions` (`id_profile`, `id_right`) VALUES
(1, 11),
(1, 21),
(1, 5),
(1, 25),
(1, 15),
(1, 14),
(1, 2),
(2, 2),
(2, 26),
(1, 24),
(2, 24),
(3, 24),
(1, 29),
(2, 29),
(1, 23),
(2, 23),
(1, 4),
(1, 20),
(2, 20),
(1, 28),
(2, 28),
(1, 27),
(2, 27),
(1, 10),
(1, 17),
(1, 7),
(1, 1),
(1, 12),
(1, 9),
(1, 19),
(1, 8),
(1, 18),
(1, 3),
(1, 13),
(1, 6),
(1, 16),
(4, 24),
(4, 24),
(4, 24),
(4, 24),
(5, 24),
(1, 35),
(1, 36),
(1, 37),
(2, 1),
(2, 6),
(2, 36),
(1, 39),
(2, 39),
(4, 24),
(1, 22),
(1, 26),
(4, 24),
(5, 24),
(6, 24),
(7, 24),
(8, 24),
(9, 24),
(10, 24),
(11, 24),
(12, 24),
(13, 24),
(14, 24),
(15, 24),
(16, 24),
(17, 24),
(18, 24),
(19, 24),
(20, 24),
(21, 24),
(4, 24),
(5, 24),
(4, 24),
(5, 24),
(6, 24),
(7, 24),
(4, 24),
(4, 24),
(4, 24),
(5, 24),
(2, 3),
(2, 4),
(2, 5),
(2, 7),
(2, 8),
(2, 9),
(2, 11),
(2, 12),
(2, 13),
(2, 16),
(2, 17),
(2, 18),
(2, 19),
(2, 25),
(2, 14),
(2, 22),
(4, 24),
(5, 24),
(6, 24),
(7, 24),
(8, 24),
(9, 24),
(10, 24),
(11, 24),
(12, 24),
(13, 24),
(14, 24),
(15, 24),
(16, 24),
(2, 40),
(1, 40),
(1, 38),
(4, 24),
(4, 24),
(4, 24),
(5, 24),
(6, 24),
(7, 24),
(8, 24),
(9, 24),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(1, 40),
(1, 41),
(1, 42);

-- --------------------------------------------------------

--
-- Structure de la table `priority_level`
--

DROP TABLE IF EXISTS `priority_level`;
CREATE TABLE IF NOT EXISTS `priority_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `profils`
--

DROP TABLE IF EXISTS `profils`;
CREATE TABLE IF NOT EXISTS `profils` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(100) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `profils`
--

INSERT INTO `profils` (`id`, `label`, `description`, `deleted`) VALUES
(1, 'Administrateur', 'Administrateur de la plateforme', 0),
(2, 'Gestionnaire', 'Gestion des travaux', 0),
(3, 'Etudiant', 'Etudiant', 0);

-- --------------------------------------------------------

--
-- Structure de la table `rights`
--

DROP TABLE IF EXISTS `rights`;
CREATE TABLE IF NOT EXISTS `rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `libelle` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `rights`
--

INSERT INTO `rights` (`id`, `label`, `description`) VALUES
(1, 'create:category', 'Créer une catégorie'),
(2, 'create:csv', 'Générer des CSV'),
(3, 'create:discipline', 'Créer une discipline'),
(4, 'create:profiles', 'Créer des profils'),
(5, 'create:qrcode', 'Générer les QRCodes'),
(6, 'create:rank', 'Créer une rang'),
(7, 'create:room', 'Créer des salles'),
(8, 'create:species', 'Créer une espèce'),
(9, 'create:subcategory', 'Créer une sous-catégorie'),
(10, 'create:users', 'Créer des utilisateurs'),
(11, 'create:workspace', 'Création de poste de travail'),
(12, 'edit:category', 'modifier une catégorie'),
(13, 'edit:discipline', 'modifier une discipline'),
(14, 'edit:evals', 'Modifier les notes'),
(15, 'edit:profiles', 'Modifier les profils'),
(16, 'edit:rank', 'modifier une rang'),
(17, 'edit:room', 'Modifie les salles'),
(18, 'edit:species', 'modifier une espèce'),
(19, 'edit:subcategory', 'modifier une sous-catégorie'),
(20, 'edit:tp', 'Modifier les TP effectués'),
(21, 'edit:users', 'Modification des paramètres de lutilisateur'),
(22, 'edit:workspace', 'Modifier les postes de travail'),
(23, 'print:others_stats', 'Afficher les statistiques des autres'),
(24, 'print:ownstats', 'Afficher ses statistiques'),
(25, 'print:qrcode', 'Imprimer les QRCodes'),
(26, 'print:stats', 'Afficher toutes les statistiques'),
(27, 'print:users', 'Affiche les utilisateurs'),
(28, 'print:workspace', 'affiche les postes de travail'),
(29, 'print:workspace_evals', 'Afficher les notes par poste'),
(31, 'edit:auth','Modifier l authorisation'),
(32, 'print:auth','Afficher l authorisation'),
(33, 'edit:root','Modifier le rooting Moodle'),
(34, 'print:root','Afficher le rooting Moodle'),
(35, 'edit:mailer','Modifier le mailer'),
(36, 'print:mailer','Afficher le mailer'),
(37, 'edit:view','Modifier les paramètres de la vue'),
(38, 'print:view','Afficher les paramètres de la vue'),
(39, 'edit:lang','Modifier la langue'),
(40, 'print:lang','Afficher les langues'),
(41, 'edit:legal','Modifier les mentions légales'),
(42, 'print:legal','Afficher les mentions légales');

-- --------------------------------------------------------

--
-- Structure de la table `room`
--

DROP TABLE IF EXISTS `room`;
CREATE TABLE IF NOT EXISTS `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `salle_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `scale`
--

DROP TABLE IF EXISTS `scale`;
CREATE TABLE IF NOT EXISTS `scale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scale_item_code` varchar(10) NOT NULL,
  `label` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `id_scale_type` int(11) DEFAULT NULL,
  `order_scale` int(11) NOT NULL,
  PRIMARY KEY (`id`,`scale_item_code`),
  UNIQUE KEY `scale_scale_item_code_uindex` (`scale_item_code`),
  KEY `scale_scale_type_id_fk` (`id_scale_type`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `scale_type`
--

DROP TABLE IF EXISTS `scale_type`;
CREATE TABLE IF NOT EXISTS `scale_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(100) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `evaluation_groupe_id_uindex` (`id`),
  UNIQUE KEY `evaluation_groupe_libelle_uindex` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `species`
--

DROP TABLE IF EXISTS `species`;
CREATE TABLE IF NOT EXISTS `species` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sub_category`
--

DROP TABLE IF EXISTS `sub_category`;
CREATE TABLE IF NOT EXISTS `sub_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upn` varchar(255) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `max_difficulty` int(11) NOT NULL DEFAULT '1',
  `id_profile` varchar(100) DEFAULT NULL,
  `auth` varchar(10) DEFAULT NULL,

  `password` varchar(255) DEFAULT NULL,
  `request` timestamp NULL DEFAULT NULL,
  `open` tinyint(1) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `utilisateur_upn_uindex` (`upn`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `workshop`
--

DROP TABLE IF EXISTS `workshop`;
CREATE TABLE IF NOT EXISTS `workshop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(10) DEFAULT NULL,
  `full_label` varchar(255) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `available` tinyint(1) DEFAULT NULL,
  `id_moodle` int(11) DEFAULT NULL,
  `id_room` int(11) DEFAULT NULL,
  `id_difficulty` int(11) DEFAULT '1',
  `duration` int(11) DEFAULT '0',
  `count_to_validate` int(11) NOT NULL DEFAULT '0',
  `id_scale_type` int(11) DEFAULT NULL,
  `id_category` int(11) DEFAULT NULL,
  `id_sub_category` int(11) DEFAULT NULL,
  `id_discipline` int(11) DEFAULT NULL,
  `id_priority_level` int(11) DEFAULT NULL,
  `id_species` int(11) DEFAULT NULL,
  `ceiling_duration` int(11) NOT NULL DEFAULT '120',
  PRIMARY KEY (`id`),
  UNIQUE KEY `libelle_court` (`label`),
  KEY `workshop_room_id_fk` (`id_room`),
  KEY `workshop_scale_type_id_fk` (`id_scale_type`),
  KEY `workshop_category_id_fk` (`id_category`),
  KEY `workshop_sub_category_id_fk` (`id_sub_category`),
  KEY `workshop_discipline_id_fk` (`id_discipline`),
  KEY `workshop_species_id_fk` (`id_species`),
  KEY `workshop_priority_level_id_fk` (`id_priority_level`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Structure de la table `workshop_comment`
--

DROP TABLE IF EXISTS `workshop_comment`;
CREATE TABLE IF NOT EXISTS `workshop_comment` (
  `id_user` int(11) NOT NULL,
  `id_workshop` int(11) NOT NULL,
  `content` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_user`,`id_workshop`),
  KEY `comments_workshop_id_fk` (`id_workshop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Structure de la table `workshop_attempt`
--

DROP TABLE IF EXISTS `workshop_attempt`;
CREATE TABLE IF NOT EXISTS `workshop_attempt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_workshop` int(11) DEFAULT NULL,
  `debut` datetime NOT NULL,
  `fin` datetime DEFAULT NULL,
  `scale` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ws_attempt_workshop_id_fk` (`id_workshop`),
  KEY `ws_attempt_user_id_fk` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Structure de la table `lang`
--

DROP TABLE IF EXISTS `lang`;
CREATE TABLE IF NOT EXISTS `lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `path_img` varchar(100) DEFAULT NULL,
  `path_file` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `lang`
--

INSERT INTO `lang` (`id`, `title`, `path_img`, `path_file`) VALUES
(1, 'en', '/dist/images/United-Kingdom.png', '../../../../lang/lang-en.inc.php'),
(2, 'fr', '/dist/images/france.png', '../../../../lang/lang-fr.inc.php'),
(3, 'de', '/dist/images/germany.png', '../../../../lang/lang-de.inc.php');


-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_workshop_attempt`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `v_workshop_attempt`;
CREATE TABLE IF NOT EXISTS `v_workshop_attempt` (
`Date` varchar(40)
,`Debut` varchar(10)
,`name` varchar(255)
,`FirstName` varchar(255)
,`Promo` varchar(100)
,`Workshop` varchar(10)
,`WorkshopFull` varchar(255)
,`Room` varchar(100)
,`Duration` varchar(18)
,`Scale` varchar(255)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_workshop_attempt_done`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `v_workshop_attempt_done`;
CREATE TABLE IF NOT EXISTS `v_workshop_attempt_done` (
`scale` varchar(255)
,`upn` varchar(255)
,`start` datetime
,`label` varchar(255)
,`comment` varchar(200)
,`idUser` int(11)
,`idWorkshop` int(11)
,`idWorkshopAttempt` int(11)
,`scale_type` int(11)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_workshop_attempt_per_month`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `v_workshop_attempt_per_month`;
CREATE TABLE IF NOT EXISTS `v_workshop_attempt_per_month` (
`uid` int(11)
,`upn` varchar(255)
,`promo` varchar(100)
,`count` bigint(21)
,`month` varchar(64)
,`nmonth` int(2)
,`year` int(4)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_workshop_attempt_validated`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `v_workshop_attempt_validated`;
CREATE TABLE IF NOT EXISTS `v_workshop_attempt_validated` (
`uid` int(11)
,`upn` varchar(255)
,`pid` int(11)
,`label` varchar(255)
,`count_to_validate` int(11)
,`count` bigint(21)
);

--
-- Structure de la vue `v_workshop_attempt`
--
DROP TABLE IF EXISTS `v_workshop_attempt`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_workshop_attempt`  AS  select date_format(`workshop_attempt`.`debut`,'%d %b %Y') AS `Date`,time_format(`workshop_attempt`.`debut`,'%H:%i') AS `Debut`,user.upn AS 'upn',ifnull(`user`.`name`,substring_index(substring_index(`user`.`upn`,'@',1),'.',-(1))) AS `name`,ifnull(`user`.`first_name`,substring_index(substring_index(`user`.`upn`,'@',1),'.',1)) AS `FirstName`,`user`.`description` AS `Promo`,`workshop`.`label` AS `Workshop`,`workshop`.`full_label` AS `WorkshopFull`,`room`.`description` AS `Room`,time_format(timediff(`workshop_attempt`.`fin`,`workshop_attempt`.`debut`),'%Hh, %im, %ss') AS `Duration`,`scale`.`label` AS `Scale` from (`room` join (((`workshop_attempt` join `user` on((`workshop_attempt`.`id_user` = `user`.`id`))) join `workshop` on((`workshop`.`id` = `workshop_attempt`.`id_workshop`))) join `scale` on((`scale`.`scale_item_code` = `workshop_attempt`.`scale`)))) where ((`room`.`id` = `workshop`.`id_room`) and (`workshop_attempt`.`fin` is not null)) order by `workshop_attempt`.`debut` ;

-- --------------------------------------------------------

--
-- Structure de la vue `v_workshop_attempt_done`
--
DROP TABLE IF EXISTS `v_workshop_attempt_done`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_workshop_attempt_done`  AS  select `scale`.`label` AS `scale`,`user`.`upn` AS `upn`,`workshop_attempt`.`debut` AS `start`,`workshop`.`full_label` AS `label`,`workshop_comment`.`content` AS `comment`,`workshop_attempt`.`id_user` AS `idUser`,`workshop`.`id` AS `idWorkshop`,`workshop_attempt`.`id` AS `idWorkshopAttempt`,`scale`.`id_scale_type` AS `scale_type` from ((((`workshop_attempt` left join `scale` on((`scale`.`scale_item_code` = `workshop_attempt`.`scale`))) left join `user` on((`user`.`id` = `workshop_attempt`.`id_user`))) left join `workshop` on((`workshop`.`id` = `workshop_attempt`.`id_workshop`))) left join `workshop_comment` on(((`workshop_comment`.`id_workshop` = `workshop`.`id`) and (`workshop_comment`.`id_user` = `user`.`id`)))) where ((`workshop_attempt`.`fin` is not null) and (`workshop`.`deleted` <> 1)) order by `scale`.`order_scale` desc,`workshop`.`id` ;

-- --------------------------------------------------------

--
-- Structure de la vue `v_workshop_attempt_per_month`
--
DROP TABLE IF EXISTS `v_workshop_attempt_per_month`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_workshop_attempt_per_month`  AS  select `u`.`id` AS `uid`,`u`.`upn` AS `upn`,`u`.`description` AS `promo`,count(`workshop_attempt`.`id`) AS `count`,date_format(`workshop_attempt`.`fin`,'%M') AS `month`,month(`workshop_attempt`.`fin`) AS `nmonth`,year(`workshop_attempt`.`fin`) AS `year` from (`workshop_attempt` join `user` `u`) where ((`workshop_attempt`.`fin` is not null) and (`workshop_attempt`.`id_user` = `u`.`id`)) group by `u`.`upn`,`u`.`id`,date_format(`workshop_attempt`.`fin`,'%M'),year(`workshop_attempt`.`fin`) order by `year`,month(`workshop_attempt`.`fin`) ;

-- --------------------------------------------------------

--
-- Structure de la vue `v_workshop_attempt_validated`
--
DROP TABLE IF EXISTS `v_workshop_attempt_validated`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_workshop_attempt_validated`  AS  select `u`.`id` AS `uid`,`u`.`upn` AS `upn`,`ws`.`id` AS `pid`,`ws`.`full_label` AS `label`,`ws`.`count_to_validate` AS `count_to_validate`,count(`workshop_attempt`.`id`) AS `count` from ((`user` `u` join `workshop` `ws`) join `workshop_attempt`) where ((`workshop_attempt`.`id_workshop` = `ws`.`id`) and (`workshop_attempt`.`id_user` = `u`.`id`)) group by `u`.`id`,`ws`.`id` having (`count` > `ws`.`count_to_validate`) order by `u`.`upn`,`ws`.`label` ;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `workshop`
--
ALTER TABLE `workshop`
  ADD CONSTRAINT `workshop_category_id_fk` FOREIGN KEY (`id_category`) REFERENCES `category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `workshop_discipline_id_fk` FOREIGN KEY (`id_discipline`) REFERENCES `discipline` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `workshop_scale_type_id_fk` FOREIGN KEY (`id_scale_type`) REFERENCES `scale_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `workshop_priority_level_id_fk` FOREIGN KEY (`id_priority_level`) REFERENCES `priority_level` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `workshop_room_id_fk` FOREIGN KEY (`id_room`) REFERENCES `room` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `workshop_species_id_fk` FOREIGN KEY (`id_species`) REFERENCES `species` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `workshop_sub_category_id_fk` FOREIGN KEY (`id_sub_category`) REFERENCES `sub_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `workshop_attempt`
--
ALTER TABLE `workshop_attempt`
  ADD CONSTRAINT `ws_attempt_workshop_id_fk` FOREIGN KEY (`id_workshop`) REFERENCES `workshop` (`id`),
  ADD CONSTRAINT `ws_attempt_user_id_fk` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);
COMMIT;
