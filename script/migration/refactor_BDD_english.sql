ALTER TABLE evaluation RENAME TO scale;
ALTER TABLE salle RENAME TO room;
ALTER TABLE evaluation_groupe RENAME TO scale_type;
ALTER TABLE rank RENAME TO priority_level;
ALTER TABLE poste_travail RENAME TO workshop;
ALTER TABLE tp RENAME TO workshop_attempt;
ALTER TABLE commentaires RENAME TO workshop_comment;
ALTER TABLE utilisateur RENAME TO user;
ALTER TABLE droits RENAME TO rights;
ALTER TABLE mod_moodle RENAME TO moodle_mod;
ALTER TABLE parametres RENAME TO parameter;

ALTER TABLE scale
  CHANGE code_evaluation scale_item_code varchar(10);
ALTER TABLE scale
  CHANGE libelle label varchar(255);
ALTER TABLE scale
  CHANGE groupe id_scale_type int(11);
ALTER TABLE scale
  CHANGE ordre order_scale varchar(255);

ALTER TABLE scale_type
  CHANGE libelle label varchar(100);
ALTER TABLE scale_type
  CHANGE actif active tinyint(1);

ALTER TABLE workshop
  CHANGE libelle full_label varchar(255);
ALTER TABLE workshop
  CHANGE id_rank id_priority_level int(11);
ALTER TABLE workshop
  CHANGE libelle_court label varchar(10);
ALTER TABLE workshop
  CHANGE disponible available tinyint(1);
ALTER TABLE workshop
  CHANGE id_salle id_room int(11);
ALTER TABLE workshop
  CHANGE note_groupe id_scale_type int(11);
ALTER TABLE workshop
  CHANGE id_eve id_scale int(11);

ALTER TABLE workshop_attempt
  CHANGE id_utilisateur id_user int(11);
ALTER TABLE workshop_attempt
  CHANGE id_poste_travail id_workshop int(11);
ALTER TABLE workshop_attempt
  CHANGE evaluation scale varchar(10);

ALTER TABLE workshop_comment
  CHANGE id_utilisateur id_user int(11);
ALTER TABLE workshop_comment
  CHANGE id_poste_travail id_workshop int(11);
ALTER TABLE workshop_comment
  CHANGE commentaire content varchar(200);

ALTER TABLE user
  CHANGE nom name varchar(100);
ALTER TABLE user
  CHANGE prenom first_name varchar(100);
ALTER TABLE user
  CHANGE id_profil id_profile varchar(100);

ALTER TABLE permissions
  CHANGE id_droit id_right int(11);
ALTER TABLE permissions
  CHANGE id_profil id_profile int(11);

ALTER TABLE rights
  CHANGE libelle label varchar(100);

ALTER TABLE profils
  CHANGE libelle label varchar(100);
