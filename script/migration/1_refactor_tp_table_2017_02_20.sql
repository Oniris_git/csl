/**
  Table TP
 */
ALTER TABLE tp
  ADD id_utilisateur INT DEFAULT 1;
ALTER TABLE tp
  ADD CONSTRAINT tp_utilisateur_id_fk
FOREIGN KEY (id_utilisateur) REFERENCES utilisateur (id);
ALTER TABLE tp
  ADD id_poste_travail INT DEFAULT 99;
ALTER TABLE tp
  ADD CONSTRAINT tp_poste_travail_id_fk
FOREIGN KEY (id_poste_travail) REFERENCES poste_travail (id);
ALTER TABLE tp
  MODIFY COLUMN id_utilisateur INT NOT NULL
  AFTER id,
  MODIFY COLUMN id_poste_travail INT NOT NULL
  AFTER upn;

UPDATE tp, poste_travail
SET id_poste_travail = poste_travail.id
WHERE tp.code_poste_travail = poste_travail.libelle_court;

UPDATE tp, utilisateur
SET id_utilisateur = utilisateur.id
WHERE tp.upn = utilisateur.upn;

ALTER TABLE tp
  DROP COLUMN upn,
  DROP COLUMN code_poste_travail;

/**
  Vue V_TP
 */
CREATE OR REPLACE VIEW `V_TP` AS
  SELECT
    date_format(`tp`.`debut`, '%d %b %Y')                                            AS `Date`,
    time_format(`tp`.`debut`, '%H:%i')                                               AS `Debut`,
    ifnull(`utilisateur`.`nom`,
           substring_index(substring_index(`utilisateur`.`upn`, '@', 1), '.', -(1))) AS `Nom`,
    ifnull(`utilisateur`.`prenom`,
           substring_index(substring_index(`utilisateur`.`upn`, '@', 1), '.', 1))    AS `Prenom`,
    `utilisateur`.`description`                                                      AS `Promo`,
    `poste_travail`.`libelle_court`                                                  AS `Poste`,
    `poste_travail`.`libelle`                                                        AS `PosteL`,
    `salle`.`description`                                                            AS `Salle`,
    time_format(timediff(`tp`.`fin`, `tp`.`debut`), '%Hh, %im, %ss') AS `Duree`,
    `evaluation`.`libelle`                                                           AS `Evaluation`,
    `tp`.`comment`                                                                   AS `Commentaire`
  FROM (`salle`
    JOIN (((`tp`
      JOIN `utilisateur`
        ON ((`tp`.`id_utilisateur` = `utilisateur`.`id`))) JOIN
      `poste_travail`
        ON ((`poste_travail`.`id` = `tp`.`id_poste_travail`))) JOIN
      `evaluation`
        ON ((`evaluation`.`code_evaluation` = `tp`.`evaluation`))))
  WHERE ((`salle`.`id` = `poste_travail`.`id_salle`) AND
         (`tp`.`fin` IS NOT NULL))
  ORDER BY `tp`.`debut`;

/**
  Vue V_TP_VALIDE
 */
CREATE OR REPLACE VIEW `V_TP_VALIDES` AS
  SELECT
    `u`.`id`                         AS `uid`,
    `u`.`upn`                        AS `upn`,
    `pt`.`id`                        AS `pid`,
    `pt`.`libelle`                   AS `libelle`,
    `pt`.`count_to_validate`         AS `count_to_validate`,
    count(`tp`.`id`) AS `count`
  FROM ((`utilisateur` `u`
    JOIN `poste_travail` `pt`) JOIN `tp`)
  WHERE
    ((`tp`.`id_poste_travail` = `pt`.`id`) AND (`tp`.`id_utilisateur` = `u`.`id`))
  GROUP BY `u`.`id`, `pt`.`id`
  HAVING (`count` > `pt`.`count_to_validate`)
  ORDER BY `u`.`upn`, `pt`.`libelle_court`;

/**
  Vue V_STAT_TP_DONE
 */
CREATE OR REPLACE VIEW `V_STAT_TP_DONE` AS
  SELECT
    date_format(`tp`.`fin`, '%M')                          AS `month`,
    month(`tp`.`fin`)                                      AS `nmonth`,
    year(`tp`.`fin`)                                       AS `year`,
    count(`tp`.`id`)                                       AS `count`,
    `p`.`libelle`                                                          AS `libelle`,
    `u`.`id`                                                               AS `uid`,
    `u`.`upn`                                                              AS `upn`,
    ifnull(u.prenom,
           substring_index(substring_index(`u`.`upn`, '@', 1), '.', 1))    AS `prenom`,
    ifnull(u.nom,
           substring_index(substring_index(`u`.`upn`, '@', 1), '.', -(1))) AS `nom`
  FROM ((`tp`
    JOIN `poste_travail` `p`) JOIN `utilisateur` `u`)
  WHERE
    ((`tp`.`fin` IS NOT NULL) AND (`tp`.`id_poste_travail` = `p`.`id`) AND
     (`tp`.`id_utilisateur` = `u`.`id`))
  GROUP BY `u`.`upn`, `p`.`libelle`, date_format(`tp`.`fin`, '%M'),
    year(`tp`.`fin`)
  ORDER BY year(`tp`.`fin`), month(`tp`.`fin`), `u`.`id`;

/**
  Vue V_STAT_TP_PER_MONTH
 */
CREATE OR REPLACE VIEW `V_STAT_TP_PER_MONTH` AS
  SELECT
    `u`.`id`                                      AS `uid`,
    `u`.`upn`                                     AS `upn`,
    count(`tp`.`id`)              AS `count`,
    date_format(`tp`.`fin`, '%M') AS `month`,
    month(`tp`.`fin`)             AS `nmonth`,
    year(`tp`.`fin`)              AS `year`
  FROM (`tp`
    JOIN `utilisateur` `u`)
  WHERE ((`tp`.`fin` IS NOT NULL) AND (`tp`.`id_utilisateur` = `u`.`id`))
  GROUP BY `u`.`upn`, date_format(`tp`.`fin`, '%M'), year(`tp`.`fin`)
  ORDER BY month(`tp`.`fin`);