<?php

use VetSims\Administration\Helper\Config;

require __DIR__ . '/../../vendor/autoload.php';

$config = require __DIR__ . '/../../config.php';
$config = new Config($config);

require __DIR__ . '/../../lib/dependencies.php';

$sql = <<<SQL
INSERT INTO difficulty (difficulty_level, title, description) VALUES (1, 'Facile', 'Difficulté de 1ère année');
INSERT INTO difficulty (difficulty_level, title, description) VALUES (2, 'Moyen', 'Difficulté de 2ème année');
INSERT INTO difficulty (difficulty_level, title, description) VALUES (3, 'Difficile', 'Difficulté de 3ème année');
INSERT INTO difficulty (difficulty_level, title, description) VALUES (4, 'Très difficile', '');
SQL;

$container->get('db')->exec($sql);
