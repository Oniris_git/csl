<?php

use VetSims\Administration\Helper\Config;

require __DIR__ . '/../../vendor/autoload.php';

$config = require __DIR__ . '/../../config.php';
$config = new Config($config);

require __DIR__ . '/../../lib/dependencies.php';

$sql = <<<SQL
INSERT INTO salle (description) VALUES ('Salle A');
INSERT INTO salle (description) VALUES ('Salle B');
INSERT INTO salle (description) VALUES ('Salle C');
SQL;

$container->get('db')->exec($sql);
