<?php

use VetSims\Administration\Helper\Config;

require __DIR__ . '/../../vendor/autoload.php';

$config = require __DIR__ . '/../../config.php';
$config = new Config($config);

require __DIR__ . '/../../lib/dependencies.php';

$sql = <<<SQL
INSERT INTO profils (label, description, deleted) VALUES ('Administrateur', 'Administrateur de la plateforme', 0);
INSERT INTO profils (label, description, deleted) VALUES ('Gestionnaire', 'Gestion des travaux', 0);
INSERT INTO profils (label, description, deleted) VALUES ('Étudiant', 'Étudiant', 0);
SQL;

$container->get('db')->exec($sql);
