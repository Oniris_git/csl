<?php

use VetSims\Administration\Helper\Config;

require __DIR__ . '/../../vendor/autoload.php';

$config = require __DIR__ . '/../../config.php';
$config = new Config($config);

require __DIR__ . '/../../lib/dependencies.php';

$sql = <<<SQL
INSERT INTO `droits` VALUES
(1,'create:csv','Générer des CSV'),
(2,'create:profiles','Créer des profils'),
(3,'create:qrcode','Générer les QRCodes'),
(4,'create:salle','Créer des salles'),
(5,'create:users','Créer des utilisateurs'),
(6,'create:workspace','Création de poste de travail'),
(7,'edit:evals','Modifier les notes'),
(8,'edit:profiles','Modifier les profils'),
(9,'edit:salle','Modifie les salles'),
(10,'edit:tp','Modifier les TP effectués'),
(11,'edit:users','Modification des paramètres de l\'utilisateur'),
(12,'edit:workspace','Modifier les postes de travail'),
(13,'print:others_stats','Afficher les statistiques des autres'),
(14,'print:ownstats','Afficher ses statistiques'),
(15,'print:qrcode','Imprimer les QRCodes'),
(16,'print:stats','Afficher toutes les statistiques'),
(17,'print:users','Affiche les utilisateurs'),
(18,'print:workspace','Affiche les postes de travail'),
(19,'print:workspace_evals','Afficher les notes par poste'),
(20,'print:settings','Affiche le menu des paramètres'),
(21,'create:category', 'Créer une catégorie'),
(22,'edit:category', 'Modifier les catégories'),
(23,'create:subcategory', 'Créer une sous-catégories'),
(24,'edit:subcategory', 'Modifier les sous-catégories'),
(25,'create:species', 'Créer une espèces'),
(26,'edit:species', 'Modifier les espèces'),
(27,'create:discipline', 'Créer une discipline'),
(28,'edit:discipline', 'Modifier les disciplines'),
(29,'create:rank', 'Créer un rang'),
(30,'edit:rank', 'Modifier les rangs');
INSERT INTO `permissions` VALUES 
(1,1),
(1,2),
(1,3),
(1,4),
(1,5),
(1,6),
(1,7),
(1,8),
(1,9),
(1,10),
(1,11),
(1,12),
(1,13),
(1,14),
(1,15),
(1,16),
(1,17),
(1,18),
(1,19),
(1,20),
(1,21),
(1,22),
(1,23),
(1,24),
(1,25),
(1,26),
(1,27),
(1,28),
(1,29),
(1,30),
(2,1),
(2,10),
(2,13),
(2,14),
(2,16),
(2,17),
(2,18),
(2,19),
(3,14);
SQL;

$container->get('db')->exec($sql);
