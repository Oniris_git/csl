<?php

use VetSims\Administration\Helper\Config;

require __DIR__ . '/../../vendor/autoload.php';

$config = require __DIR__ . '/../../config.php';
$config = new Config($config);

require __DIR__ . '/../../lib/dependencies.php';

$sql = <<<SQL
INSERT INTO evaluation_groupe (libelle, actif) VALUES ('Quadra', 1);
INSERT INTO evaluation_groupe (libelle, actif) VALUES ('Tri', 1);
SQL;

$container->get('db')->exec($sql);
