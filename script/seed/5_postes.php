<?php

use VetSims\Administration\Helper\Config;

require __DIR__ . '/../../vendor/autoload.php';

$config = require __DIR__ . '/../../config.php';
$config = new Config($config);

require __DIR__ . '/../../lib/dependencies.php';

$sql = <<<SQL
INSERT INTO poste_travail (libelle_court, libelle, description, deleted, disponible, id_eve, id_salle, id_difficulty, duration, count_to_validate, note_groupe) VALUES ('XXXXXX', '[XXXXXX] Poste de démonstration', 'Poste de démonstration
Supprimez moi !', 1, 1, 30809, 1, 1, 1, 1, 1);
SQL;

$container->get('db')->exec($sql);
