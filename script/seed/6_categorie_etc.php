<?php

use VetSims\Administration\Helper\Config;

require __DIR__ . '/../../vendor/autoload.php';

$config = require __DIR__ . '/../../config.php';
$config = new Config($config);

require __DIR__ . '/../../lib/dependencies.php';

$sql = <<<SQL
INSERT INTO category(title,description) VALUES('Catégorie','Une catégorie');
INSERT INTO discipline(title,description) VALUES('Discipline','Une discipline');
INSERT INTO species(title,description) VALUES('Espece','Une espece');
INSERT INTO sub_category(title,description) VALUES('Sous-catégorie','Une sous-catégorie');
INSERT INTO rank(title,description) VALUES('Rang','Un rang');
SQL;

$container->get('db')->exec($sql);
