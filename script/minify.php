<?php

use MatthiasMullie\Minify;

require __DIR__ . '/../vendor/autoload.php';

$jsdir = __DIR__ . '/../ressources/js/';
$cssdir = __DIR__ . '/../ressources/css/';

$jsfiles = array_diff(scandir($jsdir), ['..', '.']);
$cssfiles = array_diff(scandir($cssdir), ['..', '.']);

$minifiercss = new Minify\CSS();

foreach ($cssfiles as $css) {
    $minifiercss->add($cssdir . $css);
}

$minifiercss->minify(__DIR__ . '/../public/dist/css/app.css');

foreach ($jsfiles as $jsfile) {
    $minifierjs = new Minify\JS();
    $minifierjs->add($jsdir . $jsfile);
    $minifierjs->minify(__DIR__ . '/../public/dist/js/' . $jsfile);
}
