<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @package Vetsims
 *
 */

$config = require '../../config.php';

$sql = <<<DOC
CREATE TABLE `mod_moodle` (
`module_id` int(11) NOT NULL,
  `devoir_id` int(11) NOT NULL,
  UNIQUE KEY `mod_moodle_module_id_uindex` (`module_id`),
  UNIQUE KEY `mod_moodle_devoir_id_uindex` (`devoir_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lien entre id interne du module et id affiche'
DOC;

$BASE = $config['db']['base'];
$IP = $config['db']['server'];
$USER = $config['db']['user'];
$PASSWORD = $config['db']['password'];

$con = mysqli_connect('p:' . $IP, $USER, $PASSWORD, $BASE);
if (!$con) {
    die('Impossible de se connecter à la base ' . $BASE);
}
mysqli_set_charset($con, 'utf8');

$res = mysqli_query($con, $sql);

if ($res === false) {
    echo mysqli_error($con);
    mysqli_close($con);
    die();
}

mysqli_close($con);
