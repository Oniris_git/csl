<?php
/**
 * This file is part of VetSims
 * Copyright (C) 2016  Gabriel Poma <gabriel.poma@vet-alfort.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @package VetSims
 *
 * Met à jour le lien entre l'id du devoir et l'id du devoir interne
 * Nécessaire uniquement
 *
 */

if (!extension_loaded('curl')) {
    exit('Extension curl non chargée');
}

header('Content-Type: text/plain; charset=utf-8');
echo 'Mise à jour de la table de correspondance...' . PHP_EOL;
echo '> Récupération des devoirs...' . PHP_EOL;
$start = microtime(true);

$config = require __DIR__ . '/../../config.php';

$token = $config['moodle']['token'];
$urlexterne = $config['moodle']['domain'];
$courseid = $config['moodle']['course'];

$post_data = [
    'courseids' => [$courseid]
];

$serverurl = $urlexterne . 'webservice/rest/server.php?wstoken=' . $token
    . '&wsfunction=mod_assign_get_assignments'
    . '&moodlewsrestformat=json';

$curl = curl_init($serverurl);

curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post_data));

$post = json_decode(curl_exec($curl));
curl_close($curl);

if (isset($post->exception)) {
    exit('EXCEPTION_MOODLE_NO_USER_FOUND');
}

$assignments = [];
foreach ($post->courses as $course) {
    foreach ($course->assignments as $assignment) {
        $assignments[] = $assignment;
    }
}

echo '> ' . count($assignments) . ' devoirs trouvés...' . PHP_EOL;
echo '> Opérations sur la table...' . PHP_EOL;

$sql = <<<SQL
INSERT INTO moodle_mod (module_id, devoir_id) VALUES (:mid, :did)
ON DUPLICATE KEY UPDATE devoir_id = :did2;
SQL;

$BASE = $config['db']['base'];
$IP = $config['db']['server'];
$USER = $config['db']['user'];
$PASSWORD = $config['db']['password'];
try {
    $pdo = new PDO('mysql:host=' . $IP . ';dbname=' . $BASE . ';charset=UTF8', $USER, $PASSWORD);
} catch (PDOException $e) {
    exit('Connexion échouée : ' . $e->getMessage());
}

$stmt = $pdo->prepare($sql);

$i = 1;
foreach ($assignments as $a) {
    echo $i . " : " .$a->id . " : " . $a->cmid . PHP_EOL;
    if (!$stmt->execute(['mid' => $a->id, 'did' => $a->cmid, 'did2' => $a->cmid])) {
        echo "Erreur avec le devoir n° ".$a->cmid.PHP_EOL;
    }
    $i++;
}

echo '> Terminé...' . PHP_EOL;
echo 'Terminé en ' . (microtime(true) - $start) . ' secondes' . PHP_EOL;
